<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>gettyng.com - ¿Dudas?</title>
<link rel="shortcut icon" href="images/favi.ico" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script src="js/cufon-yui.js" type="text/javascript"></script>
<script src="js/font.js" type="text/javascript"></script>
<script src="js/cufon-config.js" type="text/javascript"></script>


</head>

<body>

<div class="section">
<div id="header" class="header">
	<div class="headerimage"><a href ="/index.html" title="Todo Colima quiere estar en gettyng.com"> <img src="/images/logo.png"/></a></div>
    <div class="topMenuContainer"> 
		<div class="topMenutext">
			<h1 class="title"><a href="index.aspx">Inicio</a></h1>
		</div>			
        <div class="topMenuLinks">
            <form id="login" >
            <div id="newAlign">
                
			</div>
			</form>
        </div>
	</div>
</div></div>


<div id="container" class="container">
 
 <div id="muro" class="d_envio">
	<div class="envio_i">
		<div style="display:block; height: 50px;"><div style="float: left;"><img src="images/dudas.gif"></div><div><h2 class="title">¿Tienes dudas?</h2> </div></div>
		<div class="toggleList_container">
			<ol class="jtoggle-list">
			<li>
				<a class="buy" href="javascript:void(0)">Que es Gettyng?</a>
				<div style="display: none;" class="">
				<p>Es un servicio que te ofrece el envió de mensajes cortos conocidos como sms a todo el estado de Colima</p>
				</div>
				</li>
				<li>
				<a class="buy" href="javascript:void(0)">¿Tiene algún costo envíar mensajes desde Gettyng?</a>
				<div style="display: none;" class="">
				<p>El servicio no tiene absolutamente ningún costo ni compromiso</p>
				</div>
				</li>
				<li>
				<a class="buy" href="javascript:void(0)">¿Cómo puedo envíar un mensaje?</a>
				<div style="display: none;" class="">
				<p>Primero que nada has de registrarte, es muy sencillo: Click en el botón de únete y llena tus datos. Se te enviará a tu celular un NIP de activación, ingresalo en la parte 2. Activar. Una vez activado inicia sesión con tu celular y contraseña. Veras los campos para el envío. Ingresa el celular al que quieres enviar el mensaje. Escribe tu mensaje y da click en enviar. Recuerda que por el momento sólo pueden ser celulares del estado de Colima.</p>
				</div>
				</li>				
				<li>
				<a class="buy" href="javascript:void(0)">¿Cuanto tiempo tarda en llegar el mensaje?</a>
				<div style="display: none;" class="">
				<p>En condiciones normales el mensaje tarda menos de 10 segundos, sin embargo hay diferentes factores que pueden afectar esto y que están fuera de nuestro alcance como las antenas de tu proveedor de celular, el clima, la saturación etc.</p>
				</div>
				</li>
				
				<li>
				<a class="buy">¿Cuál es el truco?</a>
				<div style="display: none;" class="">
				<p>En Gettyng.com no hay trucos, tu SMS se envía con un consejo de nuestros patrocinadores, es por eso que los SMS son gratis.</p></div></li>
				<li>
				<a class="buy">¿Mi información se encuentra segura?</a>
				<div style="display: none;" class="">
				<p>Tu información se encuentra segura, tus datos se utilizan solo para validar que no eres un robot.</p></div></li>
			</ol>
		</div>
	</div>
  </div>
                        
 <div id="gritalo" class="c_envio"> 
	<div class="envio_i">  
    <h2 class="title">Shoutbox</h2> 
		<div id="contentarea">
					<form method="post" id="form"> 
				<label class="cufonblue">Nick</label> 
				<input class="txt_a" id="nick" type="text" MAXLENGTH="10" /> 
				<input class="cufow" id="send" type="submit" value="Dilo" />
				<br>
				<img src="images/shoutit.jpg">
				<input class="txt_b" id="message" type="text" MAXLENGTH="25" /> 
				<div> </div>
			</form> 
			<div id="container"> 	
				<span class="clear"></span> 
				<div class="content"> 			
					<div id="loading"><img src="images/loading.gif" heigh="30" width="30" alt="cargando..." /></div> 
					<ul> 
					</ul> 
				</div> 
			</div>

		</div>
	</div>
</div>

</div>
<div id="footer" class="footer"> <ul>
        <li id="copyright">Copyrigth &copy; 2011. <b>gettyng.com</b></li>
        <li><a href="contacto">Contáctanos</a></li>
        <li><a href="faq">¿Dudas?</a></li>
        <li><a href="terminos">Términos de uso</a></li>
      </ul> 
</div> 
</body>

<script type="text/javascript" src="js/shoutbox.js"></script> 

</html>