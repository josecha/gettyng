<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>gettyng.com - Términos de uso</title>
<link rel="shortcut icon" href="images/favi.ico" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script src="js/cufon-yui.js" type="text/javascript"></script>
<script src="js/font.js" type="text/javascript"></script>
<script src="js/cufon-config.js" type="text/javascript"></script>


</head>

<body>

<div class="section">
<div id="header" class="header">
	<div class="headerimage"><a href ="/index.html" title="Todo Colima quiere estar en gettyng.com"> <img src="/images/logo.png"/></a></div>
    <div class="topMenuContainer"> 
		<div class="topMenutext">
			<h1 class="title"><a href="index.aspx">Inicio</a></h1>
		</div>			
        <div class="topMenuLinks">
            <form id="login" >
            <div id="newAlign">
                
			</div>
			</form>
        </div>
	</div>
</div></div>

<div id="container" class="container">
 
 <div id="muro" class="d_envio">
	<div class="envio_i">
		<h2 class="title">Conoce nuestros términos de uso</h2> 
		<p>El servicio ofrecido por el sitio de internet <a href="http://www.gettyng.com">www.gettyng.com</a>, en adelante "<span class="cufon">Gettyng</span>", está sujeto a los siguientes términos y condiciones.</p>
        <p><span class="cufon">Gettyng </span>se reserva el derecho de modificar los términos y condiciones de uso en cualquier momento y sin previo aviso, siendo obligatorias dichas modificaciones a partir de su publicación en la dirección de internet <a href="http://www.gettyng.com/terminos.html">www.gettyng.com/terminos.html</a> Es responsabilidad del usuario asegurarse del conocimiento de tales cambios.</p>
        <p><span class="cufon">Gettyng </span>ofrece su contenido como un servicio de entretenimiento gratuito a sus usuarios y visitantes, dicho contenido puede ser usado con carácter informativo y/o educativo.</p>
        <p>E formulario de registro dispuesto en <span class="cufon">Gettyng </span>nos permite conocer a nuestros visitantes y crear la comunidad de usuarios, así como la creación del espacio personal donde el usuario podrá acceder a los servicios de gettyng.  El usuario registrado en <span class="cufon">Gettyng </span>acepta las condiciones y términos de uso aquí expuestos.</p>
        <p><span class="cufon">Gettyng </span>puede proporcionar información y estadísticas sobre sus usuarios y clientes, ventas, modelos de tráfico y su sitio web a terceros, pero esta información no incluirá jamás datos personales o que identifiquen usuarios individualmente. Dichos terceros podrán eventualmente enviar información, contenidos y comunicación social o comercial a los usuarios de gettyng.</p>
        <p><span class="cufon">Gettyng </span>puede periódicamente enviar información a todos los usuarios del sistema así como a todos los destinatarios de los mismos.</p>
        <p>El usuario acepta que su cuenta es personal y solamente enviará mensajes personales desde la misma.</p>
        <p>El usuario reconoce que <span class="cufon">Gettyng </span>no es responsable del contenido de ningún mensaje tanto si el envío fue hecho o no por un usuario registrado de gettyng.</p>
        <p><span class="cufon">Gettyng </span>puede utilizarse para conectar con otras redes de todo el mundo y el usuario acuerda conformarse con las políticas de uso aceptable de estas redes.</p>
        <p>El usuario reconoce que el contenido de los mensajes enviados desde su cuenta son responsabilidad del mismo. <span class="cufon">Gettyng </span>puede ser utilizado solamente con fines legales.</p>
        <p><span class="cufon">Gettyng </span>prohíbe a sus usuarios:
          </p><ul>
            <li>El envío de mensajes (sms) no solicitados, masivos, ofensivos, ilegales, publicitarios que afecten derechos y privacidad propios de terceros.</li>
            <li>El anuncio o mención de sitios web diferentes a <span class="cufon">Gettyng </span>(<a href="http://www.gettyng.com">www.gettyng.com</a>).</li> 
            <li>El anuncio de material irrelevante como contenido del mensaje (sms).</li>
          </ul>
      <p></p>
        <p>AL NAVEGAR POR EL SITIO WEB, EL USUARIO SE COMPROMETE A NO ENVIAR O TRANSMITIR CONTENIDO ALGUNO QUE: 
          </p><ul>
            <li>Viole las leyes y derechos de autor en cualquier territorio.</li>
            <li>Induzca o invite a actuar de manera ilegal o en sí mismo, constituya violaciones a la ley y el orden vigentes. </li>
            <li>Incite a las personas a involucrarse o a participar en actividades peligrosas, de riesgo, ilegales o que atenten contra su integridad física y emocional.</li>
            <li>Altere el equilibrio psíquico de las personas, las amedrente, atemorice, intimide o cualquier otra alteración. </li>
            <li>Infrinja las normas legales y/o transgreda los derechos y libertades de las personas. </li>
            <li>Sea discriminatorio en cuanto al sexo, genero, religión, raza o cualquier otra violación a los derechos fundamentales y/o las libertades otorgadas y garantizadas por el ordenamiento jurídico. </li>
            <li>Contenga información falsa o parcial. </li>
            <li>Contenga virus informáticos u otras características que causen deterioros a dispositivos móviles, computadoras y otros aparatos electrónicos.</li>
            <li>Infrinja las normas legales sobre secreto de las comunicaciones, propiedad intelectual, derecho al honor y a la intimidad personal o familiar. </li>
            <li>Contenga publicidad, promocione u ofrezca servicios, incluidas sin carácter limitativo las cadenas de dinero, de mensajes, spam (mensaje no solicitado), etc. </li>
          </ul>
        <p></p>
        <p>En relación con la información descrita anteriormente, <span class="cufon">Gettyng </span>no será en ningún caso responsable por la información aportada por terceros, así como no se responsabilizará de los daños o perjuicios causados por ella. </p>
        <p><span class="cufon">Gettyng </span>se reserva el derecho de eliminar del sitio web cualquier contenido que no se ajuste a este acuerdo y además podrá, sin previo aviso, suspender y/o interrumpir el servicio y/o el acceso, a las personas que incumplan el presente acuerdo. </p>
        <p>El usuario acepta que <span class="cufon">Gettyng </span>proporcione, a petición de las autoridades, los datos de su cuenta sólo en caso de que se presente un uso ilegal del servicio.</p>
        <p><span class="cufon">Gettyng </span>advierte que los contenidos de terceros, al no ser de su titularidad, pueden presentar errores, falsedades, no estar actualizados, no citar las respectivas fuentes, etc. De igual manera, no se garantiza el cumplimiento del presente acuerdo por parte de terceros en relación con el contenido, por lo cual <span class="cufon">Gettyng </span>no se responsabilizará por éstos de ninguna manera. La publicación de contenidos de terceros no representa de ningún modo la opinión o las intenciones de gettyng. </p>
        <p><span class="cufon">Gettyng </span>no se hace responsable de las interpretaciones de sus contenidos, ni del uso indebido de los mismos, así como de sus servicios, información o aplicaciones. De igual manera no se hará responsable por los daños o perjuicios causados de forma directa o indirecta a los destinatarios. La comunidad tampoco se responsabilizará por los daños o perjuicios, directos, indirectos, o de otra forma, que acarreé la suspensión o terminación de los servicios, contenido, o información que suministra a través del sitio web. De ninguna manera se hará responsable por las consecuencias del uso indebido o negligente de las contraseñas utilizadas para acceder al sitio. </p>
        <p><span class="cufon">Gettyng </span>no se hace responsable ni da garantía de ningún tipo, acerca de la precisión, confiabilidad y disponibilidad, de la información, productos y servicios contenidos en este sitio. </p>
        <p><span class="cufon">Gettyng </span>almacena y utiliza la información del usuario según se describe en los términos de uso y que comprende la información provista por el usuario así como la derivada de la misma, como los números telefónicos relacionados con el uso y manejo del servicio de gettyng.</p>
        <p><span class="cufon">Gettyng </span>almacena los números telefónicos de los destinatarios de los mensajes (sms) y almacena el contenido de los mensajes enviados por los usuarios.</p>
        <p><span class="cufon">Gettyng </span>se reserva la posibilidad de acceder al contenido de los mensajes enviados por los ususarios para la moderación del uso del servicio.</p>
        <p>Al hacer uso de nuestro portal, el usuario acepta los términos de esta política de privacidad y consiente expresamente al procesamiento y tratamiento de su información personal de acuerdo a esta política de privacidad.</p>
      
	  </div>
  </div>
                        
 <div id="gritalo" class="c_envio"> 
	<div class="envio_i">  
    <h2 class="title">Shoutbox</h2> 
		<div id="contentarea">
					<form method="post" id="form"> 
				<label class="cufonblue">Nick</label> 
				<input class="txt_a" id="nick" type="text" MAXLENGTH="10" /> 
				<input class="cufow" id="send" type="submit" value="Dilo" />
				<br>
				<img src="images/shoutit.jpg">
				<input class="txt_b" id="message" type="text" MAXLENGTH="25" /> 
				<div> </div>
			</form> 
			<div id="container"> 	
				<span class="clear"></span> 
				<div class="content"> 			
					<div id="loading"><img src="images/loading.gif" heigh="30" width="30" alt="cargando..." /></div> 
					<ul> 
					</ul> 
				</div> 
			</div>

		</div>
	</div>
</div>

</div>
<div id="footer" class="footer"> <ul>
        <li id="copyright">Copyrigth &copy; 2011. <b>gettyng.com</b></li>
        <li><a href="contacto">Contáctanos</a></li>
        <li><a href="faq">¿Dudas?</a></li>
        <li><a href="terminos">Términos de uso</a></li>
      </ul> 
</div> 
</body>

<script type="text/javascript" src="js/shoutbox.js"></script> 

</html>