var checkscroll;
var menu_start_top;
var jump_to_section_top;
//var section_positions = {};
//var sections = ["mexican", "pizza", "specials"];

//Init 'Unobtrusive' JavaScript
addEvent(window, "load", function() {

    if ($("basketform") != null) {

        //On basket submit
        $("basketform").onsubmit = function() {

            //Check minimum order value is met
            if (order_item_total < min_order_value) {
                alert("La orden total es menor a lo que el restuarante pide de minimo de pedido");
                return false;
            }

        };

        //Default handling
        handle_handling();

        //Load menu into basket
        menu_load_item_basket();

        //Find top position of the basket
        var menu_basket_position = findpos($("menu_basket_top"));
        menu_start_top = menu_basket_position[1];

        //Set the height on the menu basket container (to prevent jumping when it goes to a fixed position)
        $("menu_basket_container").style.height = $("menu_basket_container").offsetHeight + "px";

    }

    //Find top position of the jump to section
    var jump_to_section_position = findpos($("jump_to_section_top"));
    jump_to_section_top = jump_to_section_position[1];

    //Set the height on the jump to section container (to prevent jumping when it goes to a fixed position)
    $("jump_to_section_container").style.height = $("jump_to_section_container").offsetHeight + "px";

    //Run page scrolled handler
    pagescrolled();
/*
    //Save section positions
    for (i in sections) {
        var sectionname = sections[i];
        var position = findpos($("section-" + sectionname));
        section_positions[sectionname] = position[1];
    }
*/
});

//Find position of specified element
function findpos(obj) {
    var curleft = curtop = 0;
    if (obj.offsetParent) {
        curleft = obj.offsetLeft;
        curtop = obj.offsetTop;
        while (obj = obj.offsetParent) {
            curleft += obj.offsetLeft;
            curtop += obj.offsetTop;
        }
    }
    return [curleft,curtop];
}

/*
//Find relative position of specified element
function findrelpos(scrollcontainer, obj) {

    var position = findpos(obj);

    var scrollcontainertop = scrollcontainer.scrollTop;
    var scrollcontainerleft = scrollcontainer.scrollLeft;

    return [position[0] - scrollcontainerleft, position[1] - scrollcontainertop];

}
*/

//Page scrolled handler
function pagescrolled() {

    //Retrieve current scroll position
    var scroll_height_pos = scroll_height();

    if ($("basketform") != null) {

        //If above the top of the menu, then show inline normally
        if (menu_start_top > scroll_height_pos) {
            $("menu_basket").style.position = "";
            $("menu_basket").style.top = "";
        } else {
            //Otherwise fix position at top of screen
            $("menu_basket").style.position = "fixed";
            $("menu_basket").style.top = "0px";
        }

    }

    //If above the top of the jump section box, then show inline normally
    if (jump_to_section_top > scroll_height_pos) {
        $("jump_to_section").style.position = "";
        $("jump_to_section").style.top = "";

    } else {
        //Otherwise fix position at top of screen
        $("jump_to_section").style.position = "fixed";
        $("jump_to_section").style.top = "0px";

    }

    //Clear timeout if already set
    if (checkscroll) {
        window.clearTimeout(checkscroll);
    }

    //Set to check again shortly for change in position
    checkscroll = window.setTimeout("pagescrolled();", 100);

}

//Scroll height
function scroll_height() {
    var h = window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop;
    return h ? h : 0;
}

//Jump to specified section
function jump_section(section_id) {

    var position = findpos($("section-" + section_id));
    var scrolltopos = position[1] - $("jump_to_section").clientHeight; //Section top minus height of the jump to box

    window.scroll(0, scrolltopos);

}

//Add item
function menu_item_add(restaurant_id, item_id, item_name, item_cost, item_qty) {

    //If not already in basket add it
    if (basket_items[item_id] == undefined) {

        basket_items[item_id] = {
            "restaurant_id": restaurant_id,
            "qty": 0,
            "name": item_name,
            "cost": item_cost
        };

    }

    if (item_qty == undefined) {
        //Increase quantity
        basket_items[item_id]["qty"]++;
    } else {
        basket_items[item_id]["qty"] = item_qty;
    }

    //Update display with quantity
    //$("menu_item_"+item_id+"_qty").innerHTML = basket_items[item_id]["qty"];

    //Retotal basket items
    //menu_basket_retotal();

	if(item_id==502){

     updateCombo(item_id,basket_items[item_id]["qty"]);
	
}
	
    //Resave cookie
    menu_cookie();

    //Reload basket
    menu_load_item_basket();
    //Cargo el submenu

}

//Remove item
function menu_item_remove(item_id) {

    //If can remove item
    if (basket_items[item_id] != undefined) {

        if (basket_items[item_id]['qty'] > 0) {
            basket_items[item_id]['qty']--;
        
		  if(item_id==502){

     updateCombo(item_id,basket_items[item_id]["qty"]);
}

		}

        //Update display with quantity
        //$("menu_item_"+item_id+"_qty").innerHTML = (basket_items[item_id]['qty'] == 0) ? '' : basket_items[item_id]['qty'];

        if (basket_items[item_id]['qty'] == 0) {
            delete basket_items[item_id];

		}

        //Retotal basket items
        //menu_basket_retotal();

        //Resave cookie
        menu_cookie();

        //Reload basket
        menu_load_item_basket();

    }

}

//Show basket contents
function menu_load_item_basket() {

    var basket_items_html = "";

    //Go through all items in basket
    var row = 'b';
    for (item_id in basket_items) {

        /*
        //If item on this menu
        if ($("menu_item_"+item_id+"_qty") != null) {

            //Update display with quantity
            $("menu_item_"+item_id+"_qty").innerHTML = basket_items[item_id]['qty'];

        }
        */

        //If item is for the current restaurant
        if (basket_items[item_id]["restaurant_id"] == restaurant_id) {

            row = (row == 'b') ? 'a' : 'b';

            var item_html = '';
            item_html += '<div class="item item-'+row+'">\n';
            item_html += '  <div class="name" title="'+htmlentities(basket_items[item_id]["name"])+'">'+htmlentities(basket_items[item_id]["name"])+'</div>\n';
            item_html += '  <div class="price">'+formatcurrency(basket_items[item_id]["cost"])+'</div>\n';
            item_html += '  <div class="removebtn"><a href="#" onclick="menu_item_remove('+item_id+'); return false;"><img src="'+theme_resources_path+'takeaway_menu/order/remove_item_btn.gif" width="10" height="10" alt="Remove Item" /></a></div>\n';
            item_html += '  <div class="qty">'+basket_items[item_id]["qty"]+'</div>\n';
            item_html += '  <div class="addbtn"><a href="#" onclick="menu_item_add('+restaurant_id+', '+item_id+'); return false;"><img src="'+theme_resources_path+'takeaway_menu/order/add_item_btn.gif" width="10" height="10" alt="Add Item" /></a></div>\n';
            item_html += '  <div class="clear"></div>\n';
            item_html += '</div>\n\n';

            //Add item html
            basket_items_html += item_html;

        }

    }

    if (basket_items_html.length == 0) {
        basket_items_html = '<div class="noitems">Click en el signo de + para agregar</div>';
    }

    //Update basket
    $("basket_generated").innerHTML = basket_items_html;

    //Retotal basket items
    menu_basket_retotal();

}

//Retotal basket items
var order_item_total;
function menu_basket_retotal() {

    order_item_total = 0;

    //Add items in basket to total
    for (item_id in basket_items) {

        //If item is for the current restaurant
        if (basket_items[item_id]["restaurant_id"] == restaurant_id) {
            order_item_total += basket_items[item_id]['qty'] * basket_items[item_id]['cost'];
        }

    }

    var total = order_item_total;

    //If delivery add on delivery charge
    if (handling == 1) { //delivery
        //Add delivery charge to total
        total += delivery_charge;
    }

     total = roundnumber2dp(total);
     total = formatcurrency(total);

    $("total_cost").innerHTML = total;

}

//Save cookie with menu items
function menu_cookie() {
		            var var1 = outputSelected();

    //Create string of menu item ids
    var cookie_value = "handling="+handling+"&";
    for (item_id in basket_items) {
	if(item_id==502)
       cookie_value += "menu_items["+item_id+"][qty]=" + basket_items[item_id]['qty']+"detalle["+var1+"]" +"&";
else
	cookie_value += "menu_items["+item_id+"][qty]=" + basket_items[item_id]['qty']+"&";	
	}

    //If have menu items save cookie
    if (cookie_value.length > 0) {
        createcookie("menu", cookie_value, 14)
    } else {
        //Otherwise erase cookie
        erasecookie("menu");
    }

}

//Handling changed
function handling_changed() {

    //Redisplay handling
    handle_handling();

    //Retotal basket items
    menu_basket_retotal();

    //Resave cookie
    menu_cookie();

}

//Handle delivery / collection
function handle_handling() {

    //If handling collection
    if (handling == 2) { //collection

        $("handling_name").innerHTML = 'Ir por pedido';
        $("handling_charge").innerHTML = '0.00';

        //If support delivery
        if (inarray(1, handling_accept)) {
            $("handling_name").innerHTML += ' <span class="switch_handling">[<a href="#" onclick="handling = 1; handling_changed(); return false;">Cambiar a entrega</a>]</span>';
        }

    } else {

        $("handling_name").innerHTML = 'Servicio a Domicilio';
        $("handling_charge").innerHTML = formatcurrency(delivery_charge);

        //If support collections
        if (inarray(2, handling_accept)) {
            $("handling_name").innerHTML += ' <span class="switch_handling">[<a href="#" onclick="handling = 2; handling_changed(); return false;">Ir por pedido</a>]</span>';
        }

    }

}



function updateCombo(item_id,item_qty,combo) {

var combo = document.getElementById('detalle-promociones');
var c = document.getElementById('detalle-promociones');
var text   = '';
var pepe=item_qty;
var aumentos=parseInt(pepe)+1;
//alert("Valor de pepe"+aumentos);
   if(item_id==502){

 
 for (var t = 0; t < pepe; t++) {  // repetir combos


		for (var m = 0; m < 4; m++){

    text = text + '<table><td ><SPAN id="numero">La Burger#'+ (m + 1) +' ' +  'del paquete'+(t+1)+':<select name="mm" ><option value="pollo" >Pollo</option><option value="arrachera" selected>Arrachea</option></select></SPAN></td></table></div> ';


    
									}
      text = text + '<table><td ><SPAN id="numero">El refresco del paquete #'+ (t + 1) +' ' +  'sabor:<select name="mm"><option value="Pepsi" selected>Pepsi</option><option value="manzanita">Manzanita</option></select></SPAN></td></table></div>';

								}
	if( t>=1){
		  text= text +'<form><INPUT TYPE=BUTTON  value="pedir" ONCLICK=" menu_cookie();"></form>';
     c.innerHTML = text;

	
	}
	else
     c.innerHTML = text;

}

                 }
                 
     
function outputSelected() {
var elements = document.getElementsByName("mm");
var strSel = "";
		for (var item=0; item<elements.length;  item++)       
			strSel += elements[item].value + ",";
             //alert ("Hamburgesas de :\n" + strSel);

			return strSel;

		 }     

