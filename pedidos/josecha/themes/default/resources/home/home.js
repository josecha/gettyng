//Init 'Unobtrusive' JavaScript
addEvent(window, "load", function() {

	//Init photo change code
	photochange_init();

});

/* ----- Photo Image Rotation ----- */

var currphotoid;
var photos_total;
var photoimg_ab = "b";

//Init advert code
function photochange_init() {

	photos_total = photoimgs.length;

	currphotoid = 0;

	if (photos_total > 1) {

		//Preload images
		for (i in photoimgs) {

			var preload = new Image();
			preload.src = photoimgs[i]["src"];

		}

		//Initiate change photos
		window.setTimeout("photo_loadnewimage();", 1000 * photoimg_timeoutinterval / 2);

	}

}

//Load photo image
function photo_loadnewimage() {

	var header_current = currphotoid;

	//Set next image to use
	currphotoid++;

	if (currphotoid >= photos_total) {
		currphotoid = 0;
	}

	var photo_next = currphotoid;

	//Set a/b image type
	photoimg_ab = (photoimg_ab == "a") ? "b" : "a";

	//Set next image
	document.getElementById("photoimg_" + photoimg_ab).src = photoimgs[photo_next]["src"];
	document.getElementById("photoimg_" + photoimg_ab).alt = photoimgs[photo_next]["alt"];
	document.getElementById("photoimg_" + photoimg_ab).title = photoimgs[photo_next]["title"];

	//Fade opacity
	fadeopacity("photoimg_b", (photoimg_ab == "a") ? true : false);

	//Repeat
	window.setTimeout("photo_loadnewimage();", 1000 * photoimg_timeoutinterval);

}

//Fade opacity
function fadeopacity(imageid, status, opacity) {

	var opacity_step = 5;

	if (opacity == undefined) {
		opacity = (status == true) ? 100 : 0;
	}

	changeopac(opacity, imageid);

	if (status == true) {

		if (opacity != 0) {
			window.setTimeout("fadeopacity(\""+imageid+"\", "+status + ", "+(opacity-opacity_step)+");", 100);
		}

	} else {

		if (opacity != 100) {
			window.setTimeout("fadeopacity(\""+imageid+"\", "+status + ", "+(opacity+opacity_step)+");", 100);
		}

	}

}

//Change the opacity for different browsers
function changeopac(opacity, imageid) {
	var object = document.getElementById(imageid).style;
	object.opacity = (opacity / 100);
	object.MozOpacity = (opacity / 100);
	object.KhtmlOpacity = (opacity / 100);
	object.filter = "alpha(opacity=" + opacity + ")";
}
