<?php

include $includes_path . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$auth = new auth();
$auth->handle();
$authinfo = $auth->getauthinfo();
//$auth->login_required();


$page_title = 'Gettyng.com - Preguntas frecuentes';
$metadesc = 'Sistema de pedidos en linea';

$body_html = 
$body_html = <<<EOHTML

<h1>Preguntas frecuentes</h1>

<dl class="standardfaq">

	<div style="display:block; height: 50px;"><div style="float: left;"><img src="/images/o_dudas.gif"></div>

	<div style="display:block; height: 50px;"><div><h2 class="title">�Tienes dudas?</h2> </div></div>
		  <div class="toggleList_container">
			<ol class="jtoggle-list">
			<li>
				<a class="buy" href="javascript:void(0)">�Que es Gettyng pedidos?</a>
				<div style="display: none;" class="">
				<p>Es una p�gina web que ofrece la posibilidad a los usuarios de hacerle llegar un pedido por internet. Para esto es necesario que el usuario se registre y proporcione informaci�n relevante para recibir sus pedidos. El usario puede ubicar todos los restaurantes cerca del lugar donde quiere levantar el pedido. Una vez encontrados los restaurantes puede revisar los productos que  ofrece, el horario del comercio y la cobertura  que este maneja.</p>
				</div>
				</li>
				<li>
				<a class="buy" href="javascript:void(0)">�Tiene alg�n costo pedir  desde Gettyng?</a>
				<div style="display: none;" class="">
				<p>El servicio no tiene ning�n costo ni compromiso</p>
				</div>
				</li>
				<li>
				<a class="buy" href="javascript:void(0)">�C�mo funciona el servicio?</a>
				<div style="display: none;" class="">
				<p>1.Levantas pedido desde gettyng.com</br>2.-Gettyng monitorea y confirma tu pedido.</br>3.-El restaurante prepara y lleva tu comida a tu lugar indicado</p>
				</div>
				</li>				
				
			</ol>
		</div>

</dl>

EOHTML;

$template = new template();
$template->settitle($page_title);
$template->setmetadesc($metadesc);
//$template->setheaderaddinhtml($headeraddin_html);
$template->setmainnavsection('faq');
$template->setbodyhtml($body_html);
$template->setshowsearch(true);
$template->setshowpopular(true);
$template->setshoworderprocess(true);
$template->display();

?>