<?php
session_start();
include $includes_path . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$auth = new auth();
$auth->handle();
$authinfo = $auth->getauthinfo();
//$auth->login_required();

$link_base_path = htmlentities(navfr::base_path());

//Photos to rotate through
$rotate_photos = array(
	array(
		'src' => "{$link_base_path}{$cfg['theme_resources_path']}home/photos/children.jpg",
		'alt' => "comiendo deliciosa pizza",
		'title' => '',
	),
	array(
		'src' => "{$link_base_path}{$cfg['theme_resources_path']}home/photos/family_table.jpg",
		'alt' => 'mas pizza',
		'title' => '',
	),
	array(
		'src' => "{$link_base_path}{$cfg['theme_resources_path']}home/photos/family_3_children.jpg",
		'alt' => 'que rico',
		'title' => '',
	),
	array(
		'src' => "{$link_base_path}{$cfg['theme_resources_path']}home/photos/young_adult_sofa.jpg",
		'alt' => 'sofa comiendo pizzza',
		'title' => '',
	),
	array(
		'src' => "{$link_base_path}{$cfg['theme_resources_path']}home/photos/teenage_friends.jpg",
		'alt' => '',
		'title' => 'Adolecentes mexicanos comiendo',
	),
);

//About us info
$link_about = navfr::link_h(array('about-us'));
$aboutusbox_html = <<<EOHTML
<h2>Pide ya !!</h2>
<p>Haz tu pedido facil, rapido y Gratis!!!! </p>
<a href="{$link_about}" class="readmore"><img src="{$link_base_path}{$cfg['theme_resources_path']}template/link_bullet.gif" width="6" height="7" alt="Arrow Right" />Enterate como funciona </a>
EOHTML;
//Page Handler
$pagehandler = new page_home();
$pagehandler->postdata = $_POST;
$pagehandler->getdata = $_GET;

$pagehandler->rotate_photos = $rotate_photos;

$pagehandler->titletag = 'Pide tu comida favorita en linea';
$pagehandler->metadesc = 'Pide tu comida favorita';
$pagehandler->aboutusbox_html = $aboutusbox_html;

$pagehandler->handle();

?>