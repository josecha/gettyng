<?php
session_start();
include $includes_path . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$auth = new auth();
$auth->handle();
$authinfo = $auth->getauthinfo();
//$auth->login_required();


$email_parts = explode('@', $cfg['email_contact']);
$page_title = 'Gettyng.com - Contactanos';
$metadesc = 'Gettyng pedidos de comida en linea';

//Intro text
$body_html = <<<EOHTML
<div class="envio_i">
		<div style="display:block; height: 50px;"><div style="float: left;"><img src="/images/o_contacto.gif"></div><div><h2 class="title">Contactanos</h2> </div></div>	
			<div style="float: left;"><p> �Hola! <br>
				En <span class="cufon">Gettyng</span> estamos para escucharte <br>�Tienes alguna idea sugerencia o queja?<br>Ponte en contacto con nosotros y te responderemos.
				<br><br>
				<span class="cufon">Anunciate con nosotros</span><br>
				Email: <a href="mailto:ventas@gettyng.com">ventas@gettyng.com</a>	<br><br>
				<span class="cufon">�Tienes problemas con nuestro servicio?</span><br>
				Email: <a href="mailto:soporte@gettyng.com">soporte@gettyng.com</a>	<br><br>
				<span class="cufon">Sugerencias y comentarios</span><br>
				Email: <a href="mailto:comenta@gettyng.com">comenta@gettyng.com</a>	<br>
				
			</p></div>
			<p style="float: right;" > <img src="/images/o_contacto2.gif" >
			</p>
	  </div>

EOHTML;





//Page Handler
$template = new template();
$template->settitle($page_title);
$template->setmetadesc($metadesc);
//$template->setheaderaddinhtml($headeraddin_html);
$template->setmainnavsection('faq');
$template->setbodyhtml($body_html);
$template->setshowsearch(true);
$template->setshowpopular(true);
$template->setshoworderprocess(true);
$template->display();



//$pagehandler->handle();

?>