<?php
session_start();
include $includes_path . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$auth = new auth();
$auth->handle();
$authinfo = $auth->getauthinfo();
//$auth->login_required();


$page_title = 'Gettyng.com - Acerca de Nosotros';
$metadesc = 'Gettyng pedidos de comida en linea';

$body_html = <<<EOHTML

<h1>Acerca de Nosotros</h1>


<p>
Te ponemos los mejores restaurantes de tu ciudad para que puedas tener variedad y encargar tu comida de forma sencilla y r�pida.</br>

Adem�s, conseguimos para ti las mejores promociones y descuentos para que pedir tu comida favorita te cueste un poco menos =)</br></br>

Esperamos que disfrutes de la experiencia ... ��Buen provecho!!</br></p>


EOHTML;

$template = new template();
$template->settitle($page_title);
$template->setmetadesc($metadesc);
//$template->setheaderaddinhtml($headeraddin_html);
$template->setmainnavsection('about-us');
$template->setbodyhtml($body_html);
$template->setshowsearch(true);
$template->setshoworderprocess(true);
$template->display();
/*
<p>Somos una innovadora plataforma de comercio electr�nico especializada en restaurantes y negocios con servicio de comida a domicilio</p>
</br>�Qu� podemos ofrecerte?</br>
Incrementar tus ventas</br>
Acceso a nuevos clientes</br>
Optimizar la gesti�n de tus pedidos</br>
Actualizar carta y precios online</br>
Ofrecer promociones y descuentos</br>
gettyng.com es tu sitio para pedir comida online</br>


*/
?>