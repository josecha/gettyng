<?php

class page_takeaway_order {

	protected $titletag;
	public $pagetitle = 'Ordenar';
	public $metadesc;
	public $body_html;
	public $headeraddin_html;
	public $footeraddin_html;
	public $mainnavsection;
	public $googanalyticspage;
	public $getdata;
	public $postdata;

	protected $city_data;
	protected $town_data;
	protected $restaurant_record;
	protected $order_total;
	protected $handling;
	protected $menu_items;
	protected $success = false;
	protected $order_id;
	protected $payment_type_accepted = array();
	protected $payment_type_denied = array();

	public function init() {
		global $db, $tbl, $cfg;

		$current_path = navfr::current_path();

		if (!( (isset($current_path[1])) && (isset($current_path[2])) && (isset($current_path[3])) )) {
			throw new Exception('Somehow reached this page without city, town, restaurant specified');
		}

		//Resolve city data
		$this->city_data = appgeneral::city_from_navname($current_path[1]);

		//If city not found
		if ($this->city_data == false) {
			template_lib::show_404();
			exit;
		}

		//Resolve town data
		$this->town_data = appgeneral::town_from_navname($this->city_data['id'], $current_path[2]);

		//If town not found
		if ($this->town_data == false) {
			template_lib::show_404();
			exit;
		}

		//Retrieve restaurant data

		$select_sql = '';
		$select_sql .= "(SELECT {$tbl['town']}.name FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id) AS town, ";
		$select_sql .= "(SELECT {$tbl['city']}.name FROM {$tbl['city']} WHERE {$tbl['city']}.id = (SELECT {$tbl['town']}.city_id FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id)) AS city";

		$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $db->col(array('id', 'name', 'del_radius_mi', 'min_order_value', 'delivery_charge', 'address_1', 'address_2', 'postcode', 'telephone', 'msg_dispatch', 'payment_cash', 'payment_online', 'navname')) . ', ' . $select_sql, $db->cond(array("navname='".$db->es($current_path[3])."'", "town_id = {$this->town_data['id']}", "status = 1"), 'AND'), '', 0, 1);
		if (!($this->restaurant_record = $db->record_fetch($restaurant_result))) {

			//If restaurant not found
			template_lib::show_404();
			exit;

		}

		//Retrieve payment types accepted
		$payment_type_accepted = array();
		$payment_type_denied = array();
		foreach ($cfg['payment_types'] as $type_id => $type) {

			$payment_type = $cfg['payment_types_nameassoc'][$type_id];
			if ($this->restaurant_record['payment_' . $payment_type]) {
				$payment_type_accepted[] = $type_id;
			} else {
				$payment_type_denied[] = $type_id;
			}

		}

		$this->payment_type_accepted = $payment_type_accepted;
		$this->payment_type_denied = $payment_type_denied;


		//Retrieve total amount of order, menu items

		$order_total = 0;

		//Retrieve menu items saved in cookie
		$menu_items = array();
		$menu_cookie = appgeneral::menu_cookie();
		foreach ($menu_cookie['menu_items'] as $item_id => $item) {

			//Retrieve menu item data
			$menu_item_data = appgeneral::menu_item_data($item_id);
			if ($menu_item_data) {

				//If restaurant id matches current restaurant
				if ($menu_item_data['restaurant_id'] == $this->restaurant_record['id']) {

					$menu_items[$item_id] = array(
						'qty' => $item['qty'],
					);

					$menu_items[$item_id] = array_merge($menu_items[$item_id], $menu_item_data);

					$order_total += $menu_item_data['cost'] * $item['qty'];

				}

			}

		}

		$this->menu_items = $menu_items;

		$order_total = number_format($order_total, 2);

		$this->order_total = $order_total;


		//Retrieve handling for this order
		$this->handling = appgeneral::retrieve_order_handling($this->restaurant_record['id']);

		//Add delivery onto order total
		$order_total_withdeliv = $this->order_total;
		if ( ($this->handling == 1) && ($this->restaurant_record['delivery_charge']) ) {
			$order_total_withdeliv += $this->restaurant_record['delivery_charge'];
		}

		//Format order with delivery total
		$order_total_withdeliv = number_format($order_total_withdeliv, 2);

		$this->order_total_withdeliv = $order_total_withdeliv;

	}

	//-------------------------------------------------------------------------------------

	public function handle() {
		global $auth;

		//Init data required by the page
		$this->init();

		//Order form errors
		$orderform_errormsg_html = $this->orderform_errormsg_html();

		//If valid post with no errors
		if ( (isset($this->postdata['formposted'])) && (!count($orderform_errormsg_html)) ) {

			//Mark as successful
			$this->success = true;

			//Save order and send email
			$this->process_form();

			//Show order success
			$forms_html = $this->ordersuccess_html();

		} else {

			//Order form
			$orderform_formdata = $this->orderform_data();
			$orderform_html = $this->orderform_html($orderform_formdata, $orderform_errormsg_html);

			//If not logged in
			if ($auth->login_success == false) {

				//If attempted user login
				if (isset($this->postdata['formposted_login'])) {

					//Login form errors
					$link_reset_password = navfr::link_h(array('user', 'reset-password'));
					$loginform_errormsg_html = array("Username/password incorrecto o email not verificado.  <a href=\"{$link_reset_password}\">recordar password</a>");

				} else {
					$loginform_errormsg_html = array();
				}

				$loginform_formdata = $this->loginform_data();
				$loginform_html = $this->loginform_html($loginform_formdata, $loginform_errormsg_html);

				$forms_html = <<<EOHTML

<p>Pon tus datos o logeate.</p>

<h2>Si ya tienes una cuenta</h2>

{$loginform_html}

<div class="line"></div>

<h2>o solo pon estos datos</h2>

{$orderform_html}

EOHTML;
			} else {

				$link_update_profile = navfr::link(array('user', 'profile'));

				$forms_html = <<<EOHTML

<p>Solo <strong>confirma tus datos</strong> below, click <strong>'Place Order'</strong> and your order will be on its way to you.  If any of your details are incorrect, please <a href="{$link_update_profile}">update your profile</a>.</p>

{$orderform_html}

EOHTML;

			}

		}

		//$breadcrumbs_html = $this->breadcrumbs_html();

		$breadcrumbs = $this->breadcrumbs();

		$pagetitle_h = htmlentities($this->pagetitle);

		$body_html = <<<EOHTML

<h1>{$pagetitle_h}</h1>

{$forms_html}

EOHTML;

		$this->handle_title();
		$this->handle_headeraddin_html();
		$this->handle_footeraddin_html();
		$this->handle_mainnavsection();
		$this->handle_googanalyticspage();

		//Template
		$template = new template();
		$template->settitle($this->titletag);
		$template->setmetadesc($this->metadesc);
		$template->setmainnavsection($this->mainnavsection);
		$template->setgooganalyticspage($this->googanalyticspage);
		$template->setheaderaddinhtml($this->headeraddin_html);
		$template->setfooteraddinhtml($this->footeraddin_html);
		$template->setbodyhtml($body_html);
		//$template->setshowsearch(true);
		//$template->setshoworderprocess(true);
		$template->setbreadcrumbs($breadcrumbs);
		$template->display();

	}

	protected function handle_title() {
		$this->titletag = "Place Order With {$this->restaurant_record['name']} - {$this->restaurant_record['town']}, {$this->restaurant_record['city']}";
	}

	protected function handle_headeraddin_html () {
		global $cfg;

		$link_base_path = htmlentities(navfr::base_path());

		$this->headeraddin_html = <<<EOHTML
<meta name="robots" content="noindex" />
<script src="{$link_base_path}{$cfg['theme_resources_path']}takeaway_order/takeaway_order.js" type="text/javascript"></script>
EOHTML;

	}

	protected function handle_footeraddin_html () {

		if ($this->success == true) {
			$google_ecommercetracking_html = self::google_ecommercetracking_html($this->order_id);

			$clear_basket_items_cookie = <<<EOHTML
<script type="text/javascript">
  <!--

	erasecookie("menu");

  //-->
</script>
EOHTML;

		} else {
			$google_ecommercetracking_html = '';
			$clear_basket_items_cookie = '';
		}

		$this->footeraddin_html = <<<EOHTML
{$google_ecommercetracking_html}
{$clear_basket_items_cookie}
EOHTML;

	}

	protected function handle_mainnavsection() {
		$this->mainnavsection = 'takeaway';
	}

	protected function handle_googanalyticspage() {

		if ($this->success == true) {
			//Goal Tracking: "/order/completed/$" (regex)
			$this->googanalyticspage = navfr::self() . 'completed/';
		}

	}

	//-------------------------------------------------------------------------------------

	protected function loginform_html($formdata, $errormsg_html) {
		global $cfg;

		$link_base_path = htmlentities(navfr::base_path());

		$errormsgall_html = appgeneral::errormsgs_html($errormsg_html);

		$formdatah = lib::htmlentities_array($formdata);

		$args = (isset($_GET['testform'])) ? array('testform' => 1) : array();
		$form_self = navfr::self_h($args);

		//$login_username = isset($this->postdata['login_username']) ? $this->postdata['login_username'] : '';

		$html = <<<EOHTML

<form method="post" action="{$form_self}">
	<div><input type="hidden" name="formposted_login" value="1" /></div>

	<div class="standardform">

{$errormsgall_html}

		<div class="group">
			<div class="fieldtitle"><label for="login_username">Email</label></div>
			<div class="fieldinput"><input type="text" name="login_username" id="login_username" value="{$formdatah['login_username']}" class="inputtxt" /></div>
			<div class="clear"></div>
		</div>

		<div class="group">
			<div class="fieldtitle"><label for="login_password">Password</label></div>
			<div class="fieldinput"><input type="password" name="login_password" id="login_password" value="{$formdatah['login_password']}" class="inputtxt" /></div>
			<div class="clear"></div>
		</div>

		<div><input type="image" src="{$link_base_path}{$cfg['theme_resources_path']}template/login.gif" value="Place Order" alt="Login" name="send" class="submit" /></div>

	</div>

</form>

EOHTML;

		return $html;

	}

	protected function orderform_html($formdata, $errormsg_html) {
		global $auth, $cfg;

		$link_base_path = htmlentities(navfr::base_path());

		$errormsgall_html = appgeneral::errormsgs_html($errormsg_html);

		$formdatah = lib::htmlentities_array($formdata);

		$args = (isset($_GET['testform'])) ? array('testform' => 1) : array();
		$form_self = navfr::self_h($args);

		//If not logged in
		if ($auth->login_success == false) {

			$save_details_checked = ( (isset($formdata['save_details'])) && ($formdata['save_details']) ) ? 'checked="checked"' : '';

			$save_details_html = <<<EOHTML
		<div class="group">
			<div class="fieldinput-savedetails"><input type="checkbox" name="save_details" id="save_details" value="1" onclick="savedetailchanged()" {$save_details_checked} /><label for="save_details"> Guardar tus datos</label></div>
			<div class="clear"></div>
		</div>
EOHTML;

		} else {
			$save_details_html = '';
		}

		//Disable payment types not accepted by restaurant
		$disabled = array();
		foreach ($cfg['payment_types'] as $type_id => $type) {
			if (!in_array($type_id, $cfg['payment_types_accepted'])) {
				$disabled[] = $type_id;
			}
		}

		$payment_radios_html = lib::create_radio('payment_type', $cfg['payment_types'], $formdata['payment_type'], $this->payment_type_denied);

		$html = <<<EOHTML

<form method="post" action="{$form_self}">
	<div><input type="hidden" name="formposted" value="1" /></div>

	<div class="standardform">

{$errormsgall_html}

		<div class="group">
			<div class="fieldtitle"><label for="name">Nombre</label></div>
			<div class="fieldinput"><input type="text" name="name" id="name" value="{$formdatah['name']}" class="inputtxt" /></div>
			<div class="clear"></div>
		</div>

		<div class="group">
			<div class="fieldtitle"><label for="address">Direccion<br />(for delivery)</label></div>
			<div class="fieldinput"><textarea name="address" id="address" rows="5" cols="20" class="inputtxtarea">{$formdatah['address']}</textarea></div>
			<div class="clear"></div>
		</div>

		<div class="group">
			<div class="fieldtitle"><label for="postcode">Codigo Postal</label></div>
			<div class="fieldinput"><input type="text" name="postcode" id="postcode" value="{$formdatah['postcode']}" class="inputtxt inputtxt-postcode" /></div>
			<div class="clear"></div>
		</div>

		<div class="group">
			<div class="fieldtitle"><label for="telephone">Telefono</label></div>
			<div class="fieldinput"><input type="text" name="telephone" id="telephone" value="{$formdatah['telephone']}" class="inputtxt" /></div>
			<div class="clear"></div>
		</div>

		<div class="group">
			<div class="fieldtitle"><label for="email">Email</label></div>
			<div class="fieldinput"><input type="text" name="email" id="email" value="{$formdatah['email']}" class="inputtxt" /></div>
			<div class="clear"></div>
		</div>

{$save_details_html}

		<div class="group" id="password_block">
			<div class="fieldtitle"><label for="new_password">Password</label></div>
			<div class="fieldinput"><input type="password" name="new_password" id="new_password" value="{$formdatah['new_password']}" class="inputtxt" /></div>
			<div class="clear"></div>
		</div>

		<div class="group">
			<div class="fieldtitle"><label for="notes">Comentario</label></div>
			<div class="fieldinput"><textarea name="notes" id="notes" rows="5" cols="20" class="inputtxtarea">{$formdatah['notes']}</textarea></div>
			<div class="clear"></div>
		</div>

		<div class="group">
			<div class="fieldtitle"><label>Pago en:</label></div>
			<div class="fieldinput">{$payment_radios_html}</div>
			<div class="clear"></div>
		</div>

		<div><input type="image" src="{$link_base_path}{$cfg['theme_resources_path']}takeaway_order/place_order.gif" value="Place Order" alt="Place Order" name="send" class="submit" /></div>

	</div>

</form>

EOHTML;

		return $html;

	}

	protected function loginform_data() {

		if (isset($this->postdata['formposted_login'])) {

			$formdata = array(
				'login_username' => $this->postdata['login_username'],
				'login_password' => $this->postdata['login_password'],
			);

		} else {

			$formdata = array(
				'login_username' => '',
				'login_password' => '',
			);

		}

		return $formdata;

	}

	protected function orderform_data() {
		global $cfg, $auth;

		if (isset($this->postdata['formposted'])) {

			/*
			$fields = array('name', 'address', 'postcode', 'email', 'notes', 'telephone', 'new_password');
			$formdata = array();
			foreach ($fields as $field) {
				$formdata[$field] = (isset($this->postdata[$field])) ? $this->postdata[$field] : '';
			}
			*/

			$formdata = array(
				'name' => $this->postdata['name'],
				'address' => $this->postdata['address'],
				'postcode' => $this->postdata['postcode'],
				'email' => $this->postdata['email'],
				'notes' => $this->postdata['notes'],
				'telephone' => $this->postdata['telephone'],
				'save_details' => ( (isset($this->postdata['save_details'])) && ($this->postdata['save_details']) ) ? 1 : 0,
				'new_password' => (isset($this->postdata['new_password'])) ? $this->postdata['new_password'] : '',
				'payment_type' => $this->postdata['payment_type'],
			);

		} else {

			$formdata = array(
				'name' => '',
				'address' => '',
				'postcode' => '',
				'email' => '',
				'notes' => '',
				'telephone' => '',
				'save_details' => 0,
				'new_password' => '',
				'payment_type' => $this->payment_type_accepted[0], //Default to first accepted value
			);

			if (isset($_GET['testform'])) {

				/*
				$notes = '';
				$total = rand(5, 10);
				for ($i=0; $i<$total; $i++) {
					$notes .= ucfirst(lib::randstring(rand(5, 10), lib::RANDSTRING_AZ_LC)) . ' ';
				}

				$notes = trim(ucfirst($notes));

				$address = rand(1, 100) . ' ' . ucfirst(lib::randstring(rand(10, 10), lib::RANDSTRING_AZ_LC)) . ' Road' . "\n";
				$address .=  ucfirst(lib::randstring(rand(7, 10), lib::RANDSTRING_AZ_LC));
				$address .=  ucfirst(lib::randstring(rand(7, 10), lib::RANDSTRING_AZ_LC));

				$formdata['name'] = 'Test ' . ucfirst(lib::randstring(rand(5, 10), lib::RANDSTRING_AZ_LC));
				$formdata['address'] = $address;
				$formdata['postcode'] = lib::randstring(2, lib::RANDSTRING_AZ_UC) . rand(1, 9) . ' ' . rand(1, 9) . lib::randstring(2, lib::RANDSTRING_AZ_UC);
				$formdata['telephone'] = '020 - ' . lib::randstring(4, lib::RANDSTRING_09) . ' - ' . lib::randstring(4, lib::RANDSTRING_09);
				$formdata['email'] = lib::randstring(rand(5, 10), lib::RANDSTRING_AZ_LC) . '@' . parse_url($cfg['site_url'], PHP_URL_HOST);
				$formdata['notes'] = $notes;
				*/

				$formdata['name'] = 'Joe Bloggs';
				$formdata['address'] = "1 Main High Street\nLondon";
				$formdata['postcode'] = lib::randstring(2, lib::RANDSTRING_AZ_UC) . rand(1, 9) . ' ' . rand(1, 9) . lib::randstring(2, lib::RANDSTRING_AZ_UC);
				//$formdata['telephone'] = '020 - ' . lib::randstring(4, lib::RANDSTRING_09) . ' - ' . lib::randstring(4, lib::RANDSTRING_09);
				//$formdata['email'] = lib::randstring(rand(5, 10), lib::RANDSTRING_AZ_LC) . '@' . parse_url($cfg['site_url'], PHP_URL_HOST);
				$formdata['notes'] = 'Please deliver this as soon as possible, we are hungry';

				if ( (isset($_GET['testform'])) || ($cfg['devmode']) ) {
					$formdata['email'] = $cfg['email_test'];
					$formdata['telephone'] = $cfg['telephone_test'];
				}

			} else if ($auth->login_success == true) {
				//If logged in, populate with logged in data

				$formdata['name'] = $auth->authinfo['name'];
				$formdata['address'] = $auth->authinfo['address'];
				$formdata['postcode'] = $auth->authinfo['postcode'];
				$formdata['email'] = $auth->authinfo['email'];
				$formdata['telephone'] = $auth->authinfo['telephone'];

			}

		}

		return $formdata;

	}

	protected function orderform_errormsg_html() {
		global $db, $tbl, $cfg;

		$errormsg_html = array();

		//Check not over limit for today
		$check_ts = time() - $cfg['order_limit_period'];
		$check_ts_datetime = gmdate('Y-m-d H:i:s', $check_ts);
		$order_result = $db->table_query($db->tbl($tbl['order']), 'COUNT(*) AS total', "added > '{$check_ts_datetime}'", '', 0, 1);
		if (!($order_result = $db->record_fetch($order_result))) {
			throw new Exception('Count did not return');
		}

		if ($order_result['total'] >= $cfg['order_limit']) {

			$time_period = appgeneral::sec_to_dhms($cfg['order_limit_period']);

			$errormsg_html[] = "Maximum no of orders for this rolling {$time_period} time period has been exceeded, we have been notified.  Sorry for any inconvenience caused, please try again later, items chosen will be kept in your basket for 14 days.";

			$message = "Limit of {$cfg['order_limit']} per {$time_period} time period has been exceeded.";
			$message = wordwrap($message, 70);
			$headers = 'From: ' . $cfg['email_system_from'];
			$status = mail($cfg['email_admin'], $cfg['site_name'] . ' Order Limit Exceeded', $message, $headers);
			if (!$status) {
				throw new Exception('Unable to send order limit exceeded email');
			}

		}

		$back_link = navfr::link_h(array('takeaway', $this->city_data['navname'], $this->town_data['navname'], $this->restaurant_record['navname']));

		//If no items on order
		if (!count($this->menu_items)) {
			$errormsg_html[] = "Please <a href=\"{$back_link}\">go back to the menu</a> and select items for your order.";
		} else {

			if ($this->order_total < $this->restaurant_record['min_order_value']) {
				$min_order_value = number_format($this->restaurant_record['min_order_value'], 2);
				$errormsg_html[] = "Your order total of &pound;{$order_total} (excluding delivery) is under the minimum value of &pound;{$min_order_value} accepted by " . htmlentities($this->restaurant_record['name']) . " please <a href=\"{$back_link}\">add additional items</a> to your order.";
			}

		}


		//Retrieve current restaurant open/closed status
		$restaurant_open_status = appgeneral::restaurant_open($this->restaurant_record['id']);

		if ($restaurant_open_status == false) {
			$errormsg_html[] = "Your chosen restaurant is not currently open, restaurant opening times are available on the top right of the <a href=\"{$back_link}\">restaurants menu</a>.  Items chosen will be kept in your basket for 14 days or until you place you order.  Please try again when the selected restaurant is open or choose another restaurant.";
		}

		if ($this->restaurant_record['msg_dispatch'] <= 1) {
			$errormsg_html[] = "Restaurant is not currently setup for online ordering.";
		}

		//If form posted, check user data
		if (isset($this->postdata['formposted'])) {

			if (!$this->postdata['name']) {
				$errormsg_html[] = "'Name' is required";
			}

			if (!$this->postdata['address']) {
				$errormsg_html[] = "'Address' is required";
			}

			if (!$this->postdata['postcode']) {
				$errormsg_html[] = "'Postcode' is required";
			}

			//Telephone
			$telephone = '';
			if ($this->postdata['telephone']) {

				$telephone = $this->postdata['telephone'];
				$telephone = str_replace('-', ' ', $telephone);
				$telephone = preg_replace("%[ \t]+%", ' ', $telephone);

				//Telephone number
				if (!preg_match("%[^0-9\ ]%", $telephone)) {

					$telephone_validate = $telephone;
					$telephone_validate = preg_replace("%[^0-9]%", '', $telephone_validate);

					$valid = true;

					if (!preg_match("/^0[1-9][0-9]{8,9}$/", $telephone_validate)) {
						$errormsg_html[] = "'Telephone' does not appear to be valid (should not contain country code, start with a leading zero, and be 10-11 digits in length)";
						$valid = false;
					}

					if (preg_match("/^09/", $telephone_validate)) {
						$errormsg_html[] = "'Tel No' appears to be premium rate";
						$valid = false;
					}

					if ($valid == true) {
						$this->postdata['telephone'] = $telephone;
					}

				} else {
					$errormsg_html[] = "'Telephone' should be in the format e.g. 123 4567 8901, and should contain 0-9 only";
				}

			} else {
				$errormsg_html[] = "'Telephone' not specified.";
			}

			if (!$this->postdata['email']) {
				$errormsg_html[] = "'Email' is required";
			} else {
				if (!lib::chkemailvalid($this->postdata['email'])) {
					$errormsg_html[] = "'Email' does not appear to be valid";
				}
			}

			//If save details
			if (isset($this->postdata['save_details'])) {

				//If no errors
				if (!count($errormsg_html)) {

					//Check for an account that already exists
					$customer_result = $db->table_query($db->tbl($tbl['customer']), $db->col(array('id')), $db->cond(array("email = '".$db->es($this->postdata['email'])."'", "verified = 1"), 'AND'), '', 0, 1);
					if ($customer_record = $db->record_fetch($customer_result)) {
						$link_reset_password = navfr::link_h(array('user', 'reset-password'));
						$errormsg_html[] = "You already have an account setup with this email address, please login to use it.  <a href=\"{$link_reset_password}\">Forgotten your password?</a>";
					}

					//Check password
					if ($this->postdata['new_password']) {
						if (strlen($this->postdata['new_password']) < 8) {
							$errormsg_html[] = "'Password' must be 8 or more characters in length";
						}
					} else {
						$errormsg_html[] = "'Password' must be spcified to create an account";
					}

				}

			}

			//Check payment
			if (!in_array($this->postdata['payment_type'], $this->payment_type_accepted)) {
				$type_id = $this->postdata['payment_type'];
				//$errormsg_html[] = "'Payment' '{$cfg['payment_types'][$type_id]}' is not accepted by this restaurant";
				throw new Exception("Payment \"{$cfg['payment_types'][$type_id]}\" is not accepted by this restaurant");
			}

		}

		return $errormsg_html;

	}

	protected function process_form() {
		global $db;

		//Save to database
		try {

			$db->transaction(dbmysql::TRANSACTION_START);

			$order_id = $this->save_order_details();
			$this->order_id = $order_id;

			$this->create_account();

			$db->transaction(dbmysql::TRANSACTION_COMMIT);

		} catch (Exception $exception) {

			$db->transaction(dbmysql::TRANSACTION_ROLLBACK);

			throw $exception;

		}

		//If payment via cash
		if ($this->postdata['payment_type'] == 1) {

			//Email order details
			self::email_order_detai