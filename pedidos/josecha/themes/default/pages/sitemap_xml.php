<?php
include $includes_path . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);


$extra_urls = array(
	$cfg['site_url'] . 'faq/',
	$cfg['site_url'] . 'about-us/',
	$cfg['site_url'] . 'contact-us/',
);

//Page Handler
$pagehandler = new page_sitemap_xml();
$pagehandler->postdata = $_POST;
$pagehandler->getdata = $_GET;
$pagehandler->extra_urls = $extra_urls;
$pagehandler->handle();

?>