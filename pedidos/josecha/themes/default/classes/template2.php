<?php

class template {

	private $title = '';
	private $headeraddin_html = '';
	private $footeraddin_html = '';
	private $body_html = '';
	private $mainnavsection = '';
	private $cssname = '';
	private $metadesc = '';
	private $metakeyword = '';
	private $googanalytics_page = '';
	private $showsearch = false;
	private $showpopular = false;
	private $showorderprocess = false;
	private $hoursaddinhtml = '';
	private $menubasketaddinhtml = '';
	private $breadcrumbs = array();
	private $usesecure = false;

	public function settitle($title) {
		$this->title = $title;
	}

	public function setheaderaddinhtml($headeraddinhtml) {
		$this->headeraddin_html = $headeraddinhtml;
	}

	public function setfooteraddinhtml($footeraddinhtml) {
		$this->footeraddin_html = $footeraddinhtml;
	}

	public function setmainnavsection($mainnavsection) {
		$this->mainnavsection = $mainnavsection;
	}

	public function setbodyhtml($body_html) {
		$this->body_html = $body_html;
	}

	public function setcssname($cssname) {
		$this->cssname = $cssname;
	}

	public function setmetadesc($metadesc) {
		$this->metadesc = $metadesc;
	}

	public function setmetakeyword($metakeyword) {
		$this->metakeyword = $metakeyword;
	}

	public function setheaderblock($headerblock) {
		$this->headerblock = $headerblock;
	}

	public function setgooganalyticspage($page) {
		$this->googanalytics_page = $page;
	}

	public function setshowsearch($showsearch) {
		$this->showsearch = $showsearch;
	}

	public function setshowpopular($showpopular) {
		$this->showpopular = $showpopular;
	}

	public function sethoursaddinhtml($hoursaddinhtml) {
		$this->hoursaddinhtml = $hoursaddinhtml;
	}

	public function setmenubasketaddinhtml($menubasketaddinhtml) {
		$this->menubasketaddinhtml = $menubasketaddinhtml;
	}

	public function setshoworderprocess($showorderprocess) {
		$this->showorderprocess = $showorderprocess;
	}

	public function setbreadcrumbs($breadcrumbs) {
		$this->breadcrumbs = $breadcrumbs;
	}

	public function setusesecure($usesecure) {
		$this->usesecure = $usesecure;
	}

	public function generatehtml() {
		global $cfg, $current_page, $db, $tbl;

		if ($this->usesecure) {
			$link_base_path = $cfg['site_https_url'];
		} else {
			$link_base_path = navfr::base_path();
		}

		$link_base_path = htmlentities($link_base_path);

		$current_path = appgeneral::current_path();

		$titleh = htmlentities($this->title);

		if ($this->cssname) {
			$css_name = $this->cssname;
		} else {

			/*
			//CSS Page Name
			$css_name = $current_path;
			$css_name = str_replace('/', '-', $css_name);
			$css_name = ($css_name == '-') ? 'home' : $css_name;
			if (preg_match("/(.*)\-$/", $css_name, $matches)) {
				$css_name = $matches[1];
			}
			*/

			$css_name = $current_page;

		}

		//Header navigation
		$links = array(
			'home' => array(
				'link' => navfr::link(array()),
				'name' => 'Inicio',
				'nofollow' => false,
			),
			'takeaway' => array(
				'link' => navfr::link(array('restaurant')),
				'name' => 'Restaurantes',
				'nofollow' => false,
			),
			'faq' => array(
				'link' => navfr::link(array('faq')),
				'name' => 'Preguntas',
				'nofollow' => false,
			),
			'contact-us' => array(
				'link' => navfr::link(array('contact-us')),
				'name' => 'Contacto',
				'nofollow' => false,
			),
			'about-us' => array(
				'link' => navfr::link(array('about-us')),
				'name' => 'Acerca de',
				'nofollow' => false,
			),
		);
		$nav_header_html = template_lib::header_navigation_html($links, $this->mainnavsection);

		//<a href="http://validator.w3.org/check/referer">CHECK</a>

		$curr_year = date('Y');

		//Metatags
		$metatags = array(
			'description' => $this->metadesc,
			'keywords' => $this->metakeyword,
		);
		$metatags_html = template_lib::metatags_html($metatags);

		//If show search takeaways in postcode
		if ($this->showsearch) {
			$search_html = template_lib::restaurant_search_html();
		
			
			
		} else {
			$search_html = '';
		}

		//If show popular takeaways
		if ($this->showpopular) {
			$popular_restaurants = template_lib::popular_restaurants_html();
		} else {
			$popular_restaurants = '';
		}

		if ($this->showorderprocess) {

			$orderprocess_html = <<<EOHTML

				<div class="order_process">

					<div class="info">

						<h2>Pide en <span class="larger">3</span> Pasos</h2>

						<!--//<img src="{$link_base_path}resources/template/sidepanel/numbers.gif" class="numbers"/>//-->

						<ol class="process_list">
							<li><span>Pon tu codigo postal</span></li>
							<li><span>Escoje Restaurante</span></li>
							<li><span>Seleciona tu pedido</span></li>
						</ol>

						<div class="clear"></div>

					</div>

				</div>

EOHTML;

		} else {
			$orderprocess_html = '';
		}

		//<link rel="shortcut icon" href="{$cfg['site_url']}/images/favi.ico" type="image/x-icon" />

		$googanalytics_page = ($this->googanalytics_page) ? '"' . $this->googanalytics_page . '"' : '';
		$analytics_code_html = template_lib::google_analytics_html($googanalytics_page);

		$site_nameh = htmlentities($cfg['site_name']);

		if ($current_page == 'home') {
			$logo_html = <<<EOHTML
<h1><a href="{$cfg['site_url']}" class="logo"><span>{$site_nameh}</span></a></h1>
EOHTML;
		} else {
			$logo_html = <<<EOHTML
<a href="{$cfg['site_url']}" class="logo"><span>{$site_nameh}</span></a>
EOHTML;
		}

		//Breadcrumbs
		$breadcrumbs_html = template_lib::breadcrumbs($this->breadcrumbs);

		//<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		//<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
		//<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
		//<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

		//<link rel="stylesheet" type="text/css" href="{$link_base_path}resources/template/css/print.css" media="print" />
		//<link rel="shortcut icon" href="{$link_base_path}resources/template/favicon.ico" type="image/vnd.microsoft.icon" />

		$page_html = <<<EOHTML
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>{$titleh}</title>

<link rel="stylesheet" type="text/css" href="{$link_base_path}{$cfg['theme_resources_path']}template/css/reset.css" />
<link rel="stylesheet" type="text/css" href="{$link_base_path}{$cfg['theme_resources_path']}template/css/general.css" />
<link rel="shortcut icon" href="{$cfg['site_url']}favi.ico" type="image/x-icon" />
<!--[if IE 6]>
<link rel="stylesheet" type="text/css" href="{$link_base_path}{$cfg['theme_resources_path']}template/css/ie6.css" />
<![endif]-->

<script src="{$link_base_path}{$cfg['theme_resources_path']}template/javascript/library.js" type="text/javascript"></script>

{$metatags_html}

{$cfg['headeraddin_html']}

{$this->headeraddin_html}

</head>
<body>

<div class="page-{$css_name}">

	<div class="wrapper">

		<div class="header">

			<div class="logotagline">

				{$logo_html}

				<div class="tagline"><span>La manera rapida y facil de pedir tu comida</span></div>

			</div>

			<div class="navigation">

				<ul>
{$nav_header_html}
				</ul>

			</div>

		</div>

		<div class="middle">

			<div class="mainpanel">

				<div class="breadcrumbs">
{$breadcrumbs_html}
				</div>

				<div class="maincontent">

{$this->body_html}

				</div>

			</div>

			<div class="sidepanel">

{$this->hoursaddinhtml}

{$this->menubasketaddinhtml}

{$search_html}

{$orderprocess_html}

{$popular_restaurants}

			</div>

			<div class="clear"></div>

		</div>

		<div class="footer"></div>

		<div class="footer_extra">Copyright &copy; 2011 {$cfg['site_name']}</div>

	</div>

</div>

{$analytics_code_html}

{$this->footeraddin_html}

</body>
</html>
EOHTML;

		return $page_html;

	}

	public function display() {

		//Generate html
		$page_html = $this->generatehtml();

		//Display page
		template_lib::display_headers();
		template_lib::display($page_html);

	}

}

?>