//Init 'Unobtrusive' JavaScript
addEvent(window, "load", function() {

	//Default type
	handletype();

	//Load sub items into html page
	loadsubmenuitem()

});

//Type radio changed
function typechanged() {

	handletype();

}

//Handle type changed
function handletype() {

	var type = radiogetselected("type");

	//If type Multi Item
	if (type == 2) {

		//Hide cost
		$("cost_block").style.display = "none";

		//Hide sub items
		$("subitems_block").style.display = "";

		//Clear cost
		$("cost").value = "";

	} else {
		//Otherwise type single item

		//Show cost
		$("cost_block").style.display = "";

		//Hide sub items
		$("subitems_block").style.display = "none";

		//Clear sub items
		subitems = {};
		subitems['default'] = {"id": "", "name": "", "cost": ""};
		loadsubmenuitem();

	}

}

//Add sub menu item
function addsubitem() {

	var indexid = Math.floor(Math.random()*1000000);

	//Create one default item to show
	subitems[indexid] = {"id": "", "name": "", "cost": ""};

	//Load sub menu items
	loadsubmenuitem();

}

//Delete sub menu item
function deletesubitem(indexid) {

	delete subitems[indexid];

	//Load sub menu items
	loadsubmenuitem();

}

//Load sub menu items
function loadsubmenuitem() {

	var subitems_html = "";

	//Go through all items in basket
	for (var indexid in subitems) {

		var btn_subitem_delete_use_html = btn_subitem_delete_html;
		btn_subitem_delete_use_html = btn_subitem_delete_use_html.replace(/=indexid=/g, indexid);

		var subitem_html = '';
		subitem_html += '	<tr>\n';
		subitem_html += '		<th class="col-subitemname"><label for="subitem_'+indexid+'_name">Name:</label></th>\n';
		subitem_html += '		<td class="col-subitemnamefield"><input type="text" name="subitem['+indexid+'][name]" id="subitem_'+indexid+'_name" value="'+htmlentities(subitems[indexid]["name"])+'" maxlength="255" onchange="savesubitemvalue(\''+indexid+'\', \'name\', this.value)" /></td>\n';
		subitem_html += '		<th class="col-subitemcost"><label for="subitem_'+indexid+'_cost">Cost (&pound;):</label></th>\n';
		subitem_html += '		<td class="col-subitemcostfield"><input type="text" name="subitem['+indexid+'][cost]" id="subitem_'+indexid+'_cost" value="'+htmlentities(subitems[indexid]["cost"])+'" maxlength="255" onchange="savesubitemvalue(\''+indexid+'\', \'cost\', this.value)" /></td>\n';
		subitem_html += '		<td class="col-subitembtn">'+btn_subitem_delete_use_html+'<input type="hidden" name="subitem['+indexid+'][id]" value="'+htmlentities(subitems[indexid]["id"])+'" /></td>\n';
		subitem_html += '	</tr>\n';

		//Add item html
		subitems_html += subitem_html;

	}

	if (subitems_html.length > 0) {

		var subitems_html_temp = '';
		subitems_html_temp += '<table cellspacing="0" class="subitems">\n';
		subitems_html_temp += subitems_html;
		subitems_html_temp += '</table>\n';

		subitems_html = subitems_html_temp;

	}

	//Update subitems display
	$("subitems_html").innerHTML = subitems_html;

}

//Save sub item value
function savesubitemvalue(indexid, fieldname, value) {

	subitems[indexid][fieldname] = value;

}

