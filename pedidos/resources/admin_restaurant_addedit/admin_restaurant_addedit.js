//Init 'Unobtrusive' JavaScript
addEvent(window, "load", function() {

	//Default type
	handle_deliverycharge();

});

//Delivery changed
function delivery_changed() {

	handle_deliverycharge();

}

//Handle type changed
function handle_deliverycharge() {

	//If delivery checked
	if ($("handling_delivery").checked == true) {

		//Show deliery charge
		$("delivery_charge_block").style.display = "";

	} else {

		//Hide delivery charge
		$("delivery_charge_block").style.display = "none";

		$("delivery_charge").value = "0.00";

	}

}
