//Init 'Unobtrusive' JavaScript
addEvent(window, "load", function() {

	//Default type
	handletype();

	//Load restaurants into html page
	//loadrestaurants()

});

//Type radio changed
function typechanged() {

	handletype();

}

//Handle type changed
function handletype() {

	var type = radiogetselected("usergroup");

	//If type user
	if (type == usergroup_user_id) {

		//Show restaurants
		$("restaurants_block").style.display = "";

	} else {
		//Otherwise hide restaurant permissions

		//Hide sub items
		$("restaurants_block").style.display = "none";

		//Clear sub items
		restaurants = {};
		restaurants['default'] = {"id": "", "restaurant_id": ""};

	}

	//Load restaurants into html page
	loadrestaurants();

}

//Add sub menu item
function addrestaurant() {

	//var indexid = Math.floor(Math.random()*1000000);

	var indexid = findfreeindexid();

	//Create one default restaurant to show
	restaurants[indexid] = {"id": "", "restaurant_id": ""};

	//Load restaurants
	loadrestaurants();

}

//Delete sub menu item
function deletesubitem(indexid) {

	delete restaurants[indexid];

	//Load restaurants
	loadrestaurants();

}

//Find free index id
function findfreeindexid() {

	var indexid = -1;
	for (var i in restaurants) {
		indexid = i;
	}

	indexid++;

	return indexid;

}

//Load sub menu items
function loadrestaurants() {

	var restaurants_html = "";

	//Go through all items in basket
	for (var indexid in restaurants) {

		//Restaurant options
		var restaurant_options_html = "";
		restaurant_options_html += '<option value="">-- Please Select --</option>\n';
		for (i in restaurant_options) {

			selected_html = (i == restaurants[indexid]["restaurant_id"]) ? ' selected="selected"' : '';

			restaurant_options_html += '<option value="' + htmlentities(i) + '"' + selected_html+'>' + htmlentities(restaurant_options[i]) + '</option>\n';

		}

		var btn_restaurant_delete_use_html = btn_restaurant_delete_html;
		btn_restaurant_delete_use_html = btn_restaurant_delete_use_html.replace(/=indexid=/g, indexid);

		var restaurant_html = '';
		restaurant_html += '	<tr>\n';
		restaurant_html += '		<th class="col-restaurantname"><label for="restaurant_'+indexid+'_id">Restaurant:</label></th>\n';
		restaurant_html += '		<td class="col-restaurantnamefield"><select name="restaurants['+indexid+'][restaurant_id]" id="restaurant_'+indexid+'_id" onchange="saverestaurantvalue(\''+indexid+'\', \'restaurant_id\', this.value)">'+restaurant_options_html+'</select></td>\n';
		restaurant_html += '		<td class="col-restaurantnamebtn">'+btn_restaurant_delete_use_html+'</td>\n';
		restaurant_html += '	</tr>\n';

		//Add item html
		restaurants_html += restaurant_html;

	}

	if (restaurants_html.length > 0) {

		var restaurants_html_temp = '';
		restaurants_html_temp += '<table cellspacing="0" class="restaurants">\n';
		restaurants_html_temp += restaurants_html;
		restaurants_html_temp += '</table>\n';

		restaurants_html = restaurants_html_temp;

	}

	//Update restaurants display
	$("restaurants_html").innerHTML = restaurants_html;

}

//Save sub item value
function saverestaurantvalue(indexid, fieldname, value) {

	restaurants[indexid][fieldname] = value;

}

