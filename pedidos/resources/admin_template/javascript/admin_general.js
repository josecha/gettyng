//Create navname from name
var createnavnamestatus = false;
function createnavname(nameeleid, navnameeleid) {

	//Do not autocreate navname if already there
	if (createnavnamestatus == false) {

		if ($(navnameeleid).value.length == 0) { 
			createnavnamestatus  = true
		} else {
			createnavnamestatus  = false;
		}

	}

	//If should dynamically create the navname
	if (createnavnamestatus == true) {

		var navname = $(nameeleid).value;

		navname = navname.replace(/\ /g,"-");
		navname = navname.toLowerCase();
		navname = navname.replace(/[^0-9a-z\_\-]/g,"");

		$(navnameeleid).value = navname;

	}

}
