var calendar1;
var calendar2;

//Init 'Unobtrusive' JavaScript
addEvent(window, 'load', function() {

	//From calendar
	calendar1 = new calendar("calendar1", "calendar1_disp");
	calendar1.setfield("added_from__yyyy", "added_from__mm", "added_from__dd");

	//To calendar
	calendar2 = new calendar("calendar2", "calendar2_disp");
	calendar2.setfield("added_from__yyyy", "added_to__mm", "added_to__dd");

});
