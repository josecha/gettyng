//Init 'Unobtrusive' JavaScript
addEvent(window, "load", function() {

	//Default printer
	handle_printer();

	//Default payment
	handle_payment();

});

//Delivery changed
function msgdispatch_changed() {

	handle_printer();
	handle_payment();

}

//Handle printer
function handle_printer() {

	//If message dispatch is printer
	if (radiogetselected("msg_dispatch") == 3) {

		//Show printer
		$("printer_block").style.display = "";

	} else {

		//Hide printer
		$("printer_block").style.display = "none";

		$("printer_id").value = "";

	}

}

//Handle payment
function handle_payment() {

	//If message dispatch is phone/printer
	if ( (radiogetselected("msg_dispatch") == 2) || (radiogetselected("msg_dispatch") == 3) ) {

		//Show printer
		$("payment_block").style.display = "";

	} else {

		//Hide printer
		$("payment_block").style.display = "none";

		$("payment_cash").checked = false;
		$("payment_online").checked= false;

	}

}
