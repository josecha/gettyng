<?php

//Set paths
$includes_path = 'includes/';
$publichtml_path = '';

include $includes_path . 'general/pageloadhandler.php';

$url_path = pathinfo($_SERVER['PHP_SELF'], PATHINFO_DIRNAME);
$self = $url_path . (($url_path == '/') ? '' : '/') . 'admin/';

//Handle page loading
$pageloadhandler = new pageloadhandler();
$pageloadhandler->getdata = $_GET;
$pageloadhandler->postdata = $_GET;
$pageloadhandler->init();

//If page type site, pre include configs (required for page handling)
if ($pageloadhandler->page_type == pageloadhandler::PAGE_TYPE_SITE) {

	//Include configs
	include $includes_path . 'config.php';
	include $cfg['userdata_path'] . 'config.php';

}

//Determine page
$pageloadhandler->handle();

//Retrieve page
$current_page = $pageloadhandler->page;

//Include page
include $pageloadhandler->page_path;


/*
//Retrieve page name
$pages_folder = '';//global set in nav_pagehandler
$current_page = nav_pagehandler($path);

//PAGE_TYPE_STANDARD = 1, PAGE_TYPE_ADMIN = 2
$page_type = ($pages_folder == 'pages_admin/') ? 2 : 1;
$url_path = pathinfo($_SERVER['PHP_SELF'], PATHINFO_DIRNAME);
$self = $url_path . (($url_path == '/') ? '' : '/') . 'admin/';

include $includes_path . $pages_folder . $current_page . '.php';
*/

?>