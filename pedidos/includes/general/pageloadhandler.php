<?php

class pageloadhandler {

	const PAGE_TYPE_SITE = 1;
	const PAGE_TYPE_ADMIN = 2;

	public $getdata;
	public $postdata;
	public $path;
	public $page_type;
	public $page;

	public function init() {

		//Path
		if (isset($_GET['path'])) {
			$this->path = $_GET['path'];
		} else {
			$this->path = '';
		}

		if (preg_match("#^admin\/#", $this->path, $matches)) {
			$this->page_type = self::PAGE_TYPE_ADMIN;
		} else {
			$this->page_type = self::PAGE_TYPE_SITE;
		}

	}

	public function handle() {
		global $includes_path, $cfg;

		if ($this->page_type == self::PAGE_TYPE_ADMIN) {

			$pages_folder = $includes_path . 'pages_admin/';

			if (isset($_GET['p'])) {
				$page = $_GET['p'];
			} else {
				$page = 'restaurant';
			}

		} else {

			$pages_folder = $cfg['theme_pages_path'];

			if (!$this->path) {
				$page = 'home';
			} else if (preg_match("#^restaurant\/$#U", $this->path, $matches)) { //Main listing
				$page = 'takeaway_listing';
			} else if (preg_match("#^restaurant\/([a-z0-9\-]*)\/$#U", $this->path, $matches)) { //City specified
				$page = 'takeaway_listing';
			} else if (preg_match("#^restaurant\/([a-z0-9\-]*)\/([a-z0-9\-]*)\/$#U", $this->path, $matches)) { //Town specified
				$page = 'takeaway_listing';
			} else if (preg_match("#^restaurant\/([a-z0-9\-]*)\/([a-z0-9\-]*)\/([a-z0-9\-]*)\/$#U", $this->path, $matches)) { //Restaurant specified
				$page = 'takeaway_menu';
			} else if (preg_match("#^restaurant\/([a-z0-9\-]*)\/([a-z0-9\-]*)\/([a-z0-9\-]*)\/order\/$#U", $this->path, $matches)) { //Place order specified
				$page = 'takeaway_order';
			} else if ($this->path == 'sitemap.xml') { //Sitemap specified
				$page = 'sitemap_xml';
			} else if (preg_match("#\/$#", $this->path)) { //Anything else specified
				$page = $this->path;
				$page = preg_replace("#\/$#", '', $page);
				$page = str_replace('/', '_', $page);
			} else {
				$page = 'error_404';
			}

		}

		$page = preg_replace("%[^a-zA-Z0-9\_\-]%", '', $page);

		if (!file_exists($pages_folder . $page . '.php')) {
			$page = 'error_404';
		}

		$this->page_path = $pages_folder . $page . '.php';
		$this->page = $page;

	}

}

/*
//Handle translation of path to page
function nav_pagehandler($path) {
	global $includes_path, $pages_folder;

	if (preg_match("#^admin\/#", $path, $matches)) {

		$pages_folder = 'pages_admin/';

		if (isset($_GET['p'])) {
			$page = $_GET['p'];
		} else {
			$page = 'restaurant';
		}

	} else {

		$pages_folder = 'pages/';

		if (!$path) {
			$page = 'home';
		} else if (preg_match("#^takeaway\/$#U", $path, $matches)) { //Main listing
			$page = 'takeaway_listing';
		} else if (preg_match("#^takeaway\/([a-z0-9\-]*)\/$#U", $path, $matches)) { //City specified
			$page = 'takeaway_listing';
		} else if (preg_match("#^takeaway\/([a-z0-9\-]*)\/([a-z0-9\-]*)\/$#U", $path, $matches)) { //Town specified
			$page = 'takeaway_listing';
		} else if (preg_match("#^takeaway\/([a-z0-9\-]*)\/([a-z0-9\-]*)\/([a-z0-9\-]*)\/$#U", $path, $matches)) { //Restaurant specified
			$page = 'takeaway_menu';
		} else if (preg_match("#^takeaway\/([a-z0-9\-]*)\/([a-z0-9\-]*)\/([a-z0-9\-]*)\/order\/$#U", $path, $matches)) { //Place order specified
			$page = 'takeaway_order';
		} else if ($path == 'sitemap.xml') { //Sitemap specified
			$page = 'sitemap_xml';
		} else if (preg_match("#\/$#", $path)) { //Anything else specified
			$page = $path;
			$page = preg_replace("#\/$#", '', $page);
			$page = str_replace('/', '_', $page);
		} else {
			$page = 'error_404';
		}

	}

	$page = preg_replace("%[^a-zA-Z0-9\_\-]%", '', $page);

	if (!file_exists($includes_path . $pages_folder . $page . '.php')) {
		$page = 'error_404';
	}

	return $page;

}
*/

?>