<?php

//Remove magic quotes
if (get_magic_quotes_gpc()) {

	function stripslashes_deep($value) {
		$value = is_array($value) ? array_map('stripslashes_deep', $value) : stripslashes($value);
		return $value;
	}

	$_POST = array_map('stripslashes_deep', $_POST);
	$_GET = array_map('stripslashes_deep', $_GET);
	$_COOKIE = array_map('stripslashes_deep', $_COOKIE);
	$_REQUEST = array_map('stripslashes_deep', $_REQUEST);

}

//Autoload classes
function __autoload($class_name) {
	global $includes_path, $cfg;

	/*
	if (preg_match("/^theme\_/", $class_name)) {
		$class_folder = $cfg['theme_classes_path'];
	} else {
		$class_folder = $includes_path . 'classes/';
	}

	$include_file = $class_folder . $class_name . '.php';
	if (!file_exists($include_file)) {
		throw new Exception("Class \"{$class_name}\" (\"{$include_file}\") not found");
	}
	*/

	//If user class exists
	if (file_exists($cfg['theme_classes_path'] . $class_name . '.php')) {
		$include_file = $cfg['theme_classes_path'] . $class_name . '.php';
	} else if (file_exists($includes_path . 'classes/' . $class_name . '.php')) {
		//If application class exists
		$include_file = $includes_path . 'classes/' . $class_name . '.php';
	} else {
		throw new Exception("Class \"{$class_name}\" not found");
	}

	require_once $include_file;

}

//If have a hostname (ie called from web)
if (isset($_SERVER['HTTP_HOST'])) {

	$site_url_host = parse_url($cfg['site_url'], PHP_URL_HOST);

	//If url should have www. at the start
	if (preg_match("/^www\.(.*)$/", $site_url_host, $site_url_parts)) {

		//If www. is not at the start
		if (!preg_match("/^www\./", $_SERVER['HTTP_HOST'])) {

			//If url matches what it should be, just without www.
			if ($_SERVER['HTTP_HOST'] == $site_url_parts[1]) {

				//Redirect to what the url should be
				header('Location: ' . $cfg['site_url']);

			}

		}

	}

}

/*
//Deal with situation where magic quotes is turned on (ie un-magic quotes variables)
if (get_magic_quotes_gpc()) {

	function stripslashes_deep($value) {
		$value = is_array($value) ? array_map('stripslashes_deep', $value) : stripslashes($value);
		return $value;
	}

	$_POST = array_map('stripslashes_deep', $_POST);
	$_GET = array_map('stripslashes_deep', $_GET);
	$_COOKIE = array_map('stripslashes_deep', $_COOKIE);
	$_REQUEST = array_map('stripslashes_deep', $_REQUEST);

}
*/

?>