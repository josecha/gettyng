<?php

/*

csv::generate($csvdata);

$csv_ids = array(0 => 'category',1 => 'no', 2 => 'name', 3 => 'description', 4 => 'sub_item_name', 5 => 'cost');
lib::prh(csv::retrieve_string(file_get_contents($_FILES['csvfile']['tmp_name']), $csv_ids, csv::RETRIEVE_NICEID_FROM_TITLE));

$csv_ids = array(0 => 'category',1 => 'no', 2 => 'name', 3 => 'description', 4 => 'sub_item_name', 5 => 'cost');
lib::prh(csv::retrieve_file($_FILES['csvfile']['tmp_name'], $csv_ids, csv::RETRIEVE_NICEID_FROM_TITLE));

*/

//CSV
class csv {

	const GENERATE_TITLE_FROM_ID = 1;
	const RETRIEVE_ID_FROM_TITLE = 2;
	const RETRIEVE_NICEID_FROM_TITLE = 4;

	//Generate csv
	static public function generate($csvdata, $csvtitles=false, $extraoptions='') {

		$optionvalues = lib::bitwiseopt_toarray($extraoptions);

		//Check csvdata is an array
		if (!is_array($csvdata)) {
			throw new Exception('CSV data not an array');
		}

		//Check csvtitles is an array or empty
		if ( ($csvtitles != false) && (!is_array($csvtitles)) ) {
			throw new Exception('CSV data titles not an array');
		}

		$fieldorder = array();

		//If no titles, generate them
		if (!$csvtitles) {

			//If there is at least one line
			foreach ($csvdata as $item) {
				foreach ($item as $field => $null) {
					$fieldorder[] = $field;
				}
				break;
			}

		} else {

			//If there is at least one line
			foreach ($csvtitles as $field => $null) {
				$fieldorder[] = $field;
			}

		}

		//If generate title from id
		if (in_array(self::GENERATE_TITLE_FROM_ID, $optionvalues)) {

			$csvtitles = array();
			foreach ($fieldorder as $field) {
				$csvtitles[$field] = $field;
			}

			$csvdata = array_merge(array($csvtitles), $csvdata);

		} else if ($csvtitles) {
			$csvdata = array_merge(array($csvtitles), $csvdata);
		}

		//Loop over items
		$csvdata_generated = '';
		foreach ($csvdata as $item) {

			//Loop over columns (titles)
			foreach ($fieldorder as $null => $field) {

				$data = $item[$field];
				$data = str_replace('"', '""', $data);

				$csvdata_generated .= '"' . $data . '",';

			}

			$csvdata_generated = rtrim($csvdata_generated, ',');

			$csvdata_generated .= "\r\n";

		}

		return $csvdata_generated;

	}

	/*
	//Convert csv string to array
	static function retrieve($string) {

		$csvdata = array();

		$temp = tmpfile();
		fwrite($temp, $string);

		fseek($temp, 0);

		$num = false;
		while (($data = fgetcsv($temp, false, ",", '"')) !== false) {

			$num = ($num == false) ? count($data) : $num;

			$csvdata_line = array();
			for ($c=0; $c < $num; $c++) {
				$csvdata_line[$c] = isset($data[$c]) ? $data[$c] : '';
			}

			$csvdata[] = $csvdata_line;

		}

		fclose($temp);

		return $csvdata;

	}
	*/

	//Convert csv handle to array
	static function retrieve_handle($handle, $csv_ids=false, $extraoptions='') {

		$optionvalues = lib::bitwiseopt_toarray($extraoptions);

		$csvdata = array();

		if ( (!$csv_ids) || (in_array(self::RETRIEVE_ID_FROM_TITLE, $optionvalues)) || (in_array(self::RETRIEVE_NICEID_FROM_TITLE, $optionvalues)) ) {
			$csv_ids = array();
		}

		if (!is_resource($handle)) {
			throw new Exception('Handle not a resource');
		}

		$num = false;
		$row = 0;
		while (($data = fgetcsv($handle, false, ',', '"')) !== false) {

			$num = ($num == false) ? count($data) : $num;

			//If retrieve id from title row
			if ( ($row == 0) && ( (in_array(self::RETRIEVE_ID_FROM_TITLE, $optionvalues)) || (in_array(self::RETRIEVE_NICEID_FROM_TITLE, $optionvalues)) ) ) {

				$csv_ids = $data;

				if (in_array(self::RETRIEVE_NICEID_FROM_TITLE, $optionvalues)) {
					foreach ($csv_ids  as $name => $value) {
						$csv_ids[$name] = strtolower($csv_ids[$name]);
						$csv_ids[$name] = str_replace(' ', '_', $csv_ids[$name]);
						$csv_ids[$name] = preg_replace("%[^a-z0-9\_]%", '', $csv_ids[$name]);
					}

				}

				$row++;
				continue;

			}

			$csvdata_line = array();
			for ($c=0; $c < $num; $c++) {

				if ( (count($csv_ids)) && (isset($csv_ids[$c])) ) {
					$id = $csv_ids[$c];
				} else {
					$id = $c;
				}

				$csvdata_line[$id] = isset($data[$c]) ? $data[$c] : '';

			}

			$csvdata[] = $csvdata_line;

			$row++;
		}

		return $csvdata;

	}

	//Convert csv string to array
	static function retrieve_string($string, $csv_ids=false, $extraoptions='') {

		$handle = tmpfile();
		fwrite($handle, $string);
		fseek($handle, 0);

		$csvdata = self::retrieve_handle($handle, $csv_ids, $extraoptions);

		fclose($handle);

		return $csvdata;

	}

	//Convert csv file to array
	static function retrieve_file($filename, $csv_ids=false, $extraoptions='') {

		if (!file_exists($filename)) {
			throw new Exception("Filename \"{$filename}\" does not exist");
		}

		$handle = fopen($filename, 'r');

		$csvdata = self::retrieve_handle($handle, $csv_ids, $extraoptions);

		fclose($handle);

		return $csvdata;

	}

}

?>