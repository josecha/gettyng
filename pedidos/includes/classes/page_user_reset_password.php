<?php

class page_user_reset_password {

	public $titletag = 'Reset Password';
	public $pagetitle = 'Reset Password';
	public $metadesc;
	public $body_html;
	public $headeraddin_html;
	public $footeraddin_html;
	public $mainnavsection;
	public $googanalyticspage;
	public $getdata;
	public $postdata;

	/*
	public function init() {
		global $db, $tbl;

	}
	*/

	//-------------------------------------------------------------------------------------

	public function handle() {
		global $auth;

		//Init data required by the page
		//$this->init();

		//Password reset errors
		$errormsg_html = $this->errormsg_html();

		if ( (isset($this->postdata['formposted'])) && (!count($errormsg_html)) ) {

			$this->send_reset_email();

			$page_html = $this->reset_success_html();

		} else {
			$page_html = $this->reset_form_html($errormsg_html);
		}

		$pagetitle_h = htmlentities($this->pagetitle);

		$body_html = <<<EOHTML

<h1>{$pagetitle_h}</h1>

{$page_html}

EOHTML;

		//Template
		$template = new template();
		$template->settitle($this->titletag);
		//$template->setmetadesc($metadesc);
		$template->setmainnavsection($this->mainnavsection);
		$template->setgooganalyticspage($this->googanalyticspage);
		$template->setheaderaddinhtml($this->headeraddin_html);
		$template->setfooteraddinhtml($this->footeraddin_html);
		$template->setbodyhtml($body_html);
		//$template->setshowsearch(true);
		//$template->setshoworderprocess(true);
		$template->display();

	}

	//-------------------------------------------------------------------------------------

	protected function errormsg_html() {
		global $db, $tbl, $cfg;

		$errormsg_html = array();

		if (isset($this->postdata['formposted'])) {

			$customer_result = $db->table_query($db->tbl($tbl['customer']), $db->col(array('id')), $db->cond(array("email = '".$db->es($this->postdata['email'])."'"), 'AND'), '', 0, 1);
			if (!($customer_record = $db->record_fetch($customer_result))) {
				$errormsg_html[] = 'Email no registrado';
			}

		}

		return $errormsg_html;

	}

	protected function reset_form_html($errormsg_html) {
		global $cfg;

		$link_base_path = htmlentities(navfr::base_path());

		$email = (isset($this->postdata['email'])) ? $this->postdata['email'] : '';

		$emailh = htmlentities($email);

		$errormsgall_html = appgeneral::errormsgs_html($errormsg_html);

		$form_self = navfr::self_h();

		$html = <<<EOHTML

<p>Escribe tu direccion de email para reseteaar tu password.</p>

<form method="post" action="{$form_self}">
	<div><input type="hidden" name="formposted" value="1" /></div>

	<div class="standardform">

{$errormsgall_html}

		<div class="group">
			<div class="fieldtitle"><label for="email">Email</label></div>
			<div class="fieldinput"><input type="text" name="email" id="email" value="{$email}" class="inputtxt" /></div>
			<div class="clear"></div>
		</div>

		<div><input type="image" src="{$link_base_path}{$cfg['theme_resources_path']}user_reset_password/submit.gif" alt="Reset Password" name="send" class="submit" /></div>

	</div>

</form>

EOHTML;

		return $html;

	}

	public function reset_success_html() {

		$html = <<<EOHTML

<p>Se ha enviado un email a tu corrreo, da click sobre el link para resetear tu password.</p>

EOHTML;

		return $html;

	}

	public function send_reset_email() {
		global $db, $tbl, $cfg;

		//Lookup user data from email
		$customer_result = $db->table_query($db->tbl($tbl['customer']), $db->col(array('verify_code', 'email', 'name')), $db->cond(array("email = '".$db->es($this->postdata['email'])."'"), 'AND'), '', 0, 1);
		if (!($customer_record = $db->record_fetch($customer_result))) {
			throw new Exception("Email \"{$this->postdata['email']}\" not found in database");
		}

		//Send verification email

		$verify_link = navfr::fqlink(navfr::link(array('user', 'reset-password', 'verify'), array('v' => $customer_record['verify_code'])));

		$email_body = <<<EOMAIL
To reset your {$cfg['site_name']} password, please click the link below and enter a new password.

{$verify_link}
EOMAIL;

		$mail = new phpmailer();
		$mail->Mailer = $cfg['email_method'];
		$mail->From = $cfg['email_system_from'];
		$mail->Sender = $cfg['email_system_from'];
		$mail->FromName = $cfg['email_system_from'];
		$mail->Subject = "Reset Your {$cfg['site_name']} Password";
		$mail->IsHTML(false);
		$mail->Body = $email_body;

		$mail->AddAddress($customer_record['email'], $customer_record['name']);

		if (!$mail->Send()) {
			throw new Exception("Unable to send email: {$mail->ErrorInfo}");
		}

	}

}

?>