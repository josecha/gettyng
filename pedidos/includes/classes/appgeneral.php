<?php

class appgeneral {

	static public function current_path() {
		global $cfg;

		$path = $_SERVER['PHP_SELF'];
		$path = ltrim($path, '/');
		$path_parts = explode('/', $path);

		$lastindex = count($path_parts) - 1;
		unset($path_parts[$lastindex]);

		$url_path = parse_url($cfg['site_url'], PHP_URL_PATH);
		$url_path = trim($url_path, '/');

		if ($url_path) {

			$path_parts_count = count(explode('/', $url_path));

			for ($i=1; $i<=$path_parts_count; $i++) {
				array_shift($path_parts);
			}

		}

		$current_path = ($path_parts) ? implode('/', $path_parts) . '/' : '/';

		return $current_path;

	}

	static public function trim_length($string, $trimlen) {
		if (strlen($string) > $trimlen) {
			 return substr($string, 0, $trimlen).'...';
		} else {
			return $string;
		}
	}

	static public function filesizesuffix($bytes) {

		if ($bytes >= 1099511627776) {
			$return = round($bytes / 1024 / 1024 / 1024 / 1024, 2);
			$suffix = 'TB';
		} elseif ($bytes >= 1073741824) {
			$return = round($bytes / 1024 / 1024 / 1024, 2);
			$suffix = 'GB';
		} elseif ($bytes >= 1048576) {
			$return = round($bytes / 1024 / 1024, 2);
			$suffix = 'MB';
		} elseif ($bytes >= 1024) {
			$return = round($bytes / 1024, 2);
			$suffix = 'KB';
		} else {
			$return = $bytes;
			$suffix = 'Bytes';
		}

		$return .= ' ' . $suffix;

		return $return;

	}

	static public function folder_size($folder_path) {

		if (!is_dir($folder_path)) {
			throw new Exception("Folder path \"{$folder_path}\" not valid");
		}

		$totalsize = 0;

		if ($dh = opendir($folder_path)) {

			while (($file = readdir($dh)) !== false) {

				if ( ($file != '.') && ($file != '..') ) {

					$file_path = $folder_path . '/' . $file;

					if (is_dir($file_path)) {
						$totalsize += appgeneral::folder_size($file_path);
					} else if (is_file($file_path)) {
						$totalsize += filesize($file_path);
					}

				}

			}

			closedir($dh);

		}

		return $totalsize;

	}

	static public function filternonascii_array($array) {
		$newarray = array();
		foreach ($array as $array_name => $array_data) {
			if (is_array($array_data)) {
				$newarray[$array_name] = self::filternonascii_array($array_data);
			} else {
				$newarray[$array_name] = self::filternonascii($array_data);
			}
		}
		return $newarray;
	}

	static public function filternonascii($string) {
		$newstring = preg_replace("%[^\040-\176\r\n\t\£]%", '', $string);
		return $newstring;
	}

	static public function errormsgs_html($errormsg_html) {

		//If errors, show them
		$errormsg_all_html = '';
		if (count($errormsg_html)) {
			foreach ($errormsg_html as $errormsg_item_html) {
				$errormsg_all_html .= <<<EOHTML
<div class="errormsg">Error: {$errormsg_item_html}</div>
EOHTML;
		}

		$errormsg_all_html = <<<EOHTML
<div class="errorcontainer">
{$errormsg_all_html}
</div>
EOHTML;

		} else {
			$errormsg_all_html = '';
		}

		return $errormsg_all_html;

	}

	static public function sec_to_dhms($seconds) {

		$dd = intval($seconds / 86400);
		$ss_remaining = ($seconds - ($dd * 86400));

		$hh = intval($ss_remaining / 3600);
		$ss_remaining = ($ss_remaining - ($hh * 3600));

		$mm = intval($ss_remaining / 60);
		$ss = ($ss_remaining - ($mm * 60));

		$dayhrminsec = array(
			'day' => $dd,
			'hr' => $hh,
			'min' => $mm,
			'sec' => $ss,
		);

		$result = '';
		foreach ($dayhrminsec as $name => $value) {
			if ($value) {
				//$plural = ($value > 1) ? '(s)' : '';
				$plural = '(s)';
				$result .= $value . ' ' . $name . $plural . ' ';
			}
		}

		$result = trim($result, ' ');

		return $result;

	}

	static public function email_error($subject, $message) {
		global $cfg;

		//$link_error = navpd::link_fq(array('p' => 'errorview'));

		$message = <<<EOHTML
{$cfg['site_name']}
{$cfg['site_url']}admin/?p=errorview

------------------------------

{$message}
EOHTML;

		$message = wordwrap($message, 70);

		$headers = 'From: ' . $cfg['email_system_from'];
		mail($cfg['email_admin'], $cfg['site_name'] . ' Error - ' . $subject, $message, $headers);

	}

/*
	//Convert user groups to case statement for query
	static public function options_array_sql($options) {

		$options_sql = '';
		foreach ($options as $id => $value) {
			$options_sql .= "WHEN usergroup = {$id} THEN '".$db->es($value)."' ";
		}

		$options_sql = "(SELECT CASE {$options_sql} END) AS usergroupname";

		return $options_sql;

	}
*/

	//Convert options to case statement for query
	static public function options_array_sql($options, $fieldname, $fieldname_resolved='') {
		global $db;

		if (!$fieldname_resolved) {
			$fieldname_resolved = $fieldname;
		}

		$options_sql = '';
		foreach ($options as $id => $value) {
			$options_sql .= "WHEN {$fieldname} = {$id} THEN '".$db->es($value)."' ";
		}

		$options_sql = "(SELECT CASE {$options_sql} END) AS {$fieldname_resolved}";

		return $options_sql;

	}

/*
	//Determine if restaurant is open
	static public function restaurant_open($restaurant_id) {
		global $db, $tbl, $cfg;

		$restaurant_id = intval($restaurant_id);

		$now = time();
		//$now = time()-(60*60*24*5);
		//echo "now: ".date('Y-m-d H:i:s', $now)."<br />";

		$open_status = false;

		//Retrieve open times
		$restaurant_open_result = $db->table_query($db->tbl($tbl['restaurant_open']), $db->col(array('dayno', 'open', 'close')), $db->cond(array("restaurant_id = {$restaurant_id}"), 'AND'));
		while ($restaurant_open = $db->record_fetch($restaurant_open_result)) {

			//$restaurant_open['open'] = '10:00:00';
			//$restaurant_open['close'] = '12:00:00';

			//Parse open time
			preg_match('/(\d{2})\:(\d{2})\:\d{2}/', $restaurant_open['open'], $open_time_parts);
			$open_time_parts_n[1] = intval($open_time_parts[1]);
			$open_time_parts_n[2] = intval($open_time_parts[2]);

			//Parse close time
			preg_match('/(\d{2})\:(\d{2})\:\d{2}/', $restaurant_open['close'], $close_time_parts);
			$close_time_parts_n[1] = intval($close_time_parts[1]);
			$close_time_parts_n[2] = intval($close_time_parts[2]);

			//Convert times into real current date/time timstanmps
			$open_ts = mktime($open_time_parts_n[1], $open_time_parts_n[2], 0, date('m', $now), date('d', $now)+$restaurant_open['dayno']-date('w', $now), date('Y', $now));
			$close_ts = mktime($close_time_parts_n[1], $close_time_parts_n[2], 0, date('m', $now), date('d', $now)+$restaurant_open['dayno']-date('w', $now), date('Y', $now));

			//If closing time is before opening time, assume closing time is actually for the next day
			if ($close_ts <= $open_ts) {
				$close_ts = mktime($close_time_parts_n[1], $close_time_parts_n[2], 0, date('m', $now), date('d', $now)+$restaurant_open['dayno']-date('w', $now)+1, date('Y', $now));
			}

			//$dayno = $restaurant_open['dayno'];
			//$day = $cfg['restaurant_open_days'][$dayno];
			//echo "{$day} " . date('Y-m-d H:i:s', $open_ts)."<br />";
			//echo "{$day} " . date('Y-m-d H:i:s', $close_ts)."<br /><br />";


			if ( ($now >= $open_ts) && ($now < $close_ts) ) {
				$open_status = true;
				break;
			}

		}

		return $open_status;

	}
*/

	//Determine if restaurant is open
	static public function restaurant_open_time($restaurant_id) {
		global $db, $tbl, $cfg;

		$restaurant_id = intval($restaurant_id);

		$now = time();
		//$now = time()-(60*60*24*5);
		//echo "now: ".date('Y-m-d H:i:s', $now)."<br />";

		$opentimes = array();

		//Retrieve open times
		$restaurant_open_result = $db->table_query($db->tbl($tbl['restaurant_open']), $db->col(array('dayno', 'open', 'close')), $db->cond(array("restaurant_id = {$restaurant_id}"), 'AND'));
		while ($restaurant_open = $db->record_fetch($restaurant_open_result)) {

			//$restaurant_open['open'] = '10:00:00';
			//$restaurant_open['close'] = '12:00:00';

			//Parse open time
			preg_match('/(\d{2})\:(\d{2})\:\d{2}/', $restaurant_open['open'], $open_time_parts);
			$open_time_parts_n[1] = intval($open_time_parts[1]);
			$open_time_parts_n[2] = intval($open_time_parts[2]);

			//Parse close time
			preg_match('/(\d{2})\:(\d{2})\:\d{2}/', $restaurant_open['close'], $close_time_parts);
			$close_time_parts_n[1] = intval($close_time_parts[1]);
			$close_time_parts_n[2] = intval($close_time_parts[2]);

			//Convert times into real current date/time timstanmps
			$open_ts = mktime($open_time_parts_n[1], $open_time_parts_n[2], 0, date('m', $now), date('d', $now)+$restaurant_open['dayno']-date('w', $now), date('Y', $now));
			$close_ts = mktime($close_time_parts_n[1], $close_time_parts_n[2], 0, date('m', $now), date('d', $now)+$restaurant_open['dayno']-date('w', $now), date('Y', $now));

			//If closing time is before opening time, assume closing time is actually for the next day
			if ($close_ts <= $open_ts) {
				$close_ts = mktime($close_time_parts_n[1], $close_time_parts_n[2], 0, date('m', $now), date('d', $now)+$restaurant_open['dayno']-date('w', $now)+1, date('Y', $now));
			}

			//$dayno = $restaurant_open['dayno'];
			//$day = $cfg['restaurant_open_days'][$dayno];
			//echo "{$day} " . date('Y-m-d H:i:s', $open_ts)."<br />";
			//echo "{$day} " . date('Y-m-d H:i:s', $close_ts)."<br /><br />";

			$dayno = $restaurant_open['dayno'];
			$opentimes[$dayno] = array(
				'open' => $open_ts,
				'close' => $close_ts,
			);

		}

		return $opentimes;

	}

	//Determine if restaurant is open
	static public function restaurant_open($restaurant_id) {
		global $db, $tbl, $cfg;

		//Retrieve restaurant open times
		$opentimes = self::restaurant_open_time($restaurant_id);

		$now = time();
		//$now = time()-(60*60*24*5);
		//echo "now: ".date('Y-m-d H:i:s', $now)."<br />";

		$open_status = false;
		foreach ($opentimes as $time) {

			if ( ($now >= $time['open']) && ($now < $time['close']) ) {
				$open_status = true;
				break;
			}

		}

		return $open_status;

	}

	//Cities list
	static function cities_list_html() {
		global $db, $tbl;

		$sql = <<<EOSQL

SELECT
	name,
	navname,
	(
		SELECT
			COUNT(*)
		FROM
			{$tbl['restaurant']}
		WHERE
				{$tbl['restaurant']}.status = 1
			AND

				{$tbl['restaurant']}.town_id IN(

					SELECT
						id
					FROM
						{$tbl['town']}
					WHERE
						city_id = {$tbl['city']}.id

				)

	) AS count
FROM
	{$tbl['city']}

HAVING
	count > 0

ORDER BY
	name ASC

EOSQL;

		//Show cities
		$cities_list_html = '';
		$city_result = $db->query($sql);
		while ($city_record = $db->record_fetch($city_result)) {

			$name = htmlentities($city_record['name']);

			$link = navfr::link_h(array('restaurant', $city_record['navname']));
			$cities_list_html .= <<<EOHTML
<li><a href="{$link}">{$name}</a> <span class="number">({$city_record['count']})</span></li>

EOHTML;
		}

		if ($cities_list_html) {

			$cities_list_html = <<<EOHTML

<div class="info">Elije donde deseas buscar:</div>

<div class="area_list">

	<ul>
{$cities_list_html}
	</ul>

</div>
EOHTML;

		}

		return $cities_list_html;

	}

	//Towns list html
	static function towns_list_html($city_id) {
		global $db, $tbl;

		$city_id = intval($city_id);

		/*
		//Retrieve all towns in city
		$town_ids = array();
		$town_result = $db->table_query($db->tbl($tbl['town']), $db->col(array('id')), $db->cond(array("city_id = {$city_id}"), 'AND'));
		while ($town_record = $db->record_fetch($town_result)) {
			$town_ids[] = $town_record['id'];
		}

		$town_ids_sql = implode(',', $town_ids);
		*/

		//Retrieve city name
		$city_result = $db->table_query($db->tbl($tbl['city']), $db->col(array('navname')), $db->cond(array("id = {$city_id}"), 'AND'));
		if (!($city_record = $db->record_fetch($city_result))) {
			throw new Exception("Town id \"{$city_id}\" does not have a city specified (should not be possible)");
		}

		$sql = <<<EOSQL

SELECT
	name,
	navname,
	(
		SELECT
			COUNT(*)
		FROM
			{$tbl['restaurant']}
		WHERE
				{$tbl['restaurant']}.status = 1
			AND
				{$tbl['restaurant']}.town_id = {$tbl['town']}.id

	) AS count
FROM
	{$tbl['town']}
WHERE 
	city_id = {$city_id}

HAVING
	count > 0

ORDER BY
	name ASC

EOSQL;

		//Show towns
		$i = 0;
		$towns_list_html = '';
		$town_result = $db->query($sql);
		while ($town_record = $db->record_fetch($town_result)) {

			$class = ( (($i+1) % 4) == 0) ? ' class="rowend"' : '';

			$name = htmlentities($town_record['name']);

			$link = navfr::link_h(array('restaurant', $city_record['navname'], $town_record['navname']));
			$towns_list_html .= <<<EOHTML
<li{$class}><a href="{$link}">{$name}</a> <span class="number">({$town_record['count']})</span></li>

EOHTML;
			$i++;

		}

		if ($towns_list_html) {

			$towns_list_html = <<<EOHTML

<div class="info">Elija la zona:</div>

<div class="area_list">

	<ul>
{$towns_list_html}
	</ul>

</div>
EOHTML;

		}

		return $towns_list_html;

	}

	//Resolve city from city navname
	static function city_from_navname($navname) {
		global $db, $tbl;

		$city_result = $db->table_query($db->tbl($tbl['city']), $db->col(array('id', 'name', 'navname', 'metadesc')), $db->cond(array("navname = '".$db->es($navname)."'"), 'AND'), '', 0, 1);
		if ($city_record = $db->record_fetch($city_result)) {
			return $city_record;
		} else {
			return false;
		}

	}

	//Resolve town from town navname
	static function town_from_navname($city_id, $navname) {
		global $db, $tbl;

		$city_id = intval($city_id);

		$town_result = $db->table_query($db->tbl($tbl['town']), $db->col(array('id', 'name', 'navname', 'metadesc')), $db->cond(array("navname = '".$db->es($navname)."'", "city_id = {$city_id}"), 'AND'), '', 0, 1);
		if ($town_record = $db->record_fetch($town_result)) {
			return $town_record;
		} else {
			return false;
		}

	}

	//Text to HTML paragraphs
	static function text_to_paragraphs_html($text) {

		$html = htmlentities($text);

		$html = trim($html);

		//Convert to paragraphs
		$html_parts = explode("\n\n", $html);
		$total = count($html_parts);
		$html = '';
		$i = 0;
		foreach ($html_parts as $part) {
			$i++;
			$part = nl2br($part);
			$class = ($i == $total) ? ' class="last"' : '';

			$html .= <<<EOHTML
<p{$class}>{$part}</p>\n
EOHTML;
		}

		return $html;

	}

	//Retrieve restaurant menu
	static function retrieve_menu($restaurant_id) {
		global $db, $tbl;

		$restaurant_id = intval($restaurant_id);

		$menu_items = array();

		//Retrieve all menu item categoeies for this restaurant
		$menu_cat_result = $db->table_query($db->tbl($tbl['menu_cat']), $db->col(array('id', 'name')), $db->cond(array("restaurant_id = {$restaurant_id}"), 'AND'), $db->order(array(array('catorder', 'ASC'))));
		while ($menu_cat_record = $db->record_fetch($menu_cat_result)) {
			$id = $menu_cat_record['id'];
			$menu_items[$id] = array(
				'name' => $menu_cat_record['name'],
				'menu_item' => array(),
			);
		}

		//Retrieve all menu items for this restaurant

		$sub_menu_item_id_loookup = array();

		$cond = array();
		$cond[] = "sub_menu_item_id IS NULL";
		$cond[] = "menu_cat_id IN (SELECT id FROM {$tbl['menu_cat']} WHERE restaurant_id = {$restaurant_id})";

		$select_sql = $db->col(array('id', 'menu_cat_id', 'no', 'name', 'description', 'cost')) . ', ';
		$select_sql .= 'IF(no IS NULL, 1, 0) AS isnull';

		//$db->order(array(array('isnull', 'ASC'), array('no', 'ASC'), array('name', 'ASC')))
		$menu_item_result = $db->table_query($db->tbl($tbl['menu_item']), $select_sql, $db->cond($cond, 'AND'), 'isnull ASC, no + 0 ASC, no ASC, name ASC');
		while ($menu_item_record = $db->record_fetch($menu_item_result)) {

			$item_id = $menu_item_record['id'];
			$cat_id = $menu_item_record['menu_cat_id'];
			$menu_items[$cat_id]['menu_item'][$item_id] = array(
				'no' => $menu_item_record['no'],
				'name' => $menu_item_record['name'],
				'description' => $menu_item_record['description'],
				'cost' => $menu_item_record['cost'],
				'sub_menu_item' => array(),
			);

			$sub_menu_item_id_loookup[$item_id] = $cat_id;

		}

		//Retrieve all sub menu items for this restaurant
		$sql = <<<EOSQL
SELECT
	id,
	sub_menu_item_id,
	no,
	name,
	description,
	cost,
	(SELECT menu_cat_id FROM {$tbl['menu_item']} AS menu_item_inner WHERE menu_item_outer.sub_menu_item_id = menu_item_inner.id) AS menu_cat_id

FROM
	{$tbl['menu_item']} AS menu_item_outer

WHERE
	sub_menu_item_id IN (SELECT id FROM {$tbl['menu_item']} WHERE menu_cat_id IN (SELECT id FROM {$tbl['menu_cat']} WHERE restaurant_id = {$restaurant_id}))

ORDER BY
	name + 0 ASC /* Do a natural sort */

EOSQL;

		$menu_item_result = $db->query($sql);
		while ($menu_item_record = $db->record_fetch($menu_item_result)) {

			$sub_item_id = $menu_item_record['id'];

			$cat_id = $menu_item_record['menu_cat_id'];
			$item_id = $menu_item_record['sub_menu_item_id'];

			$menu_items[$cat_id]['menu_item'][$item_id]['sub_menu_item'][$sub_item_id] = array(
				'name' => $menu_item_record['name'],
				'cost' => $menu_item_record['cost'],
			);

		}

		return $menu_items;

	}

	//Retrieve menu cookie
	static function menu_cookie() {
		global $cfg;

		$menudata = array('menu_items' => array());//delivery
		if (isset($_COOKIE['menu'])) {

			parse_str($_COOKIE['menu'], $cookie);

			if ( (isset($cookie['menu_items'])) && (is_array($cookie['menu_items'])) ) {

				foreach ($cookie['menu_items'] as $item_id => $item) {

					if (isset($item['qty'])) {

						$item_id = intval($item_id);
						$item['qty'] = intval($item['qty']);

						if ($item['qty'] > 0) {
							$menudata['menu_items'][$item_id] = array(
								'qty' => $item['qty'],
							);
						}

					}

				}

			}

			if ( (isset($cookie['handling'])) && (in_array($cookie['handling'], array_keys($cfg['order_handling']))) ) {
				$menudata['handling'] = $cookie['handling'];
			}

		}

		return $menudata;

	}

	//Retrieve handling
	static function retrieve_order_handling($restaurant_id) {
		global $db, $tbl;

		$restaurant_id = intval($restaurant_id);

		$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $db->col(array('handling_delivery', 'handling_collection')), $db->cond(array("id = {$restaurant_id}"), 'AND'), '', 0, 1);
		if (!($restaurant_record = $db->record_fetch($restaurant_result))) {
			throw new Exception("Restaurant id \"{$restaurant_id}\" not found");
		}

		//Handling accepted
		$handling_accept = array();

		if ($restaurant_record['handling_delivery']) {
			$handling_accept[] = 1; //delivery
		}

		if ($restaurant_record['handling_collection']) {
			$handling_accept[] = 2; //collection
		}

		//Default handling
		if ( ($restaurant_record['handling_delivery']) && ($restaurant_record['handling_collection']) ) {
			$handling_default = 1;
		} else if ($restaurant_record['handling_collection']) {
			$handling_default = 2;
		} else {
			$handling_default = 1;
		}

		//Retrieve user preference for handling
		$menu_cookie = self::menu_cookie();

		//If have cookie value for handling
		if (isset($menu_cookie['handling'])) {

			$handling = $menu_cookie['handling'];
			if (in_array($handling, $handling_accept)) {
				$handling = $handling;
			} else {
				$handling = $handling_default;
			}

		} else {
			$handling = $handling_default;
		}

		return $handling;

	}

	//Retrieve menu item data
	static function menu_item_data($item_id) {
		global $db, $tbl;

		$item_id = intval($item_id);

		//Retrieve information for item
		$menu_item_result = $db->table_query($db->tbl($tbl['menu_item']), $db->col(array('id', 'name', 'cost', 'menu_cat_id', 'sub_menu_item_id')), $db->cond(array("id = {$item_id}"), 'AND'), '', 0, 1);
		if ($menu_item_record = $db->record_fetch($menu_item_result)) {

			if ($menu_item_record['cost'] !== null) {

				//If this is a sub item, lookup the main item details
				if ($menu_item_record['sub_menu_item_id']) {

					//Retrieve information for main item
					$main_menu_item_result = $db->table_query($db->tbl($tbl['menu_item']), $db->col(array('name', 'menu_cat_id')), $db->cond(array("id = {$menu_item_record['sub_menu_item_id']}"), 'AND'), '', 0, 1);
					if (!($main_menu_item_record = $db->record_fetch($main_menu_item_result))) {
						throw new Exception("Unable to find main item details \"{$main_menu_item_record['sub_menu_item_id']}\" for sub item \"{$menu_item_record['id']}\"");
					}

					$name = $main_menu_item_record['name'] . ' ' . $menu_item_record['name'];
					$menu_cat_id = $main_menu_item_record['menu_cat_id'];

				} else {
					$name = $menu_item_record['name'];
					$menu_cat_id = $menu_item_record['menu_cat_id'];
				}

				//Lookup category details for this item
				$menu_cat_result = $db->table_query($db->tbl($tbl['menu_cat']), $db->col(array('restaurant_id', 'name')), $db->cond(array("id = {$menu_cat_id}"), 'AND'), '', 0, 1);
				if (!($menu_cat_record = $db->record_fetch($menu_cat_result))) {
					throw new Exception("Unable to retrieve details for category id \"{$menu_cat_id}\"");
				}

				$item_data = array(
					'restaurant_id' => $menu_cat_record['restaurant_id'],
					//'menu_cat_id' => $menu_cat_id,
					//'cat_name' => $menu_cat_record['name'],
					'name' => $name,
					'cost' => $menu_item_record['cost'],
				);

			} else {
				$item_data = false;
			}

		} else {
			$item_data = false;
		}

		return $item_data;

	}

	//Category reorder
	static function category_reorder($category_id, $move_type) {
		global $db, $tbl;

		$category_id = intval($category_id);

		$menu_cat_result = $db->table_query($db->tbl($tbl['menu_cat']), $db->col(array('restaurant_id', 'catorder')), $db->cond(array("id = {$category_id}"), 'AND'), '', 0, 1);
		if (!($menu_cat_record = $db->record_fetch($menu_cat_result))) {
			throw new Exception("Restaurant for category id \"{$category_id}\" not found");
		}

		if ($move_type == 'up') {

			if ($menu_cat_record['catorder'] == 0) {
				throw new Exception("Category id \"{$category_id}\" is already at the top");
			}

			$db->record_update($tbl['menu_cat'], 'catorder = catorder + 1', $db->cond(array("restaurant_id = {$menu_cat_record['restaurant_id']}", "catorder = {$menu_cat_record['catorder']}-1"), 'AND'));
			$order = $menu_cat_record['catorder'] - 1;

		} else if ($move_type == 'down') {

			//Total number of categories for restaurant
			$total_menu_cat_result = $db->table_query($db->tbl($tbl['menu_cat']), 'COUNT(*) AS total', $db->cond(array("restaurant_id = {$menu_cat_record['restaurant_id']}"), 'AND'), '', 0, 1);
			if (!($total_menu_cat_record = $db->record_fetch($total_menu_cat_result))) {
				throw new Exception("Total categories for restaurant id \"{$menu_cat_record['restaurant_id']}\" not found");
			}

			if ($menu_cat_record['catorder'] == $total_menu_cat_record['total']-1) {
				throw new Exception("Category id \"{$category_id}\" is already at the bottom");
			}

			$db->record_update($tbl['menu_cat'], 'catorder = catorder - 1', $db->cond(array("restaurant_id = {$menu_cat_record['restaurant_id']}", "catorder = {$menu_cat_record['catorder']}+1"), 'AND'));
			$order = $menu_cat_record['catorder'] + 1;

		} else if ($move_type == 'delete') {
			$db->record_update($tbl['menu_cat'], 'catorder = catorder - 1', $db->cond(array("restaurant_id = {$menu_cat_record['restaurant_id']}", "catorder > {$menu_cat_record['catorder']}"), 'AND'));
			$order = false;
		} else {
			throw new Exception("Unknown move type \"{$move_type}\"");
		}

		if ($order !== false) {
			$db->record_update($tbl['menu_cat'], $db->rec(array('catorder' => $order)), $db->cond(array("restaurant_id = {$menu_cat_record['restaurant_id']}", "id = {$category_id}"), 'AND'));
		}

	}

	//Find next free category order number
	static function category_order_next($restaurant_id) {
		global $db, $tbl;

		//Find last highest order number
		$menu_cat_result = $db->table_query($db->tbl($tbl['menu_cat']), $db->col(array('catorder')), $db->cond(array("restaurant_id = {$restaurant_id}"), 'AND'), $db->order(array(array('catorder', 'DESC'))), 0, 1);
		if ($menu_cat_record = $db->record_fetch($menu_cat_result)) {
			$order = $menu_cat_record['catorder'] + 1;
		} else {
			$order = 0;
		}

		return $order;

	}

	//Delete restaurant and all resources
	static function restaurant_delete($restaurant_id) {
		global $includes_path, $db, $tbl, $cfg;

		$restaurant_id = intval($restaurant_id);

		$restaurant_path = $cfg['restaurant_dir_path'] . $restaurant_id . '/';

		$db->transaction(dbmysql::TRANSACTION_START);

		try {

			$db->record_delete($tbl['restaurant'], $db->cond(array("id = " . intval($restaurant_id)), 'AND'));

			//Delete files associated with this deal
			if (is_dir($restaurant_path)) {

				if ($dh = opendir($restaurant_path)) {
					while (($file = readdir($dh)) !== false) {
						if (($file != '.') && ($file != '..')) {

							$file_path = $restaurant_path . $file;

							$status = unlink($file_path);
							if (!$status) {
								throw new Exception("Unable to delete restaurant file \"{$file_path}\"");
							}

						}
					}
					closedir($dh);
				}

			}

			//Delete main deal folder
			$status = rmdir($restaurant_path);

			if (!$status) {
				throw new Exception("Unable to delete main restaurant folder \"{$restaurant_path}\"");
			}

			$db->transaction(dbmysql::TRANSACTION_COMMIT);

		} catch (Exception $e) {

			$db->transaction(dbmysql::TRANSACTION_ROLLBACK);

			throw $e;

		}

	}

	//Update restaurant lat/lon from postcode
	static public function restaurant_latlon($restaurant_id) {
		global $db, $tbl;

		$restaurant_id = intval($restaurant_id);

		$update_sql = <<<EOESQL
			UPDATE
					{$tbl['restaurant']}
			SET
				lat = (
						SELECT
							{$tbl['postcode']}.lat
						FROM
							{$tbl['postcode']}
						WHERE
								REPLACE({$tbl['restaurant']}.postcode, ' ', ' ') /*remove the replace, was only needed with full postcode database that had no spaced in postcode data */
							LIKE
								CONCAT({$tbl['postcode']}.postcode, '%')
						ORDER BY
							LENGTH({$tbl['postcode']}.postcode) DESC
						LIMIT
							0, 1
				),
				lon = (
						SELECT
							{$tbl['postcode']}.lon
						FROM
							{$tbl['postcode']}
						WHERE
								REPLACE({$tbl['restaurant']}.postcode, ' ', ' ')
							LIKE
								CONCAT({$tbl['postcode']}.postcode, '%')
						ORDER BY
							LENGTH({$tbl['postcode']}.postcode) DESC
						LIMIT
							0, 1
				)
			WHERE
				id = {$restaurant_id}
EOESQL;

		$db->query($update_sql);

	}

	//Run cron in background (to immediately process any orders)
	static public function cron_background() {
		global $includes_path;

		$command = "cd {$includes_path}crons/;/usr/local/bin/php cron_general.php &> cron_general.txt";

		//exec("/usr/bin/php ./child_script.php > /dev/null 2>&1 &");
		exec("cd {$includes_path}crons/;/usr/local/bin/php cron_general.php &> cron_general.txt &");

	}

}

?>