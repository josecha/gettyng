<?php

/*

//Authentication
$auth = new admin_auth();
$auth->handle();
$authinfo = $auth->getauthinfo();
$auth->login_required();

*/

//Authentication
class admin_auth {

	const LOGIN_TYPE_FORM = 1;
	const LOGIN_TYPE_COOKIE = 2;
	const LOGIN_TYPE_DEMO = 3;

	const LOGIN_ERROR_USERPASS = 1;

	const LOGIN_GROUP_ADMIN_SYSTEM = 1;
	const LOGIN_GROUP_ADMIN = 2;
	const LOGIN_GROUP_USER = 3;

	private $username = '';
	private $password = '';
	private $authinfo = array();
	private $autologin = false;
	private $login_attempted = false;
	private $login_success = false;
	private $login_type = 0;
	private $login_error = 0;

	static $usergroup = array(
		self::LOGIN_GROUP_ADMIN_SYSTEM => array(
			'name' => 'System Admin',
		),
		self::LOGIN_GROUP_ADMIN => array(
			'name' => 'Admin',
			'page_exclude' => array(
				'cron',
				'errorview',
				'example_addedit',
			),
		),
		self::LOGIN_GROUP_USER => array(
			'name' => 'User (Restaurant)',
			'page' => array(
				'menu_category',
				'menu_category_addedit',
				'menu_import',
				'menu_item',
				'menu_item_addedit',
				'restaurant',
				'restaurant_addedit',
			),
		),
	
	);

	public function handle() {
		global $db, $tbl, $cfg;

		//If logout, delete cookie
		if ( (isset($_GET['logout'])) && ($_GET['logout']) ) {

			$this->cookie_clear();

		} else {
			//If demo login specified
			if ( (isset($_GET['demo'])) && ($_GET['demo'] == 1) ) {

				$this->username = $cfg['demo_username'];
				$this->password = md5('demo');
				$this->autologin = true;

				$this->login_attempted = true;
				$this->login_type = self::LOGIN_TYPE_DEMO;

			} else if ( (isset($_POST['login_username'])) && (isset($_POST['login_password'])) ) {
				//If username / password posted

				$this->username = $_POST['login_username'];
				$this->password = md5($_POST['login_password']);
				$this->autologin = (isset($_POST['login_auto']) && ($_POST['login_auto'])) ? true : false;

				$this->login_attempted = true;
				$this->login_type = self::LOGIN_TYPE_FORM;

			} else if (isset($_COOKIE['admin_auth'])) {
				//If cookie is already set

				//Parse out username / password
				parse_str($_COOKIE['admin_auth'], $cookiedata);

				$this->username = $cookiedata['username'];
				$this->password = $cookiedata['password'];
				$this->autologin = ($cookiedata['autologin']) ? true : false;

				$this->login_attempted = true;
				$this->login_type = self::LOGIN_TYPE_COOKIE;

			}

		}

		//If login attempted
		if ($this->login_attempted == true) {

			//Check username / password
			$user_result = $db->table_query($db->tbl($tbl['user']), '*', $db->cond(array("email = '".$db->es($this->username)."'", "password = '".$db->es($this->password)."'"), 'AND'), '', 0, 1);
			if ($user_record = $db->record_fetch($user_result)) {

				$this->login_success = true;
				
				$this->authinfo = $user_record;

				//Update last login
				$db->record_update($tbl['user'], $db->rec(array('lastlogin' => $db->datetimenow())), $db->cond(array("id = {$user_record['id']}"), 'AND'));

				//Save cookie
				$this->cookie_save();

			} else {

				//Otherwise if login failed

				$this->login_error = self::LOGIN_ERROR_USERPASS;

				//If cookie is set
				if ($this->login_type == self::LOGIN_TYPE_COOKIE) {

					//Clear cookie
					$this->cookie_clear();
				}

			}

		}

	}

	public function setuser($username, $password) {
		$this->username = $username;
		$this->password = $password;
		$this->autologin = false;
	}

	private function cookie_clear() {
		global $cfg;

		//Delete cookie
		$time = time() - $cfg['auth_cookie_expiry'];
		$parsedurl = parse_url($cfg['site_url']);
		$secure = ($parsedurl['scheme'] == 'https') ? true : false;
		//setcookie('admin_auth', '', $time, $parsedurl['path'], $parsedurl['host'], $secure);
		setcookie('admin_auth', '', $time, $parsedurl['path'], null, $secure);

	}

	public function cookie_save() {
		global $cfg;

		$cookiedata = array(
			'username' => $this->username,
			'password' => $this->password,
			'autologin' => ($this->autologin) ? 1 : 0,
		);

		if ($this->autologin) {
			$expiry = time() + $cfg['auth_cookie_expiry'];
		} else {
			$expiry = null;
		}

		$parsedurl = parse_url($cfg['site_url']);
		$secure = ($parsedurl['scheme'] == 'https') ? true : false;
		//setcookie('admin_auth', http_build_query($cookiedata), $expiry, $parsedurl['path'], $parsedurl['host'], $secure, true);
		setcookie('admin_auth', http_build_query($cookiedata), $expiry, $parsedurl['path'], null, $secure);

	}

	public function login_required() {

		if ($this->login_success == true) {
			//Allow continute processing....
		} else {
			$this->display_loginform();
		}

	}

	public function check_permission() {
		global $current_page;

		$status = $this->check_permission_page($current_page);
		if (!$status) {
			throw new Exception("Usergroup \"{$this->authinfo['usergroup']}\" does not have permission on page \"{$current_page}\"");
		}

	}

	public function check_permission_page($page) {

		if (!isset($this->authinfo['usergroup'])) {
			throw new Exception('Usergroup not set');
		}

		$usergroup_id = $this->authinfo['usergroup'];

		//If system admin
		if ($usergroup_id == self::LOGIN_GROUP_ADMIN_SYSTEM) {
			//System Admin
			$status = true;
		} else {
			//Otherswise check page permissions

			if (!isset(self::$usergroup[$usergroup_id])) {
				throw new Exception("Unknown usergroup id \"{$usergroup_id}\"");
			}

			//If have a list of excluded pages
			if (isset(self::$usergroup[$usergroup_id]['page_exclude'])) {

				$authpages = self::$usergroup[$usergroup_id]['page_exclude'];
				if (in_array($page, $authpages)) {
					$status = false;
				} else {
					$status = true;
				}

			} else {
				//Otherwise have a list of include pages

				$authpages = self::$usergroup[$usergroup_id]['page'];
				if (!in_array($page, $authpages)) {
					$status = false;
				} else {
					$status = true;
				}

			}

		}

		return $status;

	}

	public function check_permission_restaurant($restaurant_id) {
		global $db, $tbl;

		//If admin
		if ($this->authinfo['usergroup'] == self::LOGIN_GROUP_ADMIN_SYSTEM) {
			//ok
		} else if ($this->authinfo['usergroup'] == self::LOGIN_GROUP_ADMIN) {
			//ok
		} else if ($this->authinfo['usergroup'] == self::LOGIN_GROUP_USER) {

			$restaurant_id = intval($restaurant_id);

			//Check if in list of restaurants allowed to access
			$user_restaurant_result = $db->table_query($db->tbl($tbl['user_restaurant']), '*', $db->cond(array("restaurant_id = {$restaurant_id}"), 'AND'), '', 0, 1);
			if (!($user_restaurant_record = $db->record_fetch($user_restaurant_result))) {
				throw new Exception("User id \"{$this->authinfo['id']}\", does not have permission to access restaurant id \"{$restaurant_id}\"");
			}

		} else {
			throw new Exception("Unknown usergroup id \"{$usergroup_id}\"");
		}

	}

	public function usergroup_option() {

		$usergroup_options = array();
		foreach (self::$usergroup as $usergroup_id => $usergroup) {
			$usergroup_options[$usergroup_id] = $usergroup['name'];
		}

		return $usergroup_options;

	}

	public function display_loginform() {
		global $cfg;

		$username_h = $this->username;

		//If autologin
		if ($this->autologin) {
			$autologin_checked = 'checked="checked"';
		} else {
			$autologin_checked = '';
		}

		$errormsg_html = '';
		if ($this->login_error == self::LOGIN_ERROR_USERPASS) {

			$errormsg_html = <<<EOHTML
<div class="error"><strong>Error:</strong> Username / Password invalid</div>
EOHTML;

		}

		$link_h = navpd::self_h(array('logout' => null));

		$body_html = <<<EOHTML

<div class="loginbox">

{$errormsg_html}

	<div class="loginboxcontent">

		<div class="loginboxtitle">Login</div>

		<div class="loginboxcontentinner">

			<form method="post" action="{$link_h}">

				<div>
					<label for="login_username" class="inputxttitle">Email</label>
					<input type="text" name="login_username" id="login_username" value="{$username_h}" maxlength="255" class="inputtxt" />
				</div>

				<div>
					<label for="login_password" class="inputxttitle">Password</label>
					<input type="password" name="login_password" id="login_password" value="" maxlength="255" class="inputtxt" />
				</div>

				<div class="autologin">
					<label for="login_auto">Autologin:</label> <input type="checkbox" name="login_auto" id="login_auto" {$autologin_checked} value="1" />
				</div>

				<div><input type="submit" value="Login" name="login" /></div>

			</form>

		</div>

	</div>

</div>

<script type="text/javascript">
  //<![CDATA[

	$("login_username").focus();

  //]]>
</script>

EOHTML;

		$template = new admin_template();
		$template->settitle('Login');
		//$template->setmainnavsection($cfg['admin_home']);
		//$template->setheaderaddinhtml($headeraddin_html);
		$template->setbodyhtml($body_html);
		$template->display();

		exit;

	}

	public function getauthinfo() {
		return $this->authinfo;
	}

}

?>