<?php

class page_sitemap_xml {

	public $getdata;
	public $postdata;

	public $extra_urls = array();

	//-------------------------------------------------------------------------------------

	public function handle() {
		global $cfg;

		//Init data required by the page
		//$this->init();

		$urls = array();

		$urls[] = $cfg['site_url'];
		$urls[] = $cfg['site_url'] . 'restaurant/';

		//Add in extra URLs
		$urls = array_merge($urls, $this->extra_urls);

		//City URls
		$urls = array_merge($urls, $this->city_page_urls());

		//Town URLs
		$urls = array_merge($urls, $this->town_page_urls());

		//Restaurant URLs
		$urls = array_merge($urls, $this->restaurant_page_urls());

		//Generate XML
		$body_xml = $this->generate_sitemap($urls);

		//Show XML
		$this->xmlheader();
		echo $body_xml;

	}

	//-------------------------------------------------------------------------------------

	//Show xml header
	protected function xmlheader() {
		header('Content-type: application/xml; charset="utf-8"', true);
	}

	//Generate sitemap xml
	protected function generate_sitemap($urls) {

		$xml_sitemap = '';
		foreach ($urls as $url) {

			$url_h = htmlentities($url, null, 'UTF-8');

			$xml_sitemap .= <<<EOXML
	<url>
		<loc>{$url_h}</loc>
	</url>

EOXML;

		}

		$q = '?';
		$xml_sitemap = <<<EOXML
<{$q}xml version="1.0" encoding="UTF-8"{$q}>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
{$xml_sitemap}
</urlset>
EOXML;

		return $xml_sitemap;

	}

	//Generate urls for all city pages
	protected function city_page_urls() {
		global $db, $tbl;

		//List cities

		$sql = <<<EOSQL

SELECT
	navname,
	(
		SELECT
			COUNT(*)
		FROM
			{$tbl['restaurant']}
		WHERE
				{$tbl['restaurant']}.status = 1
			AND
				{$tbl['restaurant']}.town_id IN(
					SELECT
						id
					FROM
						{$tbl['town']}
					WHERE
						city_id = {$tbl['city']}.id
				)

	) AS count
FROM
	{$tbl['city']}

HAVING
	count > 0

ORDER BY
	name ASC

EOSQL;

		$urls = array();
		$city_result = $db->query($sql);
		while ($city_record = $db->record_fetch($city_result)) {
			$url = navfr::fqlink(navfr::link(array('restaurant', $city_record['navname'])));
			$urls[] = $url;
		}

		return $urls;

	}

	//Generate URLs for all town pages
	protected function town_page_urls() {
		global $db, $tbl;

		//List towns

		$sql = <<<EOSQL

SELECT
	navname,
	(
		SELECT
			COUNT(*)
		FROM
			{$tbl['restaurant']}
		WHERE
				{$tbl['restaurant']}.status = 1
			AND
				{$tbl['restaurant']}.town_id = {$tbl['town']}.id

	) AS count,

	(
		SELECT
			{$tbl['city']}.navname
		FROM
			{$tbl['city']}
		WHERE
			{$tbl['city']}.id = {$tbl['town']}.city_id
	) AS city_navname

FROM
	{$tbl['town']}

HAVING
	count > 0

ORDER BY
	name ASC

EOSQL;

		$urls = array();
		$town_result = $db->query($sql);
		while ($town_record = $db->record_fetch($town_result)) {
			$url = navfr::fqlink(navfr::link(array('restaurant', $town_record['city_navname'], $town_record['navname'])));
			$urls[] = $url;
		}

		return $urls;

	}

	//Generate URLs for all restaurant pages
	protected function restaurant_page_urls() {
		global $db, $tbl;

		//List restaurants
		$select_sql = 'navname, ';
		$select_sql .= "(SELECT navname FROM {$tbl['town']} WHERE id = town_id) AS town_navname, ";
		$select_sql .= "(SELECT {$tbl['city']}.navname FROM {$tbl['city']} WHERE {$tbl['city']}.id = (SELECT {$tbl['town']}.city_id FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id)) AS city_navname";
		$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $select_sql, $db->cond(array("status = 1"), 'AND'), $db->order(array(array('name', 'ASC'))));
		while ($restaurant_record = $db->record_fetch($restaurant_result)) {
			$url = navfr::fqlink(navfr::link(array('restaurant',$restaurant_record['city_navname'], $restaurant_record['town_navname'],$restaurant_record['navname'])));
			$urls[] = $url;
		}

		return $urls;

	}

}

?>