<?php

/*
$nochex_payment = new nochex_payment();
$nochex_payment->order_id = 11;
$nochex_payment->generate();

echo $nochex_payment->button_html;
*/

class nochex_payment {

	public $order_id;
	public $button_html;

	public function generate() {
		global $tbl, $db, $cfg;
	
		//Retrieve order details
		$order_result = $db->table_query($db->tbl($tbl['order']), $db->col(array('id', 'restaurant_name', 'name', 'address', 'postcode', 'email', 'telephone', 'total', 'rand', 'test')), $db->cond(array("id = {$this->order_id}"), 'AND'), '', 0, 1);
		if (!($order_record = $db->record_fetch($order_result))) {
			throw new Exception("Order id \"{$this->order_id}\" not found");
		}

		$template = new template();
		$template->settitle('Pay Online');
		//$template->setheaderaddinhtml($headeraddin_html);
		$template->setmainnavsection('takeaway');
		$template->setusesecure(true);
		$template->setbodyhtml('[=PAGE-DIVIDER=]');
		$page_html = $template->generatehtml();

		list($header_html, $footer_html) = explode('[=PAGE-DIVIDER=]', $page_html, 2);

		$hiddenfields = array(
			'merchant_id' => $cfg['nochex_merchant_id'],
			'amount' => $order_record['total'],
			'description' => "{$order_record['restaurant_name']} Order With {$cfg['site_name']}",
			'order_id' => $order_record['id'],
			'optional_1' => $order_record['rand'],
			'billing_fullname' => $order_record['name'],
			'billing_address' => $order_record['address'],
			'billing_postcode' => $order_record['postcode'],
			'email_address' => $order_record['email'],
			'customer_phone_number' => $order_record['telephone'],
			'header_html' => $header_html,
			'footer_html' => $footer_html,
			//'callback_url' => "{$cfg['site_url']}admin/?p=nochex_return",
			'callback_url' => navfr::fqlink(navfr::link(array('nochex_return'))),
		);

		if ( ($cfg['devmode']) && ($order_record['test']) ) {
			$hiddenfields['test_transaction'] = 100;
		}

		$formfields_html = lib::array_formfield($hiddenfields);

		$link_base_path = htmlentities(navfr::base_path());

		$this->button_html = <<<EOHTML

<form method="post" action="https://secure.nochex.com/" name="payform" id="payform">
	<div>
{$formfields_html}
	</div>

	<div><input type="image" src="{$link_base_path}{$cfg['theme_resources_path']}takeaway_order/pay.gif" value="Pay Online" alt="Pay Online" class="submit" /></div>

</form>

EOHTML;

	}

}

?>