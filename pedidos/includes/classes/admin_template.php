<?php

class admin_template {

	private $body_html = '';
	private $title = '';
	private $headeraddin_html = '';
	private $mainnavsection = '';
	//private $subnavsection = '';
	private $page = '';
	private $metadesc = '';

	public function __construct() {
		global $current_page;

		$this->page = $current_page;
	}

	public function setbodyhtml($body_html) {
		$this->body_html = $body_html;
	}

	public function settitle($title) {
		$this->title = $title;
	}

	public function setmetadesc($metadesc) {
		$this->metadesc = $metadesc;
	}

	public function setheaderaddinhtml($headeraddinhtml) {
		$this->headeraddin_html = $headeraddinhtml;
	}

	public function setpage($page) {
		$this->page = $page;
	}

	public function setmainnavsection($mainnavsection) {
		$this->mainnavsection = $mainnavsection;
	}

	//public function setsubnavsection($subnavsection) {
	//	$this->subnavsection = $subnavsection;
	//}

	public function display() {
		global $cfg, $authinfo, $current_page, $db, $tbl, $self, $pageself, $admin_auth;

		$title_html = htmlentities($cfg['site_name']);

		if ($this->title) {
			$title_html .= ' - ' . htmlentities($this->title);
		} else {
			$title_html .= '';
		}

		$link_base_path = htmlentities(navfr::base_path());

		$self_h = htmlentities($cfg['site_url']);

		if (isset($authinfo['id'])) {

			$navigation = array(
				'restaurant' => array(
					'name' => 'Restaurants',
					'link' => navpd::link_h(array('p' => 'restaurant')),
				),
				'town' => array(
					'name' => 'Towns',
					'link' => navpd::link_h(array('p' => 'town')),
				),
				'city' => array(
					'name' => 'Cities',
					'link' => navpd::link_h(array('p' => 'city')),
				),
				'user' => array(
					'name' => 'Users',
					'link' => navpd::link_h(array('p' => 'user')),
				),
				'printer' => array(
					'name' => 'Printers',
					'link' => navpd::link_h(array('p' => 'printer')),
				),
				'order' => array(
					'name' => 'Orders',
					'link' => navpd::link_h(array('p' => 'order')),
				),
				'site' => array(
					'name' => '[Site]',
					'link' => navfr::link(array()),
					'alwaysshow' => true,
				),
				'logout' => array(
					'name' => 'Logout',
					'link' => navpd::link_h(array('logout' => 1)),
					'alwaysshow' => true,
				),
			);

			$nav_main_html = '';
			$nav_sub_html = '';

			foreach ($navigation as $navitem_id => $navitem) {

				if (!isset($navitem['alwaysshow'])) {

					if (!$admin_auth->check_permission_page($navitem_id)) {
						continue;
					}

				}

				if ( ($navitem_id == $current_page) || ($navitem_id == $this->mainnavsection) ) {
					$class = 'selected';
				} else {
					$class = 'nonselected';
				}

				$nav_main_html .= <<<EOHTML
<a href="{$navitem['link']}" class="{$class}">{$navitem['name']}</a>
EOHTML;

			}

		} else {
			$nav_main_html = '';
		}

		//Metatags

		$metatags = array(
			'description' => $this->metadesc,
			'keywords' => '',
		);

		$metatags_html = '';
		foreach ($metatags as $name => $value) {

			if ($value) {

				$value_h = htmlentities($value);

				$metatags_html .= <<<EOHTML
<meta name="{$name}" content="{$value_h}" />
EOHTML;

			}

		}

		//<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		//<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
		//<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

		//<script src="resources/javascript/library.js" language="javascript" type="text/javascript"></script>

		$page_html = <<<EOHTML
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>{$title_html}</title>

<link rel="stylesheet" type="text/css" href="{$link_base_path}resources/admin_template/css/reset.css">
<link rel="stylesheet" type="text/css" href="{$link_base_path}resources/admin_template/css/admin_general.css">

<!--[if IE 6]>
<link rel="stylesheet" type="text/css" href="{$link_base_path}resources/admin_template/css/admin_ie6.css" />
<![endif]-->

<script src="{$link_base_path}resources/admin_template/javascript/library.js" type="text/javascript"></script>
<script src="{$link_base_path}resources/admin_template/javascript/admin_general.js" type="text/javascript"></script>

{$metatags_html}

{$this->headeraddin_html}
</head>
<body>

<div class="page-{$this->page}">

	<div class="wrapper">

		<div class="header">

			<h1><a href="{$cfg['site_url']}admin/">{$cfg['site_name']} Administracion</a></h1>

		</div>

		<div class="pagenavigation">

{$nav_main_html}

		</div>

		<div class="bodycontent">

{$this->body_html}

		</div>

	</div>
</div>

</body>
</html>
EOHTML;


		header("Content-Type: text/html; charset=ISO-8859-1");

		//If browser supports compressed encoding, and not in dev mode, and is enable in config then use it
		if ( (isset($_SERVER['HTTP_ACCEPT_ENCODING'])) && (strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== false) && ($cfg['devmode'] == false) && ($cfg['output_compressions'] == true) ) {

			header("X-Compression: gzip");
			header("Content-Encoding: gzip");
			//header("Content-Length: " . filesize());
			echo gzencode($page_html);

		} else {
			echo $page_html;
		}

	}

}

?>