<?php

class page_takeaway_listing {

	protected $titletag;
	protected $pagetitle;
	protected $metadesc;
	public $body_html;
	public $headeraddin_html;
	public $footeraddin_html;
	public $mainnavsection;
	public $googanalyticspage;
	public $getdata;
	public $postdata;

	protected $current_path;
	protected $select_sql;
	protected $breadcrumbs;
	protected $restaurant_result;
	protected $area_navigation_html = '';
	protected $nonavailable_message;
	protected $type_search = false;

	public function init() {
		global $db, $tbl, $tbl;

		$this->current_path = navfr::current_path();

		$select_sql = $db->col(array('id', 'name', 'description', 'address_1', 'address_2', 'postcode', 'del_radius_mi', 'navname')) . ', ';
		$select_sql .= "(SELECT {$tbl['town']}.name FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id) AS town, ";
		$select_sql .= "(SELECT {$tbl['city']}.name FROM {$tbl['city']} WHERE {$tbl['city']}.id = (SELECT {$tbl['town']}.city_id FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id)) AS city, ";
		$select_sql .= "(SELECT navname FROM {$tbl['town']} WHERE id = town_id) AS town_navname, ";
		$select_sql .= "(SELECT {$tbl['city']}.navname FROM {$tbl['city']} WHERE {$tbl['city']}.id = (SELECT {$tbl['town']}.city_id FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id)) AS city_navname";

		$this->select_sql = $select_sql;

	}

	//-------------------------------------------------------------------------------------

	public function handle() {
		global $auth;

		//Init data required by the page
		$this->init();

		//Redirect to friendly url if search
		$this->search_redirect();

		//Decide on data to show on page

		//Search
		if ( (isset($this->current_path[1])) && ($this->current_path[1] == 'search') && (isset($this->current_path[2])) ) {

			//Process search listing display
			$this->search_disp_process();

		} else if (isset($this->current_path[2])) {
			//Town specified

			//Process town listing display
			$this->town_disp_process();

		} else if ( (isset($this->current_path[1])) && ($this->current_path[1] != 'search') ) {
			//City specified

			//Process city listing display
			$this->city_disp_process();

		} else {
			//Nothing specified

			//Process default listing display
			$this->default_disp_process();

		}

		//Generate restaurant listing html
		$restaurants_html = $this->restaurantlisting_html();

		if ($this->area_navigation_html) {

			$area_navigation_html = <<<EOHTML
<div class="infobox">
	<div class="infobox-top">
		<div class="infobox-bottom">
			<div class="content">
{$this->area_navigation_html}
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>
EOHTML;

		} else {
			$area_navigation_html = '';
		}

		//$breadcrumbs_html = appgeneral::breadcrumbs($this->breadcrumbs);

		$pagetitle_h = htmlentities($this->pagetitle);

		$body_html = <<<EOHTML

<h1>{$pagetitle_h}</h1>

{$area_navigation_html}

{$restaurants_html}

EOHTML;

		$this->handle_title();
		$this->handle_headeraddin_html();
		$this->handle_footeraddin_html();
		$this->handle_mainnavsection();
		$this->handle_googanalyticspage();

		//Template
		$template = new template();
		$template->settitle($this->titletag);
		$template->setmetadesc($this->metadesc);
		$template->setmainnavsection($this->mainnavsection);
		$template->setgooganalyticspage($this->googanalyticspage);
		$template->setheaderaddinhtml($this->headeraddin_html);
		$template->setfooteraddinhtml($this->footeraddin_html);
		$template->setbodyhtml($body_html);
		$template->setshowsearch(true);
		$template->setshowpopular(true);
		$template->setshoworderprocess(true);
		$template->setbreadcrumbs($this->breadcrumbs);
		$template->display();

	}

	protected function handle_title() {
		//$this->titletag = "";
	}

	protected function handle_headeraddin_html () {
	}

	protected function handle_footeraddin_html () {
	}

	protected function handle_mainnavsection() {
		$this->mainnavsection = 'takeaway';
	}

	protected function handle_googanalyticspage() {
	}

	//-------------------------------------------------------------------------------------

	//Search redirect to friendly url
	protected function search_redirect() {

		//If search post, redirect to search results
		if ( (isset($this->current_path[1])) && ($this->current_path[1] == 'search') && (isset($this->postdata['postcode_search'])) ) {

			$postcode = $this->postdata['postcode_search'];
			$postcode = strtolower($postcode);

			$postcode = preg_replace("%[^a-z0-9\ ]%", '', $postcode);
			$postcode = trim($postcode);
			$postcode = str_replace(' ', '-', $postcode);

			if ($postcode) {
				$link =  navfr::fqlink_h(navfr::link(array('restaurant', 'search', $postcode)));
			} else {
				$link =  navfr::fqlink_h(navfr::link(array('restaurant')));
			}

			header("Location: {$link}");

			//Should really show fully valid html page here
			echo <<<EOHTML
<a href="{$link}">Click aqui</a> para ver resultados</a>
EOHTML;

			exit;

		}

	}

	//Handle search display processing
	protected function search_disp_process() {
		global $db, $tbl;

		$postcode = $this->current_path[2];
		$postcode = str_replace('-', ' ', $postcode);
		$postcode = preg_replace("%[^a-z0-9\ ]%", '', $postcode);

		$postcode_db = $postcode;
		$postcode_disp = strtoupper($postcode);

		$cond = array('status = 1');
		$cond_sql = $db->cond($cond, 'AND');

		$user_postcode_es = $db->es($postcode_db);

		$order_by = $db->order(array(array('distance', 'ASC')));

		//Postcode
		$sql = <<<EOSQL
SELECT @orig_lat:=ta_postcode.lat, @orig_lon:=ta_postcode.lon FROM ta_postcode WHERE REPLACE('{$user_postcode_es}', ' ', ' ') LIKE CONCAT(ta_postcode.postcode, '%') ORDER BY LENGTH(postcode) DESC LIMIT 0, 1
EOSQL;

		$db->query($sql);

		//Restaurant
		$sql = <<<EOSQL
SELECT
	{$this->select_sql},
	3956 * 2 * ASIN(SQRT( POWER(SIN((@orig_lat - ta_restaurant.lat) * pi()/180 / 2), 2) + COS(@orig_lat * pi()/180) * COS(ta_restaurant.lat * pi()/180) * POWER(SIN((@orig_lon - ta_restaurant.lon) * pi()/180 / 2), 2) )) AS distance
FROM
	{$tbl['restaurant']}
WHERE
	{$cond_sql}
ORDER BY
	{$order_by}
LIMIT
	0, 30  
EOSQL;

		//http://assets.en.oreilly.com/1/event/2/Geo%20Distance%20Search%20with%20MySQL%20Presentation.ppt

		$this->restaurant_result = $db->query($sql);

		//Breadcrumbs
		$breadcrumbs = array();
		$breadcrumbs[] = array('link' => navfr::link(array('restaurant')),'name' => 'Restaurantes');
		$breadcrumbs[] = array('link' => navfr::link(array('restaurant','search', $this->current_path[2])),'name' => 'Search Postcode ' . $postcode_disp);
		$this->breadcrumbs = $breadcrumbs;

		//H1 title
		$this->pagetitle = 'Restaurantes con servico de entrega ' . $postcode_disp;

		//Page title
		$this->titletag = 'Restaurantes con codigo  ' . $postcode_disp;

		//Page description
		//$metadesc = '';

		//$area_navigation_html = '';

		$this->nonavailable_message = 'No encontramos restaurantes disponibles en tu area .';

		$this->type_search = true;

	}

	//Handle town display processing
	protected function town_disp_process() {
		global $db, $tbl;

		//Resolve city data
		$city_data = appgeneral::city_from_navname($this->current_path[1]);

		//If city not found
		if ($city_data == false) {
			template_lib::show_404();
			exit;
		}

		//Resolve town data
		$town_data = appgeneral::town_from_navname($city_data['id'], $this->current_path[2]);

		//If town not found
		if ($town_data == false) {
			template_lib::show_404();
			exit;
		}

		//Restaurant list query
		$this->restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $this->select_sql, $db->cond(array("town_id = {$town_data['id']}", "status = 1"), 'AND'), $db->order(array(array('name', 'ASC'))));

		//Breadcrumbs
		$breadcrumbs = array();
		$breadcrumbs[] = array('link' => navfr::link(array('restaurant')),'name' => 'Restaurantes');
		$breadcrumbs[] = array('link' => navfr::link(array('restaurant',$city_data['navname'])),'name' => $city_data['name']);
		$breadcrumbs[] = array('link' => navfr::link(array('restaurant', $city_data['navname'], $town_data['navname'])),'name' => $town_data['name']);
		$this->breadcrumbs = $breadcrumbs;

		//H1 title
		$this->pagetitle = "Restaurantes en {$town_data['name']}, {$city_data['name']}";

		//Page title
		$this->titletag = "Restaurantes en{$town_data['name']}, {$city_data['name']}";

		//Page description
		$this->metadesc = $town_data['metadesc'];

		//$area_navigation_html = '';

		$this->nonavailable_message = 'No encontramos restaurantes con servicio en tu cuidad';

	}

	//Handle city display processing
	protected function city_disp_process() {
		global $db, $tbl;

		//Resolve city data
		$city_data = appgeneral::city_from_navname($this->current_path[1]);

		//If city not found
		if ($city_data == false) {
			template_lib::show_404();
			exit;
		}

		$cond = array();
		$cond[] = "town_id IN (SELECT {$tbl['town']}.id FROM {$tbl['town']} WHERE {$tbl['town']}.city_id = {$city_data['id']})";
		$cond[] = "status = 1";

		//Restaurant list query
		$this->restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $this->select_sql, $db->cond($cond, 'AND'), $db->order(array(array('name', 'ASC'))));

		//Breadcrumbs
		$breadcrumbs = array();
		$breadcrumbs[] = array('link' => navfr::link(array('restaurant')), 'name' => 'Restaurantes');
		$breadcrumbs[] = array('link' => navfr::link(array('restaurant', $city_data['navname'])), 'name' => $city_data['name']);
		$this->breadcrumbs = $breadcrumbs;

		//H1 title
		$this->pagetitle = "Restaurantes en {$city_data['name']}";

		//Page title
		$this->titletag = "Restaurantes en {$city_data['name']}";

		//Page description
		$this->metadesc = $city_data['metadesc'];

		//Show towns
		$this->area_navigation_html = appgeneral::towns_list_html($city_data['id']);

		$this->nonavailable_message = 'NO encontramos ningun restaurante en tu ciudad';

	}

	//Handle default display processing
	protected function default_disp_process() {
		global $db, $tbl;

		//Restaurant list query
		$this->restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $this->select_sql, $db->cond(array("status = 1"), 'AND'), $db->order(array(array('added', 'ASC'))), 0, 100);

		//Breadcrumbs
		$breadcrumbs = array();
		$breadcrumbs[] = array('link' => navfr::link(array('restaurant')), 'name' => 'Restaurantes');
		$this->breadcrumbs = $breadcrumbs;

		//H1 title
		$this->pagetitle = 'Gettyng.com - Lista de Restaurantes';

		//Page title
		$this->titletag = 'Gettyng.com - Restaurantes';

		//Page description
		//$metadesc = '';

		//Show cities
		$this->area_navigation_html = appgeneral::cities_list_html();

		$this->nonavailable_message = 'Restaurantes no disponibles';

	}

	//Generate restaurant listing html
	protected function restaurantlisting_html() {
		global $db, $tbl, $cfg;

		$link_base_path = htmlentities(navfr::base_path());

		//Restaurant listing
		$restaurants_html = '';
		$row = 'b';
		while ($restaurant_record = $db->record_fetch($this->restaurant_result)) {

			//If search, check distance
			if ( ($this->type_search == false) || ( ($this->type_search == true) && ($restaurant_record['distance'] !== null) && ($restaurant_record['distance'] <= $restaurant_record['del_radius_mi']) ) ) {

				$restaurant_recordh = lib::htmlentities_array($restaurant_record);

				$address = $restaurant_recordh['address_1'];
				if ($restaurant_recordh['address_2']) {
					$address .= "<br />\n" . $restaurant_recordh['address_2'];
				}

				$link = navfr::link_h(array('restaurant', $restaurant_record['city_navname'], $restaurant_record['town_navname'], $restaurant_record['navname']));

				//Retrieve restaurant open status
				//[Optimisable]
				$status_open = appgeneral::restaurant_open($restaurant_record['id']);

				if ($status_open) {
					$status_html = <<<EOHTML
<span class="open">Abierto</span>
EOHTML;
				} else {
					$status_html = <<<EOHTML
<span class="close">Cerrado</span>
EOHTML;
				}

				$row = ($row == 'b') ? 'a' : 'b';

				if (file_exists("{$cfg['restaurant_dir_path']}{$restaurant_recordh['id']}/logo.png")) {
					$logo_type = $restaurant_recordh['id'];
				} else {
					$logo_type = "default";
				}

				$restaurants_html .= <<<EOHTML

<li class="restaurant restaurant-{$row}">

	<div class="info">

		<h2><a href="{$link}">{$restaurant_recordh['name']}</a></h2>

		<div class="address">
			{$address}<br />
			{$restaurant_recordh['town']}<br />
			{$restaurant_recordh['city']} {$restaurant_recordh['postcode']}<br />
		</div>

		<div class="status">Status: {$status_html}</div>

	</div>

	<a href="{$link}" class="logo"><img src="{$link_base_path}{$cfg['restaurant_dir_path']}{$logo_type}/logo.png" width="87" height="87" alt="{$restaurant_recordh['name']} Logo" /></a>

</li>

EOHTML;

			}

		}

		if ($restaurants_html) {

			$restaurants_html = <<<EOHTML
<ul class="restaurants_list">
	{$restaurants_html}
</ul>
EOHTML;

		} else {

			$restaurants_html = <<<EOHTML
<div class="norestaurants">
	{$this->nonavailable_message}
</div>
EOHTML;

		}

		return $restaurants_html;

	}

}

?>