<?php

//Maestro Printer Postback Handling Class
class maestro_printer_postback {

	public $password;
	public $software_version;
	public $config_version;
	public $imei;
	public $sim_no;
	public $ip_address;

	public $poweron_callback;
	public $printed_callback;
	public $msgbtnpress_callback;

	function __construct() {

		if (!( (isset($_GET['a'])) && (isset($_GET['v'])) && (isset($_GET['vc'])) && (isset($_GET['i'])) && (isset($_GET['t'])) && (isset($_POST['d'])) )) {
			throw new Exception('Does not look like a postback from the printer');
		}

		$this->password = $_GET['a'];
		$this->software_version = $_GET['v'];
		$this->config_version = $_GET['vc'];
		$this->imei = $_GET['i'];
		$this->sim_no = $_GET['t'];
		$this->ip_address =  $_SERVER['REMOTE_ADDR'];

	}

	//Process all messages with callback functions
	public function process() {

		if (!is_array($_POST['d'])) {
			throw new Exception('Do not have an array of message items');
		}

		foreach ($_POST['d'] as $item_id => $item) {

			if (!isset($item['type'])) {
				throw new Exception('Item type not found');
			}

			switch ($item['type']) {

				case 'poweron':

					//If there is a power on callback function specified
					if ($this->poweron_callback) {

						//If power on callback function exists
						if (!function_exists($this->poweron_callback)) {
							throw new Exception("Power on callback function \"{$this->poweron_callback}\" not valid");
						}

						call_user_func($this->poweron_callback, $this);

					}

					break;

				case 'printed':

					//If there is a message printed callback function specified
					if ($this->printed_callback) {

						//If message printed callback function exists
						if (!function_exists($this->printed_callback)) {
							throw new Exception("Message printed callback function \"{$this->printed_callback}\" not valid");
						}

						//Check there is a message id
						if (!isset($item['msgid'])) {
							throw new Exception("Item id {$item_id}: Message id not specifid");
						}

						call_user_func($this->printed_callback, $this, $item['msgid']);

					}

					break;

				case 'msgbtnpress':

					//If there is a message button press callback function specified
					if ($this->msgbtnpress_callback) {

						//If message button presss callback function exists
						if (!function_exists($this->msgbtnpress_callback)) {
							throw new Exception("Message button press callback function \"{$this->msgbtnpress_callback}\" not valid");
						}

						//Check there is a message id
						if (!isset($item['msgid'])) {
							throw new Exception("Item id {$item_id}: Message id not specifid");
						}

						//Check there are button presses
						if (!isset($item['btnpress'])) {
							throw new Exception("Item id {$item_id}: Button press not valid");
						}

						$btnpress = intval($item['btnpress']);

						call_user_func($this->msgbtnpress_callback, $this, $item['msgid'], $btnpress);

					}

					break;

				default:

					throw new Exception("Unknown message type for item id \"{$item_id}\"");

			}

		}

	}

}

?>