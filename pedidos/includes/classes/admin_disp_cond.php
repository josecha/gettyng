<?php

class admin_disp_cond {

	public $getdata = array();
	public $cond = array();

	static function datespecified($getdata, $fieldname_from, $fieldname_to) {

		$date_fields = array($fieldname_from.'__dd', $fieldname_from.'__mm', $fieldname_from.'__yyyy', $fieldname_to.'__dd', $fieldname_to.'__mm', $fieldname_to.'__yyyy');

		$date_specified = false;
		foreach ($date_fields as $name) {
			if ( (isset($getdata[$name])) && ($getdata[$name]) ) {
				$date_specified = true;
				break;
			}
		}

		return $date_specified;

	}

	static function date_cond($getdata, $dbfieldname, $fieldname_from, $fieldname_to) {
		global $db;

		$cond = array();

		$date_specified = self::datespecified($getdata, $fieldname_from, $fieldname_to);
		if ($date_specified) {

			$approved_from = $db->datetime_validate($getdata[$fieldname_from.'__yyyy'], $getdata[$fieldname_from.'__mm'], $getdata[$fieldname_from.'__dd']);
			$approved_to = $db->datetime_validate($getdata[$fieldname_to.'__yyyy'], $getdata[$fieldname_to.'__mm'], $getdata[$fieldname_to.'__dd'], 23, 59, 59);

			if ( ($approved_from) && ($approved_to) ) {

				$from_ts = strtotime($approved_from . ' UTC');
				$to_ts = strtotime($approved_to . ' UTC');

				if ($to_ts > $from_ts) {
					$cond[] = "{$dbfieldname} >= '".gmdate('Y-m-d H:i:s', $from_ts)."'";
					$cond[] = "{$dbfieldname} < '".gmdate('Y-m-d H:i:s', $to_ts)."'";
				}

			}

		}

		return $cond;

	}

	static function date_errormsgs($getdata, $fieldname_from, $fieldname_to, $name) {
		global $db;

		$errormsgs_html = array();

		$date_specified = self::datespecified($getdata, $fieldname_from, $fieldname_to);
		if ($date_specified) {

			$from = $db->datetime_validate($getdata[$fieldname_from.'__yyyy'], $getdata[$fieldname_from.'__mm'], $getdata[$fieldname_from.'__dd']);
			$to = $db->datetime_validate($getdata[$fieldname_to.'__yyyy'], $getdata[$fieldname_to.'__mm'], $getdata[$fieldname_to.'__dd'], 23, 59, 59);

			if (!( ($from) && ($to) )) {
				$errormsgs_html[] = "'{$name}' Date range not valid";
			} else {

				$from_ts = strtotime($from . ' UTC');
				$to_ts = strtotime($to . ' UTC');

				if (!($to_ts > $from_ts)) {
					$errormsgs_html[] = "'{$name} From' date is after '{$name} To' date";
				}

			}

		}

		return $errormsgs_html;

	}

	static function order($getdata) {
		global $tbl, $db, $cfg;

		$cond = array();

		//$cond[] = "status = 1";

		if ( (isset($getdata['status'])) && ($getdata['status']) ) {
			$cond[] = "status = " . intval($getdata['status']);
		}

		if ( (isset($getdata['restaurant_id'])) && ($getdata['restaurant_id']) ) {
			$cond[] = "restaurant_id = " . intval($getdata['restaurant_id']);
		}

		if ( (isset($getdata['name'])) && ($getdata['name']) ) {
			$cond[] = "name LIKE '" . $db->es($getdata['name']) . "%'";
		}

		$cond_addon = self::date_cond($getdata, 'added', 'added_from', 'added_to');

		$cond = array_merge($cond, $cond_addon);

		return $cond;

	}

	static function restaurant($getdata) {
		global $tbl, $db, $cfg, $authinfo;

		$cond = array();

		//If admin
		if ( ($authinfo['usergroup'] == admin_auth::LOGIN_GROUP_ADMIN_SYSTEM) || ($authinfo['usergroup'] == admin_auth::LOGIN_GROUP_ADMIN) ) {
			//ok
		} else if ($authinfo['usergroup'] == admin_auth::LOGIN_GROUP_USER) {
			//User

			//Must have permssion on the restaurant
			$cond[] = <<<EOSQL
{$tbl['restaurant']}.id = ANY( (SELECT {$tbl['user_restaurant']}.restaurant_id FROM {$tbl['user_restaurant']} WHERE {$tbl['user_restaurant']}.user_id = {$authinfo['id']}) )
EOSQL;

		} else {
			throw new Exception("Unknown usergroup id \"{$usergroup_id}\"");
		}

		if ( (isset($getdata['town_id'])) && ($getdata['town_id']) ) {
			$cond[] = "town_id = " . intval($getdata['town_id']);
		}

		if ( (isset($getdata['city_id'])) && ($getdata['city_id']) ) {

			$city_id = intval($getdata['city_id']);

			$cond[] = <<<EOSQL

{$tbl['restaurant']}.town_id IN(
	SELECT
		id
	FROM
		{$tbl['town']}
	WHERE
		city_id = {$city_id}
)

EOSQL;
		}

		if ( (isset($getdata['name'])) && ($getdata['name']) ) {
			$cond[] = "name LIKE '" . $db->es($getdata['name']) . "%'";
		}

		return $cond;

	}

}

?>