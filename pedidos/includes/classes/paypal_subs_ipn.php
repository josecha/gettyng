<?php

/*

$cfg['paypal_server'] = 'www.sandbox.paypal.com';
//$cfg['paypal_server'] = 'www.paypal.com';
$cfg['paypal_receiver_email'] = 'mail@example.com';

//PayPal IPN
$paypal_subs_ipn = new paypal_subs_ipn();

//Verify transaction
if ($paypal_subs_ipn->verify()) {

	//Check for correct curency
	if ($paypal_subs_ipn->check_currency()) {

		//Check receiver email correct
		if ($paypal_subs_ipn->check_receiver_email()) {

			//Retrieve subscription event type
			$subs_type = $paypal_subs_ipn->subs_type();

			//If subscription signup
			if ($subs_type == paypal_subs_ipn::SUBSTYPE_SIGNUP) {

			} else if ($subs_type == paypal_subs_ipn::SUBSTYPE_EOT) {
				//If subscription end of term

			} else {
				//Not handled
			}

		} else {
			throw new Exception('Invalid recipient email');
		}

	} else {
		throw new Exception('Invalid currency');
	}

} else {
	throw new Exception('Error verifying transaction: ' . $paypal_subs_ipn->lasterrormsg);
}

*/


//PayPal IPN handler
class paypal_subs_ipn {

	public $lasterrormsg = '';
	public $currency = 'GBP';

	public $postdata;

	private $subs_types = array();

	const SUBSTYPE_SIGNUP = 1;
	const SUBSTYPE_CANCEL = 2;
	const SUBSTYPE_FAILED = 3;
	const SUBSTYPE_PAYMENT = 4;
	const SUBSTYPE_EOT = 5;
	const SUBSTYPE_MODIFY = 6;

	public function __construct() {

		$this->postdata = $_POST;

		$this->subs_types = array(
			'subscr_signup' => self::SUBSTYPE_SIGNUP,
			'subscr_cancel' => self::SUBSTYPE_CANCEL,
			'subscr_failed' => self::SUBSTYPE_FAILED,
			'subscr_payment' => self::SUBSTYPE_PAYMENT,
			'subscr_eot' => self::SUBSTYPE_EOT,
			'subscr_modify' => self::SUBSTYPE_MODIFY,
		);

	}

	//Validate IPN Post
	public function verify() {
		global $cfg;

		if (!isset($this->postdata['txn_type'])) {
			//throw new Exception('No txn_type, this is not a PayPal POST');
			$this->lasterrormsg = 'No txn_type, this is not a PayPal POST';
			return false;
		}

		$postdata = 'cmd=_notify-validate';
		foreach ($_POST as $name => $value) {
			$postdata .= '&' . rawurlencode($name) . '=' . rawurlencode($value);
		}

		//Post data back to PayPal to verify
		$httprequest = new httprequest();
		$httprequest->seturl("http://{$cfg['paypal_server']}/cgi-bin/webscr");
		$httprequest->setuseragent('PayPal Verifier');
		$httprequest->setpostdata($postdata);
		$httprequest->send();
		if (!($httprequest->requestsuccess())) {
			$errormsg = $httprequest->geterrormsg();
			//throw new Exception('Curl reported error: ' . $errormsg);
			$this->lasterrormsg = 'Curl reported error: ' . $errormsg;
			return false;
		}

		$httpdata = $httprequest->gethttpdata();

		//$httpdata='VERIFIED';

		//Check verified
		if (strpos($httpdata, 'VERIFIED') === false) {
			$this->lasterrormsg = "PayPal did not return verified: {$httpdata}";
			return false;
		}

		return true;

	}

	//Subscription type
	public function subs_type() {
		global $cfg;

		//Retrieve payment status
		$txn_type = $this->postdata['txn_type'];

		if (!isset($this->subs_types[$txn_type])) {
			$this->lasterrormsg = "Payment status \"{$txn_type}\" not recognised";
			return false;
		}

		return $this->subs_types[$txn_type];

	}

	//Extra validate IPN
	public function check_complete() {

		if ($this->postdata['payment_status'] == 'Completed') {
			return true;
		} else {
			return false;
		}

	}

	//Check currency
	public function check_currency() {

		if ($this->postdata['mc_currency'] == $this->currency) {
			return true;
		} else {
			return false;
		}

	}

	//Check receiver email
	public function check_receiver_email() {
		global $cfg;

		if ($this->postdata['receiver_email'] == $cfg['paypal_receiver_email']) {
			return true;
		} else {
			return false;
		}

	}

}

?>