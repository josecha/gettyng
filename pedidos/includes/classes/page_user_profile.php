<?php

class page_user_profile {

	public $titletag = 'User Profile';
	//public $pagetitle;
	public $metadesc;
	public $body_html;
	public $headeraddin_html;
	public $footeraddin_html;
	public $mainnavsection;
	public $googanalyticspage;
	public $getdata;
	public $postdata;

	//-------------------------------------------------------------------------------------

	/*
	public function init() {
		global $db, $tbl;

	}
	*/

	//-------------------------------------------------------------------------------------

	public function __construct() {

		$this->headeraddin_html = <<<EOHTML
<meta name="robots" content="noindex" />
EOHTML;

	}

	//-------------------------------------------------------------------------------------

	public function handle() {
		global $auth;

		//Init data required by the page
		//$this->init();

		//Form errors
		$errormsg_html = $this->errormsg_html();

		//If valid post with no errors
		if ( (isset($this->postdata['formposted'])) && (!count($errormsg_html)) ) {

			//Save order and send email
			$this->process_form();

			//Show success
			$forms_html = $this->profile_success_html();

		} else {

			//Order form
			$orderform_formdata = $this->profileform_data();
			$orderform_html = $this->profileform_html($orderform_formdata, $errormsg_html);

			$forms_html = <<<EOHTML

{$orderform_html}

EOHTML;

		}

		//If not logged in
		if ($auth->login_success == true) {
			$h1tag = 'Update User Profile';
		} else {
			$h1tag = 'Create User Profile';
		}

		$body_html = <<<EOHTML

<h1>{$h1tag}</h1>

{$forms_html}

EOHTML;

		//Template
		$template = new template();
		$template->settitle($this->titletag);
		//$template->setmetadesc($metadesc);
		$template->setmainnavsection($this->mainnavsection);
		$template->setgooganalyticspage($this->googanalyticspage);
		$template->setheaderaddinhtml($this->headeraddin_html);
		$template->setfooteraddinhtml($this->footeraddin_html);
		$template->setbodyhtml($body_html);
		//$template->setshowsearch(true);
		//$template->setshoworderprocess(true);
		$template->display();

	}

	//-------------------------------------------------------------------------------------

	protected function profileform_html($formdata, $errormsg_html) {
		global $cfg;

		$link_base_path = htmlentities(navfr::base_path());

		$errormsgall_html = appgeneral::errormsgs_html($errormsg_html);

		$formdatah = lib::htmlentities_array($formdata);

		$form_self = navfr::self_h();

		$html = <<<EOHTML

<form method="post" action="{$form_self}">
	<div><input type="hidden" name="formposted" value="1" /></div>

	<div class="standardform">

{$errormsgall_html}

		<div class="group">
			<div class="fieldtitle"><label for="name">Name</label></div>
			<div class="fieldinput"><input type="text" name="name" id="name" value="{$formdatah['name']}" class="inputtxt" /></div>
			<div class="clear"></div>
		</div>

		<div class="group">
			<div class="fieldtitle"><label for="address">Address</label></div>
			<div class="fieldinput"><textarea name="address" id="address" rows="5" cols="20" class="inputtxtarea">{$formdatah['address']}</textarea></div>
			<div class="clear"></div>
		</div>

		<div class="group">
			<div class="fieldtitle"><label for="postcode">Postcode</label></div>
			<div class="fieldinput"><input type="text" name="postcode" id="postcode" value="{$formdatah['postcode']}" class="inputtxt inputtxt-postcode" /></div>
			<div class="clear"></div>
		</div>

		<div class="group">
			<div class="fieldtitle"><label for="telephone">Telephone</label></div>
			<div class="fieldinput"><input type="text" name="telephone" id="telephone" value="{$formdatah['telephone']}" class="inputtxt" /></div>
			<div class="clear"></div>
		</div>

		<div class="group">
			<div class="fieldtitle"><label for="email">Email</label></div>
			<div class="fieldinput"><input type="text" name="email" id="email" value="{$formdatah['email']}" class="inputtxt" /></div>
			<div class="clear"></div>
		</div>

		<div class="group">
			<div class="fieldtitle"><label for="password">Password</label></div>
			<div class="fieldinput"><input type="password" name="password" id="password" value="{$formdatah['password']}" class="inputtxt" /></div>
			<div class="clear"></div>
		</div>

		<div><input type="image" src="{$link_base_path}{$cfg['theme_resources_path']}user_profile/submit.gif" value="Place Order" alt="Place Order" name="send" class="submit" /></div>

	</div>

</form>

EOHTML;

		return $html;

	}

	protected function profileform_data() {
		global $db, $tbl, $auth;

		if (isset($this->postdata['formposted'])) {

			/*
			$fields = array('name', 'address', 'postcode', 'email', 'notes', 'telephone', 'new_password');
			$formdata = array();
			foreach ($fields as $field) {
				$formdata[$field] = (isset($this->postdata[$field])) ? $this->postdata[$field] : '';
			}
			*/

			$formdata = array(
				'name' => $this->postdata['name'],
				'address' => $this->postdata['address'],
				'postcode' => $this->postdata['postcode'],
				'email' => $this->postdata['email'],
				'telephone' => $this->postdata['telephone'],
				'password' => $this->postdata['password'],
			);

		} else {

			if ($auth->login_success == true) {
				//If logged in

				//Retrieve customer data
				$customer_result = $db->table_query($db->tbl($tbl['customer']), $db->col(array('name', 'address', 'postcode', 'email', 'telephone', 'password')), $db->cond(array("id = {$auth->authinfo['id']}"), 'AND'), '', 0, 1);
				if (!($customer_record = $db->record_fetch($customer_result))) {
					throw new Exception("Customer id \"{}\" not found");
				}

				$formdata = $customer_record;

			} else {

				$formdata = array(
					'name' => '',
					'address' => '',
					'postcode' => '',
					'email' => '',
					'telephone' => '',
					'password' => '',
				);

			}

		}

		return $formdata;

	}

	protected function errormsg_html() {
		global $db, $tbl, $cfg, $auth;

		$errormsg_html = array();

		//If form posted, check user data
		if (isset($this->postdata['formposted'])) {

			if (!$this->postdata['name']) {
				$errormsg_html[] = "'Name' is required";
			}

			if (!$this->postdata['address']) {
				$errormsg_html[] = "'Address' is required";
			}

			if (!$this->postdata['postcode']) {
				$errormsg_html[] = "'Postcode' is required";
			}

			//Telephone
			$telephone = '';
			if ($this->postdata['telephone']) {

				$telephone = $this->postdata['telephone'];
				$telephone = str_replace('-', ' ', $telephone);
				$telephone = preg_replace("%[ \t]+%", ' ', $telephone);

				//Telephone number
				if (!preg_match("%[^0-9\ ]%", $telephone)) {

					$telephone_validate = $telephone;
					$telephone_validate = preg_replace("%[^0-9]%", '', $telephone_validate);

					$valid = true;

					if (!preg_match("/^0[1-9][0-9]{8,9}$/", $telephone_validate)) {
						$errormsg_html[] = "'Telephone' does not appear to be valid (should not contain country code, start with a leading zero, and be 10-11 digits in length)";
						$valid = false;
					}

					if (preg_match("/^09/", $telephone_validate)) {
						$errormsg_html[] = "'Tel No' appears to be premium rate";
						$valid = false;
					}

					if ($valid == true) {
						$this->postdata['telephone'] = $telephone;
					}

				} else {
					$errormsg_html[] = "'Telephone' should be in the format e.g. 123 4567 8901, and should contain 0-9 only";
				}

			} else {
				$errormsg_html[] = "'Telephone' not specified.";
			}

			if (!$this->postdata['email']) {
				$errormsg_html[] = "'Email' is required";
			} else {
				if (!lib::chkemailvalid($this->postdata['email'])) {
					$errormsg_html[] = "'Email' does not appear to be valid";
				}
			}

			//If logged in
			$cond = array("email = '".$db->es($this->postdata['email'])."'", "verified = 1");
			if ($auth->login_success == true) {
				$cond[] = "id != {$auth->authinfo['id']}";
			}

			//Check for an account that already exists with this email
			$customer_result = $db->table_query($db->tbl($tbl['customer']), $db->col(array('id')), $db->cond($cond, 'AND'), '', 0, 1);
			if ($customer_record = $db->record_fetch($customer_result)) {
				$link_reset_password = navfr::link_h(array('user', 'reset-password'));
				$errormsg_html[] = "You already have an account setup with this email address, please login to use it.  <a href=\"{$link_reset_password}\">Forgotten your password?</a>";
			}

			if ( ($auth->login_success == false) || ($this->postdata['password']) ) {

				//Check password
				if ($this->postdata['password']) {
					if (strlen($this->postdata['password']) < 8) {
						$errormsg_html[] = "'Password' must be 8 or more characters in length";
					}
				} else {
					$errormsg_html[] = "'Password' must be spcified to create an account";
				}

			}

		}

		return $errormsg_html;

	}

	protected function process_form() {
		global $db, $tbl, $auth;

		//Insert order record
		$record = array(
			'email' => $this->postdata['email'],
			'name' => $this->postdata['name'],
			'address' => $this->postdata['address'],
			'postcode' => $this->postdata['postcode'],
			'telephone' => $this->postdata['telephone'],
		);

		if ($this->postdata['password']) {
			$record['password'] = md5($this->postdata['password']);
		}

		$record = appgeneral::filternonascii_array($record);

		//If not logged in, this is a new account
		if ($auth->login_success == false) {

			$verify_code = lib::randstring(5, lib::RANDSTRING_AZ_LC);

			$record['verify_code'] = $verify_code;
			$record['registered'] = $db->datetimenow();

			$db->record_insert($tbl['customer'], $db->rec($record));
			$customer_id = $db->record_insert_id();

		} else {

			//If email changed, then need to reverify
			if ($this->postdata['email'] != $auth->authinfo['email']) {
				$verify_code = lib::randstring(5, lib::RANDSTRING_AZ_LC);
				$record['verified'] = 0;
				$record['verify_code'] = $verify_code;
			} else if ($this->postdata['password']) {
				//Password changed, change verify code
				$verify_code = lib::randstring(5, lib::RANDSTRING_AZ_LC);
				$record['verify_code'] = $verify_code;
			}

			$db->record_update($tbl['customer'], $db->rec($record), $db->cond(array("id = {$auth->authinfo['id']}"), 'AND'));
			$customer_id = $auth->authinfo['id'];
		}

		//If have a new password, and not a new account (ie unverified, update cookie)
		//x - Note: Not triggered if new email as need to reverify anyway, therefore can not login
		if ( ($this->postdata['password']) || ($this->postdata['email'] != $auth->authinfo['email']) ) {

			//Send verification email
			$this->send_verify_email($customer_id);

			if ($auth->login_success == true) {

				//Login user with new details
				$auth->login_success = true;
				$auth->authinfo['email'] = $this->postdata['email'];

				if (isset($record['password'])) {
					$auth->authinfo['password'] = $record['password'];
				}

				$auth->setuser($auth->authinfo['email'], $auth->authinfo['password']);
				$auth->cookie_save();

			}

		}

	}

	public function profile_success_html() {

		$html = <<<EOHTML

<p>Your profile has been updated.</p>

EOHTML;

		return $html;

	}

	protected function send_verify_email($customer_id) {
		global $db, $tbl, $cfg;

		$customer_result = $db->table_query($db->tbl($tbl['customer']), $db->col(array('verify_code')), $db->cond(array("id = {$customer_id}"), 'AND'), '', 0, 1);
		if (!($customer_record = $db->record_fetch($customer_result))) {
			throw new Exception("Customer id \"{$customer_id}\" not found");
		}

		//Send verification email

		$verify_link = navfr::fqlink(navfr::link(array('user', 'verify-email'), array('v' => $customer_record['verify_code'])));

		$email_body = <<<EOMAIL
Thank you for signing up for an account on {$cfg['site_name']}.

We just need to verify your email address to allow you to login, to do this please click the link:

{$verify_link}
EOMAIL;

		$mail = new phpmailer();
		$mail->Mailer = $cfg['email_method'];
		$mail->From = $cfg['email_system_from'];
		$mail->Sender = $cfg['email_system_from'];
		$mail->FromName = $cfg['email_system_from'];
		$mail->Subject = "Verify your email for {$cfg['site_name']}";
		$mail->IsHTML(false);
		$mail->Body = $email_body;

		$mail->AddAddress($this->postdata['email']);

		if (!$mail->Send()) {
			throw new Exception("Unable to send email: {$mail->ErrorInfo}");
		}

	}

}

?>