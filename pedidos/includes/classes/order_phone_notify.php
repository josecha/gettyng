<?php

class order_phone_notify {

	public $showdebugging = true;

	public $status_error_code;

	//Save debug message
	private function debuginfo($debug_level, $debug_message) {

		$spaceaddin = str_repeat(' ', $debug_level);

		if ($this->showdebugging == true) {
			$spaceaddin = str_repeat(' ', $debug_level);
			echo "{$spaceaddin}{$debug_message}\n";
		}

	}

	//Attempt phone notify
	function attempt_phone_notify($order_id) {
		global $db, $tbl, $cfg;

		$this->debuginfo(3, 'Attempting phone notify');

		//Retrieve details for this order
		$select_sql = $db->col(array('id', 'name', 'email', 'telephone', 'address', 'postcode', 'restaurant_id', 'restaurant_name', 'delivery_charge', 'total', 'handling', 'added', 'notes', 'test')) . ', ';
		$select_sql .= "(SELECT telephone FROM {$tbl['restaurant']} WHERE {$tbl['restaurant']}.id = {$tbl['order']}.restaurant_id) AS restaurant_telephone";
		$order_result = $db->table_query($db->tbl($tbl['order']), $select_sql, $db->cond(array("id = {$order_id}"), 'AND'), $db->order(array(array('id', 'ASC'))));
		if (!($order_record = $db->record_fetch($order_result))) {
			throw new Exception("Order id \"{$order_id}\" not found");
		}

		//Update notify status
		$db->record_insert($tbl['notify_log'], $db->rec(array('order_id' => $order_record['id'], 'status' => 1, 'logentry' => $db->datetimenow()))); //Status: Attempt Restaurant Notify

		//Prepare call message
		$message = $this->attempt_phone_notify_message($order_record);

		//Attempt phone notification
		$status = $this->attempt_phone_notify_call($order_record, $message);

		return $status;

	}

	//Attempt phone notify call
	function attempt_phone_notify_call($order_record, $message) {
		global $db, $tbl, $cfg;

		$this->debuginfo(5, 'Attempting phone notify call');

		$ts = time() - 60 * 60 * 24;
		$check_date = gmdate('Y-m-d H:i:s', $ts);

		//Retrieve usage
		$phone_notify_use_result = $db->table_query($db->tbl($tbl['phone_notify_use']), 'COUNT(*) AS total', $db->cond(array("notifydatetime > '{$check_date}'"), 'AND'), '', 0, 1);
		if (!($phone_notify_use_record = $db->record_fetch($phone_notify_use_result))) {
			throw new Exception('Count did not return anything');
		}

		if ($phone_notify_use_record['total'] >= 30) {
			throw new Exception('Hard limit of over 30 calls reached');
		}

		//Add a usage log record
		$db->record_insert($tbl['phone_notify_use'], $db->rec(array('notifydatetime' => $db->datetimenow())));

		$telephone = $order_record['restaurant_telephone'];

		if ( ($order_record['test']) || ($cfg['devmode']) ) {
			$telephone = $cfg['telephone_test'];
		}

		//Remove leading zero
		$telephone_cc = '01144' . preg_replace('/^0/', '', $telephone);

		$request = array(
			'anr' => array(
				'PhoneNumberToDial' => $telephone_cc,
				'TextToSay' => $message,
				'LicenseKey' => $cfg['notify_license_key'],
				'CallerIDNumber' => '',
				'CallerIDName' => '',
				//'StatusChangePostUrl' => '',
				'TransferNumber' => '',
				'NextTryInSeconds' => 60,
				'MaxCallLength' => $cfg['notify_phone_length_max'],
				'TryCount' => $cfg['notify_phone_redial_try'],
				'TTSvolume' => 100,
				'TTSrate' => 5,
				'UTCScheduledDateTime' => '1970-01-01T00:00:00Z',
				'VoiceID' => 1, // 0, 1, or 4 are the best
			),
		);

		try {

			$client = new SoapClient('http://ws.cdyne.com/NotifyWS/PhoneNotify.asmx?wsdl', array('trace' => 1));
			$result = $client->NotifyPhoneAdvanced($request);
			$soap_request_status = true;

			$this->debuginfo(5, 'SOAP API ok');

		} catch (Exception $exception){
			$soap_request_status = false;

			$this->debuginfo(5, 'SOAP API error: ' . $exception->getMessage());

			//Save exception log entry
			exceptions::savelogentry($exception);

			/*
			$soap_error = $exception->getMessage()."\n\n";
			if (isset($client)) {
				$soap_error .= "Request :\n".$client->__getLastRequest() ."\n";
				$soap_error .= "Response :\n".$client->__getLastResponse();
			}
			throw new Exception($soap_error);
			*/

		}

		//If call queued
		if ( ($soap_request_status) && ($result->NotifyPhoneAdvancedResult->ResponseCode == 0) ) {

			$this->debuginfo(5, 'Success, call queued');

			//Update notify status
			$db->record_insert($tbl['notify_log'], $db->rec(array('order_id' => $order_record['id'], 'status' => $result->NotifyPhoneAdvancedResult->ResponseCode+100, 'logentry' => $db->datetimenow())));

			$status_success = true;

		} else {

			if ($soap_request_status == false) {
				$status = 3;
				$this->status_error_code = 221;
			} else {
				$status = $result->NotifyPhoneAdvancedResult->ResponseCode+100;
				$this->status_error_code = 220;
			}

			//Update notify status
			$db->record_insert($tbl['notify_log'], $db->rec(array('order_id' => $order_record['id'], 'status' => $status, 'logentry' => $db->datetimenow())));

			$this->debuginfo(5, 'Error status is: ' . $status);

			$status_success = false;

		}

		return $status_success;

	}

	//Make phone notify data safe
	static public function notifyscriptdatasafe_array($array) {

		if (!is_array($array)) {
			throw new Exception("\"{$array}\" is not an array");
		}

		$arraynew = array();
		foreach ($array as $name => $value) {
			if (is_array($value)) {
				$arraynew[$name] = self::notifyscriptdatasafe_array($value);
			} else {

				//Remove any "~"
				$value = str_replace('~', '', $value);

				//Remove any "\"
				$value = str_replace('\\', '', $value);

				//Remove any ">"
				$value = str_replace('>', '', $value);

				//Remove any "<"
				$value = str_replace('<', '', $value);

				//Save value
				$arraynew[$name] = $value;

			}
		}

		return $arraynew;

	}

	//Prepare phone notify message
	private function attempt_phone_notify_message($order_record) {
		global $db, $tbl, $cfg;

		$this->debuginfo(5, 'Attempting phone notify call prepare message');

		$order_record = self::notifyscriptdatasafe_array($order_record);

		$message = '';

		if ( ($cfg['devmode']) || ($order_record['test']) ) {
			$debug_emails = <<<EODATA
~\DebugEmail({$cfg['email_admin']})~
~\ErrorEmail({$cfg['email_admin']})~
EODATA;
		}

		if ($cfg['notify_custom_site_url']) {
			$url = $cfg['notify_custom_site_url'];
		} else {
			$url = $cfg['site_url'];
		}

		//navpd::link(array('p' => 'phone_notify_return', 'a' => $cfg['phone_notify_ret_password']));
		$datareturn_url = $url . "admin/?p=phone_notify_return&a={$cfg['phone_notify_ret_password']}&order_id={$order_record['id']}";

		$sitename = $cfg['site_name'];
		$sitename = str_ireplace('takeaway', 'take away', $sitename);

		$address = $order_record['address'];
		$address = str_replace("\r", '', $address);
		$address = str_replace(',', '', $address);
		$address = str_replace("\n", "<break strength='x-strong' />", $address);

		$postcode = $order_record['postcode'];
		$postcode = preg_replace('//', ' ',$postcode);
		$postcode = str_replace('  ', "<break strength='x-weak' />", $postcode);
		$postcode = trim($postcode);
		//$postcode = preg_replace("%[ \t]+%", ' ', $postcode);

		$telephone = $order_record['telephone'];
		$telephone = preg_replace('//', "<break strength='medium' />",$telephone);
		$telephone = str_replace("<break strength='medium' /> <break strength='medium' />", "<break strength='x-strong' />", $telephone);
		$telephone = trim($telephone);

		//http://wiki.cdyne.com/wiki/index.php?title=Phone_Notify!_TextToSay_Speech_Control_Commands

		$numbers_words = new numbers_words();
		//echo $numbers_words->toWords('456', 'en_GB');

		$delivery_charge = $numbers_words->toCurrency($order_record['delivery_charge'], 'en_GB', 'GBP');
		$total_value = $numbers_words->toCurrency($order_record['total'], 'en_GB', 'GBP');

		//Order items
		$gobackto = 'CustomerInfo';
		$order_items = '';
		$indexid = 0;
		$order_item_result = $db->table_query($db->tbl($tbl['order_item']), $db->col(array('name', 'qty', 'cost', 'menu_item_id')), $db->cond(array("order_id = {$order_record['id']}"), 'AND'), $db->order(array(array('id', 'ASC'))));
		while ($order_item_record = $db->record_fetch($order_item_result)) {
			$indexid++;

			//Lookup item number
			$itemno = '';
			$menu_item_result = $db->table_query($db->tbl($tbl['menu_item']), $db->col(array('sub_menu_item_id', 'no')), $db->cond(array("id = {$order_item_record['menu_item_id']}"), 'AND'), '', 0, 1);
			if ($menu_item_record = $db->record_fetch($menu_item_result)) {

				if ($menu_item_record['sub_menu_item_id']) {

					//Lookup sub item number
					$menu_item_result = $db->table_query($db->tbl($tbl['menu_item']), $db->col(array('no')), $db->cond(array("id = {$menu_item_record['sub_menu_item_id']}"), 'AND'), '', 0, 1);
					if ($menu_item_record = $db->record_fetch($menu_item_result)) {
						$itemno = $menu_item_record['no'];
					}

				} else {
					$itemno = $menu_item_record['no'];
				}

			}

			if ($itemno) {
				if (preg_match("%[^0-9]%", $itemno)) {
					$itemnotext = 'Item Number: ' . $itemno;
				} else {
					$itemnotext = 'Item Number: ' . $numbers_words->toWords($itemno, 'en_GB');
				}
			} else {
				$itemnotext = 'Item Number: Unknown';
			}

			$order_item_record = self::notifyscriptdatasafe_array($order_item_record);

			$quantity = $numbers_words->toWords($order_item_record['qty'], 'en_GB');
			$cost = $numbers_words->toCurrency($order_item_record['cost'], 'en_GB', 'GBP');

			$itemname = $order_item_record['name'];
			$itemname = preg_replace('/(\d)"/', '\\1 inch', $itemname);

			$addintitle = ($indexid == 1) ? "Would like to order:\n~\PlaySilence(0.5)~\n\n" : '';

			$order_items .= <<<EODATA

~\AssignDTMF(1|{$gobackto})~

~\PlaySilence(2.5)~
~\Label(OrderItem{$indexid})~
~\AssignDTMF(1|{$gobackto})~

{$addintitle}
{$itemnotext}
<break strength='strong' />
{$itemname}
<break time='2s' />
quantity {$quantity}
<break strength='strong' />
{$cost} each
<break strength='strong' />

EODATA;

			$gobackto = "OrderItem{$indexid}";

		}

		//If have notes
		if ($order_record['notes']) {

			$notes = <<<EODATA
~\PlaySilence(2.5)~
Special Instructions:
<break strength='strong' />
{$order_record['notes']}
EODATA;

		} else {
			$notes = '';
		}

		//Order handling (confirm text)
		if ($order_record['handling'] == 2) { //collection
			$handling_confirm = 'To confirm receipt of this order press 3.';
		} else {
			$handling_confirm = <<<EOTEXT
To confirm receipt of this order for delivery in 30 minutes press 3.
40 minutes press 4.
50 minutes press 5.
60 minutes press 6.

EOTEXT;
		}

		$message = <<<EOMESSAGE
{$debug_emails}
~\StatusChangePostURL({$datareturn_url}&type=postback)~
~\SetVar(maxcallseconds|{$cfg['notify_phone_length_max']})~
~\QueryExternalServer({$datareturn_url}&type=query&queueid=[queueid]&status_id=4|Result)~
~\Label(Start)~
~\ActOnDigitPress(false)~
~\ClearDTMF()~

~\AssignDTMF(1|PlaybackOrder)~
~\AssignDTMF(3|ConfirmOrder30Min)~
~\AssignDTMF(4|ConfirmOrder40Min)~
~\AssignDTMF(5|ConfirmOrder50Min)~
~\AssignDTMF(6|ConfirmOrder60Min)~
~\ActOnDigitPress(true)~

This is an online order from {$sitename}.

To listen to this order press 1.

To repeat an item press 1.

~\PlaySilence(5)~

This is an online order from {$sitename}.

To listen to this order press 1.

~\PlaySilence(10)~
~\EndCall()~

~\Label(PlaybackOrder)~
~\AssignDTMF(1|CustomerInfo)~
~\QueryExternalServer({$datareturn_url}&type=query&queueid=[queueid]&status_id=5|Result)~

~\Label(CustomerInfo)~

Name: {$order_record['name']}

~\PlaySilence(2.5)~

Address: {$address}<break strength='x-strong' />{$postcode}

~\PlaySilence(2.5)~

Telephone: {$telephone}

{$order_items}
~\AssignDTMF(1|{$gobackto})~

~\PlaySilence(2.5)~

~\Label(TotalsNotes)~

Delivery Charge
{$delivery_charge}

Total Order Value
{$total_value}

{$notes}

~\AssignDTMF(1|TotalsNotes)~

~\PlaySilence(2.5)~

{$handling_confirm}

~\PlaySilence(5)~

{$handling_confirm}

~\PlaySilence(15)~

Order confirmation not received, we will call back in a few minutes.

Goodbye.

~\EndCall()~

~\Label(ConfirmOrder30Min)~
~\SetVar(DeliveryMinutes|30)~
~\Goto(ConfirmOrder)~

~\Label(ConfirmOrder40Min)~
~\SetVar(DeliveryMinutes|40)~
~\Goto(ConfirmOrder)~

~\Label(ConfirmOrder50Min)~
~\SetVar(DeliveryMinutes|50)~
~\Goto(ConfirmOrder)~

~\Label(ConfirmOrder60Min)~
~\SetVar(DeliveryMinutes|60)~
~\Goto(ConfirmOrder)~

~\Label(ConfirmOrder)~
~\ClearDTMF()~
~\QueryExternalServer({$datareturn_url}&type=query&queueid=[queueid]&status_id=10&delivery_min=[DeliveryMinutes]|Result)~

Order Confirmed

Thank you for being a part of {$sitename}, we will notify the customer their order has been received.

Goodbye.

~\EndCall()~

~\Label(Amd)~
Call from {$sitename}.
Answer machine detected, goodbye.
~\EndCall()~
EOMESSAGE;

		$this->debuginfo(0, "Message:\n\n" . $message);

		return $message;

	}

}

?>