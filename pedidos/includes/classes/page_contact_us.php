<?php

class page_contact_us {

	public $titletag = 'Contact Us';
	public $pagetitle = 'Contact Us';
	public $metadesc;
	public $body_html;
	public $headeraddin_html;
	public $footeraddin_html;
	public $mainnavsection = 'contact-us';
	public $googanalyticspage;
	public $getdata;
	public $postdata;

	public $formintro_html;
	public $success_html;

	//-------------------------------------------------------------------------------------

	public function __construct() {
		global $cfg;

		$email_parts = explode('@', $cfg['email_contact']);

		//Set intro text printed at top of contact form
		$this->formintro_html = <<<EOHTML

<p>If you have any questions regarding your order or the online ordering process, please contact us at <a href="#" id="email_addr">[JavaScript Required]</a> or fill in your details below, and we will get back to you as soon as possible.</p>

<script type="text/javascript">
  <!--

	var partb = "{$email_parts[1]}";
	var parta = "{$email_parts[0]}";

	document.getElementById("email_addr").innerHTML = parta + "@" + partb;
	document.getElementById("email_addr").href = "mailto:" + parta + "@" + partb;

  //-->
</script>
EOHTML;

		$this->success_html = <<<EOHTML

<p>We have received your message and will get back to you shortly.</p>

EOHTML;

	}

	//-------------------------------------------------------------------------------------

	/*
	public function init() {
		global $db, $tbl;

	}
	*/

	//-------------------------------------------------------------------------------------

	public function handle() {
		global $auth;

		//Init data required by the page
		//$this->init();

		//Form errors
		$errormsg_html = $this->errormsg_html();

		//If valid post with no errors
		if ( (isset($this->postdata['formposted'])) && (!count($errormsg_html)) ) {

			//Send email from contact form
			$this->process_form();

			//Show success
			$form_html = $this->contactform_success_html();

		} else {

			//Contact form
			$contactform_data = $this->contactform_data();
			$contactform_html = $this->contactform_html($contactform_data, $errormsg_html);

			$form_html = <<<EOHTML

{$contactform_html}

EOHTML;

		}

		$pagetitle_h = htmlentities($this->pagetitle);

		$body_html = <<<EOHTML

<h1>{$pagetitle_h}</h1>

{$form_html}

EOHTML;

		//Template
		$template = new template();
		$template->settitle($this->titletag);
		$template->setmetadesc($this->metadesc);
		$template->setmainnavsection($this->mainnavsection);
		$template->setgooganalyticspage($this->googanalyticspage);
		$template->setheaderaddinhtml($this->headeraddin_html);
		$template->setfooteraddinhtml($this->footeraddin_html);
		$template->setbodyhtml($body_html);
		//$template->setshowsearch(true);
		//$template->setshoworderprocess(true);
		$template->display();

	}

	//-------------------------------------------------------------------------------------

	protected function process_form() {
		global $cfg;

		$message = <<<EOMAIL
Name: {$_POST['name']}
Email: {$_POST['email']}
Phone: {$_POST['phone']}

{$_POST['comments']}

------------------------------
Sender IP: {$_SERVER['REMOTE_ADDR']}
<http://www.ip-adress.com/whois/{$_SERVER['REMOTE_ADDR']}>
EOMAIL;

		$mail = new phpmailer();
		$mail->Mailer = $cfg['email_method'];
		$mail->From = $cfg['email_system_from'];
		$mail->Sender = $cfg['email_system_from'];
		$mail->FromName = $cfg['email_system_from'];
		$mail->Subject = $cfg['site_name'] . ' Comment';
		$mail->IsHTML(false);
		$mail->Body = $message;

		if ($_POST['email']) {
			$name = preg_replace("%[^a-zA-Z0-9\ ]%", '', $_POST['name']);
			$mail->AddReplyTo($_POST['email'], $name);
		}

		$mail->AddAddress($cfg['email_contact']);

		if (!$mail->Send()) {
			//$errormsg_html[] = 'Internal website configuration error, please email manually';
			throw new Exception("Unable to send email: {$mail->ErrorInfo}");
		}

	}

	protected function errormsg_html() {

		$errormsg_html = array();

		//If form posted, check user data
		if (isset($this->postdata['formposted'])) {

			if (!$this->postdata['name']) {
				$errormsg_html[] = "'Nombre' es necesario";
			}

			if (!$this->postdata['email']) {
				$errormsg_html[] = "'Email' is necesario";
			} else {
				if (!lib::chkemailvalid($this->postdata['email'])) {
					$errormsg_html[] = "'Email' parece que no es valido";
				}
			}

			if (!$this->postdata['comments']) {
				$errormsg_html[] = "'Commentarios' son necesario";
			}

		}

		return $errormsg_html;

	}

	public function contactform_success_html() {

		/*
		$html = <<<EOHTML
<p>Thank you, we will get back to you shortly.</p>
EOHTML;
		*/

		$html = $this->success_html;

		return $html;

	}

	protected function contactform_data() {
		global $db, $tbl, $auth;

		if (isset($this->postdata['formposted'])) {

			$formdata = array(
				'name' => $this->postdata['name'],
				'email' => $this->postdata['email'],
				'phone' => $this->postdata['phone'],
				'comments' => $this->postdata['comments'],
			);

		} else {

			$formdata = array(
				'name' => '',
				'email' => '',
				'phone' => '',
				'comments' => '',
			);

		}

		return $formdata;

	}

	protected function contactform_html($formdata, $errormsg_html) {
		global $cfg;

		$link_base_path = htmlentities(navfr::base_path());

		$errormsgall_html = appgeneral::errormsgs_html($errormsg_html);

		$formdata_h = lib::htmlentities_array($formdata);

		$form_self = navfr::self_h();

		$html = <<<EOHTML

{$this->formintro_html}

{$errormsgall_html}



EOHTML;

		return $html;

	}

}
//contacto viejo boton
//<input type="image" src="{$link_base_path}{$cfg['theme_resources_path']}contact-us/send_details.gif" alt="Send Details" name="send" class="submit" />
?>