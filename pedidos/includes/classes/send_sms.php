<?php

/*

//Clickatell
$cfg['clickatell_api_id'] = '';
$cfg['clickatell_username'] = '';
$cfg['clickatell_password'] = '';
$cfg['sms_from'] = $cfg['site_name'];

$send_sms = new send_sms();
$status = $send_sms->send($send_sms->telno_uk_cc($sms_tel), $sms_msg);
if (!$status) {
	throw new Expception('Error sending SMS: ' . $send_sms->errormsg);
}

*/

class send_sms {

	public $errormsg;

	function send($sms_tel, $sms_msg, $flash=false) {
		global $cfg;

		$sms_tel = str_replace(' ', '', $sms_tel);
		$sms_tel = str_replace('-', '', $sms_tel);

		if ( (strlen($sms_msg) > 160) || (strlen($sms_msg) == 0) ) {
			throw new Exception('Message text must be 1-160 characters');
		}

		$urlparam = array(
			'api_id' => $cfg['clickatell_api_id'],
			'user' => $cfg['clickatell_username'],
			'password' => $cfg['clickatell_password'],
			'to' => $sms_tel,
			'text' => $sms_msg,
			'from' => $cfg['sms_from'],
		);

		if ($flash) {
			$urlparam['msg_type'] = 'SMS_FLASH';
		}

		$url = "http://api.clickatell.com/http/sendmsg?";
		foreach ($urlparam as $name => $value) {
			$url .= urlencode($name) . '=' . urlencode($value) . '&';
		}

		$url = trim($url, '&');

		$httprequest = new httprequest();
		$httprequest->seturl($url);
		$httprequest->setuseragent("{$cfg['site_name']} SMS Sender");
		$httprequest->settimeout(10);
		$httprequest->settimeoutconn(10);
		$httprequest->send();
		if ($httprequest->requestsuccess()) {

			$httpdata = $httprequest->gethttpdata();

			if (preg_match("/^ID\:\ /", $httpdata)) {
				$status = true;
			} else {
				//throw new Exception("Error sending SMS: {$httpdata}");
				$this->errormsg = "Error sending SMS: {$httpdata}";
				$status = false;
			}

		} else {
			$status = false;
			$errormsg = $httprequest->geterrormsg();
			$this->errormsg = 'Curl reported error: ' . $errormsg;
			//throw new Exception('Curl reported error sending SMS: ' . $errormsg);
		}

		return $status;

	}

	public function telno_uk_cc($sms_tel) {

		$sms_tel = preg_replace('/^0/', '44', $sms_tel, 1, $count);

		if (!$count) {
			throw new Exception("Telephone number \"{$sms_tel}\" does not have a leading '0' unable to convert to UK country code format");
		}

		return $sms_tel;

	}

}

?>