<?php

class page_home {

	public $titletag;
	public $pagetitle;
	public $metadesc;
	public $body_html;
	public $headeraddin_html;
	public $footeraddin_html;
	public $mainnavsection = 'home';
	public $googanalyticspage;
	public $getdata;
	public $postdata;

	public $rotate_photos = array();
	public $aboutusbox_html;
	public $content_footer_addin_html;

	//-------------------------------------------------------------------------------------

	public function __construct() {
		global $cfg;

		$this->titletag = $cfg['site_name'];

		$link_base_path = htmlentities(navfr::base_path());
		$this->headeraddin_html = <<<EOHTML
<script src="{$link_base_path}{$cfg['theme_resources_path']}home/home.js" type="text/javascript"></script>
EOHTML;

	}

	//-------------------------------------------------------------------------------------

	/*
	public function init() {
		global $db, $tbl;

	}
	*/

	//-------------------------------------------------------------------------------------

	public function handle() {
		global $cfg;

		//Init data required by the page
		//$this->init();

		$link_base_path = htmlentities(navfr::base_path());

		$latest_restaurants = $this->latest_restaurants_html();

		$link_takeaways = navfr::link_h(array('restaurant'));

		$rotate_photo_script = $this->rotate_photo_script();

		$photo_scr_h = htmlentities($this->rotate_photos[0]['src']);
		$photo_alt_h = htmlentities($this->rotate_photos[0]['alt']);
		$photo_title_h = htmlentities($this->rotate_photos[0]['title']);

		$body_html = <<<EOHTML

<div class="photoimg">

	<img src="{$photo_scr_h}" width="585" height="163" alt="{$photo_alt_h}" title="{$photo_title_h}" id="photoimg_a" />
	<img src="{$photo_scr_h}" width="585" height="163" alt="{$photo_alt_h}" title="{$photo_title_h}" id="photoimg_b" />

</div>

<div class="about_us">
	<div class="info">
{$this->aboutusbox_html}
	</div>
</div>

<div class="lastest_takeaways">
	<div class="info">
		<h2>Ahora ya pide en:</h2>
			<ul>
				{$latest_restaurants}
			</ul>
		<a href="{$link_takeaways}" class="readmore"><img src="{$link_base_path}{$cfg['theme_resources_path']}template/link_bullet.gif" width="6" height="7" alt="Arrow Right" />Ver todos  los restaurantes</a>
	</div>
</div>
</br>
<div class="imagen">
	</br>
	<h2>Lo que nuestros usuarios opinan:</h2>
	<br>
		
		

 <div class="buble">
			<blockquote class="example-obtuse">
				<p class="cufonw">Excelente servicio, cualquier duda te la resuelven con el chat en vivo!!</p>
			</blockquote>
					<p><img src="/images/testimonial.jpg" alt=" ">
			<span class="cufon">@ArechigaPe</span></p>
		</div>		
		
<div class="buble">
			<blockquote class="example-obtuse">
				<p class="cufonw">De maravilla , facil,comodo y sobre todo practica</p>
			</blockquote>
			<p><img src="/images/testimonial3.jpg" alt=" ">
			<span class="cufon">@Josue</span></p>
		</div>		
		
	

		
</div>
{$this->content_footer_addin_html}

{$rotate_photo_script}

EOHTML;

		//Template
		$template = new template();
		$template->settitle($this->titletag);
		$template->setmetadesc($this->metadesc);
		$template->setmainnavsection($this->mainnavsection);
		$template->setgooganalyticspage($this->googanalyticspage);
		$template->setheaderaddinhtml($this->headeraddin_html);
		$template->setfooteraddinhtml($this->footeraddin_html);
		$template->setshowsearch(true);
		$template->setshoworderprocess(true);
		$template->setbodyhtml($body_html);
		//$template->setshowsearch(true);
		//$template->setshoworderprocess(true);
		$template->display();

	}

	//-------------------------------------------------------------------------------------

	protected function latest_restaurants_html($no_restaurants=4) {
		global $db, $tbl;

		//Retrieve latest added restaurants
		$latest_restaurants = '';
		$select_sql = $db->col(array('id', 'name', 'postcode', 'navname')) . ', ';
		//$select_sql .= "(SELECT {$tbl['town']}.name FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id) AS town, ";
		$select_sql .= "(SELECT {$tbl['city']}.name FROM {$tbl['city']} WHERE {$tbl['city']}.id = (SELECT {$tbl['town']}.city_id FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id)) AS city, ";
		$select_sql .= "(SELECT navname FROM {$tbl['town']} WHERE id = town_id) AS town_navname, ";
		$select_sql .= "(SELECT {$tbl['city']}.navname FROM {$tbl['city']} WHERE {$tbl['city']}.id = (SELECT {$tbl['town']}.city_id FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id)) AS city_navname";
		$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $select_sql, $db->cond(array("status = 1"), 'AND'), $db->order(array(array('added', 'DESC'))), 0, $no_restaurants);
		while ($restaurant_record = $db->record_fetch($restaurant_result)) {

			$restaurant_record_h = lib::htmlentities_array($restaurant_record);

			$postcode = explode(' ', $restaurant_record['postcode']);

			$info = <<<EOHTML
{$restaurant_record['city']} {$postcode[0]}
EOHTML;

			$name_h = htmlentities(appgeneral::trim_length($restaurant_record['name'], 20));
			$info_h = htmlentities(appgeneral::trim_length($info, 20));

			$link = navfr::link_h(array('restaurant', $restaurant_record['city_navname'], $restaurant_record['town_navname'], $restaurant_record['navname']));

			$latest_restaurants .= <<<EOHTML
			<li><a href="{$link}">{$name_h}</a> {$info_h}</li>

EOHTML;

		}

		return $latest_restaurants;

	}

	protected function rotate_photo_script() {

		$rotate_photos_js = '';
		foreach ($this->rotate_photos as $rotate_photo) {

			$src_s = addslashes(htmlentities($rotate_photo['src']));
			$alt_s = addslashes(htmlentities($rotate_photo['alt']));
			$title_s = addslashes(htmlentities($rotate_photo['title']));

			$rotate_photos_js .= <<<EODATA
		{
			"src": "{$src_s}",
			"alt": "{$alt_s}",
			"title": "{$title_s}"
		},

EODATA;
		}

		$rotate_photos_js = rtrim($rotate_photos_js, "\r\n,");

		$html = <<<EOHTML
<script type="text/javascript">
  <!--

	/* Photo Image */

	var photoimg_timeoutinterval = 5;

	var photoimgs = [
{$rotate_photos_js}
	];

  //-->
</script>
EOHTML;

		return $html;

	}

}

?>