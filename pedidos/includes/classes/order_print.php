<?php

class order_print {

	public $showdebugging = true;

	public $msgtype;
	public $errormsg;

	//Save debug message
	private function debuginfo($debug_level, $debug_message) {

		$spaceaddin = str_repeat(' ', $debug_level);

		if ($this->showdebugging == true) {
			$spaceaddin = str_repeat(' ', $debug_level);
			echo "{$spaceaddin}{$debug_message}\n";
		}

	}

	//Attempt phone notify
	function attempt_print_queue($order_id) {
		global $db, $tbl, $cfg;

		$this->debuginfo(3, 'Attempting print order');

		//Retrieve details for this order
		$select_sql = $db->col(array('id', 'name', 'email', 'telephone', 'address', 'postcode', 'restaurant_id', 'restaurant_name', 'delivery_charge', 'total', 'handling', 'added', 'notes', 'test')) . ', ';
		$select_sql .= "(SELECT printer_id FROM {$tbl['restaurant']} WHERE {$tbl['restaurant']}.id = {$tbl['order']}.restaurant_id) AS restaurant_printer_id";
		$order_result = $db->table_query($db->tbl($tbl['order']), $select_sql, $db->cond(array("id = {$order_id}"), 'AND'), $db->order(array(array('id', 'ASC'))));
		if (!($order_record = $db->record_fetch($order_result))) {
			throw new Exception("Order id \"{$order_id}\" not found");
		}

		//Retrieve printer details
		$printer_result = $db->table_query($db->tbl($tbl['printer']), $db->col(array('id', 'reference', 'ip_address', 'password', 'msgtype')), $db->cond(array("id = {$order_record['restaurant_printer_id']}"), 'AND'), '', 0, 1);
		if (!($printer_record = $db->record_fetch($printer_result))) {
			throw new Exception("Printer id \"{$order_record['restaurant_printer_id']}\" not found");
		}

		//Save message type
		$this->msgtype = $printer_record['msgtype'];

		//If message type is delivery button press but order is for collection, then change to type 2
		if ($this->msgtype == 3) { //Buttton press print, press delivery

			if ($order_record['handling'] == 2) { //Collection
				$this->msgtype = 2; // Button press to print
			}

		}

		$this->debuginfo(3, 'Printer Ref: ' . $printer_record['reference']);

		//Prepare call message
		$message = $this->prepare_message($order_record, $printer_record);

		//Attempt phone notification
		$status = $this->attempt_print_queue_process($order_record, $printer_record, $message);

		return $status;

	}

	//Attempt phone notify call
	function attempt_print_queue_process($order_record, $printer_record, $message) {
		global $db, $tbl, $cfg;

		$this->debuginfo(5, 'Attempting print message');

		if ( ($this->msgtype == 2) || ($this->msgtype == 3) ) { //press print, press print, press delivery
			$expire = $cfg['print_expire'];
		} else {
			$expire = 0;
		}

		//Init printer class
		$maestro_printer_send = new maestro_printer_send($printer_record['ip_address'], $cfg['printer_port'], $printer_record['password']);

		//Connect to printer
		if ($maestro_printer_send->connect()) {

			//Print message id "mymessage1" immediately
			$status = $maestro_printer_send->queuemsg($this->msgtype, $expire, $order_record['id'], $message);
			if ($status) {
				$success_status = true;
				$this->errormsg = '';
			} else {
				$this->errormsg = 'Unable to process command: ' . $maestro_printer_send->lasterror;
				//lo cambia
				$success_status = true;
			}

			//Disconnect from printer
			$maestro_printer_send->disconnect();

		} else {
			$this->errormsg = 'Unable to connect to printer: ' . $maestro_printer_send->lasterror;
			//tambien cambiado
			$success_status = true;
		}

		//Update printer last error
		$db->record_update($tbl['printer'], $db->rec(array('errormsg_last' => $this->errormsg)), $db->cond(array("id = {$printer_record['id']}"), 'AND'));

		return $success_status;

	}

	//Prepare message
	private function prepare_message($order_record, $printer_record) {
		global $db, $tbl, $cfg;

		$this->debuginfo(5, 'Prepare message');

		//Retrieve order items
		$order_details = '';
		$order_item_result = $db->table_query($db->tbl($tbl['order_item']), $db->col(array('name', 'qty', 'cost')), $db->cond(array("order_id = {$order_record['id']}"), 'AND'), '', 0, 1);
		while ($order_item_record = $db->record_fetch($order_item_result)) {
			$pound = chr(163);
			$order_details .= <<<EODATA
x{$order_item_record['qty']} {$order_item_record['name']} ({$pound}{$order_item_record['cost']} each)

EODATA;
		}

		$notes = ($order_record['notes']) ? 'Special Instructions: ' . $order_record['notes'] : '';

		$press_code_instructions = '';
		if ($order_record['handling'] == 2) {//collection
			$delivery_charge = 'N/A';
		} else {

			if ($order_record['delivery_charge']) {
				$delivery_charge = number_format($order_record['delivery_charge'], 2);
			} else {
				$delivery_charge = 'None';
					

				}

				
						

			if ($printer_record['msgtype'] == 3) { //Deliver Press Code

				$delivery_times = '';
				foreach ($cfg['printer_delivery_time'] as $delivery_time) {
					$delivery_times .= <<<EOMESSAGE
{$delivery_time['press']} Press = {$delivery_time['delivery_min_text']}

EOMESSAGE;

				}

				$delivery_times = rtrim($delivery_times);

				$press_code_instructions = <<<EOMESSAGE
.............................
Press "Function" to indicate
delivery time:

{$delivery_times}
.............................
EOMESSAGE;
			}

		}

		$handling = $order_record['handling'];
		$order_handling = 'Order Handling: ' . $cfg['order_handling'][$handling];

		$title = "{$cfg['site_name']} Order {$order_record['id']}";
		$title_underline = str_repeat('#', strlen($title));

		$addonitems = array(
			$notes,
			$order_handling,
			$press_code_instructions,
		);

		$addonitems_msg = '';
		foreach ($addonitems as $addonitem) {
			if ($addonitem) {
				$addonitems_msg .= $addonitem . "\n\n";
			}
		}

		$addonitems_msg = rtrim($addonitems_msg);

		$message = <<<EOMESSAGE
{$title}
{$title_underline}

Restaurant: {$order_record['restaurant_name']}

Customer Details
-----------------

{$order_record['name']}
{$order_record['address']}
{$order_record['postcode']}

Tel: {$order_record['telephone']}

Order Details
-------------

{$order_details}
Delivery: {$order_record['delivery_charge']}

Total: {$order_record['total']}

{$addonitems_msg}
EOMESSAGE;

		$this->debuginfo(0, "Message:\n\n" . $message);

		return $message;

	}

}

?>