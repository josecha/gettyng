<?php

//Maestro Printer Send Class
class maestro_printer_send {

	const QUEUEMSG_TYPE_PRINT = 1;
	const QUEUEMSG_TYPE_BTNPRINT = 2;
	const QUEUEMSG_TYPE_BTNPRINTBTN = 3;

	private $ip_address;
	private $port;
	private $timeout = 5;
	private $password_length = 15;

	private $fp;
	private $buffer = '';

	private $password = '';

	public $lasterror = '';

	private $queuemsg_types_resolve = array(
		1 => 'print',
		2 => 'btnprint',
		3 => 'btnprintbtn',
	);

	public function __construct($ip_address, $port, $password) {

		$this->ip_address = $ip_address;
		$this->port = $port;
		$this->password = $password;

		if (strlen($this->password) != $this->password_length) {
			throw new Exception("Password must be exactly {$this->password_length} characters in length");
		}

	}

	public function connect() {

		$this->fp = fsockopen($this->ip_address, $this->port, $errno, $errstr, $this->timeout);

		if ($this->fp) {
			$status = true;
		} else {
			$this->lasterror = "Unable to open socket: {$errstr} ({$errno})";
			$status = false;
		}

		return $status;

	}

	public function disconnect() {

		if (!$this->fp) {
			throw new Exception('Not connected');
		}

		fclose($this->fp);

	}

	public function queuemsg($type, $expiry_sec, $msgid, $msgtxt) {

		if (!$this->fp) {
			throw new Exception('Socket not open');
		}

		if (!$msgtxt) {
			throw new Exception('No message specified');
		}

		if (!isset($this->queuemsg_types_resolve[$type])) {
			throw new Exception("Message type \"{$type}\" unknown");
		}

		$expiry_sec = intval($expiry_sec);
		if (!( ($expiry_sec >= 0) && ($expiry_sec <= 3600) )) {
			throw new Exception("Message \"{($expiry_sec}\" should be between 0-3600 only, 0 = no expiry");
		}

		if (preg_match("%[^a-zA-Z0-9]%", $msgid)) {
			throw new Exception("Message id \"{$msgid}\" must be a-z, 0-9 only");
		}

		$msgtxtprn = $msgtxt . "\n\n\n\n\n";

		$msgtxtprn = str_replace("\r", "", $msgtxtprn);
		//$msgtxtprn = str_replace('[rev]', chr(15), $msgtxtprn);
		//$msgtxtprn = str_replace('[/rev]', chr(14), $msgtxtprn);
		//$msgtxtprn = str_replace('[u]', chr(17), $msgtxtprn);
		//$msgtxtprn = str_replace('[/u]', chr(16), $msgtxtprn);
		$msgtxtprn = str_replace(chr(163), chr(156), $msgtxtprn); //UK Pound sign

		$status = $this->sendcommand("{$this->queuemsg_types_resolve[$type]}|{$msgid}|{$expiry_sec}|{$msgtxtprn}");

		return $status;

	}

	public function removequeuedmsg($msgid) {

		if (!$this->fp) {
			throw new Exception('Socket not open');
		}

		if (preg_match("%[^a-z0-9]%", $msgid)) {
			throw new Exception("Message id \"{$msgid}\" must be a-z, 0-9 only");
		}

		$status = $this->sendcommand("remqueuedmsgid|{$msgid}");

		return $status;

	}

	public function removeallqueuedmsg() {

		if (!$this->fp) {
			throw new Exception('Socket not open');
		}

		$status = $this->sendcommand("remqueuedmsg");

		return $status;

	}

	public function ping() {

		if (!$this->fp) {
			throw new Exception('Socket not open');
		}

		$status = $this->sendcommand("ping");

		return $status;

	}

	public function setconfigurl($url) {

		if (!$this->fp) {
			throw new Exception('Socket not open');
		}

		$status = $this->sendcommand("setconfigurl|{$url}");

		return $status;

	}

	public function setconfigpassword($password) {

		if (!$this->fp) {
			throw new Exception('Socket not open');
		}

		if (preg_match("%[^a-z0-9]%", $password)) {
			throw new Exception("Password \"{$password}\" must be a-z, 0-9 only");
		}

		$status = $this->sendcommand("setconfigpassword|{$password}");

		return $status;

	}

	public function refetchconfig() {

		if (!$this->fp) {
			throw new Exception('Socket not open');
		}

		$status = $this->sendcommand("updateconfig");

		return $status;

	}

	private function sendcommand($command) {

		if (!$this->fp) {
			throw new Exception('Socket not open');
		}

		if (!$command) {
			throw new Exception('No message specified');
		}

		$msglen = strlen($command);

		if ($msglen > 9999) {
			throw new Exception("Command is {$msglen} bytes, i.e. over the maximum limit of 9999 bytes");
		}

		//Pad length to 4 bytes
		$msglen = str_pad($msglen, 4, '0', STR_PAD_LEFT);

		$command = $this->password . '|' . $msglen . '|' . $this->checksum($command) . '|' . $command;

		//fwrite($this->fp, $command);

		$command_parts = str_split($command, 100);
		foreach ($command_parts as $command_part) {
			fwrite($this->fp, $command_part);
			//sleep(5);
		}

		//Check for status of message
		$status = $this->wait_reply($this->timeout);

		if ($status) {
			return true;
		} else {
			return false;
		}

	}

	public function checksum($data) {

		$checksum = false;

		$characters = str_split($data);
		foreach ($characters as $character) {

			if ($checksum === false) {
				$checksum = $character;
			} else {
				$checksum = $checksum ^ $character;
			}

		}

		$checksum = sprintf('%02X', ord($checksum));

		return $checksum;

	}

	//Wait for reply from modem
	private function wait_reply($timeout) {

		//Clear buffer
		$this->buffer = '';

		//Set timeout
		$timeoutat = time() + $timeout;

		//Loop until timeout reached (or expected result found)
		do {

			$buffer = fread($this->fp, 1024);
			$this->buffer .= $buffer;

			//Check if received expected responce
			if (preg_match('/^SUCCESS\:\ (.*)\r\n$/m', $this->buffer)) {
				return true;
				//break;
			} else if (preg_match('/^ERROR\:\ (.*)\r\n$/m', $this->buffer, $matches)) {
				$this->lasterror = $matches[1];
				return false;
			}

		} while ($timeoutat > time());

		return false;

	}

}

?>