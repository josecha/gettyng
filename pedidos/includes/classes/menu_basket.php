<?php

class menu_basket {

	public $restaurant_id;
	private $restaurant_record;

	function init() {
		global $db, $tbl;

		$this->restaurant_id = intval($this->restaurant_id);

		$select_sql = $db->col(array('id', 'min_order_value', 'delivery_charge', 'navname')) . ', ';
		$select_sql .= "(SELECT navname FROM {$tbl['town']} WHERE id = town_id) AS town_navname, ";
		$select_sql .= "(SELECT {$tbl['city']}.navname FROM {$tbl['city']} WHERE {$tbl['city']}.id = (SELECT {$tbl['town']}.city_id FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id)) AS city_navname";

		$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $select_sql, $db->cond(array("id = {$this->restaurant_id}"), 'AND'), '', 0, 1);
		if (!($restaurant_record = $db->record_fetch($restaurant_result))) {
			throw new Exception("Restaurant id \"{$this->restaurant_id}\" not found");
		}

		$this->restaurant_record = $restaurant_record;

	}

	function html() {
		global $cfg;

		$base_path_link = htmlentities(navfr::base_path());

		$restaurant_recordh = lib::htmlentities_array($this->restaurant_record);

		$form_post_link = navfr::link_h(array('restaurant', $this->restaurant_record['city_navname'], $this->restaurant_record['town_navname'], $this->restaurant_record['navname'], 'order'));

		$html = <<<EOHTML

<div id="menu_basket_container">

	<div id="menu_basket_top"></div>

	<div class="menu_basket" id="menu_basket">
		<div class="menu_basket-top">
			<div class="menu_basket-bottom">

				<div class="content">

					<h2>Tu pedido:</h2>

					<div class="basket">

						<form method="post" action="{$form_post_link}" id="basketform">

							<div id="basket_generated" class="basket_generated"></div>

							<div class="extra_items">

								<div class="item">
									<div class="name" id="handling_name"></div>
									<div class="price" id="handling_charge"></div>
									<div class="removebtn"></div>
									<div class="qty"></div>
									<div class="addbtn"></div>
									<div class="clear"></div>
								</div>

								<div class="item item-min_order">
									<div class="name">Minimo de pedido</div>
									<div class="price">{$restaurant_recordh['min_order_value']}</div>
									<div class="removebtn"></div>
									<div class="qty"></div>
									<div class="addbtn"></div>
									<div class="clear"></div>
								</div>

								<div class="item">
									<div class="name name-total">Total $</div>
									<div class="price price-total" id="total_cost">{$restaurant_recordh['delivery_charge']}</div>
									<div class="removebtn"></div>
									<div class="qty"></div>
									<div class="addbtn"></div>
									<div class="clear"></div>
								</div>

							</div>

							<div><input class="cufow" id="send" type="submit" value="Ordenar"></div>

						</form>

					</div>

				</div>
			</div>
		</div>
	</div>

</div>

EOHTML;

		return $html;

	}


}
// el aniguo boton
//<input type="image" src="{$base_path_link}{$cfg['theme_resources_path']}takeaway_menu/order/submit_order.gif" name="submit" value="Process Order" alt="Process Order" class="submit_order" />
?>