<?php

class page_user_verify_email {

	public $titletag = 'Verificar cuenta de correo';
	public $pagetitle = 'Verificar cuenta de correo';
	public $metadesc;
	public $body_html;
	public $headeraddin_html;
	public $footeraddin_html;
	public $mainnavsection;
	public $googanalyticspage;
	public $getdata;
	public $postdata;

	protected $verify_errormsg_html;

	//-------------------------------------------------------------------------------------

	/*
	public function init() {
		global $db, $tbl;

	}
	*/

	//-------------------------------------------------------------------------------------

	public function handle() {
		global $auth;

		//Init data required by the page
		//$this->init();

		$status = $this->verify();

		if ($status) {

			$this->associate_prev_orders();

			$page_html = <<<EOHTML
<p>La cuenta de correo ha sido verificada exitosamente.</p>
EOHTML;

		} else {

			$page_html = <<<EOHTML
<p><strong>Error:</strong> {$this->verify_errormsg_html}</p>
EOHTML;

		}

		$pagetitle_h = htmlentities($this->pagetitle);

		$body_html = <<<EOHTML

<h1>{$pagetitle_h}</h1>

{$page_html}

EOHTML;

		//Template
		$template = new template();
		$template->settitle($this->titletag);
		$template->setmetadesc($this->metadesc);
		$template->setmainnavsection($this->mainnavsection);
		$template->setgooganalyticspage($this->googanalyticspage);
		$template->setheaderaddinhtml($this->headeraddin_html);
		$template->setfooteraddinhtml($this->footeraddin_html);
		$template->setbodyhtml($body_html);
		//$template->setshowsearch(true);
		//$template->setshoworderprocess(true);
		$template->display();

	}

	//-------------------------------------------------------------------------------------

	//Verify email address
	protected function verify() {
		global $db, $tbl, $auth;

		//Check username / password
		$customer_result = $db->table_query($db->tbl($tbl['customer']), '*', $db->cond(array("verify_code = '".$db->es($this->getdata['v'])."'"), 'AND'), '', 0, 1);
		if ($customer_record = $db->record_fetch($customer_result)) {

			//Check noone else with this email has already been verified
			$customer_emailchk_result = $db->table_query($db->tbl($tbl['customer']), '*', $db->cond(array("id != {$customer_record['id']}", "verified = 1", "email = '".$db->es($customer_record['email'])."'"), 'AND'), '', 0, 1);
			if (!($customer_emailchk_record = $db->record_fetch($customer_emailchk_result))) {

				$db->record_update($tbl['customer'], $db->rec(array('verified' => 1)), $db->cond(array("id = {$customer_record['id']}"), 'AND'));

				$auth->login_success = true;
				$auth->authinfo = $customer_record;
				$auth->authinfo['verified'] = 1;

				$auth->setuser($customer_record['email'], $customer_record['password']);
				$auth->cookie_save();

				$status = true;

			} else {
				$status = false;
				$this->verify_errormsg_html = 'Esta cuenta de correo ya  ha sido verificada';
			}

		} else {
			$status = false;
			$this->verify_errormsg_html = 'Checa tu cuenta de correo y da click sobre el link.';
		}

		return $status;

	}

	//Associate previous orders with this email address to the account
	protected function associate_prev_orders() {
		global $db, $tbl, $auth;

		$order_result = $db->table_query($db->tbl($tbl['order']), $db->col(array('id')), $db->cond(array("email = '" . $db->es($auth->authinfo['email']) . "'"), 'AND'));
		while ($order_record = $db->record_fetch($order_result)) {
			$db->record_update($tbl['order'], $db->rec(array('customer_id' => $auth->authinfo['id'])), $db->cond(array("id = {$order_record['id']}"), 'AND'));
		}

	}

}

?>