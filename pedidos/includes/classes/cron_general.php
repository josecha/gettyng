<?php

class cron_general {

	public $showdebugging = true;
	private $order_handler_recur = array();

	//Run
	function run() {
		global $db, $tbl, $cfg;

		$this->debuginfo(0, 'Running @ ' . date('Y-m-d H:i:s') . "\n");

		try {

			//$db->table_empty($tbl['cronerror']);
			//$db->record_update($tbl['config'], $db->rec(array('last_cron_general' => null)));

			//$db->table_empty($tbl['notify_log']);

			//$db->record_update($tbl['order'], $db->rec(array('status' => 1)), $db->cond(array("id = 2"), 'AND'));

			//Retrieve recent error count
			$errorshold = $this->errorshold();

			//If not too many exceptions recently
			if ($errorshold == false) {

				//If not already running / exceeded time limit
				if ($this->checknotrunning() == true) {

					//Set as running
					$this->setrunning(true);

					//Process orders
					$this->process_orders();

					//Finished running
					$this->setrunning(false);

				}

			}

		} catch (Exception $e) {

			if ($cfg['devmode'] == false) {

				//Log the exception
				exceptions::savelogentry($e);

			}

			//Save cron exception to database
			$cronerror_record = array(
				'errordatetime' => $db->datetimenow(),
			);
			$db->record_insert($tbl['cronerror'], $db->rec($cronerror_record));

			//Set not running
			$this->setrunning(false);

			$this->debuginfo(0, 'Exception: ' . $e->getMessage());

			if ($cfg['devmode'] == true) {
				throw $e;
			} else {
				//Report error via email
				appgeneral::email_error('Cron General', $e->getMessage());
			}

		}

	}

	//Check if too many recent error
	private function errorshold() {
		global $db, $tbl, $cfg;

		$this->debuginfo(0, 'Checking for errors recently');

		$ts = time() - $cfg['cron_error_period'];
		$errorcheck_date = gmdate('Y-m-d H:i:s', $ts);

		//Retrieve error
		$cronerror_result = $db->table_query($db->tbl($tbl['cronerror']), 'COUNT(*) AS total', $db->cond(array("errordatetime > '{$errorcheck_date}'"), 'AND'), '', 0, 1);
		if (!($cronerror_record = $db->record_fetch($cronerror_result))) {
			throw new Exception('Count did not return anything');
		}

		$this->debuginfo(1, "{$cronerror_record['total']} errors recently");

		if ($cronerror_record['total'] < $cfg['cron_error_max']) {
			$limitreached = false;
		} else {
			$this->debuginfo(0, "Errors limit of \"{$cfg['cron_error_max']}\" reached");
			$limitreached = true;
		}

		return $limitreached;

	}

	//Set cron running
	private function setrunning($status) {
		global $db, $tbl;

		$this->debuginfo(0, 'Set as running: ' . (($status) ? 'true' : 'false'));

		$date = ($status == true) ? $db->datetimenow() : null;

		$db->record_update($tbl['config'], $db->rec(array('last_cron_general' => $date)), $db->cond(array("id = 1"), 'AND'));

	}

	//Check not already running (or if running time limit exceeded)
	private function checknotrunning() {
		global $db, $tbl, $cfg;

		$this->debuginfo(0, 'Check not running');

		//Retrieve last run date
		$config_result = $db->table_query($db->tbl($tbl['config']), $db->col(array('last_cron_general')), $db->cond(array("id = 1"), 'AND'));
		if (!($config_record = $db->record_fetch($config_result))) {
			throw new Exception('Config row 1 not found');
		}

		$this->debuginfo(1, 'Last Run @ ' . date('Y-m-d H:i:s', strtotime($config_record['last_cron_general'] . ' UTC')));

		if ($config_record['last_cron_general'] == null) {
			$lastruncronplus = 0;
		} else {
			$lastruncronplus = strtotime($config_record['last_cron_general'] . ' UTC') + $cfg['cron_running_max_reset'];
			$this->debuginfo(1, 'Checking against @ ' . date('Y-m-d H:i:s', $lastruncronplus));
		}

		if ($lastruncronplus < time()) {
			$this->debuginfo(1, 'Not running');
			return true;
		} else {
			$this->debuginfo(0, 'Already running');
			return false;
		}

	}

	//Save debug message
	private function debuginfo($debug_level, $debug_message) {

		$spaceaddin = str_repeat(' ', $debug_level);

		if ($this->showdebugging == true) {
			$spaceaddin = str_repeat(' ', $debug_level);
			echo "{$spaceaddin}{$debug_message}\n";
		}

	}

	//Process orders
	private function process_orders() {
		global $db, $tbl, $cfg;

		$this->debuginfo(0, 'Processing orders');

		$cond = array();
		$cond[] = "status IN(".implode(',', $cfg['order_status_process']).")";

		$this->debuginfo(1, 'Retrieving non finished orders');

		//Retrieve all orders that are not finished processing
		$order_result = $db->table_query($db->tbl($tbl['order']), $db->col(array('id', 'name')), $db->cond($cond, 'AND'), $db->order(array(array('id', 'ASC'))));
		while ($order_record = $db->record_fetch($order_result)) {

			//$this->debuginfo(2, 'Order Id: ' . $order_record['id']);
			//$this->debuginfo(2, 'Order Name: ' . $order_record['name']);

			$this->handle_order($order_record['id']);

		}

	}

	//Handle order
	private function handle_order($order_id) {
		global $db, $tbl, $cfg;

		$this->debuginfo(2, 'Order ID: ' . $order_id);

		$order_id = intval($order_id);

		if (!isset($this->order_handler_recur[$order_id])) {
			$this->order_handler_recur[$order_id] = 0;
		}
		$this->order_handler_recur[$order_id]++;

		if ($this->order_handler_recur[$order_id] > 10) {
			throw new Exception("Handling recursed too many times for order id \"{$order_id}\" may be out of control");
		}

		$cond = array();
		$cond[] = "status IN(".implode(',', $cfg['order_status_process']).")";
		$cond[] = "id = {$order_id}";

		//Retrieve order details
		$order_result = $db->table_query($db->tbl($tbl['order']), $db->col(array('id', 'name', 'status', 'status_change_last', 'restaurant_id')), $db->cond($cond, 'AND'), $db->order(array(array('id', 'ASC'))));
		if (!($order_record = $db->record_fetch($order_result))) {
			throw new Exception("Order id \"{$order_id}\" not found");
		}

		$this->debuginfo(2, 'Order Name: ' . $order_record['name']);
		$this->debuginfo(2, 'Status ID: ' . $order_record['status']);

		switch ($order_record['status']) {

			case 1: //New
				$this->debuginfo(2, 'Status: New Order');

				//Lookup restaurant details from order
				$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $db->col(array('msg_dispatch')), $db->cond(array("id = {$order_record['restaurant_id']}"), 'AND'));
				if (!($restaurant_record = $db->record_fetch($restaurant_result))) {
					throw new Exception("Restaurant id \"{$order_record['restaurant_id']}\" not found");
				}

				//If dispatch by phone
				if ($restaurant_record['msg_dispatch'] == 2) { //Phone

					//Update status
					$db->record_update($tbl['order'], $db->rec(array('status' => 200, 'status_change_last' => $db->datetimenow())), $db->cond(array("id = {$order_id}"), 'AND'));

					//Restart handling of order
					$this->handle_order($order_id);

				} else if ($restaurant_record['msg_dispatch'] == 3) { //Printer
					//If dispatch by printer

					//Update status
					$db->record_update($tbl['order'], $db->rec(array('status' => 300, 'status_change_last' => $db->datetimenow())), $db->cond(array("id = {$order_id}"), 'AND'));

					//Restart handling of order
					$this->handle_order($order_id);

				}

				break;

			case 200: //Pending Phone Notify
			case 201: //Phone Notify Try 1
			case 202: //Phone Notify Try 2
			case 203: //Phone Notify Try 3
			case 204: //Phone Notify Try 4
			case 205: //Phone Notify Try 5
			case 206: //Phone Notify Try 6
			case 207: //Phone Notify Try 7
			case 208: //Phone Notify Try 8
			case 209: //Phone Notify Try 9
			case 210: //Phone Notify Try 10

				$pending_status = 200;

				$this->debuginfo(2, 'Status: Phone Notify Try');

				//Checking if ok to proceed due to time from last attempt
				$oktoprocess = $this->check_status_change_last($order_record['status_change_last'], $cfg['notify_phone_retry_time']);

				/*
				//Check first attempt, or last notify action longer than the retry limit
				if ($order_record['status'] == 200) {
					$oktoprocess = true;
					$this->debuginfo(4, 'First attempt');
				} else {

					$last_notify_action_ts = strtotime($order_record['status_change_last'] . ' UTC');
					$check_ts = $last_notify_action_ts + $cfg['notify_phone_retry_time'];
					$now_ts = time();
					if ($check_ts < $now_ts) {
						$oktoprocess = true;
					} else {
						$oktoprocess = false;
					}

					$this->debuginfo(4, 'Check: ' . gmdate('Y-m-d H:i:s', $check_ts));
					$this->debuginfo(4, 'Now: ' . gmdate('Y-m-d H:i:s', $now_ts));

				}
				*/

				if ( ($oktoprocess) || ($order_record['status'] == $pending_status) ) {
					$this->debuginfo(3, 'Ok, no last notfy or over limit');

					$current_tries = $order_record['status'] - $pending_status;

					$this->debuginfo(3, 'Current Try: ' . ($current_tries + 1));

					if ($current_tries < $cfg['notify_phone_try']) {

						//Update status
						$db->record_update($tbl['order'], $db->rec(array('status' => $order_record['status']+1, 'status_change_last' => $db->datetimenow())), $db->cond(array("id = {$order_id}"), 'AND'));

						//Attempt notify
						$order_phone_notify = new order_phone_notify();
						$order_phone_notify->showdebugging = $this->showdebugging;
						$status = $order_phone_notify->attempt_phone_notify($order_record['id']);

						//If successfully queued
						if ($status) {

							$this->debuginfo(3, "Queued successfully");

						} else {

							$this->debuginfo(3, "Failed to queue");

							//Failed to queue

							//Update status
							$db->record_update($tbl['order'], $db->rec(array('status' => $order_phone_notify->status_error_code, 'status_change_last' => $db->datetimenow())), $db->cond(array("id = {$order_id}"), 'AND'));

							//Notify customer of error
							$this->notify_customer_error($order_id);

						}

					} else {

						$this->debuginfo(3, "Can not try, over limit of {$cfg['notify_phone_try']} tries (now {$current_tries} tries)");

						//Update status
						$db->record_update($tbl['order'], $db->rec(array('status' => 222, 'status_change_last' => $db->datetimenow())), $db->cond(array("id = {$order_id}"), 'AND'));

						//Notify customer of error
						$this->notify_customer_error($order_id);

					}

				} else {
					$this->debuginfo(3, 'Can not run notify processing, under limit');
				}

				break;

			case 300: //Pending Print
			case 301: //Printer Try 1
			case 302: //Printer Try 2
			case 303: //Printer Try 3
			case 304: //Printer Try 4
			case 305: //Printer Try 5
			case 306: //Printer Try 6
			case 307: //Printer Try 7
			case 308: //Printer Try 8
			case 309: //Printer Try 9
			case 310: //Printer Try 10

				$pending_status = 300;

				$this->debuginfo(2, 'Status: Printer Try');

				//Checking if ok to proceed due to time from last attempt
				$oktoprocess = $this->check_status_change_last($order_record['status_change_last'], $cfg['print_retry_time']);

				if ( ($oktoprocess) || ($order_record['status'] == $pending_status) ) {
					$this->debuginfo(3, 'Ok, no last notfy or over limit');

					$current_tries = $order_record['status'] - $pending_status;

					$current_try = $current_tries + 1;

					$this->debuginfo(3, 'Current Try: ' . $current_try);

					//Update status
					$db->record_update($tbl['order'], $db->rec(array('status' => $order_record['status']+1, 'status_change_last' => $db->datetimenow())), $db->cond(array("id = {$order_id}"), 'AND'));

					//Attempt print order
					$order_print = new order_print();
					$order_print->showdebugging = $this->showdebugging;
					$status = $order_print->attempt_print_queue($order_record['id']);

					//If successfully queued
					if ($status) {

						$this->debuginfo(3, "Queued successfully");

						//Resolve msgtype code to order status
						$msgtype = $order_print->msgtype;
						$order_status = $cfg['printer_msgtype_status'][$msgtype];

						//Update status
						$db->record_update($tbl['order'], $db->rec(array('status' => $order_status, 'status_change_last' => $db->datetimenow())), $db->cond(array("id = {$order_id}"), 'AND'));

						//If message printed immediately
						if ($order_print->msgtype == 1) {

							//Notify customer order received successfully
							
							//quito esta liena orden recibida
							//self::notify_customer_success($order_record['id']);

						}

					} else {

						$this->debuginfo(3, "Failed with error: " . $order_print->errormsg);

						//If we are at the limit for tries
						if ($current_try == $cfg['print_try']) {

							$this->debuginfo(3, "Met limit of {$cfg['print_try']} tries");

							//Update status
							$db->record_update($tbl['order'], $db->rec(array('status' => 322, 'status_change_last' => $db->datetimenow())), $db->cond(array("id = {$order_id}"), 'AND'));

							//Notify customer of error
							$this->notify_customer_error($order_id);

						}

					}

				} else {
					$this->debuginfo(3, 'Can not run print processing, under limit');
				}

				break;

			case 341: //Message Waiting For Button Print
			case 342: //Message Waiting For Button Print, Delivery Button

				$this->debuginfo(2, 'Status: Waiting button press to print');

				//Checking if ok to proceed due to time from last attempt
				$oktoprocess = $this->check_status_change_last($order_record['status_change_last'], $cfg['print_expire'] + $cfg['print_expire_buffer']);

				if ($oktoprocess) {
					$this->debuginfo(3, 'Ok, over limit');

					$this->debuginfo(3, 'Timed out waiting for button print');

					//Update status
					$db->record_update($tbl['order'], $db->rec(array('status' => 321, 'status_change_last' => $db->datetimenow())), $db->cond(array("id = {$order_id}"), 'AND'));

					//Notify customer of error
					$this->notify_customer_error($order_id);

				} else {
					$this->debuginfo(3, 'Can not run press print processing, under limit');
				}

				break;

			case 343: //Message Waiting For Delivery Button

				$this->debuginfo(2, 'Status: Waiting delivery button press');

				//Checking if ok to proceed due to time from last attempt
				$oktoprocess = $this->check_status_change_last($order_record['status_change_last'], $cfg['print_wait_delivery']);

				if ($oktoprocess) {
					$this->debuginfo(3, 'Ok, over limit');

					$this->debuginfo(3, 'Timed out waiting for delivery button print, marking as success just without any delivery info');

					//Update status
					$db->record_update($tbl['order'], $db->rec(array('status' => 322, 'status_change_last' => $db->datetimenow())), $db->cond(array("id = {$order_id}"), 'AND'));

					//Notify customer order received successfully
					
					//quito esta linea adios 
					//self::notify_customer_success($order_record['id']);

				} else {
					$this->debuginfo(3, 'Can not run print delivery button processing, under limit');
				}

				break;


			case 11: //Waiting Online Payment

				$this->debuginfo(2, 'Status: Waiting Online Payment');

				//Checking if ok to proceed due to time from last attempt
				$oktoprocess = $this->check_status_change_last($order_record['status_change_last'], $cfg['payment_wait_time']);

				if ($oktoprocess) {
					$this->debuginfo(3, 'Ok, exceeded wait time');

					//Update status
					$db->record_update($tbl['order'], $db->rec(array('status' => 30, 'status_change_last' => $db->datetimenow())), $db->cond(array("id = {$order_id}"), 'AND'));

					//Notify customer of error
					$this->notify_customer_error($order_id);

				} else {
					$this->debuginfo(3, "Have not exceeded wait time");
				}

				break;

		}

	}

	//Check ok to proceed based on last status change
	private function check_status_change_last($status_change_last_datetime, $retry_time_sec) {

		$this->debuginfo(3, 'Checking if ok to proceed due to time from last attempt');

		$last_notify_action_ts = strtotime($status_change_last_datetime . ' UTC');
		$check_ts = $last_notify_action_ts + $retry_time_sec;
		$now_ts = time();
		if ($check_ts < $now_ts) {
			$oktoprocess = true;
		} else {
			$oktoprocess = false;
		}

		$this->debuginfo(4, 'Check: ' . gmdate('Y-m-d H:i:s', $check_ts));
		$this->debuginfo(4, 'Now: ' . gmdate('Y-m-d H:i:s', $now_ts));

		return $oktoprocess;

	}

	//Notify customer of technical issue with their order
	private function notify_customer_error($order_id) {
		global $db, $tbl, $cfg;

		$this->debuginfo(2, 'Notify customer technical issue');

		//Retrieve order details
		$order_result = $db->table_query($db->tbl($tbl['order']), $db->col(array('id', 'restaurant_id', 'name', 'email', 'telephone')), $db->cond(array("id = {$order_id}"), 'AND'));
		if (!($order_record = $db->record_fetch($order_result))) {
			throw new Exception("Order id \"{$order_id}\" not found");
		}

		//Lookup restaurant details from order
		$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $db->col(array('name', 'telephone')), $db->cond(array("id = {$order_record['restaurant_id']}"), 'AND'));
		if (!($restaurant_record = $db->record_fetch($restaurant_result))) {
			throw new Exception("Restaurant id \"{$order_record['restaurant_id']}\" not found");
		}

		$email_body = <<<EOMAIL
Dear {$order_record['name']},

Thank you for your recent order with {$cfg['site_name']}.

Unfortunately we are currently experiencing technical issues and are unable to send your order to "{$restaurant_record['name']}".

We have received a notification of this error, and would like to apologise for any inconvenience caused.

If you like you could also try calling the restaurant directly on {$restaurant_record['telephone']} to place your order.

Kind Regards,

{$cfg['site_name']}
{$cfg['site_url']}

EOMAIL;

		$mail = new phpmailer();
		$mail->Mailer = $cfg['email_method'];
		$mail->From = $cfg['email_system_from'];
		$mail->Sender = $cfg['email_system_from'];
		$mail->FromName = $cfg['email_system_from'];
		$mail->Subject = "{$cfg['site_name']} Order Processing Error";
		$mail->IsHTML(false);
		$mail->Body = $email_body;

		$email = $order_record['email'];

		//if ($order_record['test']) {
		//	$email = $cfg['email_test'];
		//}

		$name = preg_replace("%[^a-zA-Z0-9\ ]%", '', $order_record['name']);
		$mail->AddAddress($email, $name);
		$mail->AddCC($cfg['email_admin'], $cfg['site_name']);

		if (!$mail->Send()) {
			throw new Exception("Unable to send email: {$mail->ErrorInfo}");
		}

		if ($cfg['sms_send']) {

			//Send SMS
			$send_sms = new send_sms();
			$sms_msg = "Regrettably due to technical issues we have been unable to process your order, you could try calling the restaurant on {$restaurant_record['telephone']}";
			$status = $send_sms->send($send_sms->telno_uk_cc($order_record['telephone']), $sms_msg);
			if (!$status) {
				throw new Exception('Error sending SMS: ' . $send_sms->errormsg);
			}

		}

	}

	//Notify customer order has been received successfully
	static public function notify_customer_success($order_id) {
		global $db, $tbl, $cfg;

		//Retrieve order details
		$order_result = $db->table_query($db->tbl($tbl['order']), $db->col(array('id', 'restaurant_id', 'restaurant_name', 'handling', 'delivery_min', 'name', 'telephone', 'email')), $db->cond(array("id = {$order_id}"), 'AND'));
		if (!($order_record = $db->record_fetch($order_result))) {
			throw new Exception("Order id \"{$order_id}\" not found");
		}

		/*
		//Lookup restaurant details from order
		$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $db->col(array('name', 'telephone')), $db->cond(array("id = {$order_record['restaurant_id']}"), 'AND'));
		if (!($restaurant_record = $db->record_fetch($restaurant_result))) {
			throw new Exception("Restaurant id \"{$order_record['restaurant_id']}\" not found");
		}
		*/

		//Restaurant name
		$sms_restaurant_name = $order_record['restaurant_name'];
		$sms_restaurant_name = substr($sms_restaurant_name, 0, 20);

		//If type is delivery, update delivery minutes
		//if ($order_record['handling'] == 1) {

		//If have a delivery time
		if ($order_record['delivery_min'] > 0) {

			$email_delivery_extra = <<<EOHTML

You should receive your order within {$order_record['delivery_min']} minutes.

EOHTML;

			$sms_msg = "Just to let you know your order has been received by {$sms_restaurant_name} and will be delivered within {$order_record['delivery_min']} minutes.";
		} else {
			$email_delivery_extra = '';
			$sms_msg = "Just to let you know your order has been received by {$sms_restaurant_name}.";
		}

		$email_body = <<<EOMAIL
Dear {$order_record['name']},

Thank you for your recent order with {$cfg['site_name']}.

Just to let you know that "{$order_record['restaurant_name']}" have now confirmed to us that they have successfully received your order.
{$email_delivery_extra}
Kind Regards,

{$cfg['site_name']}
{$cfg['site_url']}

EOMAIL;

		$mail = new phpmailer();
		$mail->Mailer = $cfg['email_method'];
		$mail->From = $cfg['email_system_from'];
		$mail->Sender = $cfg['email_system_from'];
		$mail->FromName = $cfg['email_system_from'];
		$mail->Subject = "{$cfg['site_name']} Order Received";
		$mail->IsHTML(false);
		$mail->Body = $email_body;

		$email = $order_record['email'];

		/*
		if ($order_record['test']) {
			$email = $cfg['email_test'];
		}
		*/

		$name = preg_replace("%[^a-zA-Z0-9\ ]%", '', $order_record['name']);
		$mail->AddAddress($email, $name);

		if ($cfg['email_cc_order']) {
			$mail->AddCC($cfg['email_cc_order'], $cfg['site_name']);
		}

		if (!$mail->Send()) {
			throw new Exception("Unable to send email: {$mail->ErrorInfo}");
		}

		if ($cfg['sms_send']) {

			//Send SMS
			$send_sms = new send_sms();
			$status = $send_sms->send($send_sms->telno_uk_cc($order_record['telephone']), $sms_msg);
			if (!$status) {
				throw new Exception('Error sending SMS: ' . $send_sms->errormsg);
			}

		}

	}

}





/*

//		$select_sql .= "(SELECT logentry FROM {$tbl['notify_log']} WHERE {$tbl['notify_log']}.order_id = {$tbl['order']}.id ORDER BY logentry DESC LIMIT 0, 1) AS last_notify_action, ";

		$this->debuginfo(4, 'last_notify_action: ' . $order_record['last_notify_action']);

		//Check no last notify action, or last notify action longer than the retry limit
		if ($order_record['last_notify_action'] == null) {
			$oktoprocess = true;
		} else {

			$last_notify_action_ts = strtotime($order_record['last_notify_action'] . ' UTC');
			$check_ts = $last_notify_action_ts + $cfg['notify_phone_retry_time'];
			$now_ts = time();
			if ($check_ts < $now_ts) {
				$oktoprocess = true;
			} else {
				$oktoprocess = false;
			}

			$this->debuginfo(4, 'Check: ' . gmdate('Y-m-d H:i:s', $check_ts));
			$this->debuginfo(4, 'Now: ' . gmdate('Y-m-d H:i:s', $now_ts));

		}

		if ($oktoprocess) {

			$this->debuginfo(4, 'Ok, no last notfy or over limit');

			//Count previous number of "Attempt Restaurant Notify"
			$notify_log_result = $db->table_query($db->tbl($tbl['notify_log']), 'COUNT(*) AS total', $db->cond(array("status = 1", "order_id = {$order_id}"), 'AND'));
			if (!($notify_log_record = $db->record_fetch($notify_log_result))) {
				throw new Exception("Count did not return");
			}

			$this->debuginfo(4, 'Previous notify attempts: ' . $notify_log_record['total']);

			//Check have not exceeded notification attempts
			if ($notify_log_record['total'] < $cfg['notify_phone_try']) {

				$this->debuginfo(4, 'Under limit of ' . $cfg['notify_phone_try']);

				//Update notify status
				$db->record_insert($tbl['notify_log'], $db->rec(array('order_id' => $order_record['id'], 'status' => 1, 'logentry' => $db->datetimenow()))); //Status: Attempt Restaurant Notify

				//Update order status
				$db->record_update($tbl['order'], $db->rec(array('status' => 2)), $db->cond(array("id = {$order_record['id']}"), 'AND')); //Status: Processing

				//Prepare call message
				$message = $this->attempt_phone_notify_message($order_record);

				//Attempt phone notification
				$this->attempt_phone_notify_call($order_record, $message);

			} else {

				$this->debuginfo(4, "Limit of {$cfg['notify_phone_try']} notify attempts met");

				//Send email to customer
				$added_ts = strtotime($order_record['added'] . ' UTC');
				$diff = time() - $added_ts;

				$time_added = appgeneral::sec_to_dhms($diff);

				$email_body = <<<EOMAIL
Dear {$order_record['name']},

Thank you for your recent order with {$cfg['site_name']}.

Regrettably on this occasion despite {$cfg['notify_phone_try']} attempt(s) in the last {$time_added} we have been unable to make contact with the restaurant "{$order_record['restaurant_name']}" to fulfil your order.

We would like to apologise that this has happened and will follow up with the restaurant as to the reason in order to reduce the likelihood of it happening again.

If you like you could also try calling them directly on {$order_record['restaurant_telephone']}.

Kind Regards,

{$cfg['site_name']}
{$cfg['site_url']}

EOMAIL;

				$mail = new phpmailer();
				$mail->Mailer = $cfg['email_method'];
				$mail->From = $cfg['email_system_from'];
				$mail->Sender = $cfg['email_system_from'];
				$mail->FromName = $cfg['email_system_from'];
				$mail->Subject = "{$cfg['site_name']} Order Processing Error";
				$mail->IsHTML(false);
				$mail->Body = $email_body;

				$email = $order_record['email'];

				//if ($order_record['test']) {
				//	$email = $cfg['email_test'];
				//}

				$name = preg_replace("%[^a-zA-Z0-9\ ]%", '', $order_record['name']);
				$mail->AddAddress($email, $name);
				$mail->AddCC($cfg['email_admin'], $cfg['site_name']);

				if (!$mail->Send()) {
					throw new Exception("Unable to send email: {$mail->ErrorInfo}");
				}


				$telephone = $order_record['telephone'];

				//if ( ($order_record['test']) || ($cfg['devmode']) ) {
				//	$telephone = $cfg['telephone_test'];
				//}


				//Send SMS
				$send_sms = new send_sms();
				$sms_msg = "Regrettably we have been unable to contact the restaurant to fulfil your order, you could try calling them on {$order_record['restaurant_telephone']}";
				$status = $send_sms->send($send_sms->telno_uk_cc($telephone), $sms_msg);
				if (!$status) {
					throw new Exception('Error sending SMS: ' . $send_sms->errormsg);
				}


				//Update notify status
				$db->record_insert($tbl['notify_log'], $db->rec(array('order_id' => $order_record['id'], 'status' => 2, 'logentry' => $db->datetimenow()))); //Status: Attempt Restaurant Notify Limit Met

				//Update order status
				$db->record_update($tbl['order'], $db->rec(array('status' => 4)), $db->cond(array("id = {$order_record['id']}"), 'AND')); //Status: Error

			}

		} else {
			$this->debuginfo(4, 'Can not run notify processing, under limit');
		}


*/







/*




			$this->debuginfo(5, 'Error, unable to queue call');

			$email_body = <<<EOMAIL
Dear {$order_record['name']},

Thank you for your recent order with {$cfg['site_name']}.

Unfortunately we are currently experiencing technical issues and are unable to send your order to "{$order_record['restaurant_name']}".

We have received a notification of this error, and would like to apologise for any inconvenience caused.

If you like you could also try calling the restaurant directly on {$order_record['restaurant_telephone']} to place your order.

Kind Regards,

{$cfg['site_name']}
{$cfg['site_url']}

EOMAIL;

			$mail = new phpmailer();
			$mail->Mailer = $cfg['email_method'];
			$mail->From = $cfg['email_system_from'];
			$mail->Sender = $cfg['email_system_from'];
			$mail->FromName = $cfg['email_system_from'];
			$mail->Subject = "{$cfg['site_name']} Order Processing Error";
			$mail->IsHTML(false);
			$mail->Body = $email_body;

			$email = $order_record['email'];

			//if ($order_record['test']) {
			//	$email = $cfg['email_test'];
			//}

			$name = preg_replace("%[^a-zA-Z0-9\ ]%", '', $order_record['name']);
			$mail->AddAddress($email, $name);
			$mail->AddCC($cfg['email_admin'], $cfg['site_name']);

			if (!$mail->Send()) {
				throw new Exception("Unable to send email: {$mail->ErrorInfo}");
			}

			//Send SMS
			$send_sms = new send_sms();
			$sms_msg = "Regrettably due to technical issues we have been unable to process your order, you could try calling the restaurant on {$order_record['restaurant_telephone']}";
			$status = $send_sms->send($send_sms->telno_uk_cc($order_record['telephone']), $sms_msg);
			if (!$status) {
				throw new Exception('Error sending SMS: ' . $send_sms->errormsg);
			}

			if ($soap_request_status == false) {
				$status = 3;
			} else {
				$status = $result->NotifyPhoneAdvancedResult->ResponseCode+100;
			}

			$this->debuginfo(5, 'Error status is: ' . $status);

			//Update notify status
			//$db->record_insert($tbl['notify_log'], $db->rec(array('order_id' => $order_record['id'], 'status' => $status, 'logentry' => $db->datetimenow())));

			//Update order status
			//$db->record_update($tbl['order'], $db->rec(array('status' => 4)), $db->cond(array("id = {$order_record['id']}"), 'AND')); //Status: Error

			//throw new Exception("SOAP API error: {$result->NotifyPhoneAdvancedResult->ResponseText}");




*/




?>