<?php

class page_user_reset_password_verify {

	public $titletag = 'Update Password';
	public $pagetitle = 'Update Password';
	public $metadesc;
	public $body_html;
	public $headeraddin_html;
	public $footeraddin_html;
	public $mainnavsection;
	public $googanalyticspage;
	public $getdata;
	public $postdata;

	//-------------------------------------------------------------------------------------

	/*
	public function init() {
		global $db, $tbl;

	}
	*/

	//-------------------------------------------------------------------------------------

	public function handle() {
		global $auth;

		//Init data required by the page
		//$this->init();

		$status = $this->check_verify_code();

		if ($status) {

			//Password reset errors
			$errormsg_html = $this->errormsg_html();

			if ( (isset($this->postdata['formposted'])) && (!count($errormsg_html)) ) {

				$this->process_form();

				$page_html = $this->reset_success_html();

			} else {
				$page_html = $this->reset_form_html($errormsg_html);
			}

		} else {

			$page_html = <<<EOHTML
<p><strong>Error:</strong> Revisa tu correo y da clik sobre el link.</p>
EOHTML;

		}

		$pagetitle_h = htmlentities($this->pagetitle);

		$body_html = <<<EOHTML

<h1>{$pagetitle_h}</h1>

{$page_html}

EOHTML;

		//Template
		$template = new template();
		$template->settitle($this->titletag);
		//$template->setmetadesc($metadesc);
		$template->setmainnavsection($this->mainnavsection);
		$template->setgooganalyticspage($this->googanalyticspage);
		$template->setheaderaddinhtml($this->headeraddin_html);
		$template->setfooteraddinhtml($this->footeraddin_html);
		$template->setbodyhtml($body_html);
		//$template->setshowsearch(true);
		//$template->setshoworderprocess(true);
		$template->display();

	}


	//-------------------------------------------------------------------------------------

	protected function reset_form_html($errormsg_html) {
		global $cfg;

		$link_base_path = htmlentities(navfr::base_path());

		$errormsgall_html = appgeneral::errormsgs_html($errormsg_html);

		$form_self = navfr::self_h(array('v' => $this->getdata['v']));

		$html = <<<EOHTML

<form method="post" action="{$form_self}">
	<div><input type="hidden" name="formposted" value="1" /></div>

	<div class="standardform">

{$errormsgall_html}

		<div class="group">
			<div class="fieldtitle"><label for="new_password">New Password</label></div>
			<div class="fieldinput"><input type="password" name="new_password" id="new_password" value="" class="inputtxt" /></div>
			<div class="clear"></div>
		</div>

		<div><input type="image" src="{$link_base_path}{$cfg['theme_resources_path']}user_reset_password_verify/submit.gif" alt="Update Password" name="send" class="submit" /></div>

	</div>

</form>

EOHTML;

		return $html;

	}

	protected function check_verify_code() {
		global $db, $tbl;

		$customer_result = $db->table_query($db->tbl($tbl['customer']), $db->col(array('id')), $db->cond(array("verify_code = '".$db->es($this->getdata['v'])."'"), 'AND'), '', 0, 1);
		if ($customer_record = $db->record_fetch($customer_result)) {
			$status = true;
		} else {
			$status = false;
		}

		return $status;

	}

	protected function errormsg_html() {
		global $db, $tbl;

		$errormsg_html = array();

		if (isset($this->postdata['formposted'])) {

			//Check password
			if ($this->postdata['new_password']) {
				if (strlen($this->postdata['new_password']) < 8) {
					$errormsg_html[] = "'Nuevo Password' dera ser mas de 8 caracteres";
				}
			} else {
				$errormsg_html[] = "'Password' Escribe password";
			}

		}

		return $errormsg_html;

	}

	public function reset_success_html() {

		$html = <<<EOHTML

<p>Tu password ha sido cambiado.</p>

EOHTML;

		return $html;

	}

	protected function process_form() {
		global $db, $tbl, $auth;

		//Retrieve customer id
		$customer_result = $db->table_query($db->tbl($tbl['customer']), '*', $db->cond(array("verify_code = '".$db->es($this->getdata['v'])."'"), 'AND'), '', 0, 1);
		if (!($customer_record = $db->record_fetch($customer_result))) {
			throw new Exception("Customer not found for verify code \"{$this->getdata['v']}\"");
		}

		$password = md5($this->postdata['new_password']);

		//Update customer record with new password
		$db->record_update($tbl['customer'], $db->rec(array('password' => $password)), $db->cond(array("id = {$customer_record['id']}"), 'AND'));

		//Login user with new details
		$auth->login_success = true;
		$auth->authinfo = $customer_record;
		$auth->authinfo['password'] = $password;

		$auth->setuser($auth->authinfo['email'], $auth->authinfo['password']);
		$auth->cookie_save();

	}

}

?>