<?php
setlocale(LC_ALL, 'es_AR');

class page_takeaway_menu {

	protected $titletag;
	//protected $pagetitle;
	public $metadesc;
	public $body_html;
	public $headeraddin_html;
	public $footeraddin_html;
	public $mainnavsection;
	public $googanalyticspage;
	public $getdata;
	public $postdata;

	protected $restaurant_record;
	protected $jump_to_section_html;
	protected $menu_html;
	protected $city_data;
	protected $town_data;

	public function init() {
		global $db, $tbl;

		$current_path = navfr::current_path();

		if (!( (isset($current_path[1])) && (isset($current_path[2])) && (isset($current_path[3])) )) {
			throw new Exception('Somehow reached this page without city, town, restaurant specified');
		}

		//Resolve city data
		$this->city_data = appgeneral::city_from_navname($current_path[1]);

		//If city not found
		if ($this->city_data == false) {
			template_lib::show_404();
			exit;
		}

		//Resolve town data
		$this->town_data = appgeneral::town_from_navname($this->city_data['id'], $current_path[2]);

		//If town not found
		if ($this->town_data == false) {
			template_lib::show_404();
			exit;
		}


		//Retrieve restaurant data

		$select_sql = '';
		$select_sql .= "(SELECT {$tbl['town']}.name FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id) AS town, ";
		$select_sql .= "(SELECT {$tbl['city']}.name FROM {$tbl['city']} WHERE {$tbl['city']}.id = (SELECT {$tbl['town']}.city_id FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id)) AS city";

		$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $db->col(array('id', 'name', 'description', 'address_1', 'address_2', 'postcode', 'del_radius_mi', 'min_order_value', 'delivery_charge', 'handling_delivery', 'handling_collection', 'msg_dispatch', 'navname')) . ', ' . $select_sql, $db->cond(array("navname='".$db->es($current_path[3])."'", "town_id = {$this->town_data['id']}", "status = 1"), 'AND'), '', 0, 1);
		if (!($restaurant_record = $db->record_fetch($restaurant_result))) {

			//If restaurant not found
			template_lib::show_404();
			exit;

		}

		$this->restaurant_record = $restaurant_record;

	}

	//-------------------------------------------------------------------------------------

	public function handle() {
		global $cfg;

		//Init data required by the page
		$this->init();

		//Generate menu html
		$this->menulisting_html();

		$link_base_path = htmlentities(navfr::base_path());

		//Generate basket items JavaScript
		$basket_items_js = $this->basket_items_js();

		//Breadcrumbs
		$breadcrumbs = array();
		$breadcrumbs[] = array('link' => navfr::link(array('restaurant')),'name' => 'Restaurantes');
		$breadcrumbs[] = array('link' => navfr::link(array('restaurant', $this->city_data['navname'])),'name' => $this->city_data['name']);
		$breadcrumbs[] = array('link' => navfr::link(array('restaurant', $this->city_data['navname'], $this->town_data['navname'])),'name' => $this->town_data['name']);
		$breadcrumbs[] = array('link' => navfr::link(array('restaurant', $this->city_data['navname'], $this->town_data['navname'], $this->restaurant_record['navname'])),'name' => $this->restaurant_record['name']);

		//$breadcrumbs_html = appgeneral::breadcrumbs($breadcrumbs);

		$page_title = "hola";
		$metadesc = $this->restaurant_record['description'];


		$restaurant_recordh = lib::htmlentities_array($this->restaurant_record);

		$address = $restaurant_recordh['address_1'];
		if ($restaurant_recordh['address_2']) {
			$address .= ", " . $restaurant_recordh['address_2'];
		}

		$h1_title = "{$this->restaurant_record['name']} - {$this->restaurant_record['town']}, {$this->restaurant_record['city']}";

		$h1_titleh = htmlentities($h1_title);

		$description_h = appgeneral::text_to_paragraphs_html($this->restaurant_record['description']);

		$form_post_link = navfr::link_h(array('restaurant', $this->city_data['navname'], $this->town_data['navname'], $this->restaurant_record['navname'], 'order'));

     setcookie("url", $form_post_link, time()+1800,"/");  /* expire in 1 hour */
		if ($this->restaurant_record['delivery_charge']) {
			$delivery_charge = number_format($this->restaurant_record['delivery_charge'], 2);
		} else {
			$delivery_charge = '0.00';
		}

		//Retrieve current restaurant open/closed status
		$restaurant_open_status = appgeneral::restaurant_open($this->restaurant_record['id']);

		if ($restaurant_open_status) {
			$status_html = <<<EOHTML
		<span class="open">Abierto</span>
EOHTML;
		} else {
			$status_html = <<<EOHTML
		<span class="close">Cerrado</span>
EOHTML;
		}

		$theme_resources_path_js = addslashes($link_base_path . $cfg['theme_resources_path']);

		if (file_exists("{$cfg['restaurant_dir_path']}{$this->restaurant_record['id']}/logo.png")) {
			$logo_type = $restaurant_recordh['id'];
		} else {
			$logo_type = "default";
		}

		//Handling accepted
		$handling_accept = array();

		if ($this->restaurant_record['handling_delivery']) {
			$handling_accept[] = 1; //delivery
		}

		if ($this->restaurant_record['handling_collection']) {
			$handling_accept[] = 2; //collection
		}

		//Handling accepted to javascript
		$handling_accept_js = '';
		foreach ($handling_accept as $accept) {
			$handling_accept_js .= $accept . ',';
		}

		$handling_accept_js = rtrim($handling_accept_js, ',');

		//Retrieve handling specified, or use default handling
		$handling = appgeneral::retrieve_order_handling($this->restaurant_record['id']);

		/*
		<div class="breadcrumbstitle">

			{$breadcrumbs_html}

			<h1>{$h1_titleh}</h1>

		</div>
		*/

		$body_html = <<<EOHTML

<h1>{$h1_titleh}</h1>

<div class="restaurant_info">

	<div class="content">

		<img src="{$link_base_path}{$cfg['restaurant_dir_path']}{$logo_type}/logo.png" width="87" height="87" alt="{$restaurant_recordh['name']} Logo" class="logo" />

		<div class="info">

			<div class="address">{$address}, {$restaurant_recordh['town']}, {$restaurant_recordh['city']}, {$restaurant_recordh['postcode']}</div>
			<div class="status"><strong>Status:</strong> {$status_html}</div>
			<div class="clear"></div>

			<div class="description">
				{$description_h}
			</div>

		</div>

		<div class="clear"></div>

	</div>

</div>

<div class="menu" id="restaurant_menu">
	<div class="menu-top">
		<div class="menu-bottom">
			<div class="content">

				<h2>Menu</h2>

{$this->jump_to_section_html}

{$this->menu_html}

			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
  <!--

	var restaurant_id = {$this->restaurant_record['id']};

	var delivery_charge = {$delivery_charge};

	var min_order_value = {$this->restaurant_record['min_order_value']};

	var basket_items = {
{$basket_items_js}
	};

	var handling = {$handling};

	var handling_accept = [{$handling_accept_js}];

	var theme_resources_path = "{$theme_resources_path_js}";

  //-->
</script>

EOHTML;

		if ($this->restaurant_record['msg_dispatch'] > 1) {
			//Menu basket
			$menu_basket = new menu_basket();
			$menu_basket->restaurant_id = $this->restaurant_record['id'];
			$menu_basket->init();
			$menubasketaddin_html = $menu_basket->html();
		} else {
			$menubasketaddin_html = '';
		}

		//Retaurant hours
		$hoursaddin_html = $this->restaurant_open_times_html();

		$this->handle_title();
		$this->handle_metadesc();
		$this->handle_headeraddin_html();
		$this->handle_footeraddin_html();
		$this->handle_mainnavsection();
		$this->handle_googanalyticspage();

		//Template
		$template = new template();
		$template->settitle($this->titletag);
		$template->setmetadesc($this->metadesc);
		$template->setmainnavsection($this->mainnavsection);
		$template->setgooganalyticspage($this->googanalyticspage);
		$template->setheaderaddinhtml($this->headeraddin_html);
		$template->setfooteraddinhtml($this->footeraddin_html);
		$template->setbodyhtml($body_html);
		$template->setmenubasketaddinhtml($menubasketaddin_html);
		$template->sethoursaddinhtml($hoursaddin_html);
		$template->setbreadcrumbs($breadcrumbs);
		$template->display();

	}

	protected function handle_title() {
		$this->titletag = "Gettyng.com - Restaurante - {$this->restaurant_record['name']}";
	}

	protected function handle_headeraddin_html () {
		global $cfg;

		$link_base_path = htmlentities(navfr::base_path());
		$this->headeraddin_html = <<<EOHTML
<script src="{$link_base_path}{$cfg['theme_resources_path']}takeaway_menu/takeaway_menu.js" type="text/javascript"></script>
EOHTML;

	}

	protected function handle_footeraddin_html () {
	}

	protected function handle_mainnavsection() {
		$this->mainnavsection = 'takeaway';
	}

	protected function handle_googanalyticspage() {
	}

	protected function handle_metadesc() {
		$this->metadesc = $this->restaurant_record['description'];
	}

	//-------------------------------------------------------------------------------------

	//Restaurant open times
	protected function restaurant_open_times_html() {
		global $cfg;
setlocale(LC_ALL, 'es_AR');

		//Retrieve opening times
		$opentimes = appgeneral::restaurant_open_time($this->restaurant_record['id']);

		$restaurant_open_times_html = '';
		$row = 'b';
		foreach ($cfg['restaurant_open_days'] as $dayno => $dayname) {

			if (isset($opentimes[$dayno])) {

				$open = date('g:i a', $opentimes[$dayno]['open']);
				$close = date('g:i a', $opentimes[$dayno]['close']);

				$time = <<<EOHTML
{$open} - {$close}
EOHTML;
			} else {
				$time = 'Cerrado';
			}

			$row = ($row == 'b') ? 'a' : 'b';
			$restaurant_open_times_html .= <<<EOHTML
		<div class="day day-{$row}">
			<div class="dayname">{$dayname}</div>
			<div class="time">{$time}</div>
			<div class="clear"></div>
		</div>

EOHTML;

		}

		if ($restaurant_open_times_html) {

			if ($this->restaurant_record['del_radius_mi'] >= 10) {
				$delivery_area = round($this->restaurant_record['del_radius_mi']);
			} else {
				$delivery_area = 1.6*($this->restaurant_record['del_radius_mi']);
				
			}

			$restaurant_open_times_html = <<<EOHTML

<div class="restaurant_open_times">

	<div class="content">

		<h2>Horario de Servicio</h2>

		<div class="delivery_area">Areaservicio<span class="area">{$delivery_area} km</span></div>

		<div class="clear"></div>

{$restaurant_open_times_html}

	</div>

</div>

EOHTML;

		}

		return $restaurant_open_times_html;

	}

	//Generate menu listing html
	protected function menulisting_html() {
		global $cfg;

		$link_base_path = htmlentities(navfr::base_path());

		//Retrieve restaurant menu
		$restaurant_menu = appgeneral::retrieve_menu($this->restaurant_record['id']);

		//Process menu into html
		$menu_html = '';
		$jump_to_section = '';
		$row = 'b';
		foreach ($restaurant_menu as $cat_id => $category) {

			//Go through each item in category
			$menu_item_html = '';
			foreach ($category['menu_item'] as $item_id => $item) {

				//If item has a cost, or has sub items, ok to list it
				if ( ($item['cost']) || (count($item['sub_menu_item'])) ) {

					$item_no = htmlentities($item['no']);
					$item_nameh = htmlentities($item['name']);

					$item_desc = htmlentities($item['description']);
					$item_desc = nl2br($item_desc);

					//If there are sub items
					if (count($item['sub_menu_item'])) {

						//Go through all sub items
						$sub_item_html = '';
						foreach ($item['sub_menu_item'] as $subitem_id => $subitem) {

							$subitem_nameh = htmlentities($subitem['name']);

							$add_item_name = "{$item['name']} {$subitem['name']}";
							$add_item_name_s = addslashes($add_item_name);
							$add_item_name_s = htmlentities($add_item_name_s);

							if ($this->restaurant_record['msg_dispatch'] > 1) {
								$additem_html = <<<EOHTML
<a href="#" onclick="menu_item_add(restaurant_id, {$subitem_id}, '{$add_item_name_s}', {$subitem['cost']}); return false;"><img src="{$link_base_path}{$cfg['theme_resources_path']}takeaway_menu/menu/add_item_btn.gif" width="13" height="13" alt="Add Item To Order" /></a>
EOHTML;
							} else {
								$additem_html = '';
							}

							$sub_item_html .= <<<EOHTML
				<div class="subitem">
					<div class="subitem_name">{$subitem_nameh}</div>
					<div class="subitem_price">{$subitem['cost']}</div>
					<div class="additem">{$additem_html}</div>
					<div class="clear"></div>
				</div>

EOHTML;

						}

					} else {

						$add_item_name = $item['name'];
						$add_item_name_s = addslashes($add_item_name);
						$add_item_name_s = htmlentities($add_item_name_s);

						if ($this->restaurant_record['msg_dispatch'] > 1) {
							$additem_html = <<<EOHTML
<a href="#" onclick="menu_item_add(restaurant_id, {$item_id}, '{$add_item_name_s}', {$item['cost']}); return false;"><img src="{$link_base_path}{$cfg['theme_resources_path']}takeaway_menu/menu/add_item_btn.gif" width="13" height="13" alt="Add Item To Order" /></a>
EOHTML;
						} else {
							$additem_html = '';
						}

						$sub_item_html = <<<EOHTML
				<div class="subitem">
					<div class="subitem_name"></div>
					<div class="subitem_price">{$item['cost']}</div>
					<div class="additem">{$additem_html}</div>
					<div class="clear"></div>
				</div>

EOHTML;

					}

					$row = ($row == 'b') ? 'a' : 'b';

					$menu_item_html .= <<<EOHTML
		<li class="item item-{$row}">
			<div class="no">{$item_no}</div>
			<div class="namedesc">
				<h4>{$item_nameh}</h4>
				<div class="desc">{$item_desc}</div>
			</div>
			<div class="iteminfo">
{$sub_item_html}
			</div>
			<div class="clear"></div>
		</li>

EOHTML;

				}

			}

			$menu_item_html = rtrim($menu_item_html);

			if ($menu_item_html) {

				$section_navname = $category['name'];
				$section_navname = strtolower($section_navname);
				$section_navname = str_replace(' ', '-', $section_navname);
				$section_navname = preg_replace("%[^a-z0-9\-]%", '', $section_navname);

				$category_name = htmlentities($category['name']);

				$jump_to_section .= <<<EOHTML
<li><a href="#section-{$section_navname}" onclick="jump_section('{$section_navname}'); return false;">{$category_name}</a>, </li>
EOHTML;

				$menu_html .= <<<EOHTML

<div class="category">

	<h3 id="section-{$section_navname}">{$category_name}</h3>

	<ul class="menu_item_list">
{$menu_item_html}
	</ul>

</div>
<div id="detalle-{$section_navname}">
</div>
EOHTML;

			}

		}



		//If have categories specified for jump to section
		if ($jump_to_section) {

			$jump_to_section = substr($jump_to_section, 0, -7);
			$jump_to_section .= '</li>';

		}

		$jump_to_section = <<<EOHTML

<div id="jump_to_section_container" class="jump_to_section_container">

	<div id="jump_to_section_top"></div>

	<div class="jump_to_section" id="jump_to_section" style="z-index: 1000";>
		<div class="jump_to_section-bottom">
			<div class="content">
				<div class="title">Selecion Comida:</div>

				<ul class="sections_list">
{$jump_to_section}
				</ul>

				<div class="clear"></div>

			</div>
		</div>
	</div>

</div>

EOHTML;

		$this->jump_to_section_html = $jump_to_section;
		$this->menu_html = $menu_html;

	}

	//Generate basket items JavaScript
	protected function basket_items_js() {

		//Retrieve menu items saved in cookie
		$menu_cookie = appgeneral::menu_cookie();

		//Produce JavaScript array with basket menu items
		$basket_items_js = '';
		foreach ($menu_cookie['menu_items'] as $item_id => $item) {

			//Retrieve menu item data
			$menu_item_data = appgeneral::menu_item_data($item_id);
			if ($menu_item_data) {

				$item_name_s = $menu_item_data['name'];
				$item_name_s = addslashes($item_name_s);

				$basket_items_js .= <<<EOJS
	{$item_id}: {
		"restaurant_id": {$menu_item_data['restaurant_id']},
		"name": '{$item_name_s}',
		"cost": {$menu_item_data['cost']},
		"qty": {$item['qty']}
	},

EOJS;

			}

		}

		$basket_items_js = rtrim($basket_items_js, "\r\n,");

		return $basket_items_js;

	}

}

?>