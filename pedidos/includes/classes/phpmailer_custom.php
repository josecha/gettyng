<?php

class phpmailer_custom extends phpmailer {

	function AddStringEmbeddedImage($string, $cid, $name = "", $encoding = "base64", $type = "application/octet-stream") {

		// Append to $attachment array
		$cur = count($this->attachment);
		$this->attachment[$cur][0] = $string;
		$this->attachment[$cur][1] = $name;
		$this->attachment[$cur][2] = $name;
		$this->attachment[$cur][3] = $encoding;
		$this->attachment[$cur][4] = $type;
		$this->attachment[$cur][5] = true;
		$this->attachment[$cur][6] = "inline";
		$this->attachment[$cur][7] = $cid;

		return true;

	}

}

?>