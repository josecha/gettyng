<?php

class template_lib {

	//Show 404
	static function show_404() {
		global $cfg;

		$body_html = <<<EOHTML
<h1>Error 404</h1>

<em>Pagina no encontrada.</em>

<br />
<br />

<script type="text/javascript">
  var GOOG_FIXURL_LANG = 'es';
  var GOOG_FIXURL_SITE = '{$cfg['site_url']}'
</script>
<script type="text/javascript"
  src="http://linkhelp.clients.google.com/tbproxy/lh/wm/fixurl.js">
</script>

EOHTML;

		header('HTTP/1.0 404 Not Found');
		//header('Status: 404 Not Found');

		$template = new template();
		//$template->settitle('');
		$template->setbodyhtml($body_html);
		$template->display();

	}

	//Bredcrumb navigation
	static public function breadcrumbs($breadcrumbs) {

		/*
		$breadcrumbs = array();
		$breadcrumbs[] = array('link' => navfr::link(array('')),'name' => '')
		$breadcrumbs_html = appgeneral::breadcrumbs($breadcrumbs)
		*/

		$breadcrumbs_html = '';
		foreach ($breadcrumbs as $breadcrumb) {

			$linkh = htmlentities($breadcrumb['link']);
			$nameh = htmlentities($breadcrumb['name']);

			$breadcrumbs_html .= <<<EOHTML
<a href="{$linkh}">{$nameh}</a> &gt; 
EOHTML;

		}

		$breadcrumbs_html = preg_replace('/'.preg_quote(' &gt; ', '/').'$/', '', $breadcrumbs_html);

		$breadcrumbs_html = <<<EOHTML
<div class="breadcrumbs">{$breadcrumbs_html}</div>
EOHTML;

		return $breadcrumbs_html;

	}

	//Popular restaurants
	static public function popular_restaurants_html() {
		global $db, $tbl;

		$cond = array("status = 1");

		//Retrieve popular restaurants
		$popular_restaurants = '';
		$select_sql = $db->col(array('id', 'name', 'postcode', 'navname')) . ', ';
		//$select_sql .= "(SELECT {$tbl['town']}.name FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id) AS town, ";
		$select_sql .= "(SELECT {$tbl['city']}.name FROM {$tbl['city']} WHERE {$tbl['city']}.id = (SELECT {$tbl['town']}.city_id FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id)) AS city, ";
		$select_sql .= "(SELECT navname FROM {$tbl['town']} WHERE id = town_id) AS town_navname, ";
		$select_sql .= "(SELECT {$tbl['city']}.navname FROM {$tbl['city']} WHERE {$tbl['city']}.id = (SELECT {$tbl['town']}.city_id FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id)) AS city_navname, ";
		$select_sql .= "(SELECT COUNT(*) FROM {$tbl['order']} WHERE {$tbl['order']}.restaurant_id = {$tbl['restaurant']}.id) AS total_orders";
		$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $select_sql, $db->cond($cond, 'AND'), $db->order(array(array('total_orders', 'DESC'))), 0, 3);
		while ($restaurant_record = $db->record_fetch($restaurant_result)) {

			$restaurant_record_h = lib::htmlentities_array($restaurant_record);

			$postcode = explode(' ', $restaurant_record['postcode']);

			$info = <<<EOHTML
{$restaurant_record['city']} {$postcode[0]}
EOHTML;

			$name_h = htmlentities(appgeneral::trim_length($restaurant_record['name'], 20));
			$info_h = htmlentities(appgeneral::trim_length($info, 20));

			$link = navfr::link_h(array('restaurant', $restaurant_record['city_navname'], $restaurant_record['town_navname'], $restaurant_record['navname']));

			$popular_restaurants .= <<<EOHTML
			<li><a href="{$link}">{$name_h}</a> {$info_h}</li>

EOHTML;

		}

		if ($popular_restaurants) {

			$popular_restaurants = <<<EOHTML

<div class="popular_takeaways">

	<div class="info">

		<h2>Lo mas pedido de la semana</h2>

		<ol>

{$popular_restaurants}

		</ol>

	</div>

</div>

EOHTML;

		}

		return $popular_restaurants;

	}

	//Takeaway search
	static public function restaurant_search_html() {
		global $cfg;

		$link_base_path = htmlentities(navfr::base_path());

		$link = navfr::link_h(array('restaurant', 'search'));

		$postcode_search = '';
		$postcode_searchh = htmlentities($postcode_search);

		$link_takeaway = navfr::link_h(array('restaurant'));

		$search_html = <<<EOHTML

			<div class="postcode_search">

				<h2>Hambre? pide ya!</h2>

				<form method="post" action="{$link}">
					<div><label for="postcode_search" class="label"><span>Post<br />Code</span></label></div>
					<div><input type="text" name="postcode_search" id="postcode_search" value="{$postcode_searchh}" class="postcodebox" /></div>
					<div><input type="image" src="{$link_base_path}{$cfg['theme_resources_path']}template/sidepanel/postcode_search/go.gif" alt="Go" class="go" /></div>
				</form>

				<div class="info">
					<p>Pon tu codigo postal para ver restaurantes en tu zona <a href="{$link_takeaway}">mostrar todos los restaurantes</a>.</p>
				</div>

			</div>

EOHTML;

		return $search_html;

	}

	//Google Analytics
	static public function google_analytics_html($googanalytics_page) {
		global $cfg;

		if ( ($cfg['devmode'] == false) && ($cfg['google_analytics_acct_id']) ) {

			$googanalytics_page_js = addslashes($googanalytics_page);

			$analytics_code_html = <<<EOHTML

<!--// Google Analytics //-->

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("{$cfg['google_analytics_acct_id']}");
pageTracker._trackPageview({$googanalytics_page_js});
} catch(err) {}</script>

EOHTML;

		} else {
			$analytics_code_html = '';
		}

		return $analytics_code_html;

	}

	//Header navigation
	static public function header_navigation_html($links, $mainnavsection) {

		$nav_header_html = '';
		$i = 0;
		foreach ($links as $id => $link) {

			$current = ($mainnavsection == $id) ? ' current' : '';
			$name = htmlentities($link['name']);
			$nofollow_html = ( (isset($link['nofollow'])) && ($link['nofollow']) ) ? ' rel="nofollow"' : '';
			$nav_header_html .= <<<EOHTML
				<li class="item"><a href="{$link['link']}" class="nav-{$id}{$current}"{$nofollow_html}>{$name}</a></li>

EOHTML;

			/*
			if ($i+1 != count($links)) {
			$nav_header_html .= <<<EOHTML
				<li class="sep"></li>

EOHTML;
			}
			$i++;
			*/

		}

		return $nav_header_html;

	}

	//Generate meta tags
	static public function metatags_html($metatags) {

		$metatags_html = '';
		foreach ($metatags as $name => $value) {

			if ($value) {

				$value_h = htmlentities($value);

				$metatags_html .= <<<EOHTML
<meta name="{$name}" content="{$value_h}" />

EOHTML;

			}

		}

		return $metatags_html;

	}

	//Display standard page headers
	static public function display_headers() {
		header("Content-Type: text/html; charset=ISO-8859-1");
	}

	//Display page (with compression if possible)
	static public function display($page_html) {
		global $cfg;

		//If browser supports compressed encoding, and not in dev mode, and is enable in config then use it
		if ( (isset($_SERVER['HTTP_ACCEPT_ENCODING'])) && (strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== false) && ($cfg['devmode'] == false) && ($cfg['output_compressions'] == true) ) {

			header("X-Compression: gzip");
			header("Content-Encoding: gzip");

			echo gzencode($page_html);

		} else {
			echo $page_html;
		}

	}

}

?>