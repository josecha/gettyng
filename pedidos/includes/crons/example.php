<?php

error_reporting(E_ALL | E_STRICT);

$includes_path = '../includes/';

include $includes_path . 'config.php';
include $includes_path . 'general/init.php';

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

$runbycron = new runbycron();
$runbycron->run();
$runbycron->debughandler();


class runbycron {

	const DEBUGTYPE_ERROR = 1;
	const DEBUGTYPE_STATUS = 2;

	public $fatalshandledinternally = true;
	public $showdebugging = true;

	//Run
	function run() {
		global $db;

		$this->debuginfo(self::DEBUGTYPE_STATUS, 0, 'Running @ ' . date('Y-m-d H:i:s') . "\n");

		try {




		} catch (Exception $e) {
			$this->debuginfo(self::DEBUGTYPE_ERROR, 0, 'Exception: ' . $e->getMessage());
		}

	}

	//Save debug message
	function debuginfo($debug_type, $debug_level, $debug_message) {

		//Save arguments to array
		$this->debuginfomsgs[] = func_get_args();

		$spaceaddin = str_repeat(' ', $debug_level);

		if ($this->showdebugging == true) {
			$spaceaddin = str_repeat(' ', $debug_level);
			echo "{$spaceaddin}{$debug_message}\n";
		}

	}

	//Handle debug messages
	function debughandler() {

		$debuginfotext = '';
		$fatalerror = false;
		foreach ($this->debuginfomsgs as $debugitem) {
			$spaceaddin = str_repeat(' ', $debugitem[1]);

			if ($debugitem[0] == self::DEBUGTYPE_ERROR) {
				$fatalerror = true;
				$fatalerroraddin = '#';
			} else {
				$fatalerroraddin= '';
			}

			$debuginfotext .= "{$spaceaddin}{$fatalerroraddin}{$debugitem[2]}\n";
		}

		if ($debuginfotext) {

			//If fatals should be handled internally / reported
			if ($this->fatalshandledinternally == true) {

				//Echo out debugging information
				//echo $debuginfotext;

				//If there was a fatal error reported
				if ($fatalerror == true) {

					//Fatal error...
					//...

				}

			} else {

				//Otherwise fatals should be rethrown, and handled by the application higher up the chain

				//If there was a fatal error reported
				if ($fatalerror == true) {

					//ReThrow (but not properly) exception (error)
					throw new Exception($debuginfotext);

				}

			}

		}

	}


}

?>