<?php

include $includes_path . 'config.php';
include $cfg['userdata_path'] . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$admin_auth = new admin_auth();
$admin_auth->handle();
$authinfo = $admin_auth->getauthinfo();
$admin_auth->login_required();
$admin_auth->check_permission();


$category_id = intval($_GET['category_id']);

//Retrieve restaurant id for specified category
$menu_cat_result = $db->table_query($db->tbl($tbl['menu_cat']), $db->col(array('restaurant_id')), $db->cond(array("id = {$category_id}"), 'AND'), '', 0, 1);
if (!($menu_cat_record = $db->record_fetch($menu_cat_result))) {
	throw new Exception("Category id \"{$restaurant_id}\" not found");
}

$restaurant_id = $menu_cat_record['restaurant_id'];

//Chcek have permission for this restaurant id
$admin_auth->check_permission_restaurant($restaurant_id);


//Delete restaurant
if ( (isset($_POST['actiontype'])) && ($_POST['actiontype'] == 'delete') ) {

	$db->record_delete($tbl['menu_item'], $db->cond(array("id = ".intval($_POST['actionid'])), 'AND'));

	$db->record_update($tbl['restaurant'], $db->rec(array('lastupdated' => $db->datetimenow())), $db->cond(array("id = {$restaurant_id}"), 'AND'));

}

$link_addnew = navpd::forward(array('p' => 'menu_item_addedit', 'category_id' => $category_id));
$btn_addnew = btn::create('Add Item', btn::TYPE_LINK, $link_addnew, $cfg['btn_template_path'].'icons/add.png', '', 'Add Item');
$right_html = $btn_addnew;

$left_html = btn::create('Back', btn::TYPE_LINK, navpd::back(), $cfg['btn_template_path'].'icons/back.png', '', 'Back');

//Table
$tablehtml = new tablehtml();
$tablehtml->shotbuttoncol = true;
$tablehtml->sortby = 'name';
$tablehtml->sortdir = 'asc';
//$tablehtml->table_class = 'examplelist';
$tablehtml->sortable_columns = array('no', 'name', 'cost');
$tablehtml->parsegetvars($_GET);

//$tablehtml->addcolumn('checkbox', '');
$tablehtml->addcolumn('no', 'No.');
$tablehtml->addcolumn('name', 'Name');
$tablehtml->addcolumn('description', 'Description');
$tablehtml->addcolumn('cost', 'Cost ('.chr(163).')');
$tablehtml->addcolumn('button', '');

$table_html = $tablehtml->html(
	$tablehtml->html_action(),
	$tablehtml->html_table(
		$tablehtml->html_table_titles(),
		$tablehtml->html_table_rows(
			$tablehtml->tabledatahtml_fromcallback('callback_tabledatahtml')
		),
		$tablehtml->html_table_nav($right_html, $left_html),
		$tablehtml->html_table_errors()
	)
);

function callback_tabledatahtml($tablehtml, $limit_offset, $limit_count, $query_order) {
	global $cfg, $tbl, $db, $category_id;

	$select_sql = $db->col(array('id', 'no', 'name', 'description', 'cost'));
	$result = $db->table_query($db->tbl($tbl['menu_item']), $select_sql, $db->cond(array("menu_cat_id = {$category_id}", "sub_menu_item_id IS NULL"), 'AND'), $db->order($query_order), $limit_offset, $limit_count, dbmysql::TBLQUERY_FOUNDROWS);

	$tabledatahtml = array();
	while ($data = $db->record_fetch($result)) {

		$datah = lib::htmlentities_array($data);

		//Delete button
		$name_js = addslashes($datah['name']);
		$link_self = addslashes(navpd::self());
		$onclick_js = "performaction('{$link_self}', 'Really delete menu item \'{$name_js}\'?', 'delete', {$data['id']}); return false;";
		$btn_delete = btn::create('Delete', btn::TYPE_LINK, '#', $cfg['btn_template_path'].'icons/delete.png', $onclick_js, "Delete menu item \"{$name_js}\"");

		//Edit button
		$link_edit = navpd::forward(array('p' => 'menu_item_addedit', 'editid' => $data['id']));
		$btn_edit = btn::create('Edit', btn::TYPE_LINK, $link_edit, $cfg['btn_template_path'].'icons/edit.png', '', 'Edit');

		$buttons = $tablehtml->html_table_buttons(array($btn_edit, $btn_delete));

		//If sub items
		if ($data['cost'] === null) {

			$submenu_items_html = '<strong>Sub Item(s):</strong><br />';

			//Retrieve sub items
			$sub_menu_item_result = $db->table_query($db->tbl($tbl['menu_item']), $db->col(array('name', 'cost')), $db->cond(array("sub_menu_item_id = {$data['id']}"), 'AND'), 'name + 0 ASC');
			while ($sub_menu_item_record = $db->record_fetch($sub_menu_item_result)) {
				$submenu_items_html .= htmlentities($sub_menu_item_record['name']) . ' @ &pound;' . $sub_menu_item_record['cost'] . '<br />';
			}

			if ($data['description']) {
				$submenu_items_html = $submenu_items_html . '<br />';
			}

		} else {
			$submenu_items_html = '';
		}

		$tabledatahtml[] = array(
			'no' => htmlentities(appgeneral::trim_length($data['no'], 5)),
			'name' => htmlentities(appgeneral::trim_length($data['name'], 30)),
			'description' => $submenu_items_html . nl2br(htmlentities(appgeneral::trim_length($data['description'], 60))),
			'cost' => ($data['cost']) ? number_format($data['cost'], 2) : 'N/A',
			'button' => $buttons,
		);

	}

	$tablehtml->paging_totrows = $db->query_foundrows();

	return $tabledatahtml;

}

//Retrieve restaurant information for specified category
$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $db->col(array('name')), $db->cond(array("id = {$restaurant_id}"), 'AND'), '', 0, 1);
if (!($restaurant_record = $db->record_fetch($restaurant_result))) {
	throw new Exception("Restaurant id \"{$restaurant_id}\" not found");
}

//Retrieve category information for specified category
$menu_cat_result = $db->table_query($db->tbl($tbl['menu_cat']), $db->col(array('name')), $db->cond(array("id = {$category_id}"), 'AND'), '', 0, 1);
if (!($menu_cat_record = $db->record_fetch($menu_cat_result))) {
	throw new Exception("Restaurant id \"{$restaurant_id}\" not found");
}

$categoryname = $menu_cat_record['name'];
$restaurantname = $restaurant_record['name'];

$restaurantname_h = htmlentities($restaurantname);
$categoryname_h = htmlentities($categoryname);

$body_html = <<<EOHTML

<h2>{$restaurantname_h} &gt; Menu Categories &gt; {$categoryname_h} Items</h2>

{$table_html}

EOHTML;


$template = new admin_template();
$template->setmainnavsection('restaurant');
$template->settitle("{$restaurantname} > Menu Categories > {$categoryname} Items");
//$template->setheaderaddinhtml($headeraddin_html);
$template->setbodyhtml($body_html);
$template->display();

?>