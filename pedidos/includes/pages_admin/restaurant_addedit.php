<?php

include $includes_path . 'config.php';
include $cfg['userdata_path'] . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$admin_auth = new admin_auth();
$admin_auth->handle();
$authinfo = $admin_auth->getauthinfo();
$admin_auth->login_required();
$admin_auth->check_permission();


$link_base_path = htmlentities(navfr::base_path());

define('PAGE_TYPE_ADD', 1);
define('PAGE_TYPE_EDIT', 2);

if (isset($_GET['editid'])) {
	$pagetype = PAGE_TYPE_EDIT;
	$editid = intval($_GET['editid']);
	$title = 'Edit Restaurant';
} else {
	$pagetype = PAGE_TYPE_ADD;
	$title = 'Add Restaurant';
}

//If edit, check have permission
if ($pagetype == PAGE_TYPE_EDIT) {
	$admin_auth->check_permission_restaurant($editid);
}


$errormsg_html = array();

if (isset($_POST['formposted'])) {

	if (!$_POST['name']) {
		$errormsg_html[] = "'Nombre' no especificado.";
	}

	if (!($_POST['address_1'])) {
		$errormsg_html[] = "'Direccion 1' no especificada.";
	}

	if (!($_POST['town_id'])) {
		$errormsg_html[] = "'Ciudad, Municipio' no especificado.";
	}

	if (!($_POST['postcode'])) {
		$errormsg_html[] = "'Codigo postal' no especificado.";
	}

	if (!( (isset($_POST['handling_delivery'])) || (isset($_POST['handling_collection'])) )) {
		$errormsg_html[] = "Escojer sistema Entrega/Pasar por el";
	}

	//Telephone
	$telephone = '';
	if ($_POST['telephone']) {

		$telephone = $_POST['telephone'];
		$telephone = str_replace('-', ' ', $telephone);
		$telephone = preg_replace("%[ \t]+%", ' ', $telephone);

		//Telephone number
		if (!preg_match("%[^0-9\ ]%", $telephone)) {

			$telephone_validate = $telephone;
			$telephone_validate = preg_replace("%[^0-9]%", '', $telephone_validate);

			if (!preg_match("/^31[2-4][0-9]{7}$/", $telephone_validate)) {
						$errormsg_html[] = "'Telefono' no valido debe empezar con 312 y ser 10 digitos";
			}

			
		} else {
			$errormsg_html[] = "'Telephone' should be in the format e.g. 123 4567 8901, and should contain 0-9 only";
		}

	} else {
		$errormsg_html[] = "'Telephone' not specified.";
	}

	//Validate open times
	foreach ($cfg['restaurant_open_days'] as $dayno => $day) {

		//If have a time specified
		if ( ($_POST['openingtimes'][$dayno]['open']) || ($_POST['openingtimes'][$dayno]['close']) ) {
			if ( ($_POST['openingtimes'][$dayno]['open']) && ($_POST['openingtimes'][$dayno]['close']) ) {

				if ( (preg_match('/^(\d{1,2})\:(\d{2})$/', $_POST['openingtimes'][$dayno]['open'], $open_parts)) && (preg_match('/^(\d{1,2})\:(\d{2})$/', $_POST['openingtimes'][$dayno]['close'], $close_parts)) ) {

					if (!( ($open_parts[1] >= 0) && ($open_parts[1] <= 23) && ($open_parts[2] >= 0) && ($open_parts[2] <= 59) )) {
						$errormsg_html[] = "'Opening Times' &gt; '{$day}' open time must be from 00:00 - 23:59";
					}

					if (!( ($close_parts[1] >= 0) && ($close_parts[1] <= 23) && ($close_parts[2] >= 0) && ($close_parts[2] <= 59) )) {
						$errormsg_html[] = "'Opening Times' &gt; '{$day}' close time must be from 00:00 - 23:59";
					}

				} else {
					$errormsg_html[] = "'Opening Times' &gt; '{$day}' open or close time does not appear to be correctly formed, e.g. 13:05";
				}

			} else {
				$errormsg_html[] = "'Opening Times' &gt; '{$day}' does not have both open and close time specified";
			}
		}

	}

	//Logo Image Upload
	$logo_image_valid = false;
	if (isset($_FILES['logo_image'])) {
		if ($_FILES['logo_image']['error'] != 4) {
			if ($_FILES['logo_image']['error'] == 0) {
				if ($_FILES['logo_image']['size'] <= $cfg['logo_image_maxsize']) {
					if (preg_match("/(.*)\.(png)$/", $_FILES['logo_image']['name'])) {
						$logo_image_valid = true;
					} else {
						$errormsg_html[] = "Error: File type not supported, must be: .png";
					}
				} else {
					$errormsg_html[] = "Error: Maximum limit of {$cfg['logo_image_maxsize']} bytes per file";
				}
			} else {
				$errormsg_html[] = "Error: Unable to upload file (code: {$_FILES['logo_image']['error']})";
			}
		}
	}

	if ($_POST['navname']) {
		//Check navname is valid
		if (preg_match("%[^a-z0-9\-\_]%", $_POST['navname'])) {
			$errormsg_html[] = "'Navname' contains invalidd characters (only &quot;a-z 0-9 - _&quot; are allowed";
		}
	} else {
		$errormsg_html[] = "'Navname' not specified";
	}

	//If navname and town id specified check navname not duplicated
	if ( ($_POST['navname']) && ($_POST['town_id']) ) {

		$town_id = intval($_POST['town_id']);

		//Check not a duplicate name / navname
		$cond_base = array();
		if ($pagetype == PAGE_TYPE_EDIT) {
			$cond_base[] = "id != {$editid}";
		}

		//Check for duplicate navname in town
		$cond = array_merge($cond_base, array("town_id = {$town_id}", "navname = '".$db->es($_POST['navname'])."'"));
		$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $db->col(array('id', 'name')), $db->cond($cond, 'AND'), '', 0, 1);
		if ($restaurant_record = $db->record_fetch($restaurant_result)) {
			$errormsg_html[] = "'Navname' is already used by Restaurant &quot;".htmlentities($restaurant_record['name'])."&quot;";
		}

	}

	if ( ($_POST['contact_email']) && (!lib::chkemailvalid($_POST['contact_email'])) ) {
		$errormsg_html[] = "'Contact Email' not valid";
	}

	//If no errors
	if (count($errormsg_html) == 0) {

		try {

			$db->transaction(dbmysql::TRANSACTION_START);

			$postcode = strtoupper($_POST['postcode']);

			$status = ($_POST['status']) ? 1 : 0;

			$record = array(
				'name' => $_POST['name'],
				'description' => $_POST['description'],
				//'description_full' => $_POST['description_full'],
				'address_1' => $_POST['address_1'],
				'address_2' => $_POST['address_2'],
				'town_id' => $_POST['town_id'],
				'postcode' => $postcode,
				'telephone' => $telephone,
				//'lat' => '', //Set by speicific function
				//'lon' => '', //Set by speicific function
				'handling_delivery' => (isset($_POST['handling_delivery'])) ? 1 : 0,
				'handling_collection' => (isset($_POST['handling_collection'])) ? 1 : 0,
				'del_radius_mi' => $_POST['del_radius_mi'],
				'min_order_value' => $_POST['min_order_value'],
				'navname' => $_POST['navname'],
				'contact_name' => $_POST['contact_name'],
				'contact_email' => $_POST['contact_email'],
				'status' => $status,
			);

			$record = appgeneral::filternonascii_array($record);

			$record['delivery_charge'] = (isset($_POST['handling_delivery'])) ? $_POST['delivery_charge'] : null;

			//$record[''] = ($_POST['']) ? $_POST[''] : null;

			if ($pagetype == PAGE_TYPE_EDIT) {

				$record = array_merge($record, array('lastupdated' => $db->datetimenow()));
				$db->record_update($tbl['restaurant'], $db->rec($record), $db->cond(array("id = {$editid}"), 'AND'));

			} else {

				$record_extra = array();
				$record_extra['msg_dispatch'] = $cfg['msg_dispatch_default'];
				$record_extra['added'] = $db->datetimenow();
				$record_extra['lastupdated'] = $db->datetimenow();
				$record_extra['payment_cash'] = (in_array(1, $cfg['payment_types_default'])) ? 1 : 0;
				$record_extra['payment_online'] = (in_array(2, $cfg['payment_types_default'])) ? 1 : 0;

				$record = array_merge($record, $record_extra);

				$db->record_insert($tbl['restaurant'], $db->rec($record));
				$editid = $db->record_insert_id();
				$pagetype = PAGE_TYPE_EDIT;

				//If user is adding a restaurant, associate it with their login
				if ($authinfo['usergroup'] == admin_auth::LOGIN_GROUP_USER) {
					$db->record_insert($tbl['user_restaurant'], $db->rec(array('user_id' => $authinfo['id'], 'restaurant_id' => $editid)));
				}

			}

			//Delete all existing times for this restaurant
			$db->record_delete($tbl['restaurant_open'], $db->cond(array("restaurant_id = {$editid}"), 'AND'));

			//Save open times
			foreach ($cfg['restaurant_open_days'] as $dayno => $day) {

				//If have a time specified
				if ($_POST['openingtimes'][$dayno]['open']) {

					$record = array(
						'restaurant_id' => $editid,
						'dayno' => $dayno,
						'open' => $_POST['openingtimes'][$dayno]['open'] . ':00',
						'close' => $_POST['openingtimes'][$dayno]['close'] . ':00',
					);

					$db->record_insert($tbl['restaurant_open'], $db->rec($record));

				}

			}

			//Update restaurant lat/lon from postcode
			appgeneral::restaurant_latlon($editid);


			//Create directory
			$restaurant_path = $cfg['restaurant_dir_path'] . $editid . '/';

			//If does not already exist
			if (!is_dir($restaurant_path)) {

				$status = mkdir($restaurant_path);
				if (!$status) {
					throw new Exception("Unable to create main restaurant folder \"{$restaurant_path}\"");
				}

			}

			//Logo path
			$logo_image_path = $cfg['restaurant_dir_path'] . $editid . '/logo.png';

			//If delete specified
			if (isset($_POST['logo_image_delete'])) {
				unlink($logo_image_path);
			}

			//If save uploaded file
			if ($logo_image_valid) {

				//Save uploaded logo
				if (file_exists($logo_image_path)) {
					unlink($logo_image_path);
				}

				$status = move_uploaded_file($_FILES['logo_image']['tmp_name'], $logo_image_path);
				if (!$status) {
					throw new Exception("Unable to move uploaded logo file to \"{$logo_image_path}\"");
				}

			}

			$db->transaction(dbmysql::TRANSACTION_COMMIT);

		} catch (Exception $exception) {

			$db->transaction(dbmysql::TRANSACTION_ROLLBACK);

			throw $exception;

		}

		header("Location: {$cfg['site_url']}" . navpd::back());

	}

}




//If page posted
if (isset($_POST['formposted'])) {

	$formdata = $_POST;

	$formdata['handling_delivery'] = (isset($formdata['handling_delivery'])) ? 1 : 0;
	$formdata['handling_collection'] = (isset($formdata['handling_collection'])) ? 1 : 0;

} else {

	//If page type edit
	if ($pagetype == PAGE_TYPE_EDIT) {

		//Retrieve table data
		$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $db->col(array('name', 'description', 'address_1', 'address_2', 'town_id', 'postcode', 'telephone', 'del_radius_mi', 'min_order_value', 'handling_delivery', 'handling_collection', 'delivery_charge', 'navname', 'contact_name', 'contact_email', 'status')), $db->cond(array("id = {$editid}"), 'AND'), '', 0, 1);
		if (!($restaurant_record = $db->record_fetch($restaurant_result))) {
			throw new Exception("Specified edit id \"{$editid}\" not found");
		}

		$formdata = $restaurant_record;

		$formdata['openingtimes'] = array();

		//Retrieve opening times
		$restaurant_open_result = $db->table_query($db->tbl($tbl['restaurant_open']), $db->col(array('dayno', 'open', 'close')), $db->cond(array("restaurant_id = {$editid}"), 'AND'));
		while ($restaurant_open_record = $db->record_fetch($restaurant_open_result)) {

			$dayno = $restaurant_open_record['dayno'];

			$formdata['openingtimes'][$dayno] = array(
				'open' => $restaurant_open_record['open'],
				'close' => $restaurant_open_record['close'],
			);

			$formdata['openingtimes'][$dayno]['open'] = preg_replace('/(\:00)$/', '', $formdata['openingtimes'][$dayno]['open']);
			$formdata['openingtimes'][$dayno]['close'] = preg_replace('/(\:00)$/', '', $formdata['openingtimes'][$dayno]['close']);

		}

	} else {

		$formdata = array(
			'name' => '',
			'description' => '',
			//'description_full' => '',
			'address_1' => '',
			'address_2' => '',
			'town_id' => '',
			'postcode' => '',
			'telephone' => '',
			'del_radius_mi' => '',
			'min_order_value' => '',
			'delivery_charge' => '',
			'handling_delivery' => 1,
			'handling_collection' => '',
			'navname' => '',
			'contact_name' => '',
			'contact_email' => '',
			'status' => 1,
		);

		$formdata['openingtimes'] = array();

	}

}


$self_args = array();

if ($pagetype == PAGE_TYPE_EDIT) {
	$self_args = array('editid' => $editid);
}


$link_h = navpd::self($self_args);

$formdatah = lib::htmlentities_array($formdata);


//Retrieve provinces
$town_options = array();

$query_sql = <<<EOSQL
SELECT
	{$tbl['town']}.id,
	{$tbl['town']}.name AS town_name,
	{$tbl['city']}.name AS city_name
FROM
	{$tbl['town']},
	{$tbl['city']}
WHERE
	{$tbl['town']}.city_id = {$tbl['city']}.id
ORDER BY
	city_name ASC,
	town_name ASC
EOSQL;

$town_result = $db->query($query_sql);
while ($town_record = $db->record_fetch($town_result)) {
	$id = $town_record['id'];
	$town_options[$id] = "{$town_record['city_name']}, {$town_record['town_name']}";
}

$town_options_html = lib::create_options($town_options, $formdata['town_id'], lib::CREATEOPT_PLEASESELECT);

$status_radio_html = lib::create_radio('status', $cfg['restaurant_status'], $formdata['status']);

//Convert errors to html
$errormsgs_html = appgeneral::errormsgs_html($errormsg_html);

$btn_back = btn::create('<< Back', btn::TYPE_LINK, navpd::back(), '', '', 'Back');
$btn_update = btn::create('Update', btn::TYPE_SUBMIT);

$nav_html = <<<EOHTML
<div class="navigation">
	<div class="left">{$btn_back}</div>
	<div class="right">{$btn_update}</div>
</div>
EOHTML;

$opening_times_html = '';
foreach ($cfg['restaurant_open_days'] as $dayno => $day) {

	$open = (isset($formdata['openingtimes'][$dayno]['open'])) ? $formdata['openingtimes'][$dayno]['open'] : '';
	$close = (isset($formdata['openingtimes'][$dayno]['close'])) ? $formdata['openingtimes'][$dayno]['close'] : '';

	$open = htmlentities($open);
	$close = htmlentities($close);

	$opening_times_html .= <<<EOHTML
							<tr class="row">
								<td class="col-dayname"><label for="openingtimes_{$dayno}_open">{$day}</label></td>
								<td class="col-time"><input type="text" name="openingtimes[{$dayno}][open]" id="openingtimes_{$dayno}_open" value="{$open}" class="timefield" maxlength="5" /></td>
								<td class="col-spacer">-</td>
								<td class="col-time"><input type="text" name="openingtimes[{$dayno}][close]" id="openingtimes_{$dayno}_close" value="{$close}" class="timefield" maxlength="5" /></td>
							</tr>

EOHTML;

}

//If this is an edit
if ( ($pagetype == PAGE_TYPE_EDIT) && (file_exists($cfg['restaurant_dir_path'] . "{$editid}/logo.png")) ){

	$logo_image_preview = <<<EOHTML
<br />
<img src="{$link_base_path}{$cfg['restaurant_dir_path']}{$editid}/logo.png" width="87" height="87" alt="Logo Preview" /> &nbsp; <input type="checkbox" id="logo_image_delete" name="logo_image_delete" value="1" /> <label for="logo_image_delete">Delete</label>
EOHTML;

} else {
	$logo_image_preview = '';
}


$handling_delivery_checked = ($formdata['handling_delivery']) ? 'checked="checked"' : '';
$handling_collection_checked = ($formdata['handling_collection']) ? 'checked="checked"' : '';

$body_html = <<<EOHTML

<div class="addeditpage">

	<h2>{$title}</h2>

	<form method="post" action="{$link_h}" id="mainform" class="mainform" enctype="multipart/form-data">
		<div><input type="hidden" name="formposted" value="1" /></div>

{$nav_html}

{$errormsgs_html}

		<div class="tablecontainer">
			<table cellspacing="0" class="addedit">
				<tr>
					<th class="addedit"><label for="name">Nombre: <span class="required">*</span></label></th>
					<td class="addedit"><input type="text" name="name" id="name" value="{$formdatah['name']}" class="inputtxt" maxlength="255" onchange="createnavname('name', 'navname')" onkeyup="createnavname('name', 'navname')" /></td>
				</tr>
				<tr>
					<th class="addedit"><label for="description">Descripcion:</label></th>
					<td class="addedit"><textarea rows="2" name="description" id="description" cols="20" class="inputtxtarea">{$formdatah['description']}</textarea></td>
				</tr>
				<!--//
				<tr>
					<th class="addedit"><label for="description">Full Description:</label></th>
					<td class="addedit"><textarea rows="2" name="description_full" id="description_full" cols="20" class="inputtxtarea">{formdatah['description_full']}</textarea></td>
				</tr>
				//-->
				<tr>
					<th class="addedit"><label for="address_1" class="inputxttitle">Direccion 1: <span class="required">*</span></label></th>
					<td class="addedit"><input type="text" name="address_1" id="address_1" value="{$formdatah['address_1']}" class="inputtxt" maxlength="30" /></td>
				</tr>
				<tr>
					<th class="addedit"><label for="address_2" class="inputxttitle">Address Line 2:</label></th>
					<td class="addedit"><input type="text" name="address_2" id="address_2" value="{$formdatah['address_2']}" class="inputtxt" maxlength="20" /></td>
				</tr>
				<tr>
					<th class="addedit"><label for="town_id" class="inputxttitle">Ciudad, Municipio: <span class="required">*</span></label></th>
					<td class="addedit"><select size="1" name="town_id" id="town_id">{$town_options_html}</select></td>
				</tr>
				<tr>
					<th class="addedit"><label for="postcode" class="inputxttitle">Codigo Postal: <span class="required">*</span></label></th>
					<td class="addedit"><input type="text" name="postcode" id="postcode" value="{$formdatah['postcode']}" class="inputtxt" maxlength="20" /></td>
				</tr>
				<tr>
					<th class="addedit"><label for="telephone" class="inputxttitle">Telefono: <span class="required">*</span></label></th>
					<td class="addedit"><input type="text" name="telephone" id="telephone" value="{$formdatah['telephone']}" class="inputtxt" maxlength="20" /></td>
				</tr>
				<tr>
					<th class="addedit"><label for="openingtimes" class="inputxttitle">Horario de Servicio:<br />(ejemplo. 13:00 - 20:00)</label></th>
					<td class="addedit">

						<table cellspacing="0" class="openingtimes">
{$opening_times_html}
						</table>

					</td>
				</tr>
				<tr>
					<th class="addedit"><label for="del_radius_mi" class="inputxttitle">Radio de Entrega (km):</label></th>
					<td class="addedit"><input type="text" name="del_radius_mi" id="del_radius_mi" value="{$formdatah['del_radius_mi']}" class="inputtxt inputtxt-delivery" maxlength="20" /></td>
				</tr>
				<tr>
					<th class="addedit"><label>Entrega/Recojer: <span class="required">*</span></label></th>
					<td class="addedit"><input type="checkbox" name="handling_delivery" id="handling_delivery" value="1" {$handling_delivery_checked} onclick="delivery_changed()" /> <label for="handling_delivery">Delivery</label> &nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="handling_collection" id="handling_collection" value="1" {$handling_collection_checked} /> <label for="handling_collection">Collection</label></td>
				</tr>
				<tr id="delivery_charge_block">
					<th class="addedit"><label for="delivery_charge">Cargos por servicio ($):</label></th>
					<td class="addedit"><input type="text" name="delivery_charge" id="delivery_charge" value="{$formdatah['delivery_charge']}" class="inputtxt" maxlength="20" /></td>
				</tr>
				<tr>
					<th class="addedit"><label for="min_order_value">Min Order Value ($):</label></th>
					<td class="addedit"><input type="text" name="min_order_value" id="min_order_value" value="{$formdatah['min_order_value']}" class="inputtxt" maxlength="20" /></td>
				</tr>
				<tr>
					<th class="addedit"><label for="logo_image">Logo Imagen:</label></th>
					<td class="addedit">
						<input type="file" name="logo_image" id="logo_image" size="20" />
						<div class="note"><em>87px x 87px png esquinas transparentes y redondeadas.</em></div>
						{$logo_image_preview}
					</td>
				</tr>
				<tr>
					<th class="addedit"><label for="navname">Nick-navegacion: <span class="required">*</span></label></th>
					<td class="addedit"><input type="text" name="navname" id="navname" value="{$formdatah['navname']}" class="inputtxt" maxlength="40" /></td>
				</tr>
				<tr>
					<th class="addedit"><label for="contact_name" class="inputxttitle">Nombre de contacto:</label></th>
					<td class="addedit"><input type="text" name="contact_name" id="contact_name" value="{$formdatah['contact_name']}" class="inputtxt" maxlength="20" /></td>
				</tr>
				<tr>
					<th class="addedit"><label for="contact_email" class="inputxttitle">Email contacto:</label></th>
					<td class="addedit"><input type="text" name="contact_email" id="contact_email" value="{$formdatah['contact_email']}" class="inputtxt" maxlength="20" /></td>
				</tr>
				<tr>
					<th class="addedit"><label for="status" class="inputxttitle">Status:</label></th>
					<td class="addedit">{$status_radio_html}</td>
				</tr>
			</table>
		</div>

{$nav_html}

	</form>

</div>

EOHTML;


$headeraddin_html = <<<EOHTML
<script src="{$link_base_path}resources/admin_restaurant_addedit/admin_restaurant_addedit.js" type="text/javascript"></script>
EOHTML;

$template = new admin_template();
$template->setmainnavsection('restaurant');
$template->settitle('Restaurant > Add/Edit');
$template->setheaderaddinhtml($headeraddin_html);
$template->setbodyhtml($body_html);
$template->display();

?>