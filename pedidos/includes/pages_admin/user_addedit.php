<?php

include $includes_path . 'config.php';
include $cfg['userdata_path'] . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$admin_auth = new admin_auth();
$admin_auth->handle();
$authinfo = $admin_auth->getauthinfo();
$admin_auth->login_required();
$admin_auth->check_permission();


define('PAGE_TYPE_ADD', 1);
define('PAGE_TYPE_EDIT', 2);

if (isset($_GET['editid'])) {
	$pagetype = PAGE_TYPE_EDIT;
	$editid = intval($_GET['editid']);
	$title = 'Edit User';
} else {
	$pagetype = PAGE_TYPE_ADD;
	$title = 'Add User';
}

//If edit, check have permissions to edit this user
if ($pagetype == PAGE_TYPE_EDIT) {

	if ($authinfo['usergroup'] == admin_auth::LOGIN_GROUP_ADMIN_SYSTEM) {
		//ok
	} else if ($authinfo['usergroup'] == admin_auth::LOGIN_GROUP_ADMIN) {

		//Retrieve group of user to edit
		$user_result = $db->table_query($db->tbl($tbl['user']), $db->col(array('usergroup')), $db->cond(array("id = {$editid}"), 'AND'), '', 0, 1);
		if (!($user_record = $db->record_fetch($user_result))) {
			throw new Exception("User to edit id \"{$actionid}\" not found");
		}

		if (!( ($user_record['usergroup'] == admin_auth::LOGIN_GROUP_ADMIN) || ($user_record['usergroup'] == admin_auth::LOGIN_GROUP_USER) )) {
			throw new Exception("User to edit id \"{$editid}\" belongs to group \"{$user_record['usergroup']}\", usergroup \"{$authinfo['usergroup']}\" can not delete them");
		}

	} else {
		throw new Exception("Usergroup \"{$authinfo['usergroup']}\" unknown");
	}

}


//Usergroup radios
$usergroup_radios = $admin_auth->usergroup_option();
if ($authinfo['usergroup'] == admin_auth::LOGIN_GROUP_ADMIN_SYSTEM) {
	//Remove no options
} else if ($authinfo['usergroup'] == admin_auth::LOGIN_GROUP_ADMIN) {
	//Remove system admin
	unset($usergroup_radios[admin_auth::LOGIN_GROUP_ADMIN_SYSTEM]);
}



$errormsg_html = array();

if (isset($_POST['formposted'])) {

	if ($_POST['email']) {
		if (!lib::chkemailvalid($_POST['email'])) {
			$errormsg_html[] = "'Email' not valid.";
		} else {

			//Check not a duplicate name / navname
			$cond_base = array();
			if ($pagetype == PAGE_TYPE_EDIT) {
				$cond_base[] = "id != {$editid}";
			}

			//Check for duplicate email
			$cond = array_merge($cond_base, array("email = '".$db->es($_POST['email'])."'"));
			$user_result = $db->table_query($db->tbl($tbl['user']), $db->col(array('id')), $db->cond($cond, 'AND'), '', 0, 1);
			if ($user_record = $db->record_fetch($user_result)) {
				$errormsg_html[] = "'Email' is already setup with an account";
			}

		}
	} else {
		$errormsg_html[] = "'Email' not specified.";
	}

	if ($pagetype == PAGE_TYPE_ADD) {
		if (!$_POST['password_new']) {
			$errormsg_html[] = "'Password' not specified.";
		} else {
			if ($_POST['password_new'] != $_POST['password_new_confirm']) {
				$errormsg_html[] = "'Password' does not match 'Password Confirm'.";
			}
		}
	} else {
		if ( ($_POST['password_new']) || ($_POST['password_new']) ) {
			if ($_POST['password_new'] != $_POST['password_new_confirm']) {
				$errormsg_html[] = "'Password' does not match 'Password Confirm'.";
			}
		}
	}

	if (!$_POST['name']) {
		$errormsg_html[] = "'Name' not specified.";
	}

	//Telephone
	$telephone = '';
	if ($_POST['telephone']) {

		$telephone = $_POST['telephone'];
		$telephone = str_replace('-', ' ', $telephone);
		$telephone = preg_replace("%[ \t]+%", ' ', $telephone);

		//Telephone number
		if (!preg_match("%[^0-9\ ]%", $telephone)) {

			$telephone_validate = $telephone;
			$telephone_validate = preg_replace("%[^0-9]%", '', $telephone_validate);

			if (!preg_match("/^0[1-9][0-9]{8,9}$/", $telephone_validate)) {
				$errormsg_html[] = "'Telephone' does not appear to be valid (should not contain country code, start with a leading zero, and be 10-11 digits in length)";
			}

		} else {
			$errormsg_html[] = "'Telephone' should be in the format e.g. 123 4567 8901, and should contain 0-9 only";
		}

	}// else {
	//	$errormsg_html[] = "'Telephone' not specified.";
	//}

	//Check usergroup valid
	$usergroup = $_POST['usergroup'];
	if (!isset($usergroup_radios[$usergroup])) {
		throw new Exception("User group \"{$usergroup}\" not valid");
	}

	//If no errors
	if (count($errormsg_html) == 0) {





		try {

			$db->transaction(dbmysql::TRANSACTION_START);

			$record = array(
				'email' => $_POST['email'],
				'name' => $_POST['name'],
				'telephone' => $_POST['telephone'],
				'usergroup' => $_POST['usergroup'],
			);

			if ($_POST['password_new']) {
				$record = array_merge($record, array('password' => md5($_POST['password_new'])));
			}

			$record = appgeneral::filternonascii_array($record);

			if ($pagetype == PAGE_TYPE_EDIT) {

				//$record = array_merge($record, array('lastupdated' => $db->datetimenow()));
				$db->record_update($tbl['user'], $db->rec($record), $db->cond(array("id = {$editid}"), 'AND'));

			} else {

				$record = array_merge($record, array('registered' => $db->datetimenow()));
				$db->record_insert($tbl['user'], $db->rec($record));
				$editid = $db->record_insert_id();
				$pagetype = PAGE_TYPE_EDIT;

			}

			//Delete existing restaurant associations
			$db->record_delete($tbl['user_restaurant'], $db->cond(array("user_id = {$editid}"), 'AND'));

			//If type is user, add in any restaurant associations
			$restaurants_ids_added = array();
			if ($_POST['usergroup'] == admin_auth::LOGIN_GROUP_USER) {

				if (isset($_POST['restaurants'])) {

					foreach ($_POST['restaurants'] as $restaurant) {

						$restaurant['restaurant_id'] = intval($restaurant['restaurant_id']);
						if ($restaurant['restaurant_id']) {

							if (!in_array($restaurant['restaurant_id'], $restaurants_ids_added)) {
								$restaurants_ids_added[] = $restaurant['restaurant_id'];
								$db->record_insert($tbl['user_restaurant'], $db->rec(array('user_id' => $editid, 'restaurant_id' => $restaurant['restaurant_id'])));
							}

						}

					}

				}

			}

			$db->transaction(dbmysql::TRANSACTION_COMMIT);

			header("Location: {$cfg['site_url']}" . navpd::back());

		} catch (Exception $exception) {

			$db->transaction(dbmysql::TRANSACTION_ROLLBACK);

			throw $exception;

		}

	}

}



//If page posted
if (isset($_POST['formposted'])) {

	$formdata = $_POST;

	$formdata['restaurants'] = isset($_POST['restaurants']) ? $_POST['restaurants'] : array();

} else {

	//If page type edit
	if ($pagetype == PAGE_TYPE_EDIT) {

		//Retrieve table data
		$user_result = $db->table_query($db->tbl($tbl['user']), $db->col(array('email', 'name', 'telephone', 'usergroup')), $db->cond(array("id = {$editid}"), 'AND'), '', 0, 1);
		if (!($user_record = $db->record_fetch($user_result))) {
			throw new Exception("Specified edit id \"{$editid}\" not found");
		}

		$formdata = $user_record;

		$formdata['restaurants'] = array();

		//If type is a user
		if ($user_record['usergroup'] == admin_auth::LOGIN_GROUP_USER) {

			//Retrieve restaurants user have permssions on
			$indexid = 0;
			$user_restaurant_result = $db->table_query($db->tbl($tbl['user_restaurant']), $db->col(array('id', 'restaurant_id')), $db->cond(array("user_id = {$editid}"), 'AND'));
			while ($user_restaurant_record = $db->record_fetch($user_restaurant_result)) {
				$id = $user_restaurant_record['id'];
				$formdata['restaurants'][$indexid] = array(
					'id' => $user_restaurant_record['id'],
					'restaurant_id' => $user_restaurant_record['restaurant_id'],
				);
				$indexid++;
			}

		}

	} else {

		$formdata = array(
			'email' => '',
			'name' => '',
			'telephone' => '',
			'usergroup' => admin_auth::LOGIN_GROUP_USER,
		);

		$formdata['restaurants'] = array('default' => array('id' => '', 'restaurant_id' => ''));

	}

}

$formdata['password_new'] = '';
$formdata['password_new_confirm'] = '';


$self_args = array();

if ($pagetype == PAGE_TYPE_EDIT) {
	$self_args = array('editid' => $editid);
}




$link_h = navpd::self($self_args);

$formdatah = lib::htmlentities_array($formdata);

//Convert errors to html
$errormsgs_html = appgeneral::errormsgs_html($errormsg_html);

$btn_back = btn::create('<< Back', btn::TYPE_LINK, navpd::back(), '', '', 'Back');
$btn_update = btn::create('Update', btn::TYPE_SUBMIT);

$nav_html = <<<EOHTML
<div class="navigation">
	<div class="left">{$btn_back}</div>
	<div class="right">{$btn_update}</div>
</div>
EOHTML;

$usergroup_radios_html = lib::create_radio('usergroup', $usergroup_radios, $formdata['usergroup'], '', '', 'onclick="typechanged()"');

$btn_restaurant_add = btn::create('Add', btn::TYPE_LINK, '#', $cfg['btn_template_path'].'icons/addrestaurant.png', 'addrestaurant(); return false;', "Add Restaurant");

$btn_restaurant_delete_html_js = btn::create('', btn::TYPE_LINK, '#', $cfg['btn_template_path'].'icons/delete.png', 'deletesubitem(\'=indexid=\'); return false;', "Delete Restaurant");
$btn_restaurant_delete_html_js = addslashes($btn_restaurant_delete_html_js);

$restaurants_js = lib::array_jsarray('restaurants', $formdata['restaurants']);

$usergroup_user_id = admin_auth::LOGIN_GROUP_USER;

//Retrieve restaurant names list
$restaurant_options = array();
$select_sql = $db->col(array('id', 'name')) . ', ';
$select_sql .= "(SELECT {$tbl['town']}.name FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id) AS town, ";
$select_sql .= "(SELECT {$tbl['city']}.name FROM {$tbl['city']} WHERE {$tbl['city']}.id = (SELECT {$tbl['town']}.city_id FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id)) AS city";
$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $select_sql, $db->cond(array(), 'AND'), $db->order(array(array('name', 'ASC'))));
while ($restaurant_record = $db->record_fetch($restaurant_result)) {
	$id = $restaurant_record['id'];
	$restaurant_options[$id] = "{$restaurant_record['name']} ({$restaurant_record['town']}, {$restaurant_record['city']})";
}


$restaurant_options_js = lib::array_jsarray('restaurant_options', $restaurant_options);

$body_html = <<<EOHTML

<div class="addeditpage">

	<h2>{$title}</h2>

	<form method="post" action="{$link_h}" id="mainform" class="mainform">
		<div><input type="hidden" name="formposted" value="1" /></div>

{$nav_html}

{$errormsgs_html}

		<div class="tablecontainer">
			<table cellspacing="0" class="addedit">
				<tr>
					<th class="addedit"><label for="email">Email: <span class="required">*</span></label></th>
					<td class="addedit"><input type="text" name="email" id="email" value="{$formdatah['email']}" class="inputtxt" maxlength="255" /></td>
				</tr>
				<tr>
					<th class="addedit"><label for="password_new">Password: </label></th>
					<td class="addedit"><input type="password" name="password_new" id="password_new" value="{$formdatah['password_new']}" class="inputtxt" maxlength="255" /></td>
				</tr>
				<tr>
					<th class="addedit"><label for="password_new_confirm">Password Confirm: </label></th>
					<td class="addedit"><input type="password" name="password_new_confirm" id="password_new_confirm" value="{$formdatah['password_new_confirm']}" class="inputtxt" maxlength="255" /></td>
				</tr>
				<tr>
					<th class="addedit"><label for="name">Name: <span class="required">*</span></label></th>
					<td class="addedit"><input type="text" name="name" id="name" value="{$formdatah['name']}" class="inputtxt" maxlength="255" /></td>
				</tr>
				<tr>
					<th class="addedit"><label for="telephone">Telephone:</label></th>
					<td class="addedit"><input type="text" name="telephone" id="telephone" value="{$formdatah['telephone']}" class="inputtxt" maxlength="255" /></td>
				</tr>
				<tr>
					<th class="addedit"><label for="usergroup">User Group:</label></th>
					<td class="addedit">{$usergroup_radios_html}</td>
				</tr>
				<tr id="restaurants_block">
					<th class="addedit"><label for="restaurants">Restaurants:</label></th>
					<td class="addedit">

						<div id="restaurants_html"></div>
						{$btn_restaurant_add}

					</td>
				</tr>
			</table>
		</div>

{$nav_html}

	</form>

</div>

<script type="text/javascript">
  <!--

	var btn_restaurant_delete_html = "{$btn_restaurant_delete_html_js}";

	var {$restaurants_js}
	var {$restaurant_options_js}

	var usergroup_user_id = {$usergroup_user_id};

  //-->
</script>

EOHTML;


$headeraddin_html = <<<EOHTML
<script src="{$cfg['site_url']}resources/admin_user_addedit/admin_user_addedit.js" type="text/javascript"></script>
EOHTML;

$template = new admin_template();
$template->setmainnavsection('user');
$template->settitle('User > Add/Edit');
$template->setheaderaddinhtml($headeraddin_html);
$template->setbodyhtml($body_html);
$template->display();

?>