<?php

include $includes_path . 'config.php';
include $cfg['userdata_path'] . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$admin_auth = new admin_auth();
$admin_auth->handle();
$authinfo = $admin_auth->getauthinfo();
$admin_auth->login_required();
$admin_auth->check_permission();


//Show exceptions
exceptions::viewlogs();

?>