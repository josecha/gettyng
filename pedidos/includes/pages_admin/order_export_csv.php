<?php

include $includes_path . 'config.php';
include $cfg['userdata_path'] . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$admin_auth = new admin_auth();
$admin_auth->handle();
$authinfo = $admin_auth->getauthinfo();
$admin_auth->login_required();
$admin_auth->check_permission();


$cond = array();

//Add on filter conditions
$filter_cond = admin_disp_cond::order($_GET);
$cond = array_merge($cond, $filter_cond);

$select_sql = $db->col(array('id', 'name', 'address', 'postcode', 'telephone', 'email', 'notes', 'handling', 'delivery_charge', 'total', 'restaurant_name', 'delivery_min', 'test', 'status', 'added'));
$order_result = $db->table_query($db->tbl($tbl['order']), $select_sql, $db->cond($cond, 'AND'), $db->order(array(array('added', 'DESC'))));

$csvdata = array();
while ($order_data = $db->record_fetch($order_result)) {

	$handling = $order_data['handling'];
	$status = $order_data['status'];

	$csvdata[] = array(
		'name' => $order_data['name'],
		'address' => $order_data['address'],
		'postcode' => $order_data['postcode'],
		'telephone' => $order_data['telephone'],
		'email' => $order_data['email'],
		'notes' => $order_data['notes'],
		'handling' => isset($cfg['order_handling'][$handling]) ? $cfg['order_handling'][$handling] : '',
		'delivery_charge' => $order_data['delivery_charge'],
		'total' => $order_data['total'],
		'restaurant_name' => $order_data['restaurant_name'],
		'delivery_min' => $order_data['delivery_min'],
		'test' => $order_data['test'],
		'status' => isset($cfg['order_status'][$status]) ? $cfg['order_status'][$status] : '',
		'added' => date('Y-m-d H:i:s', strtotime($order_data['added'] . ' UTC')),
	);

}


header("Content-type: text/comma-separated-values");
header('Content-Disposition: attachment; filename="orders.csv"');

echo csv::generate($csvdata, false, csv::GENERATE_TITLE_FROM_ID);

?>