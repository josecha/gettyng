<?php

include $includes_path . 'config.php';
include $cfg['userdata_path'] . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$admin_auth = new admin_auth();
$admin_auth->handle();
$authinfo = $admin_auth->getauthinfo();
$admin_auth->login_required();
$admin_auth->check_permission();


define('PAGE_TYPE_ADD', 1);
define('PAGE_TYPE_EDIT', 2);

if (isset($_GET['editid'])) {
	$pagetype = PAGE_TYPE_EDIT;
	$editid = intval($_GET['editid']);
	$title = 'Edit Category';
} else {
	$pagetype = PAGE_TYPE_ADD;
	$title = 'Add Category';
}

if ($pagetype == PAGE_TYPE_EDIT) {

	//Lookup restaurant id from category id
	$menu_cat_result = $db->table_query($db->tbl($tbl['menu_cat']), $db->col(array('restaurant_id')), $db->cond(array("id = {$editid}"), 'AND'), '', 0, 1);
	if (!($menu_cat_record = $db->record_fetch($menu_cat_result))) {
		throw new Exception("Specified edit id \"{$editid}\" not found");
	}

	$restaurant_id = intval($menu_cat_record['restaurant_id']);

} else {
	$restaurant_id = intval($_GET['restaurant_id']);
}

//Chcek have permission for this restaurant id
$admin_auth->check_permission_restaurant($restaurant_id);

/*
//Lookup restaurant from retaurant id
$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $db->col(array('aaa')), $db->cond(array("id = {$restaurant_id}"), 'AND'), '', 0, 1);
if (!($restaurant_record = $db->record_fetch($restaurant_result))) {
	throw new Exception("Restaurant id \"{$category_record['restaurant_id']}\" not found");
}
*/

$errormsg_html = array();

if (isset($_POST['formposted'])) {

	if (!$_POST['name']) {
		$errormsg_html[] = "'Name' not specified.";
	}

	//Check not a duplicate name
	$cond_base = array();
	if ($pagetype == PAGE_TYPE_EDIT) {
		$cond_base[] = "id != {$editid}";
	}

	//Check for duplicate name
	$cond = array_merge($cond_base, array("name = '".$db->es($_POST['name'])."'"));
	$menu_cat_result = $db->table_query($db->tbl($tbl['menu_cat']), $db->col(array('id')), $db->cond($cond, 'AND'), '', 0, 1);
	if ($menu_cat_record = $db->record_fetch($menu_cat_result)) {
		$errormsg_html[] = "'Name' has already been used";
	}


	//If no errors
	if (count($errormsg_html) == 0) {

		//$db->transaction(dbmysql::TRANSACTION_START);

		$record = array(
			'name' => $_POST['name'],
		);

		$record = appgeneral::filternonascii_array($record);

		if ($pagetype == PAGE_TYPE_EDIT) {

			//$record = array_merge($record, array('lastupdated' => $db->datetimenow()));
			$db->record_update($tbl['menu_cat'], $db->rec($record), $db->cond(array("id = {$editid}"), 'AND'));

		} else {

			/*
			//Find last highest order number
			$menu_cat_result = $db->table_query($db->tbl($tbl['menu_cat']), $db->col(array('catorder')), $db->cond(array("restaurant_id = {$restaurant_id}"), 'AND'), $db->order(array(array('catorder', 'DESC'))), 0, 1);
			if ($menu_cat_record = $db->record_fetch($menu_cat_result)) {
				$order = $menu_cat_record['catorder'] + 1;
			} else {
				$order = 0;
			}
			*/

			//Find last highest order number
			$order = appgeneral::category_order_next($restaurant_id);

			$record = array_merge($record, array('restaurant_id' => $restaurant_id, 'catorder' => $order));
			//$record = array_merge($record, array('added' => $db->datetimenow(), 'lastupdated' => $db->datetimenow()));
			$db->record_insert($tbl['menu_cat'], $db->rec($record));
			$editid = $db->record_insert_id();
			$pagetype = PAGE_TYPE_EDIT;

		}

		$db->record_update($tbl['restaurant'], $db->rec(array('lastupdated' => $db->datetimenow())), $db->cond(array("id = {$restaurant_id}"), 'AND'));

		//$db->transaction(dbmysql::TRANSACTION_COMMIT);

		header("Location: {$cfg['site_url']}" . navpd::back());

	}

}




//If page posted
if (isset($_POST['formposted'])) {

	$formdata = $_POST;

} else {

	//If page type edit
	if ($pagetype == PAGE_TYPE_EDIT) {

		//Retrieve table data
		$menu_cat_result = $db->table_query($db->tbl($tbl['menu_cat']), $db->col(array('name')), $db->cond(array("id = {$editid}"), 'AND'), '', 0, 1);
		if (!($menu_cat_record = $db->record_fetch($menu_cat_result))) {
			throw new Exception("Specified edit id \"{$editid}\" not found");
		}

		$formdata = $menu_cat_record;

	} else {

		$formdata = array(
			'name' => '',
		);

	}

}


$self_args = array();

if ($pagetype == PAGE_TYPE_EDIT) {
	$self_args = array('editid' => $editid);
}


$link_h = navpd::self($self_args);

$formdatah = lib::htmlentities_array($formdata);

//Convert errors to html
$errormsgs_html = appgeneral::errormsgs_html($errormsg_html);

$btn_back = btn::create('<< Back', btn::TYPE_LINK, navpd::back(), '', '', 'Back');
$btn_update = btn::create('Update', btn::TYPE_SUBMIT);

$nav_html = <<<EOHTML
<div class="navigation">
	<div class="left">{$btn_back}</div>
	<div class="right">{$btn_update}</div>
</div>
EOHTML;

//Retrieve restaurant information
$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $db->col(array('name')), $db->cond(array("id = {$restaurant_id}"), 'AND'), '', 0, 1);
if (!($restaurant_record = $db->record_fetch($restaurant_result))) {
	throw new Exception("Restaurant id \"{$restaurant_id}\" not found");
}

$pagename = $restaurant_record['name'];

$pagename_h = htmlentities($pagename);

$body_html = <<<EOHTML

<div class="addeditpage">

	<h2>{$pagename_h} &gt; Menu Categories &gt; {$title}</h2>

	<form method="post" action="{$link_h}" id="mainform" class="mainform">
		<div><input type="hidden" name="formposted" value="1" /></div>

{$nav_html}

{$errormsgs_html}

		<div class="tablecontainer">
			<table cellspacing="0" class="addedit">
				<tr>
					<th class="addedit"><label for="name">Name: <span class="required">*</span></label></th>
					<td class="addedit"><input type="text" name="name" id="name" value="{$formdatah['name']}" class="inputtxt" maxlength="255" /></td>
				</tr>
			</table>
		</div>

{$nav_html}

	</form>

</div>

EOHTML;


$template = new admin_template();
$template->setmainnavsection('restaurant');
$template->settitle("{$pagename} > Menu Categories > Add/Edit");
//$template->setheaderaddinhtml($headeraddin_html);
$template->setbodyhtml($body_html);
$template->display();

?>