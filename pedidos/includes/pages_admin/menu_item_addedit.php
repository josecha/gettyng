<?php

include $includes_path . 'config.php';
include $cfg['userdata_path'] . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$admin_auth = new admin_auth();
$admin_auth->handle();
$authinfo = $admin_auth->getauthinfo();
$admin_auth->login_required();
$admin_auth->check_permission();


define('PAGE_TYPE_ADD', 1);
define('PAGE_TYPE_EDIT', 2);

if (isset($_GET['editid'])) {
	$pagetype = PAGE_TYPE_EDIT;
	$editid = intval($_GET['editid']);
	$title = 'Edit Item';
} else {
	$pagetype = PAGE_TYPE_ADD;
	$title = 'Add Item';
}

if ($pagetype == PAGE_TYPE_EDIT) {

	//Lookup category id from item id
	$menu_item_result = $db->table_query($db->tbl($tbl['menu_item']), $db->col(array('menu_cat_id')), $db->cond(array("id = {$editid}"), 'AND'), '', 0, 1);
	if (!($menu_item_record = $db->record_fetch($menu_item_result))) {
		throw new Exception("Specified edit id \"{$editid}\" not found");
	}

	$category_id = $menu_item_record['menu_cat_id'];

} else {
	$category_id = intval($_GET['category_id']);
}

//Lookup restaurant id from category id
$menu_cat_result = $db->table_query($db->tbl($tbl['menu_cat']), $db->col(array('restaurant_id')), $db->cond(array("id = {$category_id}"), 'AND'), '', 0, 1);
if (!($menu_cat_record = $db->record_fetch($menu_cat_result))) {
	throw new Exception("Specified category id \"{$editid}\" not found");
}

$restaurant_id = intval($menu_cat_record['restaurant_id']);


//Chcek have permission for this restaurant id
$admin_auth->check_permission_restaurant($restaurant_id);


$errormsg_html = array();

if (isset($_POST['formposted'])) {

	//if ( ($_POST['no']) && (preg_match("%[^0-9]%", $_POST['no'])) ) {
	//	$errormsg_html[] = "'No' must be numeric only.";
	//}

	if (strlen($_POST['no']) > 4) {
		$errormsg_html[] = "'No' must be 0-4 characters in length";
	} else {
		if (preg_match("%[^0-9a-zA-Z]%", $_POST['no'])) {
			$errormsg_html[] = "'No' must consist of a-z, 0-9 only";
		}
	}

	if (!$_POST['name']) {
		$errormsg_html[] = "'Name' not specified.";
	}


	$type = $_POST['type'];
	if (!isset($cfg['menu_item_type'][$type])) {
		throw new Exception("Type \"{$type}\" unknown");
	}

	//If single item
	if ($type == 1) {

		if ($_POST['cost']) {
			if (preg_match("%[^0-9\.]%", $_POST['cost'])) {
			$errormsg_html[] = "'Cost' must be numeric only";
			}
		} else {
			$errormsg_html[] = "'Cost' not specified";
		}

	} else {

		//Multi items
		$subitems = array();
		if ( (isset($_POST['subitem'])) && (is_array($_POST['subitem'])) ) {

			$no = 1;
			foreach ($_POST['subitem'] as $subitem) {

				if ( ($subitem['name']) || ($subitem['cost']) ) {

					if ( ($subitem['name']) && ($subitem['cost']) ) {

						if (!$subitem['name']) {
							$errormsg_html[] = "'Sub Item' &gt; {$no} &gt; 'Name' not specified";
						}

						if ($subitem['cost']) {
							if (preg_match("%[^0-9\.]%", $subitem['cost'])) {
								$errormsg_html[] = "'Sub Item' &gt; {$no} &gt; 'Cost' not numeric";
							}
						} else {
							$errormsg_html[] = "'Sub Item' &gt; {$no} &gt; 'Cost' not specified";
						}

						$subitem['id'] = intval($subitem['id']);
						$subitems[] = $subitem;

					} else {
						$errormsg_html[] = "'Sub Item' &gt; {$no} &gt; 'Cost' / 'Name' not specified";
					}

				}

				$no++;

			}

		}

		if (!count($subitems)) {
			$errormsg_html[] = "'Type' &gt; 'Multi Item(s)' specified, however without any 'Sub Item(s)'";
		}

	}

	//If no errors
	if (count($errormsg_html) == 0) {

		try {

			$db->transaction(dbmysql::TRANSACTION_START);

			if ($_POST['type'] == 2) {
				$cost = null;
			} else {
				$cost = $_POST['cost'];
			}

			$no = $_POST['no'];
			$no = strtolower($no);

			$record = array(
				'no' => $no,
				'name' => $_POST['name'],
				'description' => $_POST['description'],
			);

			$record = appgeneral::filternonascii_array($record);

			$record['cost'] = $cost;
			$record['no'] = ($record['no']) ? $record['no'] : null;

			if ($pagetype == PAGE_TYPE_EDIT) {

				//$record = array_merge($record, array('lastupdated' => $db->datetimenow()));
				$db->record_update($tbl['menu_item'], $db->rec($record), $db->cond(array("id = {$editid}"), 'AND'));

			} else {

				$record = array_merge($record, array('menu_cat_id' => $category_id));
				//$record = array_merge($record, array('added' => $db->datetimenow(), 'lastupdated' => $db->datetimenow()));
				$db->record_insert($tbl['menu_item'], $db->rec($record));
				$editid = $db->record_insert_id();
				$pagetype = PAGE_TYPE_EDIT;

			}

			//If multi item
			if ($_POST['type'] == 2) {

				//Retrieve previous sub items
				$delete_sub_item_ids = array();
				$sub_menu_item_result = $db->table_query($db->tbl($tbl['menu_item']), $db->col(array('id')), $db->cond(array("sub_menu_item_id = {$editid}"), 'AND'));
				while ($sub_menu_item_record = $db->record_fetch($sub_menu_item_result)) {
					$delete_sub_item_ids[] = $sub_menu_item_record['id'];
				}

				//Save sub items
				foreach ($subitems as $subitem) {

					$record = array(
						'name' => $subitem['name'],
						'cost' => $subitem['cost'],
					);

					$record = appgeneral::filternonascii_array($record);

					if ($subitem['id']) {
						$db->record_update($tbl['menu_item'], $db->rec($record), $db->cond(array("id = {$subitem['id']}", "sub_menu_item_id = {$editid} /* for security */"), 'AND'));
					} else {
						$record = array_merge($record, array('sub_menu_item_id' => $editid));
						$db->record_insert($tbl['menu_item'], $db->rec($record));
					}

					//If editing an existing item
					if ($subitem['id']) {

						//Remove from the list of sub items to delete
						$key = array_search($subitem['id'], $delete_sub_item_ids);
						unset($delete_sub_item_ids[$key]);

					}

				}

				//Go through all sub items that have dissapeared (been deleted)
				foreach ($delete_sub_item_ids as $id) {
					$db->record_delete($tbl['menu_item'], $db->cond(array("id = {$id}"), 'AND'));
				}

			}

			$db->record_update($tbl['restaurant'], $db->rec(array('lastupdated' => $db->datetimenow())), $db->cond(array("id = {$restaurant_id}"), 'AND'));

			$db->transaction(dbmysql::TRANSACTION_COMMIT);

		} catch (Exception $exception) {

			$db->transaction(dbmysql::TRANSACTION_ROLLBACK);

			throw $exception;

		}

		header("Location: {$cfg['site_url']}" . navpd::back());

	}

}




//If page posted
if (isset($_POST['formposted'])) {

	$formdata = $_POST;

	$formdata['subitem'] = isset($_POST['subitem']) ? $_POST['subitem'] : array();

} else {

	//If page type edit
	if ($pagetype == PAGE_TYPE_EDIT) {

		//Retrieve table data
		$menu_item_result = $db->table_query($db->tbl($tbl['menu_item']), $db->col(array('no', 'name', 'description', 'cost')), $db->cond(array("id = {$editid}", "sub_menu_item_id IS NULL"), 'AND'), '', 0, 1);
		if (!($menu_item_record = $db->record_fetch($menu_item_result))) {
			throw new Exception("Specified edit id \"{$editid}\" not found");
		}

		$formdata = $menu_item_record;

		$formdata['subitem'] = array();

		//If null cost, must have sub items
		if ($menu_item_record['cost'] == null) {

			//Retrieve sub items
			$sub_menu_item_result = $db->table_query($db->tbl($tbl['menu_item']), $db->col(array('id', 'name', 'cost')), $db->cond(array("sub_menu_item_id = {$editid}"), 'AND'), 'name + 0 ASC');
			while ($sub_menu_item_record = $db->record_fetch($sub_menu_item_result)) {
				$id = $sub_menu_item_record['id'];
				$formdata['subitem'][$id] = array(
					'id' => $sub_menu_item_record['id'],
					'name' => $sub_menu_item_record['name'],
					'cost' => $sub_menu_item_record['cost'],
				);
			}

			$formdata['type'] = 2; //Multi Item(s)

		} else {
			$formdata['type'] = 1; //Single Item
		}

	} else {

		$formdata = array(
			'no' => '',
			'name' => '',
			'description' => '',
			'cost' => '',
			'type' => 1, //Single Item
		);

		$formdata['subitem'] = array('default' => array('id' => '', 'name' => '', 'cost'=> ''));

	}

}


$self_args = array();

if ($pagetype == PAGE_TYPE_EDIT) {
	$self_args = array('editid' => $editid);
}


$link_h = navpd::self($self_args);

$formdatah = lib::htmlentities_array($formdata);

//Convert errors to html
$errormsgs_html = appgeneral::errormsgs_html($errormsg_html);

$btn_back = btn::create('<< Back', btn::TYPE_LINK, navpd::back(), '', '', 'Back');
$btn_update = btn::create('Update', btn::TYPE_SUBMIT);

$nav_html = <<<EOHTML
<div class="navigation">
	<div class="left">{$btn_back}</div>
	<div class="right">{$btn_update}</div>
</div>
EOHTML;

//Retrieve restaurant information for specified category
$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $db->col(array('name')), $db->cond(array("id = {$restaurant_id}"), 'AND'), '', 0, 1);
if (!($restaurant_record = $db->record_fetch($restaurant_result))) {
	throw new Exception("Restaurant id \"{$restaurant_id}\" not found");
}

//Retrieve category information for specified category
$menu_cat_result = $db->table_query($db->tbl($tbl['menu_cat']), $db->col(array('name')), $db->cond(array("id = {$category_id}"), 'AND'), '', 0, 1);
if (!($menu_cat_record = $db->record_fetch($menu_cat_result))) {
	throw new Exception("Restaurant id \"{$restaurant_id}\" not found");
}

$categoryname = $menu_cat_record['name'];
$restaurantname = $restaurant_record['name'];

$restaurantname_h = htmlentities($restaurantname);
$categoryname_h = htmlentities($categoryname);

//$type_radios_html = lib::create_radio('type', $cfg['menu_item_type'], $formdata['type']);

$type_radios_html = lib::create_radio('type', $cfg['menu_item_type'], $formdata['type'], '', '', 'onclick="typechanged()"');

$btn_subitem_delete_html_js = btn::create('', btn::TYPE_LINK, '#', $cfg['btn_template_path'].'icons/delete.png', 'deletesubitem(\'=indexid=\'); return false;', "Delete sub item");
$btn_subitem_delete_html_js = addslashes($btn_subitem_delete_html_js);

$btn_subitem_add = btn::create('Add', btn::TYPE_LINK, '#', $cfg['btn_template_path'].'icons/additem.png', 'addsubitem(); return false;', "Add sub item");

$subitems_js = lib::array_jsarray('subitems', $formdata['subitem']);

$body_html = <<<EOHTML

<div class="addeditpage">

	<h2>{$restaurantname_h} &gt; Menu Categories &gt; {$categoryname_h} Items &gt; {$title}</h2>

	<form method="post" action="{$link_h}" id="mainform" class="mainform">
		<div><input type="hidden" name="formposted" value="1" /></div>

{$nav_html}

{$errormsgs_html}

		<div class="tablecontainer">
			<table cellspacing="0" class="addedit">
				<tr>
					<th class="addedit"><label for="no">No:</label></th>
					<td class="addedit"><input type="text" name="no" id="no" value="{$formdatah['no']}" class="inputtxt" maxlength="4" /></td>
				</tr>
				<tr>
					<th class="addedit"><label for="name">Nombre: <span class="required">*</span></label></th>
					<td class="addedit"><input type="text" name="name" id="name" value="{$formdatah['name']}" class="inputtxt" maxlength="255" /></td>
				</tr>
				<tr>
					<th class="addedit"><label for="description">Descripcion:</label></th>
					<td class="addedit"><textarea rows="2" name="description" id="description" cols="20" class="inputtxtarea">{$formdatah['description']}</textarea></td>
				</tr>
				<tr>
					<th class="addedit"><label for="type">Tipo: <span class="required">*</span></label></th>
					<td class="addedit">{$type_radios_html}</td>
				</tr>
				<tr id="cost_block">
					<th class="addedit"><label for="cost">Precio: <span class="required">*</span></label></th>
					<td class="addedit"><input type="text" name="cost" id="cost" value="{$formdatah['cost']}" class="inputtxt" maxlength="255" /></td>
				</tr>
				<tr id="subitems_block">
					<th class="addedit"><label>Sub Item(s):</label></th>
					<td class="addedit">
						<div id="subitems_html"></div>
						{$btn_subitem_add}
					</td>
				</tr>
			</table>
		</div>

{$nav_html}

	</form>

</div>

<script type="text/javascript">
  <!--

	var btn_subitem_delete_html = "{$btn_subitem_delete_html_js}";

	var {$subitems_js}

  //-->
</script>

EOHTML;

$headeraddin_html = <<<EOHTML
<script src="{$cfg['site_url']}resources/admin_menu_item_addedit/admin_menu_item_addedit.js" type="text/javascript"></script>
EOHTML;

$template = new admin_template();
$template->setmainnavsection('restaurant');
$template->settitle("{$restaurantname} > Menu Categories > {$categoryname} Items > Add/Edit");
$template->setheaderaddinhtml($headeraddin_html);
$template->setbodyhtml($body_html);
$template->display();

?>