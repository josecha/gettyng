<?php

include $includes_path . 'config.php';
include $cfg['userdata_path'] . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$admin_auth = new admin_auth();
$admin_auth->handle();
$authinfo = $admin_auth->getauthinfo();
$admin_auth->login_required();
$admin_auth->check_permission();


$restaurant_id = intval($_GET['restaurant_id']);

//Chcek have permission for this restaurant id
$admin_auth->check_permission_restaurant($restaurant_id);

//Retrieve restaurant information for specified category
$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $db->col(array('name')), $db->cond(array("id = {$restaurant_id}"), 'AND'), '', 0, 1);
if (!($restaurant_record = $db->record_fetch($restaurant_result))) {
	throw new Exception("Restaurant id \"{$restaurant_id}\" not found");
}

$pagename = $restaurant_record['name'];


//Delete restaurant
if (isset($_POST['actiontype'])) {

	$actionid = intval($_POST['actionid']);

	if ($_POST['actiontype'] == 'delete') {

		try {

			$db->transaction(dbmysql::TRANSACTION_START);

			//Reorder categories
			appgeneral::category_reorder($actionid, 'delete');

			//Delete category
			$db->record_delete($tbl['menu_cat'], $db->cond(array("id = ".$actionid), 'AND'));

			//Set restaurant last updated
			$db->record_update($tbl['restaurant'], $db->rec(array('lastupdated' => $db->datetimenow())), $db->cond(array("id = {$restaurant_id}"), 'AND'));

			$db->transaction(dbmysql::TRANSACTION_COMMIT);

		} catch (Exception $exception) {
			$db->transaction(dbmysql::TRANSACTION_ROLLBACK);
			throw $exception;
		}

	} else if ( ($_POST['actiontype'] == 'move_up') || ($_POST['actiontype'] == 'move_down') ) {

		$move_type = preg_replace('/^move\_/', '', $_POST['actiontype']);

		appgeneral::category_reorder($actionid, $move_type);

	} else {
		throw new Exception("Action type \"{$_POST['actiontype']}\" unknown");
	}

}

$link_addnew = navpd::forward(array('p' => 'menu_category_addedit', 'restaurant_id' => $restaurant_id));
$btn_addnew = btn::create('Add Category', btn::TYPE_LINK, $link_addnew, $cfg['btn_template_path'].'icons/add.png', '', 'Add Category');
$right_html = $btn_addnew;

$left_html = btn::create('Back', btn::TYPE_LINK, navpd::back(), $cfg['btn_template_path'].'icons/back.png', '', 'Back');

//Table
$tablehtml = new tablehtml();
$tablehtml->shotbuttoncol = true;
$tablehtml->sortby = 'catorder';
$tablehtml->sortdir = 'asc';
//$tablehtml->table_class = 'examplelist';
$tablehtml->sortable_columns = array();
$tablehtml->parsegetvars($_GET);

//$tablehtml->addcolumn('checkbox', '');
$tablehtml->addcolumn('button_move', '');
$tablehtml->addcolumn('name', 'Name');
$tablehtml->addcolumn('button', '');

$table_html = $tablehtml->html(
	$tablehtml->html_action(),
	$tablehtml->html_table(
		$tablehtml->html_table_titles(),
		$tablehtml->html_table_rows(
			$tablehtml->tabledatahtml_fromcallback('callback_tabledatahtml')
		),
		$tablehtml->html_table_nav($right_html, $left_html),
		$tablehtml->html_table_errors()
	)
);

function callback_tabledatahtml($tablehtml, $limit_offset, $limit_count, $query_order) {
	global $cfg, $tbl, $db, $restaurant_id;

	$select_sql = $db->col(array('id', 'name', 'catorder'));
	$result = $db->table_query($db->tbl($tbl['menu_cat']), $select_sql, $db->cond(array("restaurant_id = {$restaurant_id}"), 'AND'), $db->order($query_order), $limit_offset, $limit_count, dbmysql::TBLQUERY_FOUNDROWS);

	$tablehtml->paging_totrows = $db->query_foundrows();

	$tabledatahtml = array();
	while ($data = $db->record_fetch($result)) {

		$datah = lib::htmlentities_array($data);

		//Delete button
		$name_js = addslashes($datah['name']);
		$link_self = addslashes(navpd::self());
		$onclick_js = "performaction('{$link_self}', 'Really delete category \'{$name_js}\' and all menu items in the category?', 'delete', {$data['id']}); return false;";
		$btn_delete = btn::create('Delete', btn::TYPE_LINK, '#', $cfg['btn_template_path'].'icons/delete.png', $onclick_js, "Delete category \"{$name_js}\"");

		//If can move up
		if ($data['catorder'] != 0) {

			//Move up button
			$name_js = addslashes($datah['name']);
			$link_self = addslashes(navpd::self());
			$onclick_js = "performaction('{$link_self}', '', 'move_up', {$data['id']}); return false;";
			$btn_moveup = btn::create('Up', btn::TYPE_LINK, '#', $cfg['btn_template_path'].'icons/move_up.png', $onclick_js, "Move category \"{$name_js}\" up", "Move category \"{$name_js}\" up");

		} else {
			$btn_moveup = btn::create_nolink('Up', $cfg['btn_template_path'].'icons/move_up.png', "Move category \"{$name_js}\" down", "Move category \"{$name_js}\" up");
		}

		//If can move down
		if ($data['catorder'] != $tablehtml->paging_totrows-1) {

			//Move down button
			$name_js = addslashes($datah['name']);
			$link_self = addslashes(navpd::self());
			$onclick_js = "performaction('{$link_self}', '', 'move_down', {$data['id']}); return false;";
			$btn_movedown = btn::create('Down', btn::TYPE_LINK, '#', $cfg['btn_template_path'].'icons/move_down.png', $onclick_js, "Move category \"{$name_js}\" down", "Move category \"{$name_js}\" down");

		} else {
			$btn_movedown = btn::create_nolink('Down', $cfg['btn_template_path'].'icons/move_down.png', "Move category \"{$name_js}\" down", "Move category \"{$name_js}\" down");
		}

		//Edit button
		$link_edit = navpd::forward(array('p' => 'menu_category_addedit', 'editid' => $data['id']));
		$btn_edit = btn::create('Edit', btn::TYPE_LINK, $link_edit, $cfg['btn_template_path'].'icons/edit.png', '', 'Edit');

		//Menu items button
		$link_menu_item = navpd::forward(array('p' => 'menu_item', 'category_id' => $data['id']));
		$btn_menu_item = btn::create('Items', btn::TYPE_LINK, $link_menu_item, $cfg['btn_template_path'].'icons/menu.png', '', 'Items');

		$buttons = $tablehtml->html_table_buttons(array($btn_menu_item, $btn_edit, $btn_delete));

		$tabledatahtml[] = array(
			'button_move' => $tablehtml->html_table_buttons(array($btn_moveup, $btn_movedown)),
			'name' => htmlentities(appgeneral::trim_length($data['name'], 30)),
			'button' => $buttons,
		);

	}

	return $tabledatahtml;

}

$pagename_h = htmlentities($pagename);

$body_html = <<<EOHTML

<h2>{$pagename_h} &gt; Menu Categories</h2>

{$table_html}

<!--//
<div class="maintable-back_btn">
{btn_back}
</div>
//-->

EOHTML;


$template = new admin_template();
$template->setmainnavsection('restaurant');
$template->settitle("{$pagename} > Menu Categories");
//$template->setheaderaddinhtml($headeraddin_html);
$template->setbodyhtml($body_html);
$template->display();

?>