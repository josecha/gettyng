<?php

include $includes_path . 'config.php';
include $cfg['userdata_path'] . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$admin_auth = new admin_auth();
$admin_auth->handle();
$authinfo = $admin_auth->getauthinfo();
$admin_auth->login_required();
$admin_auth->check_permission();


//Delete restaurant
if ( (isset($_POST['actiontype'])) && ($_POST['actiontype'] == 'delete') ) {
	$db->record_delete($tbl['town'], $db->cond(array("id = ".intval($_POST['actionid'])), 'AND'));
}

$link_addnew = navpd::forward(array('p' => 'town_addedit'));
$btn_addnew = btn::create('Add Town', btn::TYPE_LINK, $link_addnew, $cfg['btn_template_path'].'icons/add.png', '', 'Add Town');
$right_html = $btn_addnew;


//Table
$tablehtml = new tablehtml();
$tablehtml->shotbuttoncol = true;
$tablehtml->sortby = 'name';
$tablehtml->sortdir = 'asc';
//$tablehtml->table_class = 'examplelist';
$tablehtml->sortable_columns = array('name', 'navname', 'city', 'no_restaurants');
$tablehtml->parsegetvars($_GET);

//$tablehtml->addcolumn('checkbox', '');
$tablehtml->addcolumn('name', 'Name');
$tablehtml->addcolumn('city', 'City');
$tablehtml->addcolumn('no_restaurants', 'No. Restaurants');
$tablehtml->addcolumn('button', '');

$table_html = $tablehtml->html(
	$tablehtml->html_action(),
	$tablehtml->html_table(
		$tablehtml->html_table_titles(),
		$tablehtml->html_table_rows(
			$tablehtml->tabledatahtml_fromcallback('callback_tabledatahtml')
		),
		$tablehtml->html_table_nav($right_html),
		$tablehtml->html_table_errors()
	)
);

function callback_tabledatahtml($tablehtml, $limit_offset, $limit_count, $query_order) {
	global $cfg, $tbl, $db;

	$select_sql = $db->col(array('id', 'name')) . ', ';
	$select_sql .= "(SELECT name FROM {$tbl['city']} WHERE id = city_id) AS city, ";
	$select_sql .= "(SELECT COUNT(*) FROM {$tbl['restaurant']} WHERE {$tbl['restaurant']}.town_id = {$tbl['town']}.id) AS no_restaurants";
	$result = $db->table_query($db->tbl($tbl['town']), $select_sql, $db->cond(array(), 'AND'), $db->order($query_order), $limit_offset, $limit_count, dbmysql::TBLQUERY_FOUNDROWS);

	$tabledatahtml = array();
	while ($data = $db->record_fetch($result)) {

		$datah = lib::htmlentities_array($data);

		if (!$data['no_restaurants']) {
			//Delete button
			$name_js = addslashes($datah['name']);
			$link_self = addslashes(navpd::self());
			$onclick_js = "performaction('{$link_self}', 'Really delete town \'{$name_js}\'?', 'delete', {$data['id']}); return false;";
			$btn_delete = btn::create('Delete', btn::TYPE_LINK, '#', $cfg['btn_template_path'].'icons/delete.png', $onclick_js, "Delete town \"{$name_js}\"");
		} else {
			$btn_delete = btn::create_nolink('Delete', $cfg['btn_template_path'].'icons/delete.png', "Delete city \"{$data['name']}\"");
		}

		//Edit button
		$link_edit = navpd::forward(array('p' => 'town_addedit', 'editid' => $data['id']));
		$btn_edit = btn::create('Edit', btn::TYPE_LINK, $link_edit, $cfg['btn_template_path'].'icons/edit.png', '', 'Edit');

		$buttons = $tablehtml->html_table_buttons(array($btn_edit, $btn_delete));

		$tabledatahtml[] = array(
			//'checkbox' => '<input type="checkbox" name="actionids[]" value="'.$data['id'].'">',
			//'checkbox' => '',
			'name' => htmlentities(appgeneral::trim_length($data['name'], 30)),
			'city' => htmlentities(appgeneral::trim_length($data['city'], 30)),
			'no_restaurants' => htmlentities(appgeneral::trim_length($data['no_restaurants'], 30)),
			'button' => $buttons,
		);

	}

	$tablehtml->paging_totrows = $db->query_foundrows();

	return $tabledatahtml;

}

$body_html = <<<EOHTML

{$table_html}

EOHTML;


$template = new admin_template();
$template->setmainnavsection('town');
$template->settitle('Towns');
//$template->setheaderaddinhtml($headeraddin_html);
$template->setbodyhtml($body_html);
$template->display();

?>