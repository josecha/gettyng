<?php

include $includes_path . 'config.php';
include $cfg['userdata_path'] . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$admin_auth = new admin_auth();
$admin_auth->handle();
$authinfo = $admin_auth->getauthinfo();
$admin_auth->login_required();
$admin_auth->check_permission();


//Delete restaurant
if ( (isset($_POST['actiontype'])) && ($_POST['actiontype'] == 'delete') ) {

	$action_id = intval($_POST['actionid']);

	//Check user has permission on this restaurant / can delete it
	$admin_auth->check_permission_restaurant($action_id);

	//Delete restaurant
	appgeneral::restaurant_delete($action_id);
	//$db->record_delete($tbl['restaurant'], $db->cond(array("id = ".intval($_POST['actionid'])), 'AND'));

}

$link_addnew = navpd::forward(array('p' => 'restaurant'));
$btn_addnew = btn::create('Add Restaurant', btn::TYPE_LINK, $link_addnew, $cfg['btn_template_path'].'icons/add.png', '', 'Add Restaurant');
$right_html = $btn_addnew;


$errormsg_html = array();



//Table
$tablehtml = new tablehtml();
$tablehtml->shotbuttoncol = true;
$tablehtml->sortby = 'added';
$tablehtml->sortdir = 'desc';
//$tablehtml->table_class = 'examplelist';
$tablehtml->sortable_columns = array('name', 'city', 'navname', 'added');
$tablehtml->parsegetvars($_GET);

//$tablehtml->addcolumn('checkbox', '');
$tablehtml->addcolumn('name', 'Nombre');
//$tablehtml->addcolumn('address_1', 'Address 1');
//$tablehtml->addcolumn('town', 'Town');
//$tablehtml->addcolumn('city', 'City');
$tablehtml->addcolumn('city', 'municipio, ciudad');
$tablehtml->addcolumn('added', 'Fecha de apertura');
$tablehtml->addcolumn('button', '');

$table_html = $tablehtml->html(
	$tablehtml->html_action(),
	$tablehtml->html_table(
		$tablehtml->html_table_titles(),
		$tablehtml->html_table_rows(
			$tablehtml->tabledatahtml_fromcallback('callback_tabledatahtml')
		),
		$tablehtml->html_table_nav($right_html),
		$tablehtml->html_table_errors()
	)
);

function callback_tabledatahtml($tablehtml, $limit_offset, $limit_count, $query_order) {
	global $cfg, $tbl, $db, $errormsg_html, $admin_auth;

	//If no errors
	if (count($errormsg_html) == 0) {;

		$cond = array();

		//Add on filter conditions
		$filter_cond = admin_disp_cond::restaurant($_GET);
		$cond = array_merge($cond, $filter_cond);

		$select_sql = $db->col(array('id', 'name', 'address_1', 'status', 'navname', 'added')) . ', ';
		$select_sql .= "(SELECT {$tbl['town']}.name FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id) AS town, ";
		$select_sql .= "(SELECT {$tbl['city']}.name FROM {$tbl['city']} WHERE {$tbl['city']}.id = (SELECT {$tbl['town']}.city_id FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id)) AS city, ";
		$select_sql .= "(SELECT navname FROM {$tbl['town']} WHERE id = town_id) AS town_navname, ";
		$select_sql .= "(SELECT {$tbl['city']}.navname FROM {$tbl['city']} WHERE {$tbl['city']}.id = (SELECT {$tbl['town']}.city_id FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id)) AS city_navname";

		$result = $db->table_query($db->tbl($tbl['restaurant']), $select_sql, $db->cond($cond, 'AND'), $db->order($query_order), $limit_offset, $limit_count, dbmysql::TBLQUERY_FOUNDROWS);

		$tabledatahtml = array();
		while ($data = $db->record_fetch($result)) {

			$datah = lib::htmlentities_array($data);

			//Delete button
			$name_js = addslashes($datah['name']);
			$link_self = addslashes(navpd::self());
			$onclick_js = "performaction('{$link_self}', 'Really delete restaurant \'{$name_js}\'?', 'delete', {$data['id']}); return false;";
			$btn_delete = btn::create('Delete', btn::TYPE_LINK, '#', $cfg['btn_template_path'].'icons/delete.png', $onclick_js, "Delete restaurant \"{$name_js}\"");

			//View button
			if ($data['status'] == 1) {
				$link_view = navfr::link(array('restaurant', $data['city_navname'], $data['town_navname'], $data['navname']));
				$btn_view = btn::create('View', btn::TYPE_LINK, $link_view, $cfg['btn_template_path'].'icons/view.png', '', 'View');
			} else {
				$btn_view = btn::create_nolink('View', $cfg['btn_template_path'].'icons/view.png', "View", "Restaurant Status: Disabled");
			}

			//Edit button
			$link_edit = navpd::forward(array('p' => 'restaurant_addedit', 'editid' => $data['id']));
			$btn_edit = btn::create('Edit', btn::TYPE_LINK, $link_edit, $cfg['btn_template_path'].'icons/edit.png', '', 'Edit');

			//If have permissions on restaurant admin page
			if ($admin_auth->check_permission_page('restaurant_admin')) {
				//Admin button
				$link_admin = navpd::forward(array('p' => 'restaurant_admin', 'editid' => $data['id']));
				$btn_admin = btn::create('Admin', btn::TYPE_LINK, $link_admin, $cfg['btn_template_path'].'icons/edit.png', '', 'Admin');
			} else {
				$btn_admin = '';
			}

			//Menu categories button
			$link_menu_categories = navpd::forward(array('p' => 'menu_category', 'restaurant_id' => $data['id']));
			$btn_menu_categories = btn::create('Menu', btn::TYPE_LINK, $link_menu_categories, $cfg['btn_template_path'].'icons/menu.png', '', 'Menu Categories');

			//Menu import button
			$link_menu_import = navpd::forward(array('p' => 'menu_import', 'restaurant_id' => $data['id']));
			$btn_menu_import = btn::create('Import', btn::TYPE_LINK, $link_menu_import, $cfg['btn_template_path'].'icons/csv.png', '', 'Menu Import');

			$buttons = $tablehtml->html_table_buttons(array($btn_menu_categories, $btn_menu_import, $btn_view, $btn_admin, $btn_edit, $btn_delete));

			$tabledatahtml[] = array(
				//'checkbox' => '<input type="checkbox" name="actionids[]" value="'.$data['id'].'">',
				//'checkbox' => '',
				'name' => htmlentities(appgeneral::trim_length($data['name'], 30)),
				//'address_1' => htmlentities(appgeneral::trim_length($data['address_1'], 20)),
				//'town' => htmlentities(appgeneral::trim_length($data['town'], 15)),
				//'city' => htmlentities(appgeneral::trim_length($data['city'], 15)),
				'city' => htmlentities(appgeneral::trim_length($data['town'] . ', ' . $data['city'], 13)),
				'added' => date('Y-m-d H:i:s', strtotime($data['added'] . ' UTC')),
				'button' => $buttons,
			);

		}

		$tablehtml->paging_totrows = $db->query_foundrows();

	} else {
		$tabledatahtml = array();
	}

	return $tabledatahtml;

}


$filter_fields = array('town_id', 'city_id', 'name');

$filter_args = array('i' => null);
foreach ($filter_fields as $name) {
	if (isset($_GET[$name])) {
		$filter_args[$name] = $_GET[$name];
	}
}

$self_linkh = navpd::link_h();

$formdata = array();
foreach ($filter_fields as $name) {
	$formdata[$name] = isset($_GET[$name]) ? $_GET[$name] : '';
}

$btn_city_search = $btn_town_search = $btn_name_search = btn::create('Search', btn::TYPE_SUBMIT, '', $cfg['btn_template_path'].'icons/search.png');

//Town
$args = array('town_id' => null, 'i' => null);
$form_town_self = navpd::self_form($args);
if ( (isset($_GET['town_id'])) && ($_GET['town_id']) ) {
	$link = navpd::self($args);
	$btn_town_clear = btn::create('Clear', btn::TYPE_LINK, $link, $cfg['btn_template_path'].'icons/clear.png');
} else {
	$btn_town_clear = '';
}

//City
$args = array('city_id' => null, 'i' => null);
$form_city_self = navpd::self_form($args);
if ( (isset($_GET['city_id'])) && ($_GET['city_id']) ) {
	$link = navpd::self($args);
	$btn_city_clear = btn::create('Clear', btn::TYPE_LINK, $link, $cfg['btn_template_path'].'icons/clear.png');
} else {
	$btn_city_clear = '';
}

//Name
$args = array('name' => null, 'i' => null);
$form_name_self = navpd::self_form($args);
if ( (isset($_GET['name'])) && ($_GET['name']) ) {
	$link = navpd::self($args);
	$btn_name_clear = btn::create('Clear', btn::TYPE_LINK, $link, $cfg['btn_template_path'].'icons/clear.png');
} else {
	$btn_name_clear = '';
}

//Retrieve town options
$town_options = array();
$select_sql = $db->col(array('id', 'name')) . ', ';
$select_sql .= "(SELECT {$tbl['city']}.name FROM {$tbl['city']} WHERE {$tbl['city']}.id = {$tbl['town']}.city_id) AS city";
$town_result = $db->table_query($db->tbl($tbl['town']), $select_sql, $db->cond(array(), 'AND'), $db->order(array(array('city', 'ASC'), array('name', 'ASC'))));
while ($town_record = $db->record_fetch($town_result)) {
	$id = $town_record['id'];
	$town_options[$id] = "{$town_record['city']}, {$town_record['name']}";
}

//Retrieve city options
$city_options = array();
$select_sql = $db->col(array('id', 'name'));
$city_result = $db->table_query($db->tbl($tbl['city']), $select_sql, $db->cond(array(), 'AND'), $db->order(array(array('name', 'ASC'))));
while ($city_record = $db->record_fetch($city_result)) {
	$id = $city_record['id'];
	$city_options[$id] = $city_record['name'];
}

//Town
$town_options_html = lib::create_options($town_options, $formdata['town_id'], lib::CREATEOPT_PLEASESELECT);

//City
$city_options_html = lib::create_options($city_options, $formdata['city_id'], lib::CREATEOPT_PLEASESELECT);


$formdatah = lib::htmlentities_array($formdata);

$body_html = <<<EOHTML

{$table_html}

<div class="panel_filter">

	<div class="panelitem">

		<form method="get" action="{$self_linkh}" id="frm_name">

			<div>
{$form_name_self}
			</div>

			<div class="row">
				<div class="title"><label for="name">Nombre</label></div>
				<div class="userinput"><input type="text" name="name" id="name" value="{$formdatah['name']}" class="inputtxt" /></div>
			</div>

			{$btn_name_search} {$btn_name_clear}

		</form>

	</div>

	<div class="panelitem">

		<form method="get" action="{$self_linkh}" id="frm_city">

			<div>
{$form_city_self}
			</div>

			<div class="row">
				<div class="title"><label for="city_id">Ciudad</label></div>
				<div class="userinput"><select name="city_id" id="city_id" class="inputselect" onchange="$('frm_restaurant').submit()">{$city_options_html}</select></div>
			</div>

			{$btn_city_search} {$btn_city_clear}

		</form>

	</div>

	<div class="panelitem">

		<form method="get" action="{$self_linkh}" id="frm_town">

			<div>
{$form_town_self}
			</div>

			<div class="row">
				<div class="title"><label for="town_id">Ubicacion</label></div>
				<div class="userinput"><select name="town_id" id="town_id" class="inputselect" onchange="$('frm_town').submit()">{$town_options_html}</select></div>
			</div>

			{$btn_town_search} {$btn_town_clear}

		</form>

	</div>

</div>

EOHTML;



$template = new admin_template();
$template->setmainnavsection('restaurant');
$template->settitle('Restaurants');
//$template->setheaderaddinhtml($headeraddin_html);
$template->setbodyhtml($body_html);
$template->display();

?>