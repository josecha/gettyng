<?php

include $includes_path . 'config.php';
include $cfg['userdata_path'] . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$admin_auth = new admin_auth();
$admin_auth->handle();
$authinfo = $admin_auth->getauthinfo();
$admin_auth->login_required();
$admin_auth->check_permission();


define('PAGE_TYPE_ADD', 1);
define('PAGE_TYPE_EDIT', 2);

if (isset($_GET['editid'])) {
	$pagetype = PAGE_TYPE_EDIT;
	$editid = intval($_GET['editid']);
	$title = 'Edit Printer';
} else {
	$pagetype = PAGE_TYPE_ADD;
	$title = 'Add Printer';
}

$errormsg_html = array();

if (isset($_POST['formposted'])) {

	if (!$_POST['reference']) {
		$errormsg_html[] = "'Reference' not specified.";
	}

	if (!preg_match("/^\d{15}$/", $_POST['imei'])) {
		$errormsg_html[] = "'IMEI' not valid.";
	}

	if (!preg_match("/^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/", $_POST['ip_address'])) {
		$errormsg_html[] = "'IP Address' not valid.";
	}

	if (strlen($_POST['password']) != 15) {
		$errormsg_html[] = "'Password' not valid.";
	}

	$msgtype = $_POST['msgtype'];
	if (!isset($cfg['printer_msgtype'][$msgtype])) {
		$errormsg_html[] = "'Message Type' not valid.";
	}

	//Check not a duplicate name / navname
	$cond_base = array();
	if ($pagetype == PAGE_TYPE_EDIT) {
		$cond_base[] = "id != {$editid}";
	}

	//Check for duplicate imei
	$cond = array_merge($cond_base, array("imei = '".$db->es($_POST['imei'])."'"));
	$printer_result = $db->table_query($db->tbl($tbl['printer']), $db->col(array('id', 'reference')), $db->cond($cond, 'AND'), '', 0, 1);
	if ($printer_record = $db->record_fetch($printer_result)) {
		$errormsg_html[] = "'IP Address' is already used by Printer &quot;".htmlentities($printer_record['reference'])."&quot;";
	}

	//Check for duplicate ip address
	$cond = array_merge($cond_base, array("ip_address = '".$db->es($_POST['ip_address'])."'"));
	$printer_result = $db->table_query($db->tbl($tbl['printer']), $db->col(array('id', 'reference')), $db->cond($cond, 'AND'), '', 0, 1);
	if ($printer_record = $db->record_fetch($printer_result)) {
		$errormsg_html[] = "'IP Address' is already used by Printer &quot;".htmlentities($printer_record['reference'])."&quot;";
	}

	//If no errors
	if (count($errormsg_html) == 0) {

		//$db->transaction(dbmysql::TRANSACTION_START);

		$record = array(
			'reference' => $_POST['reference'],
			'imei' => $_POST['imei'],
			'ip_address' => $_POST['ip_address'],
			'password' => $_POST['password'],
			'msgtype' => $_POST['msgtype'],
		);

		$record = appgeneral::filternonascii_array($record);

		if ($pagetype == PAGE_TYPE_EDIT) {

			//$record = array_merge($record, array('lastupdated' => $db->datetimenow()));
			$db->record_update($tbl['printer'], $db->rec($record), $db->cond(array("id = {$editid}"), 'AND'));

		} else {

			//$record = array_merge($record, array('added' => $db->datetimenow(), 'lastupdated' => $db->datetimenow()));
			$db->record_insert($tbl['printer'], $db->rec($record));
			$editid = $db->record_insert_id();
			$pagetype = PAGE_TYPE_EDIT;

		}

		//$db->transaction(dbmysql::TRANSACTION_COMMIT);

		header("Location: {$cfg['site_url']}" . navpd::back());

	}

}




//If page posted
if (isset($_POST['formposted'])) {

	$formdata = $_POST;

} else {

	//If page type edit
	if ($pagetype == PAGE_TYPE_EDIT) {

		//Retrieve table data
		$city_result = $db->table_query($db->tbl($tbl['printer']), $db->col(array('reference', 'imei', 'ip_address', 'password', 'msgtype')), $db->cond(array("id = {$editid}"), 'AND'), '', 0, 1);
		if (!($city_record = $db->record_fetch($city_result))) {
			throw new Exception("Specified edit id \"{$editid}\" not found");
		}

		$formdata = $city_record;

	} else {

		$formdata = array(
			'reference' => '',
			'imei' => '',
			'ip_address' => '',
			'password' => '',
			'msgtype' => 1,
		);

	}

}


$self_args = array();

if ($pagetype == PAGE_TYPE_EDIT) {
	$self_args = array('editid' => $editid);
}


$link_h = navpd::self($self_args);

$formdatah = lib::htmlentities_array($formdata);

$msgtype_radio_html = lib::create_radio('msgtype', $cfg['printer_msgtype'], $formdata['msgtype']);

//Convert errors to html
$errormsgs_html = appgeneral::errormsgs_html($errormsg_html);

$btn_back = btn::create('<< Back', btn::TYPE_LINK, navpd::back(), '', '', 'Back');
$btn_update = btn::create('Update', btn::TYPE_SUBMIT);

$nav_html = <<<EOHTML
<div class="navigation">
	<div class="left">{$btn_back}</div>
	<div class="right">{$btn_update}</div>
</div>
EOHTML;

$body_html = <<<EOHTML

<div class="addeditpage">

	<h2>{$title}</h2>

	<form method="post" action="{$link_h}" id="mainform" class="mainform">
		<div><input type="hidden" name="formposted" value="1" /></div>

{$nav_html}

{$errormsgs_html}

		<div class="tablecontainer">
			<table cellspacing="0" class="addedit">
				<tr>
					<th class="addedit"><label for="reference">Reference: <span class="required">*</span></label></th>
					<td class="addedit"><input type="text" name="reference" id="reference" value="{$formdatah['reference']}" class="inputtxt" /></td>
				</tr>
				<tr>
					<th class="addedit"><label for="imei">IMEI: <span class="required">*</span></label></th>
					<td class="addedit"><input type="text" name="imei" id="imei" value="{$formdatah['imei']}" class="inputtxt" maxlength="15" /></td>
				</tr>
				<tr>
					<th class="addedit"><label for="ip_address">IP Address: <span class="required">*</span></label></th>
					<td class="addedit"><input type="text" name="ip_address" id="ip_address" value="{$formdatah['ip_address']}" class="inputtxt" maxlength="15" /></td>
				</tr>
				<tr>
					<th class="addedit"><label for="password">Password: <span class="required">*</span></label></th>
					<td class="addedit"><input type="text" name="password" id="password" value="{$formdatah['password']}" class="inputtxt" maxlength="15" /></td>
				</tr>
				<tr>
					<th class="addedit"><label for="msgtype">Message Type: <span class="required">*</span></label></th>
					<td class="addedit">{$msgtype_radio_html}</td>
				</tr>
			</table>
		</div>

{$nav_html}

	</form>

</div>

EOHTML;


$template = new admin_template();
$template->setmainnavsection('printer');
$template->settitle('Printer > Add/Edit');
//$template->setheaderaddinhtml($headeraddin_html);
$template->setbodyhtml($body_html);
$template->display();

?>