<?php

include $includes_path . 'config.php';
include $cfg['userdata_path'] . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
//exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

if ($cfg['printer_return_log']) {
	file_put_contents($cfg['printer_return_log'], print_r(array(date('r'), $_GET, $_POST), true), FILE_APPEND);
}

try {

	//-------------------------------

	function display_error($errormsg) {

		//Do not inform the printer as we do not want a retry

		//header('HTTP/1.0 404 Not Found');
		//header('Status: 404 Not Found');

		echo $errormsg;

	}

	function check_authorised($printer_id, $order_id) {
		global $db, $tbl;

		$printer_id = intval($printer_id);
		$order_id = intval($order_id);

		//Retrieve order details
		$order_result = $db->table_query($db->tbl($tbl['order']), $db->col(array('restaurant_id')), $db->cond(array("id = {$order_id}"), 'AND'), '', 0, 1);
		if (!($order_record = $db->record_fetch($order_result))) {
			throw new Exception("Order id \"{$order_id}\" (message id) not found");
		}

		//Retrieve restaurant details
		$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $db->col(array('printer_id')), $db->cond(array("id = {$order_record['restaurant_id']}"), 'AND'), '', 0, 1);
		if (!($restaurant_record = $db->record_fetch($restaurant_result))) {
			throw new Exception("Restaurant id \"{$order_record['restaurant_id']}\" (message id) not found");
		}

		//Check printer is associated with restaurant
		if ($restaurant_record['printer_id'] != $printer_id) {
			throw new Exception("Order id \"{$order_id}\" (message id) is not associated with printer id {$restaurant_record['printer_id']} through restaurant id \"{$order_record['restaurant_id']}\"");
		}

	}

	//-------------------------------

	//Handle power on notification
	function maestro_handle_poweron($postback) {
		global $db, $tbl, $printer_record;

		$db->record_update($tbl['printer'], $db->rec(array('lastpoweron' => $db->datetimenow())), $db->cond(array("id = {$printer_record['id']}"), 'AND'));

	}

	//Handle message printed notification
	function maestro_handle_printed($postback, $msgid) {
		global $db, $tbl, $printer_record;

		$msgid = intval($msgid);

		//Check message id / order id is authorised to use this printer
		check_authorised($printer_record['id'], $msgid);

		//Retrieve order details
		$order_result = $db->table_query($db->tbl($tbl['order']), $db->col(array('id', 'status')), $db->cond(array("id = {$msgid}"), 'AND'), '', 0, 1);
		if (!($order_record = $db->record_fetch($order_result))) {
			throw new Exception("Order id \"{$order_id}\" (message id) not found");
		}

		if ($order_record['status'] == 341) { //Message Waiting For Button Print

			//Update status, message printed after press
			$db->record_update($tbl['order'], $db->rec(array('status' => 332)), $db->cond(array("id = {$order_record['id']}"), 'AND')); //Message Printed After Press

			//Notify customer order received successfully
			cron_general::notify_customer_success($order_record['id']);

		} else if ($order_record['status'] == 342) { //Message Waiting For Button Print, Delivery Button

			//Update status, waiting for delivery press
			$db->record_update($tbl['order'], $db->rec(array('status' => 343)), $db->cond(array("id = {$order_record['id']}"), 'AND')); //Message Waiting For Delivery Button

		} else if ($order_record['status'] == 331) { //Message print immediately

			//No need to do anything, already sent the email confirmation / marked successful because it was a print immediately message

		} else {
			throw new Exception("Postback for unhandled status \"{$order_record['status']}\"");
		}

	}

	//Handle message button press notification
	function maestro_handle_msgbtnpress($postback, $msgid, $no_presses) {
		global $db, $tbl, $cfg, $printer_record;

		$msgid = intval($msgid);

		//Check message id / order id is authorised to use this printer
		check_authorised($printer_record['id'], $msgid);

		//Retrieve order details
		$order_result = $db->table_query($db->tbl($tbl['order']), $db->col(array('id', 'status')), $db->cond(array("id = {$msgid}"), 'AND'), '', 0, 1);
		if (!($order_record = $db->record_fetch($order_result))) {
			throw new Exception("Order id \"{$order_id}\" (message id) not found");
		}

		if ($order_record['status'] == 343) { //Message Waiting For Delivery Button

			//Update status, message printed after press
			$db->record_update($tbl['order'], $db->rec(array('status' => 333)), $db->cond(array("id = {$order_record['id']}"), 'AND')); //Message Printed After Press, Delivery

			//Check if number of presses is valid
			$found = false;
			foreach ($cfg['printer_delivery_time'] as $delivery_time) {
				if ($delivery_time['press'] == $no_presses) {
					$found = true;
					break;
				}
			}

			if ($found) {

				//Update delivery
				$db->record_update($tbl['order'], $db->rec(array('delivery_min' => $delivery_time['delivery_min'])), $db->cond(array("id = {$order_record['id']}"), 'AND'));

			} else {
				//Found $no_presses which is not a valid number of presses
			}

			//Notify customer order received successfully, and when it will be delivered
			cron_general::notify_customer_success($order_record['id']);

		} else {
			throw new Exception("Postback for unhandled status \"{$order_record['status']}\" (message should have been in status watiing for delivery button presses)");
		}

	}

	//-------------------------------

	//Initialise postback handling
	$maestro_printer_postback = new maestro_printer_postback();

	//If auto add printers enabled
	if ($cfg['print_manage_auto']) {

		//See if already have printer details (by IMEI)
		$printer_result = $db->table_query($db->tbl($tbl['printer']), $db->col(array('id', 'imei', 'password')), $db->cond(array("imei = '".$db->es($maestro_printer_postback->imei)."'"), 'AND'), '', 0, 1);
		if ($printer_record = $db->record_fetch($printer_result)) {

			$record = array(
				'ip_address' => $maestro_printer_postback->ip_address,
				'password' => $maestro_printer_postback->password,
			);
			
			$db->record_update($tbl['printer'], $db->rec($record), $db->cond(array("id = {$printer_record['id']}"), 'AND'));

		} else {

			//See if already have printer details (by ip address
			$printer_result = $db->table_query($db->tbl($tbl['printer']), $db->col(array('id', 'imei', 'password')), $db->cond(array("ip_address = '".$db->es($maestro_printer_postback->ip_address)."'"), 'AND'), '', 0, 1);
			if ($printer_record = $db->record_fetch($printer_result)) {

				$record = array(
					'imei' => $maestro_printer_postback->imei,
					'password' => $maestro_printer_postback->password,
				);
				
				$db->record_update($tbl['printer'], $db->rec($record), $db->cond(array("id = {$printer_record['id']}"), 'AND'));

			} else {

				$record = array(
					'reference' => 'Auto added on ' . date('Y-m-d H:i:s'),
					'imei' => $maestro_printer_postback->imei,
					'ip_address' => $maestro_printer_postback->ip_address,
					'password' => $maestro_printer_postback->password,
					'msgtype' => 1,
					'errormsg_last' => '',
				);
				$db->record_insert($tbl['printer'], $db->rec($record));

			}

		}

	}

	//Retrieve printer details by imei
	$printer_result = $db->table_query($db->tbl($tbl['printer']), $db->col(array('id', 'imei', 'password')), $db->cond(array("imei = '".$db->es($maestro_printer_postback->imei)."'"), 'AND'), '', 0, 1);
	if ($printer_record = $db->record_fetch($printer_result)) {

		//Check password valid
		if ($printer_record['password'] == $maestro_printer_postback->password) {

			//Update details
			$record = array(
				'version_software' => $maestro_printer_postback->software_version,
				'version_config' => $maestro_printer_postback->config_version,
				'sim_no' => $maestro_printer_postback->sim_no,
			);
			
			$db->record_update($tbl['printer'], $db->rec($record), $db->cond(array("id = {$printer_record['id']}"), 'AND'));

			//Set callback to use for poweron notification
			$maestro_printer_postback->poweron_callback = 'maestro_handle_poweron';

			//Set callback to use for message printed notification
			$maestro_printer_postback->printed_callback = 'maestro_handle_printed';

			//Set callback to use for message button press notification
			$maestro_printer_postback->msgbtnpress_callback = 'maestro_handle_msgbtnpress';

			//Process notifications using specified callback functions
			$maestro_printer_postback->process();

		} else {

			//Save error
			$db->record_update($tbl['printer'], $db->rec(array('errormsg_last' => 'Postback password not valid')), $db->cond(array("id = {$printer_record['id']}"), 'AND'));

			display_error('Password not valid');

		}

	} else {
		display_error('IMEI not found');
	}

} catch (Exception $e) {

	//Report error via email
	appgeneral::email_error('Printer Postback', $e->getMessage());

	//throw $e;
	exceptions::savelogentry($e);

	//exceptions::sethandler();
	//throw $e;

}

?>