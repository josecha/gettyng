<?php

include $includes_path . 'config.php';
include $cfg['userdata_path'] . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$admin_auth = new admin_auth();
$admin_auth->handle();
$authinfo = $admin_auth->getauthinfo();
$admin_auth->login_required();
$admin_auth->check_permission();


$link_base_path = htmlentities(navfr::base_path());

$editid = intval($_GET['editid']);

//If edit, check have permission
//if ($pagetype == PAGE_TYPE_EDIT) {
//	$admin_auth->check_permission_restaurant($editid);
//}

//Message dispatch options
$msg_dispatch_options = $cfg['msg_dispatch'];

//Remove phone (2) if phone notify key not specified
if (!$cfg['notify_license_key']) {
	unset($msg_dispatch_options[2]);
}

$errormsg_html = array();

if (isset($_POST['formposted'])) {

	$msg_dispatch = $_POST['msg_dispatch'];
	if (!isset($msg_dispatch_options[$msg_dispatch])) {
		$errormsg_html[] = "'Message Dispatch' not specified.";
	} else {

		if ($msg_dispatch == 3) { //Printer
			if (!$_POST['printer_id']) {
				$errormsg_html[] = "'Printer' not specified.";
			}
		}

		if (!( ( (isset($_POST['payment_cash'])) && ($_POST['payment_cash']) )|| ( (isset($_POST['payment_online'])) && ($_POST['payment_online']) ) )) {
				$errormsg_html[] = "At least one 'Payment' type must be accepted if online ordering is enabled.";
		}

	}

	//If no errors
	if (count($errormsg_html) == 0) {

		try {

			$db->transaction(dbmysql::TRANSACTION_START);

			$postcode = strtoupper($_POST['postcode']);

			$status = ($_POST['status']) ? 1 : 0;

			$record = array(
				'msg_dispatch' => $_POST['msg_dispatch'],
			);

			$record = appgeneral::filternonascii_array($record);

			//Payment types
			foreach ($cfg['payment_types'] as $payment_type_id => $name) {

				$payment_type = $cfg['payment_types_nameassoc'][$payment_type_id];
				$record['payment_' . $payment_type] = 0; //default to no

				if (in_array($payment_type_id, $cfg['payment_types_accepted'])) {

					if ( (isset($_POST['payment_' . $payment_type])) && ($_POST['payment_' . $payment_type]) ) {
						$record['payment_' . $payment_type] = 1;
					}

				}

			}

			$record['printer_id'] = ($msg_dispatch == 3) ? $_POST['printer_id'] : null;

			$record = array_merge($record, array('lastupdated' => $db->datetimenow()));
			$db->record_update($tbl['restaurant'], $db->rec($record), $db->cond(array("id = {$editid}"), 'AND'));

			$db->transaction(dbmysql::TRANSACTION_COMMIT);

		} catch (Exception $exception) {

			$db->transaction(dbmysql::TRANSACTION_ROLLBACK);

			throw $exception;

		}

		header("Location: {$cfg['site_url']}" . navpd::back());

	}

}




//If page posted
if (isset($_POST['formposted'])) {

	$formdata = $_POST;

	$formdata['printer_id'] = (isset($formdata['printer_id'])) ? $formdata['printer_id'] : '';

	$formdata['payment_cash'] = (isset($formdata['payment_cash'])) ? 1 : 0;
	$formdata['payment_online'] = (isset($formdata['payment_online'])) ? 1 : 0;

} else {

	//Retrieve table data
	$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $db->col(array('msg_dispatch', 'printer_id', 'payment_cash', 'payment_online')), $db->cond(array("id = {$editid}"), 'AND'), '', 0, 1);
	if (!($restaurant_record = $db->record_fetch($restaurant_result))) {
		throw new Exception("Specified edit id \"{$editid}\" not found");
	}

	$formdata = $restaurant_record;

}


$self_args = array();
$self_args = array('editid' => $editid);

$link_h = navpd::self($self_args);

$formdatah = lib::htmlentities_array($formdata);

//Printer options
$printer_options = array();
$printer_result = $db->table_query($db->tbl($tbl['printer']), $db->col(array('id', 'reference')), $db->cond(array(), 'AND'), $db->order(array(array('reference', 'ASC'))));
while ($printer_record = $db->record_fetch($printer_result)) {
	$id = $printer_record['id'];
	$printer_options[$id] = $printer_record['reference'];
}

$printer_options_html = lib::create_options($printer_options, $formdata['printer_id'], lib::CREATEOPT_PLEASESELECT);

//Payment types
$payment_checkbox_html = '';
foreach ($cfg['payment_types'] as $payment_type_id => $name) {

	if (in_array($payment_type_id, $cfg['payment_types_accepted'])) {
		$payment_type = $cfg['payment_types_nameassoc'][$payment_type_id];

		$name_h = htmlentities($name);

		$checked = ($formdata['payment_'.$payment_type]) ? 'checked="checked"' : '';

		$payment_checkbox_html .= <<<EOHTML
<input type="checkbox" name="payment_{$payment_type}" id="payment_{$payment_type}" value="1" {$checked} /> <label for="payment_{$payment_type}" class="inputxttitle">{$name_h}</label> &nbsp;&nbsp;&nbsp;&nbsp;
EOHTML;

	}

}


$msg_dispatch_radio_html = lib::create_radio('msg_dispatch', $msg_dispatch_options, $formdata['msg_dispatch'], '', '', 'onclick="msgdispatch_changed()"');


//Convert errors to html
$errormsgs_html = appgeneral::errormsgs_html($errormsg_html);

$btn_back = btn::create('<< Back', btn::TYPE_LINK, navpd::back(), '', '', 'Back');
$btn_update = btn::create('Update', btn::TYPE_SUBMIT);

$nav_html = <<<EOHTML
<div class="navigation">
	<div class="left">{$btn_back}</div>
	<div class="right">{$btn_update}</div>
</div>
EOHTML;

//Retrieve table data
$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $db->col(array('name')), $db->cond(array("id = {$editid}"), 'AND'), '', 0, 1);
if (!($restaurant_record = $db->record_fetch($restaurant_result))) {
	throw new Exception("Specified edit id \"{$editid}\" not found");
}

$title = "{$restaurant_record['name']} > Restaurant Admin";
$titleh = htmlentities($title);

$body_html = <<<EOHTML

<div class="addeditpage">

	<h2>{$titleh}</h2>

	<form method="post" action="{$link_h}" id="mainform" class="mainform" enctype="multipart/form-data">
		<div><input type="hidden" name="formposted" value="1" /></div>

{$nav_html}

{$errormsgs_html}

		<div class="tablecontainer">
			<table cellspacing="0" class="addedit">
				<tr>
					<th class="addedit"><label class="inputxttitle">Message Dispatch:</label></th>
					<td class="addedit">{$msg_dispatch_radio_html}</td>
				</tr>
				<tr id="printer_block">
					<th class="addedit"><label class="inputxttitle" for="printer_id">Printer:</label></th>
					<td class="addedit"><select name="printer_id" id="printer_id">{$printer_options_html}</select></td>
				</tr>
				<tr id="payment_block">
					<th class="addedit"><label class="inputxttitle">Payment</label></th>
					<td class="addedit">{$payment_checkbox_html}</td>
				</tr>
			</table>
		</div>

{$nav_html}

	</form>

</div>

EOHTML;


$headeraddin_html = <<<EOHTML
<script src="{$link_base_path}resources/admin_restaurant_admin/restaurant_admin.js" type="text/javascript"></script>
EOHTML;

$template = new admin_template();
$template->setmainnavsection('restaurant');
$template->settitle($title);
$template->setheaderaddinhtml($headeraddin_html);
$template->setbodyhtml($body_html);
$template->display();

?>