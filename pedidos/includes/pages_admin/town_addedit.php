<?php

include $includes_path . 'config.php';
include $cfg['userdata_path'] . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$admin_auth = new admin_auth();
$admin_auth->handle();
$authinfo = $admin_auth->getauthinfo();
$admin_auth->login_required();
$admin_auth->check_permission();


define('PAGE_TYPE_ADD', 1);
define('PAGE_TYPE_EDIT', 2);

if (isset($_GET['editid'])) {
	$pagetype = PAGE_TYPE_EDIT;
	$editid = intval($_GET['editid']);
	$title = 'Edit Town';
} else {
	$pagetype = PAGE_TYPE_ADD;
	$title = 'Add Town';
}

$errormsg_html = array();

if (isset($_POST['formposted'])) {

	if (!$_POST['name']) {
		$errormsg_html[] = "'Name' not specified.";
	}

	if (!$_POST['city_id']) {
		$errormsg_html[] = "'City' not specified.";
	}

	if ($_POST['navname']) {
		//Check navname is valid
		if (preg_match("%[^a-z0-9\-\_]%", $_POST['navname'])) {
			$errormsg_html[] = "'Navname' contains invalidd characters (only &quot;a-z 0-9 - _&quot; are allowed.";
		}
	} else {
		$errormsg_html[] = "'Navname' not specified.";
	}

	//Check not a duplicate name / navname
	$cond_base = array();
	if ($pagetype == PAGE_TYPE_EDIT) {
		$cond_base[] = "id != {$editid}";
	}

	//Check for duplicate name
	$cond = array_merge($cond_base, array("name = '".$db->es($_POST['name'])."'"));
	$town_result = $db->table_query($db->tbl($tbl['town']), $db->col(array('id')), $db->cond($cond, 'AND'), '', 0, 1);
	if ($town_record = $db->record_fetch($town_result)) {
		$errormsg_html[] = "'Name' has already been used";
	}

	//Check for duplicate navname
	$cond = array_merge($cond_base, array("navname = '".$db->es($_POST['navname'])."'"));
	$town_result = $db->table_query($db->tbl($tbl['town']), $db->col(array('id', 'name')), $db->cond($cond, 'AND'), '', 0, 1);
	if ($town_record = $db->record_fetch($town_result)) {
		$errormsg_html[] = "'Navname' is already used by Town &quot;".htmlentities($town_record['name'])."&quot;";
	}

	//If no errors
	if (count($errormsg_html) == 0) {

		//$db->transaction(dbmysql::TRANSACTION_START);

		$record = array(
			'name' => $_POST['name'],
			'city_id' => $_POST['city_id'],
			'navname' => $_POST['navname'],
			'metadesc' => $_POST['metadesc'],
		);

		$record = appgeneral::filternonascii_array($record);

		if ($pagetype == PAGE_TYPE_EDIT) {

			//$record = array_merge($record, array('lastupdated' => $db->datetimenow()));
			$db->record_update($tbl['town'], $db->rec($record), $db->cond(array("id = {$editid}"), 'AND'));

		} else {

			//$record = array_merge($record, array('added' => $db->datetimenow(), 'lastupdated' => $db->datetimenow()));
			$db->record_insert($tbl['town'], $db->rec($record));
			$editid = $db->record_insert_id();
			$pagetype = PAGE_TYPE_EDIT;

		}

		//$db->transaction(dbmysql::TRANSACTION_COMMIT);

		header("Location: {$cfg['site_url']}" . navpd::back());

	}

}




//If page posted
if (isset($_POST['formposted'])) {

	$formdata = $_POST;

} else {

	//If page type edit
	if ($pagetype == PAGE_TYPE_EDIT) {

		//Retrieve table data
		$town_result = $db->table_query($db->tbl($tbl['town']), $db->col(array('name', 'city_id', 'navname', 'metadesc')), $db->cond(array("id = {$editid}"), 'AND'), '', 0, 1);
		if (!($town_record = $db->record_fetch($town_result))) {
			throw new Exception("Specified edit id \"{$editid}\" not found");
		}

		$formdata = $town_record;

	} else {

		$formdata = array(
			'name' => '',
			'city_id' => '',
			'navname' => '',
			'metadesc' => '',
		);

	}

}


$self_args = array();

if ($pagetype == PAGE_TYPE_EDIT) {
	$self_args = array('editid' => $editid);
}


//Retrieve provinces
$city_options = '';
$city_result = $db->table_query($db->tbl($tbl['city']), $db->col(array('id', 'name')), $db->cond(array(), 'AND'), $db->order(array(array('name', 'ASC'))));
while ($city_record = $db->record_fetch($city_result)) {
	$id = $city_record['id'];
	$city_options[$id] = $city_record['name'];
}

$city_options_html = lib::create_options($city_options, $formdata['city_id'], lib::CREATEOPT_PLEASESELECT);


$link_h = navpd::self($self_args);

$formdatah = lib::htmlentities_array($formdata);

//Convert errors to html
$errormsgs_html = appgeneral::errormsgs_html($errormsg_html);

$btn_back = btn::create('<< Back', btn::TYPE_LINK, navpd::back(), '', '', 'Back');
$btn_update = btn::create('Update', btn::TYPE_SUBMIT);

$nav_html = <<<EOHTML
<div class="navigation">
	<div class="left">{$btn_back}</div>
	<div class="right">{$btn_update}</div>
</div>
EOHTML;

$body_html = <<<EOHTML

<div class="addeditpage">

	<h2>{$title}</h2>

	<form method="post" action="{$link_h}" id="mainform" class="mainform">
		<div><input type="hidden" name="formposted" value="1" /></div>

{$nav_html}

{$errormsgs_html}

		<div class="tablecontainer">
			<table cellspacing="0" class="addedit">
				<tr>
					<th class="addedit"><label for="name">Name: <span class="required">*</span></label></th>
					<td class="addedit"><input type="text" name="name" id="name" value="{$formdatah['name']}" class="inputtxt" maxlength="255" onchange="createnavname('name', 'navname')" onkeyup="createnavname('name', 'navname')" /></td>
				</tr>
				<tr>
					<th class="addedit"><label for="navname">Navname: <span class="required">*</span></label></th>
					<td class="addedit"><input type="text" name="navname" id="navname" value="{$formdatah['navname']}" class="inputtxt" maxlength="20" /></td>
				</tr>
				<tr>
					<th class="addedit"><label for="city_id">City: <span class="required">*</span></label></th>
					<td class="addedit"><select name="city_id" id="city_id">{$city_options_html}</select></td>
				</tr>
				<tr>
					<th class="addedit"><label for="metadesc">Meta Description:</label></th>
					<td class="addedit"><textarea rows="2" name="metadesc" id="metadesc" cols="20" class="inputtxtarea">{$formdatah['metadesc']}</textarea></td>
				</tr>
			</table>
		</div>

{$nav_html}

	</form>

</div>

EOHTML;


$template = new admin_template();
$template->setmainnavsection('town');
$template->settitle('Town > Add/Edit');
//$template->setheaderaddinhtml($headeraddin_html);
$template->setbodyhtml($body_html);
$template->display();

?>