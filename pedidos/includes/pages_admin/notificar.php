<?php
include $includes_path . 'config.php';
include $cfg['userdata_path'] . 'config.php';
include $includes_path . 'general/init.php';
//Set exception handler
exceptions::sethandler();

$usr=htmlspecialchars($_POST['user_name'],ENT_QUOTES);
$order_id=$_GET['id'];

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);




		global $db, $tbl, $cfg;

			//Retrieve order details
		$order_result = $db->table_query($db->tbl($tbl['order']), $db->col(array('id', 'notes', 'handling', 'delivery_charge', 'email', 'name', 'address', 'postcode', 'telephone', 'total', 'restaurant_id', 'restaurant_name','delivery_min')), $db->cond(array("id = {$order_id}"), 'AND'), '', 0, 1);
		if (!($order_record = $db->record_fetch($order_result))) {
			throw new Exception("Order id \"{$order_id}\" not found");
		}

		if( $order_record['delivery_min']==NULL){
	        $db->record_update($tbl['order'], $db->rec(array('delivery_min' => $usr, 'status_change_last' => $db->datetimenow())), $db->cond(array("id = {$order_id}"), 'AND'));
		
		$order_result2 = $db->table_query($db->tbl($tbl['order']), $db->col(array('id', 'notes', 'handling', 'delivery_charge', 'email', 'name', 'address', 'postcode', 'telephone', 'total', 'restaurant_id', 'restaurant_name','delivery_min')), $db->cond(array("id = {$order_id}"), 'AND'), '', 0, 1);
		if (!($order_record2 = $db->record_fetch($order_result2))) {
			throw new Exception("Order id \"{$order_id}\" not found");
		}
		if ($order_record2['delivery_min'] > 0) {
$email_delivery_extra = <<<EOHTML

y ha contestado que un timpo aproximado de  {$order_record2['delivery_min']} minutos recibiras tu orden.

EOHTML;

		} else {
		$email_delivery_extra =<<<EOHTML

y ha contestado que en este momento es imposible enviar tu pedido ya que no cuentan con servicio de entrega en tu localidad.

EOHTML;
	
		}

			$email_body = <<<EOMAIL
Estimado {$order_record['name']},

Gracias por usar nuestro servicio para ser tus pedidos en  {$cfg['site_name']}.

Te enviamos la confirmacion de "{$order_record['restaurant_name']}" que ha recibido tu orden.
{$email_delivery_extra}
Muchas Gracias,

{$cfg['site_name']}
{$cfg['site_url']}

EOMAIL;

		$mail = new phpmailer();
		$mail->Mailer = $cfg['email_method'];
		$mail->From = $cfg['email_system_from'];
		$mail->Sender = $cfg['email_system_from'];
		$mail->FromName = $cfg['email_system_from'];
		$mail->Subject = "{$cfg['site_name']} Orden Receivida";
		$mail->IsHTML(false);
		$mail->Body = $email_body;

		$email = $order_record['email'];

		/*
		if ($order_record['test']) {
			$email = $cfg['email_test'];
		}
		*/

		$name = preg_replace("%[^a-zA-Z0-9\ ]%", '', $order_record['name']);
		$mail->AddAddress($email, $name);

			if ($cfg['email_cc_order']) {
			$mail->AddCC($cfg['email_cc_order'], $cfg['site_name']);
		}

		if (!$mail->Send()) {
			throw new Exception("Unable to send email: {$mail->ErrorInfo}");
		}
		
		
		echo "yes";
		
		
		
		
		
		
		}
		else{
		echo "no";
		
		}
		
		

?>