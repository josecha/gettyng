<?php

include $includes_path . 'config.php';
include $cfg['userdata_path'] . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);


file_put_contents($cfg['phone_notify_return_log'], print_r(array(date('r'), $_GET, $_POST), true), FILE_APPEND);

try {

	if (!isset($_GET['type'])) {
		throw new Exception("Does not look like action from phone notify");
	}

	if ($cfg['phone_notify_ret_password'] != $_GET['a']) {
		throw new Exception("Phone notify password incorrect");
	}

	if (!isset($_GET['order_id'])) {
		throw new Exception("Order id not specified");
	}

	$order_id = intval($_GET['order_id']);

	//If this is an in call status update
	if ($_GET['type'] == 'query') {

		$status_id = $_GET['status_id'];
		if (!isset($cfg['notify_status'][$status_id])) {
			throw new Exception("Status id \"{$status_id}\" unknown");
		}

		//Retrieve details for this order
		$select_sql = $db->col(array('id', 'name', 'email', 'restaurant_name', 'telephone', 'handling', 'test'));
		//$select_sql .= "(SELECT telephone FROM {$tbl['restaurant']} WHERE {$tbl['restaurant']}.id = {$tbl['order']}.restaurant_id) AS restaurant_telephone";
		$order_result = $db->table_query($db->tbl($tbl['order']), $select_sql, $db->cond(array("id = {$order_id}"), 'AND'), $db->order(array(array('id', 'ASC'))));
		if (!($order_record = $db->record_fetch($order_result))) {
			throw new Exception("Order id \"{$order_id}\" not found");
		}

		//Update status last time (used to prevent retries because we have user interaction)
		//Update status
		$db->record_update($tbl['order'], $db->rec(array('status_change_last' => $db->datetimenow())), $db->cond(array("id = {$order_id}"), 'AND'));

		//Update notify status
		$db->record_insert($tbl['notify_log'], $db->rec(array('order_id' => $order_id, 'status' => $status_id, 'logentry' => $db->datetimenow())));

		//If success
		if ($status_id == 10) { //Order Received Successfully

			//Update status to received
			$db->record_update($tbl['order'], $db->rec(array('status' => 231)), $db->cond(array("id = {$order_id}"), 'AND')); //Confirmed by restaurant

			//If type is delivery, update delivery minutes
			if ($order_record['handling'] == 1) {

				$delivery_min = intval($_GET['delivery_min']);

				//Update delivery time minutes
				$db->record_update($tbl['order'], $db->rec(array('delivery_min' => $delivery_min)), $db->cond(array("id = {$order_id}"), 'AND'));

			}

			//Notify customer order received successfully, and when it will be delivered
			cron_general::notify_customer_success($order_id);

		}

	} else {
		//Otherwise this is a call postback

		$status_id = intval($_POST['ResponseCode']) + 100;
		if (!isset($cfg['notify_status'][$status_id])) {
			throw new Exception("Status id \"{$status_id}\" unknown");
		}

		//Update notify status
		$db->record_insert($tbl['notify_log'], $db->rec(array('order_id' => $order_id, 'status' => $status_id, 'logentry' => $db->datetimenow())));

	}

} catch (Exception $e) {

	//Report error via email
	appgeneral::email_error('Phone Notify Postback', $e->getMessage());

	throw $e;

}

?>