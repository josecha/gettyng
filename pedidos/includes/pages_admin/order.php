<?php

include $includes_path . 'config.php';
include $cfg['userdata_path'] . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$admin_auth = new admin_auth();
$admin_auth->handle();
$authinfo = $admin_auth->getauthinfo();
$admin_auth->login_required();
$admin_auth->check_permission();


$link_base_path = htmlentities(navfr::base_path());




$errormsg_html = array();

//Retrieve filter error messages
$errormsg_filter_html = admin_disp_cond::date_errormsgs($_GET, 'added_from', 'added_to', 'Order Date');
$errormsg_html = array_merge($errormsg_html, $errormsg_filter_html);


$right_html = '';

//Table
$tablehtml = new tablehtml();
$tablehtml->shotbuttoncol = true;
$tablehtml->sortby = 'added';
$tablehtml->sortdir = 'desc';
//$tablehtml->table_class = 'examplelist';
$tablehtml->sortable_columns = array('name', 'restaurant_name', 'total', 'status', 'added');
$tablehtml->parsegetvars($_GET);

//$tablehtml->addcolumn('checkbox', '');
$tablehtml->addcolumn('name', 'Customer Name');
$tablehtml->addcolumn('restaurant_name', 'Restaurant');
$tablehtml->addcolumn('total', 'Order Value ('.chr(163).')');
$tablehtml->addcolumn('added', 'Date');
//$tablehtml->addcolumn('button', '');

$table_html = $tablehtml->html(
	$tablehtml->html_action(),
	$tablehtml->html_table(
		$tablehtml->html_table_titles(),
		$tablehtml->html_table_rows(
			$tablehtml->tabledatahtml_fromcallback('callback_tabledatahtml')
		),
		$tablehtml->html_table_nav($right_html),
		$tablehtml->html_table_errors()
	)
);

function callback_tabledatahtml($tablehtml, $limit_offset, $limit_count, $query_order) {
	global $cfg, $tbl, $db, $errormsg_html;

	//If no errors
	if (count($errormsg_html) == 0) {

		$cond = array();

		//Add on filter conditions
		$filter_cond = admin_disp_cond::order($_GET);
		$cond = array_merge($cond, $filter_cond);

		$select_sql = $db->col(array('id', 'name', 'restaurant_name', 'total', 'added'));
		//$select_sql .= "(SELECT name FROM {$tbl['city']} WHERE id = city_id) AS city, ";
		$result = $db->table_query($db->tbl($tbl['order']), $select_sql, $db->cond($cond, 'AND'), $db->order($query_order), $limit_offset, $limit_count, dbmysql::TBLQUERY_FOUNDROWS);

		$tabledatahtml = array();
		while ($data = $db->record_fetch($result)) {

			$datah = lib::htmlentities_array($data);

			//Edit button
			$link_edit = navpd::forward(array('p' => 'town_addedit', 'editid' => $data['id']));
			$btn_edit = btn::create('Edit', btn::TYPE_LINK, $link_edit, $cfg['btn_template_path'].'icons/edit.png', '', 'Edit');

			$tabledatahtml[] = array(
				//'checkbox' => '<input type="checkbox" name="actionids[]" value="'.$data['id'].'">',
				//'checkbox' => '',
				'name' => htmlentities(appgeneral::trim_length($data['name'], 30)),
				'restaurant_name' => htmlentities(appgeneral::trim_length($data['restaurant_name'], 30)),
				'total' => number_format($data['total'], 2),
				'added' => date('Y-m-d H:i:s', strtotime($data['added'] . ' UTC')),
			);

		}

		$tablehtml->paging_totrows = $db->query_foundrows();

	} else {
		$tabledatahtml = array();
	}

	return $tabledatahtml;

}



$filter_fields = array('restaurant_id', 'name', 'status', 'added_from__dd', 'added_from__mm', 'added_from__yyyy', 'added_to__dd', 'added_to__mm', 'added_to__yyyy');

$filter_args = array('i' => null);
foreach ($filter_fields as $name) {
	if (isset($_GET[$name])) {
		$filter_args[$name] = $_GET[$name];
	}
}

$self_linkh = navpd::link_h();

$formdata = array();
foreach ($filter_fields as $name) {
	$formdata[$name] = isset($_GET[$name]) ? $_GET[$name] : '';
}

$btn_added_search = $btn_restaurant_search = $btn_status_search = $btn_name_search = btn::create('Search', btn::TYPE_SUBMIT, '', $cfg['btn_template_path'].'icons/search.png');

//Date
$args = array('i' => null, 'added_from__dd' => null, 'added_from__mm' => null, 'added_from__yyyy' => null, 'added_to__dd' => null, 'added_to__mm' => null, 'added_to__yyyy' => null);
$form_added_self = navpd::self_form($args);
if ( ($formdata['added_from__dd']) || ($formdata['added_from__mm']) || ($formdata['added_from__yyyy']) || ($formdata['added_to__dd']) || ($formdata['added_to__mm']) || ($formdata['added_to__yyyy']) ) {
	$link = navpd::self($args);
	$btn_added_clear = btn::create('Clear', btn::TYPE_LINK, $link, $cfg['btn_template_path'].'icons/clear.png');
} else {
	$btn_added_clear = '';
}

//Restaurant
$args = array('restaurant_id' => null, 'i' => null);
$form_restaurant_self = navpd::self_form($args);
if ( (isset($_GET['restaurant_id'])) && ($_GET['restaurant_id']) ) {
	$link = navpd::self($args);
	$btn_restaurant_clear = btn::create('Clear', btn::TYPE_LINK, $link, $cfg['btn_template_path'].'icons/clear.png');
} else {
	$btn_restaurant_clear = '';
}

//Name
$args = array('name' => null, 'i' => null);
$form_name_self = navpd::self_form($args);
if ( (isset($_GET['name'])) && ($_GET['name']) ) {
	$link = navpd::self($args);
	$btn_name_clear = btn::create('Clear', btn::TYPE_LINK, $link, $cfg['btn_template_path'].'icons/clear.png');
} else {
	$btn_name_clear = '';
}

//Status
$args = array('status' => null, 'i' => null);
$form_status_self = navpd::self_form($args);
if ( (isset($_GET['status'])) && ($_GET['status']) ) {
	$link = navpd::self($args);
	$btn_status_clear = btn::create('Clear', btn::TYPE_LINK, $link, $cfg['btn_template_path'].'icons/clear.png');
} else {
	$btn_status_clear = '';
}

//Retrieve restaurant options
$restaurant_options = array();
$select_sql = $db->col(array('id', 'name')) . ', ';
$select_sql .= "(SELECT {$tbl['town']}.name FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id) AS town, ";
$select_sql .= "(SELECT {$tbl['city']}.name FROM {$tbl['city']} WHERE {$tbl['city']}.id = (SELECT {$tbl['town']}.city_id FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id)) AS city";
$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $select_sql, $db->cond(array("status = 1"), 'AND'), $db->order(array(array('name', 'ASC'))));//array('city', 'ASC'), array('town', 'ASC'), array('name', 'ASC')
while ($restaurant_record = $db->record_fetch($restaurant_result)) {
	$id = $restaurant_record['id'];
	$restaurant_options[$id] = "{$restaurant_record['name']} ({$restaurant_record['city']}, {$restaurant_record['town']})";
}

$restaurant_options_html = lib::create_options($restaurant_options, $formdata['restaurant_id'], lib::CREATEOPT_PLEASESELECT);

//Calendar
$onclick_js = "calendar1.showhide(this); return false;";
$btn_added_from_cal = btn::create('', btn::TYPE_LINK, '#', $cfg['btn_template_path'].'icons/calendar.png', $onclick_js, 'Show Calendar', 'Show Calendar');

$onclick_js = "calendar2.showhide(this); return false;";
$btn_added_to_cal = btn::create('', btn::TYPE_LINK, '#', $cfg['btn_template_path'].'icons/calendar.png', $onclick_js, 'Show Calendar', 'Show Calendar');

//Export
$args = array_merge(array('p' => 'order_export_csv'), $filter_args);
$link = navpd::link($args);
$btn_exportcsv = btn::create('Export CSV', btn::TYPE_LINK, $link, $cfg['btn_template_path'].'icons/csv.png');

//Status
$status_options_html = lib::create_options($cfg['order_status'], $formdata['status'], lib::CREATEOPT_PLEASESELECT);


$formdatah = lib::htmlentities_array($formdata);

//$self_linkh = navpd::link_h();

$body_html = <<<EOHTML

{$table_html}

<div class="panel_filter">

	<div class="panelitem">

		<form method="get" action="{$self_linkh}" id="frm_restaurant">

			<div>
{$form_restaurant_self}
			</div>

			<div class="row">
				<div class="title"><label for="restaurant_id">Restaurant</label></div>
				<div class="userinput"><select name="restaurant_id" id="restaurant_id" class="inputselect" onchange="$('frm_restaurant').submit()">{$restaurant_options_html}</select></div>
			</div>

			{$btn_restaurant_search} {$btn_restaurant_clear}

		</form>

	</div>

	<div class="panelitem">

		<form method="get" action="{$self_linkh}" id="frm_name">

			<div>
{$form_name_self}
			</div>

			<div class="row">
				<div class="title"><label for="name">Customer Name</label></div>
				<div class="userinput"><input type="text" name="name" id="name" value="{$formdatah['name']}" class="inputtxt" /></div>
			</div>

			{$btn_name_search} {$btn_name_clear}

		</form>

	</div>

	<div class="panelitem">

		<form method="get" action="{$self_linkh}" id="frm_status">

			<div>
{$form_status_self}
			</div>

			<div class="row">
				<div class="title"><label for="status">Order Status</label></div>
				<div class="userinput"><select name="status" id="status" class="inputselect" onchange="$('frm_status').submit()">{$status_options_html}</select></div>
			</div>

			{$btn_status_search} {$btn_status_clear}

		</form>

	</div>

	<div class="panelitem">

		<form method="get" action="{$self_linkh}">

			<div>
{$form_added_self}
			</div>

			<div class="row">
				<div class="title"><label for="added_from__dd">Order Date Start</label></div>
				<div class="userinput">
				
					<table cellspacing="0" class="dateentry">
						<tr>
							<td><input type="text" name="added_from__dd" id="added_from__dd" value="{$formdatah['added_from__dd']}" class="date-dd" size="2" maxlength="2" /></td>
							<td class="sep"> / </td>
							<td><input type="text" name="added_from__mm" id="added_from__mm" value="{$formdatah['added_from__mm']}" class="date-mm" size="2" maxlength="2" /></td>
							<td class="sep"> / </td>
							<td><input type="text" name="added_from__yyyy" id="added_from__yyyy" value="{$formdatah['added_from__yyyy']}" class="date-yyyy" size="4" maxlength="4" /></td>
							<td><div class="showhide">{$btn_added_from_cal}</div></td>
						</tr>
						<tr>
							<td class="info"><label for="added_from__dd">dd</label></td>
							<td class="info"> / </td>
							<td class="info"><label for="added_from__mm">mm</label></td>
							<td class="info"> / </td>
							<td class="info"><label for="added_from__yyyy">yyyy</label></td>
							<td class="info"></td>
						</tr>
					</table>

					<div id="calendar1_disp" style="display: none"></div>
				
				</div>
			</div>

			<div class="row">
				<div class="title"><label for="added_to__dd">Order Date End</label></div>
				<div class="userinput">

					<table cellspacing="0" class="dateentry">
						<tr>
							<td><input type="text" name="added_to__dd" id="added_to__dd" value="{$formdatah['added_to__dd']}" class="date-dd" size="2" maxlength="2" /></td>
							<td class="sep"> / </td>
							<td><input type="text" name="added_to__mm" id="added_to__mm" value="{$formdatah['added_to__mm']}" class="date-mm" size="2" maxlength="2" /></td>
							<td class="sep"> / </td>
							<td><input type="text" name="added_to__yyyy" id="added_to__yyyy" value="{$formdatah['added_to__yyyy']}" class="date-yyyy" size="4" maxlength="4" /></td>
							<td><div class="showhide">{$btn_added_to_cal}</div></td>
						</tr>
						<tr>
							<td class="info"><label for="added_to__dd">dd</label></td>
							<td class="info"> / </td>
							<td class="info"><label for="added_to__mm">mm</label></td>
							<td class="info"> / </td>
							<td class="info"><label for="added_to__yyyy">yyyy</label></td>
							<td class="info"></td>
						</tr>
					</table>

					<div id="calendar2_disp" style="display: none"></div>

				</div>
			</div>

			{$btn_added_search} {$btn_added_clear}

		</form>

	</div>

	<div class="panelitem panelitem-exportcsv">
		{$btn_exportcsv}
	</div>

</div>

<div class="clear"></div>

EOHTML;

$headeraddin_html = <<<EOHTML
<script src="{$link_base_path}resources/admin_template/javascript/calendar.js" type="text/javascript"></script>
<script src="{$link_base_path}resources/admin_order/admin_order.js" type="text/javascript"></script>
EOHTML;

$template = new admin_template();
$template->setmainnavsection('order');
$template->settitle('Orders');
$template->setheaderaddinhtml($headeraddin_html);
$template->setbodyhtml($body_html);
$template->display();

?>