<?php

include $includes_path . 'config.php';
include $cfg['userdata_path'] . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$admin_auth = new admin_auth();
$admin_auth->handle();
$authinfo = $admin_auth->getauthinfo();
$admin_auth->login_required();
$admin_auth->check_permission();


$restaurant_id = intval($_GET['restaurant_id']);

//Check have permission to import for this restaurant
$admin_auth->check_permission_restaurant($restaurant_id);


$errormsg_html = array();
$statusmsg_html = array();

if (isset($_POST['formposted'])) {

	if ($_FILES['csvfile']['error'] == UPLOAD_ERR_NO_FILE) {
		$errormsg_html[] = "'CSV File' not specified.";
	} else if ($_FILES['csvfile']['error'] != UPLOAD_ERR_OK) {
		$errormsg_html[] = "Error uploading 'CSV File' <a href=\"http://www.php.net/manual/en/features.file-upload.errors.php\">Code {$_FILES['csvfile']['error']}</a>.";
	} else {

		//If has .csv extension
		if (preg_match("/\.csv$/", $_FILES['csvfile']['name'])) {

			//Retrieve CS Vdata from file
			$csvdata = csv::retrieve_file($_FILES['csvfile']['tmp_name'], '', csv::RETRIEVE_NICEID_FROM_TITLE);

			//If file contains data
			if (count($csvdata)) {

				//Check have expected fields
				$required_fields = array('category', 'no', 'name', 'description', 'sub_item_name', 'cost');
				foreach ($required_fields as $field) {
					if (!isset($csvdata[0][$field])) {
						$errormsg_html[] = "Required column \"{$field}\" not found.";
					}
				}

				//If no errors
				if (!count($errormsg_html)) {
					//Ok to continue parsing file

					//Check data valid
					$line = 1;
					foreach ($csvdata as $indexid => $item) {
						$line++;

						$csvdata[$indexid] = appgeneral::filternonascii_array($csvdata[$indexid]);

						if ( ($item['category']) || ($item['no']) || ($item['name']) || ($item['description']) || ($item['sub_item_name']) || ($item['cost']) ) {

							if (!$item['category']) {
								$errormsg_html[] = "Line {$line}, 'Category' not specified";
							}

							//if ( ($item['no']) && (preg_match("%[^0-9]%", $item['no'])) ) {
							//	$errormsg_html[] = "Line {$line}, 'No' must be numeric only";
							//}

							if (strlen($item['no']) > 4) {
								$errormsg_html[] = "Line {$line}, 'No' must be 0-4 characters in length";
							} else {
								if (preg_match("%[^0-9a-zA-Z]%", $item['no'])) {
									$errormsg_html[] = "Line {$line}, 'No' must consist of a-z, 0-9 only";
								}
							}

							//if ( ($item['no']) && ($item['sub_item_name']) ) {
							//	$errormsg_html[] = "Line {$line}, 'No' can not be specified for a sub item";
							//}

							if (!$item['name']) {
								$errormsg_html[] = "Line {$line}, 'Name' not specified";
							}

							//if ( ($item['description']) && ($item['sub_item_name']) ) {
							//	$errormsg_html[] = "Line {$line}, 'Description' can not be specified for a sub item";
							//}

							if (strlen($item['cost']) > 0) {
								if (preg_match("%[^0-9\.]%", $item['cost'])) {
									$errormsg_html[] = "Line {$line}, 'Cost' must be numeric only";
								}
							} else {
								$errormsg_html[] = "Line {$line}, 'Cost' not specified";
							}

							$csvdata[$indexid]['no'] = strtolower($csvdata[$indexid]['no']);

							$csvdata[$indexid]['type'] = ($item['sub_item_name']) ? 'sub' : 'main';

						} else {
							//Remove item
							unset($csvdata[$indexid]);
						}

					}

					//lib::prh($csvdata);

				}

			} else {
				$errormsg_html[] = "No CSV data available";
			}


		} else {
			$errormsg_html[] = "'CSV File' does not appear to be of type .csv";
		}

	}

	//If no errors
	if (count($errormsg_html) == 0) {

		try {

			$db->transaction(dbmysql::TRANSACTION_START);

			//Retrieve existing categories
			$categories = array();
			$unused_categories = array();
			$menu_cat_result = $db->table_query($db->tbl($tbl['menu_cat']), $db->col(array('id', 'name')), $db->cond(array("restaurant_id = {$restaurant_id}"), 'AND'), $db->order(array(array('name', 'ASC'))));
			while ($menu_cat_record = $db->record_fetch($menu_cat_result)) {
				$id = $menu_cat_record['id'];
				$categories[$id] = $menu_cat_record['name'];
				$unused_categories[] = $id;
			}

			//Import categories
			foreach ($csvdata as $csvdata_indexid => $item) {

				//Search for the category in the existing list
				$category_id = array_search($item['category'], $categories);

				//If match found
				if ($category_id) {

					//Remove category from the unused category list
					$indexid = array_search($category_id, $unused_categories);

					if ($indexid !== false) {
						unset($unused_categories[$indexid]);
					}

				} else {

					//Find last highest order number
					$order = appgeneral::category_order_next($restaurant_id);

					//Otherwise add in the category
					$record = array(
						'restaurant_id' => $restaurant_id,
						'name' => $item['category'],
						'catorder' => $order,
					);
					$db->record_insert($tbl['menu_cat'], $db->rec($record));
					$category_id = $db->record_insert_id();

					//Add it into the array may be used other items
					$categories[$category_id] = $item['category'];

					$statusmsg_html[] = "Adding category \"".htmlentities($item['category'])."\"";

				}
				
				//Add on category id to item
				$csvdata[$csvdata_indexid]['category_id'] = $category_id;

			}

			//Remove unused categories
			foreach ($unused_categories as $category_id) {

				$statusmsg_html[] = "Removing unused category \"".htmlentities($categories[$category_id])."\"";

				unset($categories[$category_id]);

				//Reorder categories
				appgeneral::category_reorder($category_id, 'delete');

				$db->record_delete($tbl['menu_cat'], $db->cond(array("id = {$category_id}"), 'AND'));

			}


			//Retrieve existing main level menu items
			$cond = array();
			$cond[] = "sub_menu_item_id IS NULL";
			$cond[] = "menu_cat_id IN (SELECT id FROM {$tbl['menu_cat']} WHERE restaurant_id = {$restaurant_id})";

			$menu_items = array();
			$unused_menu_items = array();
			$menu_item_result = $db->table_query($db->tbl($tbl['menu_item']), $db->col(array('id', 'menu_cat_id', 'name', 'cost')), $db->cond($cond, 'AND'), $db->order(array(array('no', 'ASC'), array('name', 'ASC'))));
			while ($menu_item_record = $db->record_fetch($menu_item_result)) {
				$id = $menu_item_record['id'];
				$menu_items[$id] = $menu_item_record;
				$unused_menu_items[] = $id;
				$menu_items[$id]['type'] = (strlen($menu_item_record['cost'])) ? 'main' : 'sub';
				unset($menu_items[$id]['cost']);//not needed
			}

			//Import main level menu items
			foreach ($csvdata as $csvdata_indexid => $item) {

				//See if the item is already on the menu
				$match_found = false;

				foreach ($menu_items as $existing_item) {

					//echo "({$existing_item['menu_cat_id']} == {$item['category_id']}) && ({$existing_item['name']} == {$item['name']}) && ({$existing_item['type']} == {$item['type']})<br />";

					//If category id matches, and name matches
					if ( ($existing_item['menu_cat_id'] == $item['category_id']) && ($existing_item['name'] == $item['name']) && ($existing_item['type'] == $item['type']) ) {
						$match_found = true;
						break;
					}

				}

				//echo "match: {$match_found}<br />";

				$no = ($item['no']) ? $item['no'] : null;
				$cost = ($item['sub_item_name']) ? null : $item['cost'];

				$record = array(
					'no' => $no,
					'name' => $item['name'],
					'description' => $item['description'],
					'cost' => $cost,
				);

				//Alread have the item
				if ($match_found) {

					$item_id = $existing_item['id'];

					//Remove menu item from the unused menu item list
					$indexid = array_search($item_id, $unused_menu_items);

					if ($indexid !== false) {
						unset($unused_menu_items[$indexid]);
					}

					//Update with new details
					$db->record_update($tbl['menu_item'], $db->rec($record), $db->cond(array("id = {$existing_item['id']}"), 'AND'));

				} else {

					$statusmsg_html[] = "Adding menu item \"".htmlentities($item['name'])."\"";

					//Otherwise add in the menu item
					$record = array_merge($record, array('menu_cat_id' => $item['category_id']));
					$db->record_insert($tbl['menu_item'], $db->rec($record));
					$item_id = $db->record_insert_id();

					//Add it into the array may be used other items
					$menu_items[$item_id] = array(
						'id' => $item_id,
						'menu_cat_id' => $item['category_id'],
						'name' => $item['name'],
						//'cost' => (strlen($item['cost'])) ? $item['cost'] : null,
						'type' => $item['type'],
					);

				}

				//Add on item id to item
				$csvdata[$csvdata_indexid]['item_id'] = $item_id;

			}

			//Remove unused menu items
			foreach ($unused_menu_items as $item_id) {

				$statusmsg_html[] = "Removing unused menu item \"".htmlentities($menu_items[$item_id]['name'])."\"";

				unset($menu_items[$item_id]);

				$db->record_delete($tbl['menu_item'], $db->cond(array("id = {$item_id}"), 'AND'));

			}


			//Retrieve existing sub level menu items
			$sql = <<<EOSQL
SELECT
	id,
	name,
	cost,
	sub_menu_item_id,
	(SELECT menu_cat_id FROM {$tbl['menu_item']} AS menu_item_inner WHERE menu_item_outer.sub_menu_item_id = menu_item_inner.id) AS menu_cat_id,
	(SELECT name FROM {$tbl['menu_item']} AS menu_item_inner WHERE menu_item_outer.sub_menu_item_id = menu_item_inner.id) AS name_main

FROM
	{$tbl['menu_item']} AS menu_item_outer

WHERE
	sub_menu_item_id IN (SELECT id FROM {$tbl['menu_item']} WHERE menu_cat_id IN (SELECT id FROM {$tbl['menu_cat']} WHERE restaurant_id = {$restaurant_id}))
EOSQL;

			$submenu_items = array();
			$unused_submenu_items = array();
			$menu_item_result = $db->query($sql);
			while ($menu_item_record = $db->record_fetch($menu_item_result)) {
				$id = $menu_item_record['id'];
				$submenu_items[$id] = $menu_item_record;
				$unused_submenu_items[] = $id;
			}

			//Import sub level menu items
			foreach ($csvdata as $csvdata_indexid => $item) {

				if ($item['type'] == 'sub') {

					//See if the item is already on the menu
					$match_found = false;

					foreach ($submenu_items as $existing_item) {

						//If category id matches, and name matches
						if ( ($existing_item['sub_menu_item_id'] == $item['item_id']) && ($existing_item['name'] == $item['sub_item_name']) ) {
							$match_found = true;
							break;
						}

					}

					//echo "match: {$match_found}<br />";

					$record = array(
						'name' => $item['sub_item_name'],
						'cost' => $item['cost'],
						'sub_menu_item_id' => $item['item_id'],
					);

					//Already have the sub item
					if ($match_found) {

						$item_id = $existing_item['id'];

						//Remove menu item from the unused menu item list
						$indexid = array_search($item_id, $unused_submenu_items);

						if ($indexid !== false) {
							unset($unused_submenu_items[$indexid]);
						}

						//Update with new details
						$db->record_update($tbl['menu_item'], $db->rec($record), $db->cond(array("id = {$existing_item['id']}"), 'AND'));

					} else {

						$statusmsg_html[] = "Adding sub menu item ".htmlentities("\"".$item['name']."\" > \"".$item['sub_item_name'] . "\"");

						//Otherwise add in the menu item
						$db->record_insert($tbl['menu_item'], $db->rec($record));
						$item_id = $db->record_insert_id();

						//Add it into the array may be used other sub menu items
						$submenu_items[$item_id] = array(
							'id' => $item_id,
							'sub_menu_item_id' => $item['item_id'],
							'name' => $item['sub_item_name'],
							'name_main' => $item['name'],
							'cost' => $item['cost'],
						);

					}

					//Add on sub item id to item
					$csvdata[$csvdata_indexid]['subitem_id'] = $item_id;

				}

			}

			//Remove unused sub  menu items
			foreach ($unused_submenu_items as $item_id) {

				$statusmsg_html[] = "Removing unused sub menu item ".htmlentities("\"" . $submenu_items[$item_id]['name_main']."\" > \"".$submenu_items[$item_id]['name'] . "\"");

				unset($unused_submenu_items[$item_id]);

				$db->record_delete($tbl['menu_item'], $db->cond(array("id = {$item_id}"), 'AND'));

			}

			$db->record_update($tbl['restaurant'], $db->rec(array('lastupdated' => $db->datetimenow())), $db->cond(array("id = {$restaurant_id}"), 'AND'));

			$statusmsg_html[] = "Import Completed";

			$db->transaction(dbmysql::TRANSACTION_COMMIT);

		} catch (Exception $exception) {

			$db->transaction(dbmysql::TRANSACTION_ROLLBACK);

			throw $exception;

		}


	}

}

//If successmsgs, show them
$successmsg_all_html = '';
if (count($statusmsg_html)) {
	foreach ($statusmsg_html as $successmsg_item_html) {
		$successmsg_all_html .= <<<EOHTML
<div class="successmsg">Success: {$successmsg_item_html}</div>
EOHTML;
	}

	$successmsg_all_html = <<<EOHTML
<div class="successcontainer">
{$successmsg_all_html}
</div>
EOHTML;

} else {
	$successmsg_all_html = '';
}



$self_args = array();
$link_h = navpd::self($self_args);

//Convert errors to html
$errormsgs_html = appgeneral::errormsgs_html($errormsg_html);

$btn_back = btn::create('<< Back', btn::TYPE_LINK, navpd::back(), '', '', 'Back');
$btn_update = btn::create('Import', btn::TYPE_SUBMIT);

$nav_html = <<<EOHTML
<div class="navigation">
	<div class="left">{$btn_back}</div>
	<div class="right">{$btn_update}</div>
</div>
EOHTML;

//Retrieve restaurant information for specified category
$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $db->col(array('name')), $db->cond(array("id = {$restaurant_id}"), 'AND'), '', 0, 1);
if (!($restaurant_record = $db->record_fetch($restaurant_result))) {
	throw new Exception("Restaurant id \"{$restaurant_id}\" not found");
}

$restaurantname = $restaurant_record['name'];
$restaurantname_h = htmlentities($restaurantname);

$body_html = <<<EOHTML

<div class="addeditpage">

	<h2>{$restaurantname_h} &gt; Menu Import</h2>

	<form method="post" action="{$link_h}" id="mainform" class="mainform" enctype="multipart/form-data">
		<div><input type="hidden" name="formposted" value="1" /></div>

{$nav_html}

{$errormsgs_html}

{$successmsg_all_html}

		<div class="tablecontainer">
			<table cellspacing="0" class="addedit">
				<tr>
					<th class="addedit"><label for="csvfile">CSV File: <span class="required">*</span></label></th>
					<td class="addedit">
						<input type="file" name="csvfile" id="csvfile" size="20" /><br/ >
						[Template: <a href="{$cfg['site_url']}resources/admin_menu_import/template.csv">CSV</a>]<br />
						<br />
						<strong>Warning:</strong> During the import process existing categories / menu items will be deleted.
					</td>
				</tr>
			</table>
		</div>

{$nav_html}

	</form>

</div>

EOHTML;


$template = new admin_template();
$template->setmainnavsection('restaurant');
$template->settitle("{$restaurantname} > Menu Import");
//$template->setheaderaddinhtml($headeraddin_html);
$template->setbodyhtml($body_html);
$template->display();

?>