<?php

include $includes_path . 'config.php';
include $cfg['userdata_path'] . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$admin_auth = new admin_auth();
$admin_auth->handle();
$authinfo = $admin_auth->getauthinfo();
$admin_auth->login_required();
$admin_auth->check_permission();


//Delete restaurant
if ( (isset($_POST['actiontype'])) && ($_POST['actiontype'] == 'delete') ) {

	$actionid = intval($_POST['actionid']);

	if ($authinfo['usergroup'] == admin_auth::LOGIN_GROUP_ADMIN_SYSTEM) {
		$db->record_delete($tbl['user'], $db->cond(array("id = ".$actionid), 'AND'));
	} else if ($authinfo['usergroup'] == admin_auth::LOGIN_GROUP_ADMIN) {

		//Retrieve group of user to delete
		$user_result = $db->table_query($db->tbl($tbl['user']), $db->col(array('usergroup')), $db->cond(array("id = {$actionid}"), 'AND'), '', 0, 1);
		if (!($user_record = $db->record_fetch($user_result))) {
			throw new Exception("User to delete id \"{$actionid}\" not found");
		}

		if ( ($user_record['usergroup'] == admin_auth::LOGIN_GROUP_ADMIN) || ($user_record['usergroup'] == admin_auth::LOGIN_GROUP_USER) ) {
			$db->record_delete($tbl['user'], $db->cond(array("id = ".$actionid), 'AND'));
		} else {
			throw new Exception("User to delete id \"{$actionid}\" belongs to group \"{$user_record['usergroup']}\", usergroup \"{$authinfo['usergroup']}\" can not delete them");
		}

	} else {
		throw new Exception("Usergroup \"{$authinfo['usergroup']}\" unknown");
	}

}

$link_addnew = navpd::forward(array('p' => 'user_addedit'));
$btn_addnew = btn::create('Add User', btn::TYPE_LINK, $link_addnew, $cfg['btn_template_path'].'icons/add.png', '', 'Add User');
$right_html = $btn_addnew;


//Table
$tablehtml = new tablehtml();
$tablehtml->shotbuttoncol = true;
$tablehtml->sortby = 'email';
$tablehtml->sortdir = 'asc';
//$tablehtml->table_class = 'examplelist';
$tablehtml->sortable_columns = array('email', 'name', 'telephone', 'usergroup_name');
$tablehtml->parsegetvars($_GET);

//$tablehtml->addcolumn('checkbox', '');
$tablehtml->addcolumn('email', 'Email');
$tablehtml->addcolumn('name', 'Name');
$tablehtml->addcolumn('telephone', 'Telephone');
$tablehtml->addcolumn('usergroup_name', 'User Group');
$tablehtml->addcolumn('button', '');

$table_html = $tablehtml->html(
	$tablehtml->html_action(),
	$tablehtml->html_table(
		$tablehtml->html_table_titles(),
		$tablehtml->html_table_rows(
			$tablehtml->tabledatahtml_fromcallback('callback_tabledatahtml')
		),
		$tablehtml->html_table_nav($right_html),
		$tablehtml->html_table_errors()
	)
);

function callback_tabledatahtml($tablehtml, $limit_offset, $limit_count, $query_order) {
	global $cfg, $tbl, $db, $admin_auth, $authinfo;

	$cond = array();

	if ($authinfo['usergroup'] == admin_auth::LOGIN_GROUP_ADMIN_SYSTEM) {
		//See everyone
	} else if ($authinfo['usergroup'] == admin_auth::LOGIN_GROUP_ADMIN) {
		$cond[] = "(usergroup = " . admin_auth::LOGIN_GROUP_ADMIN . " OR usergroup = " . admin_auth::LOGIN_GROUP_USER . ")";
	} else {
		throw new Exception("Usergroup \"{$authinfo['usergroup']}\" unknown");
	}

	//Retrieve usergroup options
	$usergroup_options = $admin_auth->usergroup_option();
	$usergroup_select_sql = appgeneral::options_array_sql($usergroup_options, 'usergroup', 'usergroup_name');

	$select_sql = $db->col(array('id', 'email', 'name', 'telephone')) . ', ';
	$select_sql .= $usergroup_select_sql;
	$result = $db->table_query($db->tbl($tbl['user']), $select_sql, $db->cond($cond, 'AND'), $db->order($query_order), $limit_offset, $limit_count, dbmysql::TBLQUERY_FOUNDROWS);

	$tabledatahtml = array();
	while ($data = $db->record_fetch($result)) {

		$datah = lib::htmlentities_array($data);

		//Delete button
		$name_js = addslashes($datah['name']);
		$link_self = addslashes(navpd::self());
		$onclick_js = "performaction('{$link_self}', 'Really delete user \'{$name_js}\'?', 'delete', {$data['id']}); return false;";
		$btn_delete = btn::create('Delete', btn::TYPE_LINK, '#', $cfg['btn_template_path'].'icons/delete.png', $onclick_js, "Delete user \"{$name_js}\"");

		//Edit button
		$link_edit = navpd::forward(array('p' => 'user_addedit', 'editid' => $data['id']));
		$btn_edit = btn::create('Edit', btn::TYPE_LINK, $link_edit, $cfg['btn_template_path'].'icons/edit.png', '', 'Edit');

		$buttons = $tablehtml->html_table_buttons(array($btn_edit, $btn_delete));

		$tabledatahtml[] = array(
			//'checkbox' => '<input type="checkbox" name="actionids[]" value="'.$data['id'].'">',
			//'checkbox' => '',
			'email' => htmlentities(appgeneral::trim_length($data['email'], 40)),
			'name' => htmlentities(appgeneral::trim_length($data['name'], 40)),
			'telephone' => htmlentities(appgeneral::trim_length($data['telephone'], 30)),
			'usergroup_name' => htmlentities(appgeneral::trim_length($data['usergroup_name'], 30)),
			'button' => $buttons,
		);

	}

	$tablehtml->paging_totrows = $db->query_foundrows();

	return $tabledatahtml;

}

$body_html = <<<EOHTML

{$table_html}

EOHTML;


$template = new admin_template();
$template->setmainnavsection('user');
$template->settitle('Users');
//$template->setheaderaddinhtml($headeraddin_html);
$template->setbodyhtml($body_html);
$template->display();

?>