<?php

include $includes_path . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$auth = new auth();
$auth->handle();
$authinfo = $auth->getauthinfo();
//$auth->login_required();

$link_base_path = htmlentities(navfr::base_path());

$current_path = navfr::current_path();

if (!( (isset($current_path[1])) && (isset($current_path[2])) && (isset($current_path[3])) )) {
	throw new Exception('Somehow reached this page without city, town, restaurant specified');
}

//Resolve city data
$city_data = appgeneral::city_from_navname($current_path[1]);

//If city not found
if ($city_data == false) {
	template_lib::show_404();
	exit;
}

//Resolve town data
$town_data = appgeneral::town_from_navname($city_data['id'], $current_path[2]);

//If town not found
if ($town_data == false) {
	template_lib::show_404();
	exit;
}


//Retrieve restaurant data

$select_sql = '';
$select_sql .= "(SELECT {$tbl['town']}.name FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id) AS town, ";
$select_sql .= "(SELECT {$tbl['city']}.name FROM {$tbl['city']} WHERE {$tbl['city']}.id = (SELECT {$tbl['town']}.city_id FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id)) AS city";

$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $db->col(array('id', 'name', 'description', 'address_1', 'address_2', 'postcode', 'del_radius_mi', 'min_order_value', 'delivery_charge', 'handling_delivery', 'handling_collection', 'msg_dispatch', 'navname')) . ', ' . $select_sql, $db->cond(array("navname='".$db->es($current_path[3])."'", "town_id = {$town_data['id']}", "status = 1"), 'AND'), '', 0, 1);
if (!($restaurant_record = $db->record_fetch($restaurant_result))) {

	//If restaurant not found
	template_lib::show_404();
	exit;

}

//Retrieve opening times
$opentimes = template_lib::restaurant_open_time($restaurant_record['id']);

$restaurant_open_times_html = '';
$row = 'b';
foreach ($cfg['restaurant_open_days'] as $dayno => $dayname) {

	if (isset($opentimes[$dayno])) {

		$open = date('g:i a', $opentimes[$dayno]['open']);
		$close = date('g:i a', $opentimes[$dayno]['close']);

		$time = <<<EOHTML
{$open} - {$close}
EOHTML;
	} else {
		$time = 'Closed';
	}

	$row = ($row == 'b') ? 'a' : 'b';
	$restaurant_open_times_html .= <<<EOHTML
		<div class="day day-{$row}">
			<div class="dayname">{$dayname}</div>
			<div class="time">{$time}</div>
			<div class="clear"></div>
		</div>

EOHTML;

}

if ($restaurant_open_times_html) {

	if ($restaurant_record['del_radius_mi'] >= 10) {
		$delivery_area = round($restaurant_record['del_radius_mi']);
	} else {
		$delivery_area = $restaurant_record['del_radius_mi'];
	}

	$restaurant_open_times_html = <<<EOHTML

<div class="restaurant_open_times">

	<div class="content">

		<h2>Hours of operation</h2>

		<div class="delivery_area">Delivery area: <span class="area">{$delivery_area} mi</span></div>

		<div class="clear"></div>

		{$restaurant_open_times_html}

	</div>

</div>

EOHTML;

}

//Retrieve restaurant menu
$restaurant_menu = appgeneral::retrieve_menu($restaurant_record['id']);

//Process menu into html
$menu_html = '';
$jump_to_section = '';
$row = 'b';
foreach ($restaurant_menu as $cat_id => $category) {

	//Go through each item in category
	$menu_item_html = '';
	foreach ($category['menu_item'] as $item_id => $item) {

		//If item has a cost, or has sub items, ok to list it
		if ( ($item['cost']) || (count($item['sub_menu_item'])) ) {

			$item_no = htmlentities($item['no']);
			$item_nameh = htmlentities($item['name']);

			$item_desc = htmlentities($item['description']);
			$item_desc = nl2br($item_desc);

			//If there are sub items
			if (count($item['sub_menu_item'])) {

				//Go through all sub items
				$sub_item_html = '';
				foreach ($item['sub_menu_item'] as $subitem_id => $subitem) {

					$subitem_nameh = htmlentities($subitem['name']);

					$add_item_name = "{$item['name']} {$subitem['name']}";
					$add_item_name_s = addslashes($add_item_name);
					$add_item_name_s = htmlentities($add_item_name_s);

					if ($restaurant_record['msg_dispatch'] > 1) {
						$additem_html = <<<EOHTML
<a href="#" onclick="menu_item_add(restaurant_id, {$subitem_id}, '{$add_item_name_s}', {$subitem['cost']}); return false;"><img src="{$link_base_path}resources/takeaway_menu/menu/add_item_btn.gif" width="13" height="13" alt="Add Item To Order" /></a>
EOHTML;
					} else {
						$additem_html = '';
					}

					$sub_item_html .= <<<EOHTML
				<div class="subitem">
					<div class="subitem_name">{$subitem_nameh}</div>
					<div class="subitem_price">{$subitem['cost']}</div>
					<div class="additem">{$additem_html}</div>
					<div class="clear"></div>
				</div>

EOHTML;

				}

			} else {

				$add_item_name = $item['name'];
				$add_item_name_s = addslashes($add_item_name);
				$add_item_name_s = htmlentities($add_item_name_s);

				if ($restaurant_record['msg_dispatch'] > 1) {
					$additem_html = <<<EOHTML
<a href="#" onclick="menu_item_add(restaurant_id, {$item_id}, '{$add_item_name_s}', {$item['cost']}); return false;"><img src="{$link_base_path}resources/takeaway_menu/menu/add_item_btn.gif" width="13" height="13" alt="Add Item To Order" /></a>
EOHTML;
				} else {
					$additem_html = '';
				}

				$sub_item_html = <<<EOHTML
				<div class="subitem">
					<div class="subitem_name"></div>
					<div class="subitem_price">{$item['cost']}</div>
					<div class="additem">{$additem_html}</div>
					<div class="clear"></div>
				</div>

EOHTML;

			}

			$row = ($row == 'b') ? 'a' : 'b';

			$menu_item_html .= <<<EOHTML
		<li class="item item-{$row}">
			<div class="no">{$item_no}</div>
			<div class="namedesc">
				<h4>{$item_nameh}</h4>
				<div class="desc">{$item_desc}</div>
			</div>
			<div class="iteminfo">
{$sub_item_html}
			</div>
			<div class="clear"></div>
		</li>

EOHTML;

		}

	}

	$menu_item_html = rtrim($menu_item_html);

	if ($menu_item_html) {

		$section_navname = $category['name'];
		$section_navname = strtolower($section_navname);
		$section_navname = str_replace(' ', '-', $section_navname);
		$section_navname = preg_replace("%[^a-z0-9\-]%", '', $section_navname);

		$category_name = htmlentities($category['name']);

		$jump_to_section .= <<<EOHTML
<li><a href="#section-{$section_navname}" onclick="jump_section('{$section_navname}'); return false;">{$category_name}</a>, </li>
EOHTML;

		$menu_html .= <<<EOHTML

<div class="category">

	<h3 id="section-{$section_navname}">{$category_name}</h3>

	<ul class="menu_item_list">
{$menu_item_html}
	</ul>

</div>

EOHTML;

	}

}



//If have categories specified for jump to section
if ($jump_to_section) {

	$jump_to_section = substr($jump_to_section, 0, -7);
	$jump_to_section .= '</li>';

}

$jump_to_section = <<<EOHTML

<div id="jump_to_section_container" class="jump_to_section_container">

	<div id="jump_to_section_top"></div>

	<div class="jump_to_section" id="jump_to_section">
		<div class="jump_to_section-bottom">
			<div class="content">
				<div class="title">Jump to section:</div>

				<ul class="sections_list">
{$jump_to_section}
				</ul>

				<div class="clear"></div>

			</div>
		</div>
	</div>

</div>

EOHTML;





//Retrieve menu items saved in cookie
$menu_cookie = appgeneral::menu_cookie();

//Produce JavaScript array with basket menu items
$basket_items_js = '';
foreach ($menu_cookie['menu_items'] as $item_id => $item) {

	//Retrieve menu item data
	$menu_item_data = appgeneral::menu_item_data($item_id);
	if ($menu_item_data) {

		$item_name_s = $menu_item_data['name'];
		$item_name_s = addslashes($item_name_s);

		$basket_items_js .= <<<EOJS
	{$item_id}: {
		"restaurant_id": {$menu_item_data['restaurant_id']},
		"name": '{$item_name_s}',
		"cost": {$menu_item_data['cost']},
		"qty": {$item['qty']}
	},

EOJS;

	}

}

$basket_items_js = rtrim($basket_items_js, "\r\n,");




//Breadcrumbs
$breadcrumbs = array();
$breadcrumbs[] = array('link' => navfr::link(array('takeaway')),'name' => 'Takeaways');
$breadcrumbs[] = array('link' => navfr::link(array('takeaway', $city_data['navname'])),'name' => $city_data['name']);
$breadcrumbs[] = array('link' => navfr::link(array('takeaway', $city_data['navname'], $town_data['navname'])),'name' => $town_data['name']);
$breadcrumbs[] = array('link' => navfr::link(array('takeaway', $city_data['navname'], $town_data['navname'], $restaurant_record['navname'])),'name' => $restaurant_record['name']);

$breadcrumbs_html = template_lib::breadcrumbs($breadcrumbs);

$page_title = "{$restaurant_record['name']} Menu Online - {$restaurant_record['town']}, {$restaurant_record['city']}";
$metadesc = $restaurant_record['description'];


$restaurant_recordh = lib::htmlentities_array($restaurant_record);

$address = $restaurant_recordh['address_1'];
if ($restaurant_recordh['address_2']) {
	$address .= ", " . $restaurant_recordh['address_2'];
}

$h1_title = "{$restaurant_recordh['name']} - {$restaurant_record['town']}, {$restaurant_record['city']}";

$h1_titleh = htmlentities($h1_title);


$description_h = appgeneral::text_to_paragraphs_html($restaurant_record['description']);

$form_post_link = navfr::link_h(array('takeaway', $city_data['navname'], $town_data['navname'], $restaurant_record['navname'], 'order'));

if ($restaurant_record['delivery_charge']) {
	$delivery_charge = number_format($restaurant_record['delivery_charge'], 2);
} else {
	$delivery_charge = '0.00';
}

//Retrieve current restaurant open/closed status
$restaurant_open_status = appgeneral::restaurant_open($restaurant_record['id']);

if ($restaurant_open_status) {
	$status_html = <<<EOHTML
<span class="open">Open</span>
EOHTML;
} else {
	$status_html = <<<EOHTML
<span class="close">Closed</span>
EOHTML;
}


$link_base_path_js = addslashes($link_base_path);

if (file_exists($publichtml_path . "resources/takeaway/{$restaurant_recordh['id']}/logo.png")) {
	$logo_type = $restaurant_recordh['id'];
} else {
	$logo_type = "default";
}

//Handling accepted
$handling_accept = array();

if ($restaurant_record['handling_delivery']) {
	$handling_accept[] = 1; //delivery
}

if ($restaurant_record['handling_collection']) {
	$handling_accept[] = 2; //collection
}

//Handling accepted to javascript
$handling_accept_js = '';
foreach ($handling_accept as $accept) {
	$handling_accept_js .= $accept . ',';
}

$handling_accept_js = rtrim($handling_accept_js, ',');

//Retrieve handling specified, or use default handling
$handling = appgeneral::retrieve_order_handling($restaurant_record['id']);

$body_html = <<<EOHTML

<div class="breadcrumbstitle">

	{$breadcrumbs_html}

	<h1>{$h1_titleh}</h1>

</div>

<div class="restaurant_info">

	<div class="content">

		<img src="{$link_base_path}resources/takeaway/{$logo_type}/logo.png" width="87" height="87" alt="{$restaurant_recordh['name']} Logo" class="logo" />

		<div class="info">

			<div class="address">{$address}, {$restaurant_recordh['town']}, {$restaurant_recordh['city']}, {$restaurant_recordh['postcode']}</div>
			<div class="status"><strong>Status:</strong> {$status_html}</div>
			<div class="clear"></div>

			<div class="description">
				{$description_h}
			</div>

		</div>

		<div class="clear"></div>

	</div>

</div>

<div class="menu" id="restaurant_menu">
	<div class="menu-top">
		<div class="menu-bottom">
			<div class="content">

				<h2>Menu</h2>

{$jump_to_section}

{$menu_html}

			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
  <!--

	var restaurant_id = {$restaurant_record['id']};

	var delivery_charge = {$delivery_charge};

	var min_order_value = {$restaurant_record['min_order_value']};

	var basket_items = {
{$basket_items_js}
	};

	var handling = {$handling};

	var handling_accept = [{$handling_accept_js}];

	var base_path = "{$link_base_path_js}";

  //-->
</script>

EOHTML;

if ($restaurant_record['msg_dispatch'] > 1) {
	//Menu basket
	$menu_basket = new menu_basket();
	$menu_basket->restaurant_id = $restaurant_record['id'];
	$menu_basket->init();
	$menubasketaddin_html = $menu_basket->html();
} else {
	$menubasketaddin_html = '';
}

$hoursaddin_html = $restaurant_open_times_html;

$headeraddin_html = <<<EOHTML
<script src="{$link_base_path}resources/takeaway_menu/takeaway_menu.js" type="text/javascript"></script>
EOHTML;

$template = new template();
$template->settitle($page_title);
$template->setmetadesc($metadesc);
$template->setmainnavsection('takeaway');
$template->setheaderaddinhtml($headeraddin_html);
$template->sethoursaddinhtml($hoursaddin_html);
$template->setbodyhtml($body_html);
$template->setmenubasketaddinhtml($menubasketaddin_html);
$template->display();

?>