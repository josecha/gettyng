<?php

include $includes_path . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$auth = new auth();
$auth->handle();
$authinfo = $auth->getauthinfo();
//$auth->login_required();

$current_path = navfr::current_path();

//If search post, redirect to search results
if ( (isset($current_path[1])) && ($current_path[1] == 'search') && (isset($_POST['postcode_search'])) ) {

	$postcode = $_POST['postcode_search'];
	$postcode = strtolower($postcode);

	$postcode = preg_replace("%[^a-z0-9\ ]%", '', $postcode);
	$postcode = str_replace(' ', '-', $postcode);

	if ($postcode) {
		$link =  navfr::fqlink_h(navfr::link(array('takeaway', 'search', $postcode)));
	} else {
		$link =  navfr::fqlink_h(navfr::link(array('takeaway')));
	}

	header("Location: {$link}");

	echo <<<EOHTML
<a href="{$link}">Click here</a> to view search results.</a>
EOHTML;

	exit;

}




$link_base_path = htmlentities(navfr::base_path());

$restaurant_records = array();

$select_sql = $db->col(array('id', 'name', 'description', 'address_1', 'address_2', 'postcode', 'del_radius_mi', 'navname')) . ', ';
$select_sql .= "(SELECT {$tbl['town']}.name FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id) AS town, ";
$select_sql .= "(SELECT {$tbl['city']}.name FROM {$tbl['city']} WHERE {$tbl['city']}.id = (SELECT {$tbl['town']}.city_id FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id)) AS city, ";
$select_sql .= "(SELECT navname FROM {$tbl['town']} WHERE id = town_id) AS town_navname, ";
$select_sql .= "(SELECT {$tbl['city']}.navname FROM {$tbl['city']} WHERE {$tbl['city']}.id = (SELECT {$tbl['town']}.city_id FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id)) AS city_navname";


$breadcrumbs = array();

$type_search = false;

//Search
if ( (isset($current_path[1])) && ($current_path[1] == 'search') && (isset($current_path[2])) ) {

	$postcode = $current_path[2];
	$postcode = str_replace('-', ' ', $postcode);
	$postcode = preg_replace("%[^a-z0-9\ ]%", '', $postcode);

	$postcode_db = $postcode;
	$postcode_disp = strtoupper($postcode);

	$cond = array('status = 1');
	$cond_sql = $db->cond($cond, 'AND');

	$user_postcode_es = $db->es($postcode_db);

	$order_by = $db->order(array(array('distance', 'ASC')));

	//Postcode
	$sql = <<<EOSQL
SELECT @orig_lat:=ta_postcode.lat, @orig_lon:=ta_postcode.lon FROM ta_postcode WHERE REPLACE('{$user_postcode_es}', ' ', ' ') LIKE CONCAT(ta_postcode.postcode, '%') ORDER BY LENGTH(postcode) DESC LIMIT 0, 1
EOSQL;

	$db->query($sql);

	//Restaurant
	$sql = <<<EOSQL
SELECT
	{$select_sql},
	3956 * 2 * ASIN(SQRT( POWER(SIN((@orig_lat - ta_restaurant.lat) * pi()/180 / 2), 2) + COS(@orig_lat * pi()/180) * COS(ta_restaurant.lat * pi()/180) * POWER(SIN((@orig_lon - ta_restaurant.lon) * pi()/180 / 2), 2) )) AS distance
FROM
	{$tbl['restaurant']}
WHERE
	{$cond_sql}
ORDER BY
	{$order_by}
LIMIT
	0, 30
EOSQL;

	//http://assets.en.oreilly.com/1/event/2/Geo%20Distance%20Search%20with%20MySQL%20Presentation.ppt

	$restaurant_result = $db->query($sql);

	//Breadcrumbs
	$breadcrumbs[] = array('link' => navfr::link(array('takeaway')),'name' => 'Takeaways');
	$breadcrumbs[] = array('link' => navfr::link(array('takeaway','search', $current_path[2])),'name' => 'Search Postcode ' . $postcode_disp);

	//H1 title
	$h1_title = 'Takeaways deliverting to postcode ' . $postcode_disp;

	//Page title
	$page_title = 'Takeaways In Postcode ' . $postcode_disp;

	//Page description
	$metadesc = '';

	$area_navigation_html = '';

	$nonavailable_message = 'Sorry we could not find any takeaways available in your postcode area.';

	$type_search = true;

} else if (isset($current_path[2])) {
	//Town specified

	//Resolve city data
	$city_data = appgeneral::city_from_navname($current_path[1]);

	//If city not found
	if ($city_data == false) {
		template_lib::show_404();
		exit;
	}

	//Resolve town data
	$town_data = appgeneral::town_from_navname($city_data['id'], $current_path[2]);

	//If town not found
	if ($town_data == false) {
		template_lib::show_404();
		exit;
	}

	//Restaurant list query
	$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $select_sql, $db->cond(array("town_id = {$town_data['id']}", "status = 1"), 'AND'), $db->order(array(array('name', 'ASC'))));

	//Breadcrumbs
	$breadcrumbs[] = array('link' => navfr::link(array('takeaway')),'name' => 'Takeaways');
	$breadcrumbs[] = array('link' => navfr::link(array('takeaway',$city_data['navname'])),'name' => $city_data['name']);
	$breadcrumbs[] = array('link' => navfr::link(array('takeaway', $city_data['navname'], $town_data['navname'])),'name' => $town_data['name']);

	//H1 title
	$h1_title = "Takeaways in {$town_data['name']}, {$city_data['name']}";

	//Page title
	$page_title = "Takeaways in {$town_data['name']}, {$city_data['name']}";

	//Page description
	$metadesc = $town_data['metadesc'];

	$area_navigation_html = '';

	$nonavailable_message = 'Sorry we do not currently have any takeaways in your town';

} else if ( (isset($current_path[1])) && ($current_path[1] != 'search') ) {
	//City specified

	//Resolve city data
	$city_data = appgeneral::city_from_navname($current_path[1]);

	//If city not found
	if ($city_data == false) {
		template_lib::show_404();
		exit;
	}

	$cond = array();
	$cond[] = "town_id IN (SELECT {$tbl['town']}.id FROM {$tbl['town']} WHERE {$tbl['town']}.city_id = {$city_data['id']})";
	$cond[] = "status = 1";

	//Restaurant list query
	$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $select_sql, $db->cond($cond, 'AND'), $db->order(array(array('name', 'ASC'))));

	//Breadcrumbs
	$breadcrumbs[] = array('link' => navfr::link(array('takeaway')), 'name' => 'Takeaways');
	$breadcrumbs[] = array('link' => navfr::link(array('takeaway', $city_data['navname'])), 'name' => $city_data['name']);

	//H1 title
	$h1_title = "Takeaways in {$city_data['name']}";

	//Page title
	$page_title = "Takeaways in {$city_data['name']}";

	//Page description
	$metadesc = $city_data['metadesc'];

	//Show towns
	$area_navigation_html = appgeneral::towns_list_html($city_data['id']);

	$nonavailable_message = 'Sorry we do not currently have any takeaways in your city';

} else {
	//Nothing specified

	//Restaurant list query
	$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $select_sql, $db->cond(array("status = 1"), 'AND'), $db->order(array(array('added', 'ASC'))), 0, 100);

	//Breadcrumbs
	$breadcrumbs[] = array('link' => navfr::link(array('takeaway')), 'name' => 'Takeaways');

	//H1 title
	$h1_title = 'Takeaways Listing';

	//Page title
	$page_title = 'Local Takeaway Menus, Order Online';

	//Page description
	$metadesc = '';

	//Show cities
	$area_navigation_html = appgeneral::cities_list_html();

	$nonavailable_message = 'No takeaways available';

}



//Restaurant listing
$restaurants_html = '';
$row = 'b';
while ($restaurant_record = $db->record_fetch($restaurant_result)) {

	//If search, check distance
	if ( ($type_search == false) || ( ($type_search == true) && ($restaurant_record['distance'] !== null) && ($restaurant_record['distance'] <= $restaurant_record['del_radius_mi']) ) ) {

		$restaurant_recordh = lib::htmlentities_array($restaurant_record);

		$address = $restaurant_recordh['address_1'];
		if ($restaurant_recordh['address_2']) {
			$address .= "<br />\n" . $restaurant_recordh['address_2'];
		}

		$link = navfr::link_h(array('takeaway', $restaurant_record['city_navname'], $restaurant_record['town_navname'], $restaurant_record['navname']));

		//Retrieve restaurant open status
		//[Optimisable]
		$status_open = appgeneral::restaurant_open($restaurant_record['id']);

		if ($status_open) {
			$status_html = <<<EOHTML
<span class="open">Open</span>
EOHTML;
		} else {
			$status_html = <<<EOHTML
<span class="close">Closed</span>
EOHTML;
		}

		$row = ($row == 'b') ? 'a' : 'b';

		if (file_exists($publichtml_path . "resources/takeaway/{$restaurant_recordh['id']}/logo.png")) {
			$logo_type = $restaurant_recordh['id'];
		} else {
			$logo_type = "default";
		}

		$restaurants_html .= <<<EOHTML

<li class="restaurant restaurant-{$row}">

	<div class="info">

		<h2><a href="{$link}">{$restaurant_recordh['name']}</a></h2>

		<div class="address">
			{$address}<br />
			{$restaurant_recordh['town']}<br />
			{$restaurant_recordh['city']} {$restaurant_recordh['postcode']}<br />
		</div>

		<div class="status">Status: {$status_html}</div>

	</div>

	<a href="{$link}" class="logo"><img src="{$link_base_path}resources/takeaway/{$logo_type}/logo.png" width="87" height="87" alt="{$restaurant_recordh['name']} Logo" /></a>

</li>

EOHTML;

	}

}

if ($restaurants_html) {

	$restaurants_html = <<<EOHTML
<ul class="restaurants_list">
	{$restaurants_html}
</ul>
EOHTML;

} else {

	$restaurants_html = <<<EOHTML
<div class="norestaurants">
	{$nonavailable_message}
</div>
EOHTML;

}


if ($area_navigation_html) {

	$area_navigation_html = <<<EOHTML
<div class="infobox">
	<div class="infobox-top">
		<div class="infobox-bottom">
			<div class="content">
{$area_navigation_html}
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>
EOHTML;

}

$breadcrumbs_html = template_lib::breadcrumbs($breadcrumbs);

$h1_titleh = htmlentities($h1_title);

$body_html = <<<EOHTML

{$breadcrumbs_html}

<h1>{$h1_titleh}</h1>

{$area_navigation_html}

{$restaurants_html}

EOHTML;

$template = new template();
$template->settitle($page_title);
$template->setmetadesc($metadesc);
//$template->setheaderaddinhtml($headeraddin_html);
$template->setmainnavsection('takeaway');
$template->setbodyhtml($body_html);
$template->setshowsearch(true);
$template->setshowpopular(true);
$template->setshoworderprocess(true);
$template->display();

?>