<?php

class cron_general {

	const DEBUGTYPE_ERROR = 1;
	const DEBUGTYPE_STATUS = 2;

	public $fatalshandledinternally = true;
	public $showdebugging = true;

	//Run
	function run() {
		global $db, $tbl;

		$this->debuginfo(self::DEBUGTYPE_STATUS, 0, 'Running @ ' . date('Y-m-d H:i:s') . "\n");

		try {

			//$db->table_empty($tbl['cronerror']);
			//$db->record_update($tbl['config'], $db->rec(array('last_cron_general' => null)));

			//$db->table_empty($tbl['notify_log']);

			//Retrieve recent error count
			$errorshold = $this->errorshold();

			//If not too many exceptions recently
			if ($errorshold == false) {

				//If not already running / exceeded time limit
				if ($this->checknotrunning() == true) {

					//Set as running
					$this->setrunning(true);

					//Process orders
					$this->process_orders();

					//Finished running
					$this->setrunning(false);

				}

			}

		} catch (Exception $e) {

			//Log the exception
			exceptions::savelogentry($e);

			//Save cron exception to database
			$cronerror_record = array(
				'errordatetime' => $db->datetimenow(),
			);
			$db->record_insert($tbl['cronerror'], $db->rec($cronerror_record));

			//Set not running
			$this->setrunning(false);

			$this->debuginfo(self::DEBUGTYPE_ERROR, 0, 'Exception: ' . $e->getMessage());

			//Report error via email
			appgeneral::email_error('Cron General', $e->getMessage());

		}

	}

	//Check if too many recent error
	private function errorshold() {
		global $db, $tbl, $cfg;

		$this->debuginfo(self::DEBUGTYPE_STATUS, 0, 'Checking for errors recently');

		$ts = time() - $cfg['cron_error_period'];
		$errorcheck_date = gmdate('Y-m-d H:i:s', $ts);

		//Retrieve error
		$cronerror_result = $db->table_query($db->tbl($tbl['cronerror']), 'COUNT(*) AS total', $db->cond(array("errordatetime > '{$errorcheck_date}'"), 'AND'), '', 0, 1);
		if (!($cronerror_record = $db->record_fetch($cronerror_result))) {
			throw new Exception('Count did not return anything');
		}

		$this->debuginfo(self::DEBUGTYPE_STATUS, 1, "{$cronerror_record['total']} errors recently");

		if ($cronerror_record['total'] < $cfg['cron_error_max']) {
			$limitreached = false;
		} else {
			$this->debuginfo(self::DEBUGTYPE_STATUS, 0, "Errors limit of \"{$cfg['cron_error_max']}\" reached");
			$limitreached = true;
		}

		return $limitreached;

	}

	//Set cron running
	private function setrunning($status) {
		global $db, $tbl;

		$this->debuginfo(self::DEBUGTYPE_STATUS, 0, 'Set as running: ' . (($status) ? 'true' : 'false'));

		$date = ($status == true) ? $db->datetimenow() : null;

		$db->record_update($tbl['config'], $db->rec(array('last_cron_general' => $date)), $db->cond(array("id = 1"), 'AND'));

	}

	//Check not already running (or if running time limit exceeded)
	private function checknotrunning() {
		global $db, $tbl, $cfg;

		$this->debuginfo(self::DEBUGTYPE_STATUS, 0, 'Check not running');

		//Retrieve last run date
		$config_result = $db->table_query($db->tbl($tbl['config']), $db->col(array('last_cron_general')), $db->cond(array("id = 1"), 'AND'));
		if (!($config_record = $db->record_fetch($config_result))) {
			throw new Exception('Config row 1 not found');
		}

		$this->debuginfo(self::DEBUGTYPE_STATUS, 1, 'Last Run @ ' . date('Y-m-d H:i:s', strtotime($config_record['last_cron_general'] . ' UTC')));

		if ($config_record['last_cron_general'] == null) {
			$lastruncronplus = 0;
		} else {
			$lastruncronplus = strtotime($config_record['last_cron_general'] . ' UTC') + $cfg['cron_running_max_reset'];
			$this->debuginfo(self::DEBUGTYPE_STATUS, 1, 'Checking against @ ' . date('Y-m-d H:i:s', $lastruncronplus));
		}

		if ($lastruncronplus < time()) {
			$this->debuginfo(self::DEBUGTYPE_STATUS, 1, 'Not running');
			return true;
		} else {
			$this->debuginfo(self::DEBUGTYPE_STATUS, 0, 'Already running');
			return false;
		}

	}

	//Save debug message
	private function debuginfo($debug_type, $debug_level, $debug_message) {

		$spaceaddin = str_repeat(' ', $debug_level);

		if ($this->showdebugging == true) {
			$spaceaddin = str_repeat(' ', $debug_level);
			echo "{$spaceaddin}{$debug_message}\n";
		}

	}

	//Process orders
	private function process_orders() {
		global $db, $tbl, $cfg;

		$this->debuginfo(self::DEBUGTYPE_STATUS, 0, 'Processing orders');

		$cond = array();
		$cond[] = "status IN(".implode(',', $cfg['order_status_process']).")";

		$this->debuginfo(self::DEBUGTYPE_STATUS, 1, 'Retrieving non finished orders');

		//Retrieve all orders that are not finished processing
		$order_result = $db->table_query($db->tbl($tbl['order']), $db->col(array('id', 'name')), $db->cond($cond, 'AND'), $db->order(array(array('id', 'ASC'))));
		while ($order_record = $db->record_fetch($order_result)) {

			$this->debuginfo(self::DEBUGTYPE_STATUS, 2, 'Order Id: ' . $order_record['id']);
			$this->debuginfo(self::DEBUGTYPE_STATUS, 2, 'Order Name: ' . $order_record['name']);

			//Attempt notify
			$this->attempt_phone_notify($order_record['id']);

		}

	}

	//Attempt phone notify
	function attempt_phone_notify($order_id) {
		global $db, $tbl, $cfg;

		$this->debuginfo(self::DEBUGTYPE_STATUS, 3, 'Attempting phone notify');

		//Retrieve details for this order
		$select_sql = $db->col(array('id', 'name', 'email', 'telephone', 'address', 'postcode', 'restaurant_id', 'restaurant_name', 'delivery_charge', 'total', 'handling', 'added', 'notes', 'test')) . ', ';
		$select_sql .= "(SELECT logentry FROM {$tbl['notify_log']} WHERE {$tbl['notify_log']}.order_id = {$tbl['order']}.id ORDER BY logentry DESC LIMIT 0, 1) AS last_notify_action, ";
		$select_sql .= "(SELECT telephone FROM {$tbl['restaurant']} WHERE {$tbl['restaurant']}.id = {$tbl['order']}.restaurant_id) AS restaurant_telephone";
		$order_result = $db->table_query($db->tbl($tbl['order']), $select_sql, $db->cond(array("id = {$order_id}"), 'AND'), $db->order(array(array('id', 'ASC'))));
		if (!($order_record = $db->record_fetch($order_result))) {
			throw new Exception("Order id \"{$order_id}\" not found");
		}

		$this->debuginfo(self::DEBUGTYPE_STATUS, 4, 'last_notify_action: ' . $order_record['last_notify_action']);

		//Check no last notify action, or last notify action longer than the retry limit
		if ($order_record['last_notify_action'] == null) {
			$oktoprocess = true;
		} else {

			$last_notify_action_ts = strtotime($order_record['last_notify_action'] . ' UTC');
			$check_ts = $last_notify_action_ts + $cfg['notify_phone_retry_time'];
			$now_ts = time();
			if ($check_ts < $now_ts) {
				$oktoprocess = true;
			} else {
				$oktoprocess = false;
			}

			$this->debuginfo(self::DEBUGTYPE_STATUS, 4, 'Check: ' . gmdate('Y-m-d H:i:s', $check_ts));
			$this->debuginfo(self::DEBUGTYPE_STATUS, 4, 'Now: ' . gmdate('Y-m-d H:i:s', $now_ts));

		}

		if ($oktoprocess) {

			$this->debuginfo(self::DEBUGTYPE_STATUS, 4, 'Ok, no last notfy or over limit');

			//Count previous number of "Attempt Restaurant Notify"
			$notify_log_result = $db->table_query($db->tbl($tbl['notify_log']), 'COUNT(*) AS total', $db->cond(array("status = 1", "order_id = {$order_id}"), 'AND'));
			if (!($notify_log_record = $db->record_fetch($notify_log_result))) {
				throw new Exception("Count did not return");
			}

			$this->debuginfo(self::DEBUGTYPE_STATUS, 4, 'Previous notify attempts: ' . $notify_log_record['total']);

			//Check have not exceeded notification attempts
			if ($notify_log_record['total'] < $cfg['notify_phone_try']) {

				$this->debuginfo(self::DEBUGTYPE_STATUS, 4, 'Under limit of ' . $cfg['notify_phone_try']);

				//Update notify status
				$db->record_insert($tbl['notify_log'], $db->rec(array('order_id' => $order_record['id'], 'status' => 1, 'logentry' => $db->datetimenow()))); //Status: Attempt Restaurant Notify

				//Update order status
				$db->record_update($tbl['order'], $db->rec(array('status' => 2)), $db->cond(array("id = {$order_record['id']}"), 'AND')); //Status: Processing

				//Prepare call message
				$message = $this->attempt_phone_notify_message($order_record);

				//Attempt phone notification
				$this->attempt_phone_notify_call($order_record, $message);

			} else {

				$this->debuginfo(self::DEBUGTYPE_STATUS, 4, "Limit of {$cfg['notify_phone_try']} notify attempts met");

				//Send email to customer
				$added_ts = strtotime($order_record['added'] . ' UTC');
				$diff = time() - $added_ts;

				$time_added = appgeneral::sec_to_dhms($diff);

				$email_body = <<<EOMAIL
Dear {$order_record['name']},

Thank you for your recent order with {$cfg['site_name']}.

Regrettably on this occasion despite {$cfg['notify_phone_try']} attempt(s) in the last {$time_added} we have been unable to make contact with the restaurant "{$order_record['restaurant_name']}" to fulfil your order.

We would like to apologise that this has happened and will follow up with the restaurant as to the reason in order to reduce the likelihood of it happening again.

If you like you could also try calling them directly on {$order_record['restaurant_telephone']}.

Kind Regards,

{$cfg['site_name']}
{$cfg['site_url']}

EOMAIL;

				$mail = new phpmailer();
				$mail->Mailer = $cfg['email_method'];
				$mail->From = $cfg['email_system_from'];
				$mail->Sender = $cfg['email_system_from'];
				$mail->FromName = $cfg['email_system_from'];
				$mail->Subject = "{$cfg['site_name']} Order Processing Error";
				$mail->IsHTML(false);
				$mail->Body = $email_body;

				$email = $order_record['email'];

				/*
				if ($order_record['test']) {
					$email = $cfg['email_test'];
				}
				*/

				$name = preg_replace("%[^a-zA-Z0-9\ ]%", '', $order_record['name']);
				$mail->AddAddress($email, $name);
				$mail->AddCC($cfg['email_admin'], $cfg['site_name']);

				if (!$mail->Send()) {
					throw new Exception("Unable to send email: {$mail->ErrorInfo}");
				}


				$telephone = $order_record['telephone'];
				/*
				if ( ($order_record['test']) || ($cfg['devmode']) ) {
					$telephone = $cfg['telephone_test'];
				}
				*/

				//Send SMS
				$send_sms = new send_sms();
				$sms_msg = "Regrettably we have been unable to contact the restaurant to fulfil your order, you could try calling them on {$order_record['restaurant_telephone']}";
				$status = $send_sms->send($send_sms->telno_uk_cc($telephone), $sms_msg);
				if (!$status) {
					throw new Exception('Error sending SMS: ' . $send_sms->errormsg);
				}


				//Update notify status
				$db->record_insert($tbl['notify_log'], $db->rec(array('order_id' => $order_record['id'], 'status' => 2, 'logentry' => $db->datetimenow()))); //Status: Attempt Restaurant Notify Limit Met

				//Update order status
				$db->record_update($tbl['order'], $db->rec(array('status' => 4)), $db->cond(array("id = {$order_record['id']}"), 'AND')); //Status: Error

			}

		} else {
			$this->debuginfo(self::DEBUGTYPE_STATUS, 4, 'Can not run notify processing, under limit');
		}

	}

	//Attempt phone notify call
	function attempt_phone_notify_call($order_record, $message) {
		global $db, $tbl, $cfg;

		$this->debuginfo(self::DEBUGTYPE_STATUS, 5, 'Attempting phone notify call');

		$ts = time() - 60 * 60 * 24;
		$check_date = gmdate('Y-m-d H:i:s', $ts);

		//Retrieve usage
		$phone_notify_use_result = $db->table_query($db->tbl($tbl['phone_notify_use']), 'COUNT(*) AS total', $db->cond(array("notifydatetime > '{$check_date}'"), 'AND'), '', 0, 1);
		if (!($phone_notify_use_record = $db->record_fetch($phone_notify_use_result))) {
			throw new Exception('Count did not return anything');
		}

		if ($phone_notify_use_record['total'] >= 30) {
			throw new Exception('Hard limit of over 30 calls reached');
		}

		//Add a usage log record
		$db->record_insert($tbl['phone_notify_use'], $db->rec(array('notifydatetime' => $db->datetimenow())));

		$telephone = $order_record['restaurant_telephone'];

		if ( ($order_record['test']) || ($cfg['devmode']) ) {
			$telephone = $cfg['telephone_test'];
		}

		//Remove leading zero
		$telephone_cc = '01144' . preg_replace('/^0/', '', $telephone);

		$request = array(
			'anr' => array(
				'PhoneNumberToDial' => $telephone_cc,
				'TextToSay' => $message,
				'LicenseKey' => $cfg['notify_license_key'],
				'CallerIDNumber' => '',
				'CallerIDName' => '',
				//'StatusChangePostUrl' => '',
				'TransferNumber' => '',
				'NextTryInSeconds' => 60,
				'MaxCallLength' => $cfg['notify_phone_length_max'],
				'TryCount' => $cfg['notify_phone_redial_try'],
				'TTSvolume' => 100,
				'TTSrate' => 5,
				'UTCScheduledDateTime' => '1970-01-01T00:00:00Z',
				'VoiceID' => 1, // 0, 1, or 4 are the best
			),
		);

		try {

			$client = new SoapClient('http://ws.cdyne.com/NotifyWS/PhoneNotify.asmx?wsdl', array('trace' => 1));
			$result = $client->NotifyPhoneAdvanced($request);
			$soap_request_status = true;

			$this->debuginfo(self::DEBUGTYPE_STATUS, 5, 'SOAP API ok');

		} catch (Exception $exception){
			$soap_request_status = false;

			$this->debuginfo(self::DEBUGTYPE_STATUS, 5, 'SOAP API error: ' . $exception->getMessage());

			//Save exception log entry
			exceptions::savelogentry($exception);

			/*
			$soap_error = $exception->getMessage()."\n\n";
			if (isset($client)) {
				$soap_error .= "Request :\n".$client->__getLastRequest() ."\n";
				$soap_error .= "Response :\n".$client->__getLastResponse();
			}
			throw new Exception($soap_error);
			*/

		}

		//If call queued
		if ( ($soap_request_status) && ($result->NotifyPhoneAdvancedResult->ResponseCode == 0) ) {

			$this->debuginfo(self::DEBUGTYPE_STATUS, 5, 'Success, call queued');

			//Update notify status
			$db->record_insert($tbl['notify_log'], $db->rec(array('order_id' => $order_record['id'], 'status' => $result->NotifyPhoneAdvancedResult->ResponseCode+100, 'logentry' => $db->datetimenow())));

		} else {

			$this->debuginfo(self::DEBUGTYPE_STATUS, 5, 'Error, unable to queue call');

			$email_body = <<<EOMAIL
Dear {$order_record['name']},

Thank you for your recent order with {$cfg['site_name']}.

Unfortunately we are currently experiencing technical issues and are unable to send your order to "{$order_record['restaurant_name']}".

We have received a notification of this error, and would like to apologise for any inconvenience caused.

If you like you could also try calling the restaurant directly on {$order_record['restaurant_telephone']} to place your order.

Kind Regards,

{$cfg['site_name']}
{$cfg['site_url']}

EOMAIL;

			$mail = new phpmailer();
			$mail->Mailer = $cfg['email_method'];
			$mail->From = $cfg['email_system_from'];
			$mail->Sender = $cfg['email_system_from'];
			$mail->FromName = $cfg['email_system_from'];
			$mail->Subject = "{$cfg['site_name']} Order Processing Error";
			$mail->IsHTML(false);
			$mail->Body = $email_body;

			$email = $order_record['email'];

			/*
			if ($order_record['test']) {
				$email = $cfg['email_test'];
			}
			*/

			$name = preg_replace("%[^a-zA-Z0-9\ ]%", '', $order_record['name']);
			$mail->AddAddress($email, $name);
			$mail->AddCC($cfg['email_admin'], $cfg['site_name']);

			if (!$mail->Send()) {
				throw new Exception("Unable to send email: {$mail->ErrorInfo}");
			}

			//Send SMS
			$send_sms = new send_sms();
			$sms_msg = "Regrettably due to technical issues we have been unable to process your order, you could try calling the restaurant on {$order_record['restaurant_telephone']}";
			$status = $send_sms->send($send_sms->telno_uk_cc($order_record['telephone']), $sms_msg);
			if (!$status) {
				throw new Exception('Error sending SMS: ' . $send_sms->errormsg);
			}

			if ($soap_request_status == false) {
				$status = 3;
			} else {
				$status = $result->NotifyPhoneAdvancedResult->ResponseCode+100;
			}

			$this->debuginfo(self::DEBUGTYPE_STATUS, 5, 'Error status is: ' . $status);

			//Update notify status
			$db->record_insert($tbl['notify_log'], $db->rec(array('order_id' => $order_record['id'], 'status' => $status, 'logentry' => $db->datetimenow())));

			//Update order status
			$db->record_update($tbl['order'], $db->rec(array('status' => 4)), $db->cond(array("id = {$order_record['id']}"), 'AND')); //Status: Error

			//throw new Exception("SOAP API error: {$result->NotifyPhoneAdvancedResult->ResponseText}");

		}

	}

	//Make phone notify data safe
	static public function notifyscriptdatasafe_array($array) {

		if (!is_array($array)) {
			throw new Exception("\"{$array}\" is not an array");
		}

		$arraynew = array();
		foreach ($array as $name => $value) {
			if (is_array($value)) {
				$arraynew[$name] = self::notifyscriptdatasafe_array($value);
			} else {

				//Remove any "~"
				$value = str_replace('~', '', $value);

				//Remove any "\"
				$value = str_replace('\\', '', $value);

				//Remove any ">"
				$value = str_replace('>', '', $value);

				//Remove any "<"
				$value = str_replace('<', '', $value);

				//Save value
				$arraynew[$name] = $value;

			}
		}

		return $arraynew;

	}

	//Prepare phone notify message
	private function attempt_phone_notify_message($order_record) {
		global $db, $tbl, $cfg;

		$this->debuginfo(self::DEBUGTYPE_STATUS, 5, 'Attempting phone notify call prepare message');

		$order_record = self::notifyscriptdatasafe_array($order_record);

		$message = '';

		if ( ($cfg['devmode']) || ($order_record['test']) ) {
			$debug_emails = <<<EODATA
~\DebugEmail({$cfg['email_admin']})~
~\ErrorEmail({$cfg['email_admin']})~
EODATA;
		}

		if ($cfg['notify_custom_site_url']) {
			$url = $cfg['notify_custom_site_url'];
		} else {
			$url = $cfg['site_url'];
		}

		//navpd::link(array('p' => 'phone_notify_return', 'a' => $cfg['phone_notify_ret_password']));
		$datareturn_url = $url . "admin/?p=phone_notify_return&a={$cfg['phone_notify_ret_password']}&order_id={$order_record['id']}";

		$sitename = $cfg['site_name'];
		$sitename = str_ireplace('takeaway', 'take away', $sitename);

		$address = $order_record['address'];
		$address = str_replace("\r", '', $address);
		$address = str_replace(',', '', $address);
		$address = str_replace("\n", "<break strength='x-strong' />", $address);

		$postcode = $order_record['postcode'];
		$postcode = preg_replace('//', ' ',$postcode);
		$postcode = str_replace('  ', "<break strength='x-weak' />", $postcode);
		$postcode = trim($postcode);
		//$postcode = preg_replace("%[ \t]+%", ' ', $postcode);

		$telephone = $order_record['telephone'];
		$telephone = preg_replace('//', "<break strength='medium' />",$telephone);
		$telephone = str_replace("<break strength='medium' /> <break strength='medium' />", "<break strength='x-strong' />", $telephone);
		$telephone = trim($telephone);

		//http://wiki.cdyne.com/wiki/index.php?title=Phone_Notify!_TextToSay_Speech_Control_Commands

		$numbers_words = new numbers_words();
		//echo $numbers_words->toWords('456', 'en_GB');

		$delivery_charge = $numbers_words->toCurrency($order_record['delivery_charge'], 'en_GB', 'GBP');
		$total_value = $numbers_words->toCurrency($order_record['total'], 'en_GB', 'GBP');

		//Order items
		$gobackto = 'CustomerInfo';
		$order_items = '';
		$indexid = 0;
		$order_item_result = $db->table_query($db->tbl($tbl['order_item']), $db->col(array('name', 'qty', 'cost')), $db->cond(array("order_id = {$order_record['id']}"), 'AND'), $db->order(array(array('id', 'ASC'))));
		while ($order_item_record = $db->record_fetch($order_item_result)) {
			$indexid++;

			$order_item_record = self::notifyscriptdatasafe_array($order_item_record);

			$quantity = $numbers_words->toWords($order_item_record['qty'], 'en_GB');
			$cost = $numbers_words->toCurrency($order_item_record['cost'], 'en_GB', 'GBP');

			$itemname = $order_item_record['name'];
			$itemname = preg_replace('/(\d)"/', '\\1 inch', $itemname);

			$addintitle = ($indexid == 1) ? "Would like to order:\n~\PlaySilence(0.5)~\n\n" : '';

			$order_items .= <<<EODATA

~\AssignDTMF(1|{$gobackto})~

~\PlaySilence(2.5)~
~\Label(OrderItem{$indexid})~
~\AssignDTMF(1|{$gobackto})~

{$addintitle}
Item: {$indexid}
<break strength='strong' />
{$itemname}
<break time='2s' />
quantity {$quantity}
<break strength='strong' />
{$cost} each
<break strength='strong' />

EODATA;

			$gobackto = "OrderItem{$indexid}";

		}

		//If have notes
		if ($order_record['notes']) {

			$notes = <<<EODATA
~\PlaySilence(2.5)~
Special Instructions:
<break strength='strong' />
{$order_record['notes']}
EODATA;

		} else {
			$notes = '';
		}

		//Order handling (confirm text)
		if ($order_record['handling'] == 2) { //collection
			$handling_confirm = 'To confirm receipt of this order press 3.';
		} else {
			$handling_confirm = <<<EOTEXT
To confirm receipt of this order for delivery in 30 minutes press 3.
40 minutes press 4.
50 minutes press 5.
60 minutes press 6.

EOTEXT;
		}

		$message = <<<EOMESSAGE
{$debug_emails}
~\StatusChangePostURL({$datareturn_url}&type=postback)~
~\SetVar(maxcallseconds|{$cfg['notify_phone_length_max']})~
~\QueryExternalServer({$datareturn_url}&type=query&queueid=[queueid]&status_id=4|Result)~
~\Label(Start)~
~\ActOnDigitPress(false)~
~\ClearDTMF()~

~\AssignDTMF(1|PlaybackOrder)~
~\AssignDTMF(3|ConfirmOrder30Min)~
~\AssignDTMF(4|ConfirmOrder40Min)~
~\AssignDTMF(5|ConfirmOrder50Min)~
~\AssignDTMF(6|ConfirmOrder60Min)~
~\ActOnDigitPress(true)~

This is an online order from {$sitename}.

To listen to this order press 1.

To repeat an item press 1.

~\PlaySilence(5)~

This is an online order from {$sitename}.

To listen to this order press 1.

~\PlaySilence(10)~
~\EndCall()~

~\Label(PlaybackOrder)~
~\AssignDTMF(1|CustomerInfo)~
~\QueryExternalServer({$datareturn_url}&type=query&queueid=[queueid]&status_id=5|Result)~

~\Label(CustomerInfo)~

Name: {$order_record['name']}

~\PlaySilence(2.5)~

Address: {$address}<break strength='x-strong' />{$postcode}

~\PlaySilence(2.5)~

Telephone: {$telephone}

{$order_items}
~\AssignDTMF(1|{$gobackto})~

~\PlaySilence(2.5)~

~\Label(TotalsNotes)~

Delivery Charge
{$delivery_charge}

Total Order Value
{$total_value}

{$notes}

~\AssignDTMF(1|TotalsNotes)~

~\PlaySilence(2.5)~

{$handling_confirm}

~\PlaySilence(5)~

{$handling_confirm}

~\PlaySilence(15)~

Order confirmation not received, we will call back in a few minutes.

Goodbye.

~\EndCall()~

~\Label(ConfirmOrder30Min)~
~\SetVar(DeliveryMinutes|30)~
~\Goto(ConfirmOrder)~

~\Label(ConfirmOrder40Min)~
~\SetVar(DeliveryMinutes|40)~
~\Goto(ConfirmOrder)~

~\Label(ConfirmOrder50Min)~
~\SetVar(DeliveryMinutes|50)~
~\Goto(ConfirmOrder)~

~\Label(ConfirmOrder60Min)~
~\SetVar(DeliveryMinutes|60)~
~\Goto(ConfirmOrder)~

~\Label(ConfirmOrder)~
~\ClearDTMF()~
~\QueryExternalServer({$datareturn_url}&type=query&queueid=[queueid]&status_id=10&delivery_min=[DeliveryMinutes]|Result)~

Order Confirmed

Thank you for being a part of {$sitename}, we will notify the customer their order has been received.

Goodbye.

~\EndCall()~

~\Label(Amd)~
Call from {$sitename}.
Answer machine detected, goodbye.
~\EndCall()~
EOMESSAGE;

		$this->debuginfo(self::DEBUGTYPE_STATUS, 0, "Message:\n\n" . $message);

		return $message;

	}

}

?>