<?php

include $includes_path . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);


$xml_sitemap = '';


//List cities

$sql = <<<EOSQL

SELECT
	navname,
	(
		SELECT
			COUNT(*)
		FROM
			{$tbl['restaurant']}
		WHERE
				{$tbl['restaurant']}.status = 1
			AND
				{$tbl['restaurant']}.town_id IN(
					SELECT
						id
					FROM
						{$tbl['town']}
					WHERE
						city_id = {$tbl['city']}.id
				)

	) AS count
FROM
	{$tbl['city']}

HAVING
	count > 0

ORDER BY
	name ASC

EOSQL;

$cities_list_html = '';
$city_result = $db->query($sql);
while ($city_record = $db->record_fetch($city_result)) {

	$url = navfr::fqlink_h(navfr::link(array('takeaway', $city_record['navname'])));

	$xml_sitemap .= <<<EOXML
	<url>
		<loc>{$url}</loc>
	</url>
EOXML;

}



//List towns

$sql = <<<EOSQL



SELECT
	navname,
	(
		SELECT
			COUNT(*)
		FROM
			{$tbl['restaurant']}
		WHERE
				{$tbl['restaurant']}.status = 1
			AND
				{$tbl['restaurant']}.town_id = {$tbl['town']}.id

	) AS count,

	(
		SELECT
			{$tbl['city']}.navname
		FROM
			{$tbl['city']}
		WHERE
			{$tbl['city']}.id = {$tbl['town']}.city_id
	) AS city_navname

FROM
	{$tbl['town']}

HAVING
	count > 0

ORDER BY
	name ASC

EOSQL;

//Show towns
$towns_list_html = '';
$town_result = $db->query($sql);
while ($town_record = $db->record_fetch($town_result)) {

	$url = navfr::fqlink_h(navfr::link(array('takeaway', $town_record['city_navname'], $town_record['navname'])));

	$xml_sitemap .= <<<EOXML
	<url>
		<loc>{$url}</loc>
	</url>
EOXML;

}

//List restaurants
$select_sql = 'navname, ';
$select_sql .= "(SELECT navname FROM {$tbl['town']} WHERE id = town_id) AS town_navname, ";
$select_sql .= "(SELECT {$tbl['city']}.navname FROM {$tbl['city']} WHERE {$tbl['city']}.id = (SELECT {$tbl['town']}.city_id FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id)) AS city_navname";
$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $select_sql, $db->cond(array("status = 1"), 'AND'), $db->order(array(array('name', 'ASC'))));
while ($restaurant_record = $db->record_fetch($restaurant_result)) {

	$url = navfr::fqlink_h(navfr::link(array('takeaway', $restaurant_record['city_navname'], $restaurant_record['town_navname'], $restaurant_record['navname'])));

	$xml_sitemap .= <<<EOXML
	<url>
		<loc>{$url}</loc>
	</url>
EOXML;

}


$body_xml = <<<EOXML
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	<url>
		<loc>{$cfg['site_url']}</loc>
	</url>
	<url>
		<loc>{$cfg['site_url']}takeaway/</loc>
	</url>
	<url>
		<loc>{$cfg['site_url']}faq/</loc>
	</url>
	<url>
		<loc>{$cfg['site_url']}contact-us/</loc>
	</url>
	<url>
		<loc>{$cfg['site_url']}about-us/</loc>
	</url>
{$xml_sitemap}
</urlset>
EOXML;

header('Content-type: application/xml; charset="utf-8"',true);
echo $body_xml;

?>