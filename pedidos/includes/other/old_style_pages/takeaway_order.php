<?php

include $includes_path . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$auth = new auth();
$auth->handle();
$authinfo = $auth->getauthinfo();
//$auth->login_required();


$link_base_path = htmlentities(navfr::base_path());

$current_path = navfr::current_path();

if (!( (isset($current_path[1])) && (isset($current_path[2])) && (isset($current_path[3])) )) {
	throw new Exception('Somehow reached this page without city, town, restaurant specified');
}

//Resolve city data
$city_data = appgeneral::city_from_navname($current_path[1]);

//If city not found
if ($city_data == false) {
	template_lib::show_404();
	exit;
}

//Resolve town data
$town_data = template_lib::town_from_navname($city_data['id'], $current_path[2]);

//If town not found
if ($town_data == false) {
	template_lib::show_404();
	exit;
}

//Retrieve restaurant data

$select_sql = '';
$select_sql .= "(SELECT {$tbl['town']}.name FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id) AS town, ";
$select_sql .= "(SELECT {$tbl['city']}.name FROM {$tbl['city']} WHERE {$tbl['city']}.id = (SELECT {$tbl['town']}.city_id FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id)) AS city";

$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $db->col(array('id', 'name', 'del_radius_mi', 'min_order_value', 'delivery_charge', 'address_1', 'address_2', 'postcode', 'telephone', 'navname')) . ', ' . $select_sql, $db->cond(array("navname='".$db->es($current_path[3])."'", "town_id = {$town_data['id']}", "status = 1"), 'AND'), '', 0, 1);
if (!($restaurant_record = $db->record_fetch($restaurant_result))) {

	//If restaurant not found
	template_lib::show_404();
	exit;

}



//Breadcrumbs
$breadcrumbs = array();
$breadcrumbs[] = array('link' => navfr::link(array('takeaway')),'name' => 'Takeaways');
$breadcrumbs[] = array('link' => navfr::link(array('takeaway', $city_data['navname'])),'name' => $city_data['name']);
$breadcrumbs[] = array('link' => navfr::link(array('takeaway', $city_data['navname'], $town_data['navname'])),'name' => $town_data['name']);
$breadcrumbs[] = array('link' => navfr::link(array('takeaway', $city_data['navname'], $town_data['navname'], $restaurant_record['navname'])),'name' => $restaurant_record['name']);
$breadcrumbs[] = array('link' => navfr::link(array('takeaway', $city_data['navname'], $town_data['navname'], $restaurant_record['navname'], 'order')),'name' => 'Order');

$breadcrumbs_html = template_lib::breadcrumbs($breadcrumbs);






$order_total = 0;

//Retrieve menu items saved in cookie
$menu_items = array();
$menu_cookie = appgeneral::menu_cookie();
foreach ($menu_cookie['menu_items'] as $item_id => $item) {

	//Retrieve menu item data
	$menu_item_data = appgeneral::menu_item_data($item_id);
	if ($menu_item_data) {

		//If restaurant id matches current restaurant
		if ($menu_item_data['restaurant_id'] == $restaurant_record['id']) {

			$menu_items[$item_id] = array(
				'qty' => $item['qty'],
			);

			$menu_items[$item_id] = array_merge($menu_items[$item_id], $menu_item_data);

			$order_total += $menu_item_data['cost'] * $item['qty'];

		}

	}

}




//Retrieve handling for this order
$handling = appgeneral::retrieve_order_handling($restaurant_record['id']);

$order_total_withdeliv = $order_total;

//Add delivery onto order total
if ( ($handling == 1) && ($restaurant_record['delivery_charge']) ) {
	$order_total_withdeliv += $restaurant_record['delivery_charge'];
}

//Format order total
$order_total = number_format($order_total, 2);
$order_total_withdeliv = number_format($order_total_withdeliv, 2);


$fields = array('name', 'address', 'postcode', 'email', 'notes', 'telephone', 'new_password');

$formdata = array();
foreach ($fields as $field) {
	$formdata[$field] = (isset($_POST[$field])) ? $_POST[$field] : '';
}

$errormsg_html = array();

if (isset($_POST['formposted'])) {

	if (!$_POST['name']) {
		$errormsg_html[] = "'Name' is required";
	}

	if (!$_POST['address']) {
		$errormsg_html[] = "'Address' is required";
	}

	if (!$_POST['postcode']) {
		$errormsg_html[] = "'Postcode' is required";
	}

	//Telephone
	$telephone = '';
	if ($_POST['telephone']) {

		$telephone = $_POST['telephone'];
		$telephone = str_replace('-', ' ', $telephone);
		$telephone = preg_replace("%[ \t]+%", ' ', $telephone);

		//Telephone number
		if (!preg_match("%[^0-9\ ]%", $telephone)) {

			$telephone_validate = $telephone;
			$telephone_validate = preg_replace("%[^0-9]%", '', $telephone_validate);

			if (!preg_match("/^0[1-9][0-9]{8,9}$/", $telephone_validate)) {
				$errormsg_html[] = "'Telephone' does not appear to be valid (should not contain country code, start with a leading zero, and be 10-11 digits in length)";
			}

			if (preg_match("/^09/", $telephone_validate)) {
				$errormsg_html[] = "'Tel No' appears to be premium rate";
			}

		} else {
			$errormsg_html[] = "'Telephone' should be in the format e.g. 123 4567 8901, and should contain 0-9 only";
		}

	} else {
		$errormsg_html[] = "'Telephone' not specified.";
	}

	if (!$_POST['email']) {
		$errormsg_html[] = "'Email' is required";
	} else {
		if (!lib::chkemailvalid($_POST['email'])) {
			$errormsg_html[] = "'Email' does not appear to be valid";
		}
	}

	//If save details
	if (isset($_POST['save_details'])) {

		//If no errors
		if (!count($errormsg_html)) {

			//Check for an account that already exists
			$customer_result = $db->table_query($db->tbl($tbl['customer']), $db->col(array('id')), $db->cond(array("email = '".$db->es($_POST['email'])."'", "verified = 1"), 'AND'), '', 0, 1);
			if ($customer_record = $db->record_fetch($customer_result)) {
				$errormsg_html[] = "You already have an account setup with this email address, please login to use it.  <a href=\"#\">Forgotten your password</a>?"; //###################FORGOTTED PASSWORD
			}

			//Check password
			if ($_POST['new_password']) {
				if (strlen($_POST['new_password']) < 8) {
					$errormsg_html[] = "'Password' must be greater than 8 characters";
				}
			} else {
				$errormsg_html[] = "'Password' must be spcified to create an account";
			}

		}

	}

} else {

	if (isset($_GET['testform'])) {

		/*
		$notes = '';
		$total = rand(5, 10);
		for ($i=0; $i<$total; $i++) {
			$notes .= ucfirst(lib::randstring(rand(5, 10), lib::RANDSTRING_AZ_LC)) . ' ';
		}

		$notes = trim(ucfirst($notes));

		$address = rand(1, 100) . ' ' . ucfirst(lib::randstring(rand(10, 10), lib::RANDSTRING_AZ_LC)) . ' Road' . "\n";
		$address .=  ucfirst(lib::randstring(rand(7, 10), lib::RANDSTRING_AZ_LC));
		$address .=  ucfirst(lib::randstring(rand(7, 10), lib::RANDSTRING_AZ_LC));

		$formdata['name'] = 'Test ' . ucfirst(lib::randstring(rand(5, 10), lib::RANDSTRING_AZ_LC));
		$formdata['address'] = $address;
		$formdata['postcode'] = lib::randstring(2, lib::RANDSTRING_AZ_UC) . rand(1, 9) . ' ' . rand(1, 9) . lib::randstring(2, lib::RANDSTRING_AZ_UC);
		$formdata['telephone'] = '020 - ' . lib::randstring(4, lib::RANDSTRING_09) . ' - ' . lib::randstring(4, lib::RANDSTRING_09);
		$formdata['email'] = lib::randstring(rand(5, 10), lib::RANDSTRING_AZ_LC) . '@' . parse_url($cfg['site_url'], PHP_URL_HOST);
		$formdata['notes'] = $notes;
		*/

		$formdata['name'] = 'Joe Bloggs';
		$formdata['address'] = "1 Main High Street\nLondon";
		$formdata['postcode'] = lib::randstring(2, lib::RANDSTRING_AZ_UC) . rand(1, 9) . ' ' . rand(1, 9) . lib::randstring(2, lib::RANDSTRING_AZ_UC);
		//$formdata['telephone'] = '020 - ' . lib::randstring(4, lib::RANDSTRING_09) . ' - ' . lib::randstring(4, lib::RANDSTRING_09);
		//$formdata['email'] = lib::randstring(rand(5, 10), lib::RANDSTRING_AZ_LC) . '@' . parse_url($cfg['site_url'], PHP_URL_HOST);
		$formdata['notes'] = 'Please deliver this as soon as possible, we are hungry';

		if ( (isset($_GET['testform'])) || ($cfg['devmode']) ) {
			$formdata['email'] = $cfg['email_test'];
			$formdata['telephone'] = $cfg['telephone_test'];
		}

	}

}

//Check not over limit for today
$check_ts = time() - $cfg['order_limit_period'];
$check_ts_datetime = gmdate('Y-m-d H:i:s', $check_ts);
$order_result = $db->table_query($db->tbl($tbl['order']), 'COUNT(*) AS total', "added > '{$check_ts_datetime}'", '', 0, 1);
if (!($order_result = $db->record_fetch($order_result))) {
	throw new Exception('Count did not return');
}

if ($order_result['total'] >= $cfg['order_limit']) {

	$time_period = appgeneral::sec_to_dhms($cfg['order_limit_period']);

	$errormsg_html[] = "Maximum no of orders for this rolling {$time_period} time period has been exceeded, we have been notified.  Sorry for any inconvenience caused, please try again later, items chosen will be kept in your basket for 14 days.";

	$message = "Limit of {$cfg['order_limit']} per {$time_period} time period has been exceeded.";
	$message = wordwrap($message, 70);
	$headers = 'From: ' . $cfg['email_system_from'];
	$status = mail($cfg['email_admin'], $cfg['site_name'] . ' Order Limit Exceeded', $message, $headers);
	if (!$status) {
		throw new Exception('Unable to send order limit exceeded email');
	}

}


$back_link= navfr::link_h(array('takeaway', $city_data['navname'], $town_data['navname'], $restaurant_record['navname']));

//If no items on order
if (!count($menu_items)) {
	$errormsg_html[] = "Please <a href=\"{$back_link}\">go back to the menu</a> and select items for your order.";
} else {

	if ($order_total < $restaurant_record['min_order_value']) {
		$min_order_value = number_format($restaurant_record['min_order_value'], 2);
		$errormsg_html[] = "Your order total of &pound;{$order_total} (excluding delivery) is under the minimum value of &pound;{$min_order_value} accepted by " . htmlentities($restaurant_record['name']) . " please <a href=\"{$back_link}\">add additional items</a> to your order.";
	}

}


//Retrieve current restaurant open/closed status
$restaurant_open_status = appgeneral::restaurant_open($restaurant_record['id']);

if ($restaurant_open_status == false) {
	$errormsg_html[] = "Your chosen restaurant is not currently open, restaurant opening times are available on the top right of the <a href=\"{$back_link}\">restaurants menu</a>.  Items chosen will be kept in your basket for 14 days or until you place you order.  Please try again when the selected restaurant is open or choose another restaurant.";
}



//If valid post with no errors
if ( (isset($_POST['formposted'])) && (!count($errormsg_html)) ) {

	//Save to database
	try {

		$db->transaction(dbmysql::TRANSACTION_START);

		//Insert order record
		$record = array(
			'name' => $_POST['name'],
			'address' => $_POST['address'],
			'postcode' => $_POST['postcode'],
			'email' => $_POST['email'],
			'telephone' => $telephone,
			'notes' => $_POST['notes'],
			'total' => $order_total_withdeliv,
			'handling' => $handling,
			'restaurant_id' => $restaurant_record['id'],
			'restaurant_name' => $restaurant_record['name'],
			'test' => (isset($_GET['testform']) ? 1 : 0),
			'status' => 1, //New Order
			'added' => $db->datetimenow(),
		);

		$record = appgeneral::filternonascii_array($record);

		//If delivery, make delivery charge
		if ($handling == 1) { //delivery
			$record['delivery_charge'] = $restaurant_record['delivery_charge'];
		}

		$db->record_insert($tbl['order'], $db->rec($record));

		$order_id = $db->record_insert_id();


		//Insert order item records
		foreach ($menu_items as $item_id => $menu_item) {

			$record = array(
				'name' => $menu_item['name'],
				'cost' => $menu_item['cost'],
				'qty' => $menu_item['qty'],
				'menu_item_id' => $item_id,
				'order_id' => $order_id,
			);
			$db->record_insert($tbl['order_item'], $db->rec($record));

		}

		$verify_code = lib::randstring(5, lib::RANDSTRING_AZ_LC);

		//Create account if selected
		if (isset($_POST['save_details'])) {

			//Insert order record
			$record = array(
				'email' => $_POST['email'],
				'password' => md5($_POST['new_password']),
				'name' => $_POST['name'],
				'address' => $_POST['address'],
				'postcode' => $_POST['postcode'],
				'telephone' => $telephone,
				'verified' => 0,
				'verify_code' => $verify_code,
				'registered' => $db->datetimenow(),
			);

			$record = appgeneral::filternonascii_array($record);

			$db->record_insert($tbl['customer'], $db->rec($record));

			$send_verify_email = true;

		} else {
			$send_verify_email = false;
		}

		$db->transaction(dbmysql::TRANSACTION_COMMIT);

		if ($send_verify_email) {

			//Send verification email

			$verify_link = navfr::fqlink(navfr::link(array('customer', 'verify-email'), array('v' => $verify_code)));

			$email_body = <<<EOMAIL
Thank you for saving your details with us for future orders on {$cfg['site_name']}.

We just need to verify your email address to allow you to login, to do this please click the link:

{$verify_link}
EOMAIL;

			$mail = new phpmailer();
			$mail->Mailer = $cfg['email_method'];
			$mail->From = $cfg['email_system_from'];
			$mail->Sender = $cfg['email_system_from'];
			$mail->FromName = $cfg['email_system_from'];
			$mail->Subject = "Verify your email for {$cfg['site_name']}";
			$mail->IsHTML(false);
			$mail->Body = $email_body;

			$mail->AddAddress($_POST['email']);

			if (!$mail->Send()) {
				throw new Exception("Unable to send email: {$mail->ErrorInfo}");
			}

		}

	} catch (Exception $exception) {

		$db->transaction(dbmysql::TRANSACTION_ROLLBACK);

		throw $exception;

	}

	//Compile order details for email
	$order_details = '';
	foreach ($menu_items as $item) {
		$cost = $item['cost'];
		$order_details .= <<<EODATA
x{$item['qty']} {$item['name']} (�{$cost} each)

EODATA;
	}

	$takeaway_address = $restaurant_record['address_1'];
	if ($restaurant_record['address_2']) {
		$takeaway_address .= "<br />\n" . $restaurant_record['address_2'];
	}

	$notes = ($_POST['notes']) ? 'Special Instructions: ' . $_POST['notes'] . "\n\n" : '';

	if ($handling == 2) {//collection
		$delivery_charge = 'N/A (order for collection)';
	} else {
		if ($restaurant_record['delivery_charge']) {
			$delivery_charge = number_format($restaurant_record['delivery_charge'], 2);
		} else {
			$delivery_charge = 'None';
		}
	}

	$email_body = <<<EOMAIL
We would like to confirm receipt of your takeaway order with {$cfg['site_name']}

----------------------------------
Your Details
----------------------------------

{$_POST['name']}
{$_POST['address']}
{$_POST['postcode']}

Tel: {$_POST['telephone']}

----------------------------------
Order Details:
----------------------------------

{$order_details}
Delivery: {$delivery_charge}

Total: {$order_total_withdeliv}

{$notes}----------------------------------
Takeaway Details:
----------------------------------

{$restaurant_record['name']}
{$takeaway_address}
{$restaurant_record['town']}
{$restaurant_record['city']}
{$restaurant_record['postcode']}

Tel: {$restaurant_record['telephone']}

----------------------------------

Thank you for your order.

If you notice any errors with your order please call the takeaway directly on {$restaurant_record['telephone']}.

__________________________________

This order was initiated by IP address: {$_SERVER['REMOTE_ADDR']}

EOMAIL;

	$mail = new phpmailer();
	$mail->Mailer = $cfg['email_method'];
	$mail->From = $cfg['email_system_from'];
	$mail->Sender = $cfg['email_system_from'];
	$mail->FromName = $cfg['email_system_from'];
	$mail->Subject = $cfg['site_name'] . ' Order';
	$mail->IsHTML(false);
	$mail->Body = $email_body;

	$email = $_POST['email'];
	if (isset($_GET['testform'])) {
		$email = $cfg['email_test'];
	}

	$name = preg_replace("%[^a-zA-Z0-9\ ]%", '', $_POST['name']);
	$mail->AddAddress($email, $name);

	if ($cfg['email_cc_order']) {
		$mail->AddBCC($cfg['email_cc_order'], $cfg['site_name']);
	}

	if (!$mail->Send()) {
		throw new Exception("Unable to send email: {$mail->ErrorInfo}");
	}

	/*
	$message = wordwrap($message, 70);

	$email_order = array($_POST['email'], $cfg['email_cc_order']);
	foreach ($email_order as $email) 

		if ($email) {

			$headers = 'From: ' . $cfg['email_system_from'];
			$status = mail($_POST['email'], $cfg['site_name'] . ' Order', $message, $headers);

			if (!$status) {
				throw new Exception('Unable to send order confirmation email');
			}

		}

	}
	*/

}

if ( (isset($_POST['formposted'])) && (!count($errormsg_html)) ) {

	$googanalyticspage = navfr::self() . 'completed/';

	$form_html = <<<EOHTML
<p>Thank you for your order, we have sent a copy to your email.  We will email you again once the takeaway acknowledges receipt of your order.</p>

<script type="text/javascript">
  <!--
	erasecookie("menu");
  //-->
</script>
EOHTML;

	$footeraddin_html = appgeneral::google_ecommercetracking_html($order_id);

} else {

	$footeraddin_html = '';

	$googanalyticspage = '';

	$errormsgall_html = appgeneral::errormsgs_html($errormsg_html);

	$formdatah = lib::htmlentities_array($formdata);

	$args = (isset($_GET['testform'])) ? array('testform' => 1) : array();
	$form_self = navfr::self_h($args);

	$save_details_checked = (isset($_POST['save_details'])) ? 'checked="checked"' : '';

	$form_html = <<<EOHTML

<p>Just enter your details below or login, and your order will be on its way to you shortly.</p>

<h2>Already have an account</h2>

<form method="post" action="{$form_self}">
	<div><input type="hidden" name="formposted_login" value="1" /></div>

	<div class="form">

{$login_errormsgall_html}

		<div class="group">
			<div class="fieldtitle"><label for="login_username">Email</label></div>
			<div class="fieldinput"><input type="text" name="login_username" id="login_username" value="{$formdatah['login_username']}" class="inputtxt" /></div>
			<div class="clear"></div>
		</div>

		<div class="group">
			<div class="fieldtitle"><label for="login_password">Password</label></div>
			<div class="fieldinput"><input type="text" name="login_password" id="login_password" value="{$formdatah['login_password']}" class="inputtxt" /></div>
			<div class="clear"></div>
		</div>

		<div><input type="image" src="{$link_base_path}resources/takeaway_order/login.gif" value="Place Order" alt="Login" name="send" class="submit" /></div>

	</div>

</form>

<div class="line"></div>

<h2>or just enter your details</h2>

<form method="post" action="{$form_self}">
	<div><input type="hidden" name="formposted" value="1" /></div>

	<div class="form">

{$errormsgall_html}

		<div class="group">
			<div class="fieldtitle"><label for="name">Name</label></div>
			<div class="fieldinput"><input type="text" name="name" id="name" value="{$formdatah['name']}" class="inputtxt" /></div>
			<div class="clear"></div>
		</div>

		<div class="group">
			<div class="fieldtitle"><label for="address">Address<br />(for delivery)</label></div>
			<div class="fieldinput"><textarea name="address" id="address" rows="5" cols="20" class="inputtxtarea">{$formdatah['address']}</textarea></div>
			<div class="clear"></div>
		</div>

		<div class="group">
			<div class="fieldtitle"><label for="postcode">Postcode</label></div>
			<div class="fieldinput"><input type="text" name="postcode" id="postcode" value="{$formdatah['postcode']}" class="inputtxt inputtxt-postcode" /></div>
			<div class="clear"></div>
		</div>

		<div class="group">
			<div class="fieldtitle"><label for="telephone">Telephone</label></div>
			<div class="fieldinput"><input type="text" name="telephone" id="telephone" value="{$formdatah['telephone']}" class="inputtxt" /></div>
			<div class="clear"></div>
		</div>

		<div class="group">
			<div class="fieldtitle"><label for="email">Email</label></div>
			<div class="fieldinput"><input type="text" name="email" id="email" value="{$formdatah['email']}" class="inputtxt" /></div>
			<div class="clear"></div>
		</div>

		<div class="group">
			<div class="fieldinput-savedetails"><input type="checkbox" name="save_details" id="save_details" value="1" onclick="savedetailchanged()" {$save_details_checked} /><label for="save_details"> Save my details for next time</label></div>
			<div class="clear"></div>
		</div>

		<div class="group" id="password_block">
			<div class="fieldtitle"><label for="new_password">Password</label></div>
			<div class="fieldinput"><input type="password" name="new_password" id="new_password" value="{$formdatah['new_password']}" class="inputtxt" /></div>
			<div class="clear"></div>
		</div>

		<div class="group">
			<div class="fieldtitle"><label for="notes">Special Instructions</label></div>
			<div class="fieldinput"><textarea name="notes" id="notes" rows="5" cols="20" class="inputtxtarea">{$formdatah['notes']}</textarea></div>
			<div class="clear"></div>
		</div>

		<div><input type="image" src="{$link_base_path}resources/takeaway_order/place_order.gif" value="Place Order" alt="Place Order" name="send" class="submit" /></div>

	</div>

</form>

EOHTML;

}

/*
$order_details = '';
foreach ($menu_items as $item) {
	$name = htmlentities($item['name']);
	$order_details .= <<<EOHTML
x1 
EOHTML;
}
*/

$body_html = <<<EOHTML

{$breadcrumbs_html}

<h1>Place Order</h1>

{$form_html}

EOHTML;

$page_title = "Place Order With {$restaurant_record['name']} - {$restaurant_record['town']}, {$restaurant_record['city']}";

$headeraddin_html = <<<EOHTML
<meta name="robots" content="noindex" />
<script src="{$link_base_path}resources/takeaway_order/takeaway_order.js" type="text/javascript"></script>
EOHTML;

//Goal Tracking: "/order/completed/$" (regex)

$template = new template();
$template->settitle($page_title);
//$template->setmetadesc($metadesc);
$template->setmainnavsection('takeaway');
$template->setgooganalyticspage($googanalyticspage);
$template->setheaderaddinhtml($headeraddin_html);
$template->setfooteraddinhtml($footeraddin_html);
$template->setbodyhtml($body_html);
//$template->setshowsearch(true);
//$template->setshoworderprocess(true);
$template->display();

?>