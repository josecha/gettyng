<?php

include $includes_path . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$auth = new auth();
$auth->handle();
$authinfo = $auth->getauthinfo();
//$auth->login_required();


$page_title = 'Online Takeaway Menus, Order From Your Local Restaurant';
$metadesc = 'Takeaways Direct simplifies the process of ordering takeout, allowing you to quickly find local takeaways, view menus and place your order online.';

$link_base_path = htmlentities(navfr::base_path());

//Retrieve latest added restaurants
$latest_restaurants = '';
$select_sql = $db->col(array('id', 'name', 'postcode', 'navname')) . ', ';
//$select_sql .= "(SELECT {$tbl['town']}.name FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id) AS town, ";
$select_sql .= "(SELECT {$tbl['city']}.name FROM {$tbl['city']} WHERE {$tbl['city']}.id = (SELECT {$tbl['town']}.city_id FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id)) AS city, ";
$select_sql .= "(SELECT navname FROM {$tbl['town']} WHERE id = town_id) AS town_navname, ";
$select_sql .= "(SELECT {$tbl['city']}.navname FROM {$tbl['city']} WHERE {$tbl['city']}.id = (SELECT {$tbl['town']}.city_id FROM {$tbl['town']} WHERE {$tbl['town']}.id = town_id)) AS city_navname";
$restaurant_result = $db->table_query($db->tbl($tbl['restaurant']), $select_sql, $db->cond(array("status = 1"), 'AND'), $db->order(array(array('added', 'DESC'))), 0, 4);
while ($restaurant_record = $db->record_fetch($restaurant_result)) {

	$restaurant_record_h = lib::htmlentities_array($restaurant_record);

	$postcode = explode(' ', $restaurant_record['postcode']);

	$info = <<<EOHTML
{$restaurant_record['city']} {$postcode[0]}
EOHTML;

	$name_h = htmlentities(appgeneral::trim_length($restaurant_record['name'], 20));
	$info_h = htmlentities(appgeneral::trim_length($info, 20));

	$link = navfr::link_h(array('takeaway', $restaurant_record['city_navname'], $restaurant_record['town_navname'], $restaurant_record['navname']));

	$latest_restaurants .= <<<EOHTML
			<li><a href="{$link}">{$name_h}</a> {$info_h}</li>

EOHTML;

}

$link_about = navfr::link_h(array('about-us'));
$link_takeaways = navfr::link_h(array('takeaway'));

$body_html = <<<EOHTML

<div class="photoimg">

	<img src="{$link_base_path}resources/home/photos/children.jpg" width="585" height="163" alt="4 Children eating pizza" id="photoimg_a" />
	<img src="{$link_base_path}resources/home/photos/children.jpg" width="585" height="163" alt="4 Children eating pizza" id="photoimg_b" />

</div>

<div class="about_us">
	<div class="info">
		<h2>About Us</h2>
		<p>Takeaways Direct simplifies the process of ordering takeout, allowing you to quickly find local takeaways, view menus and place your order online.</p>
		<a href="{$link_about}" class="readmore">Read more about us <img src="{$link_base_path}resources/template/link_bullet.gif" width="6" height="7" alt="Arrow Right" /></a>
	</div>
</div>

<div class="lastest_takeaways">
	<div class="info">
		<h2>Latest Takeaways Listed</h2>
		<ul>
{$latest_restaurants}
		</ul>
		<a href="{$link_takeaways}" class="readmore">Complete list of all takeaways <img src="{$link_base_path}resources/template/link_bullet.gif" width="6" height="7" alt="Arrow Right" /></a>
	</div>
</div>

<div class="clear"></div>




<script type="text/javascript">
  <!--

	/* Photo Image */

	var photoimg_timeoutinterval = 5;

	var photoimgs = [
		/*
		{
			"src": "{$link_base_path}resources/home/photos/young_adult_table.jpg",
			"alt": "5 Young people eating at a table",
			"title": ""
		},
		*/
		{
			"src": "{$link_base_path}resources/home/photos/children.jpg",
			"alt": "4 Children eating pizza",
			"title": ""
		},
		{
			"src": "{$link_base_path}resources/home/photos/family_table.jpg",
			"alt": "Mother and father with their two children eating pizza",
			"title": ""
		},
		{
			"src": "{$link_base_path}resources/home/photos/family_3_children.jpg",
			"alt": "Mother and two children eating pizza",
			"title": ""
		},
		{
			"src": "{$link_base_path}resources/home/photos/young_adult_sofa.jpg",
			"alt": "3 Young adults eating pizza on a sofa",
			"title": ""
		},
		{
			"src": "{$link_base_path}resources/home/photos/teenage_friends.jpg",
			"alt": "4 Teenage friends eating pizza",
			"title": ""
		}
	];

  //-->
</script>

EOHTML;

$headeraddin_html = <<<EOHTML
<script src="{$link_base_path}resources/home/home.js" type="text/javascript"></script>
EOHTML;

$template = new template();
$template->settitle($page_title);
$template->setmetadesc($metadesc);
$template->setheaderaddinhtml($headeraddin_html);
$template->setmainnavsection('home');
$template->setbodyhtml($body_html);
$template->setshowsearch(true);
$template->setshoworderprocess(true);
$template->display();

?>