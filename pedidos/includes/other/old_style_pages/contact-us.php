<?php

include $includes_path . 'config.php';
include $includes_path . 'general/init.php';

//Set exception handler
exceptions::sethandler();

//Connect to database
$db = new dbmysql($cfg['db_server'], $cfg['db_username'], $cfg['db_password'], $cfg['db_database']);

//Authentication
$auth = new auth();
$auth->handle();
$authinfo = $auth->getauthinfo();
//$auth->login_required();

$page_title = 'Contact Takeaways Direct';
$metadesc = 'Contact Takeaways Direct if you have any queries regarding the online web ordering process.';

$link_base_path = htmlentities(navfr::base_path());

$email_parts = explode('@', $cfg['email_contact']);

$fields = array('name', 'email', 'phone', 'comments');

$formdata = array();
foreach ($fields as $field) {
	$formdata[$field] = (isset($_POST[$field])) ? $_POST[$field] : '';
}

$errormsg_html = array();

if (isset($_POST['formposted'])) {

	if (!$_POST['name']) {
		$errormsg_html[] = "'Name' is required";
	}

	if (!$_POST['email']) {
		$errormsg_html[] = "'Email' is required";
	} else {
		if (!lib::chkemailvalid($_POST['email'])) {
			$errormsg_html[] = "'Email' does not appear to be valid";
		}
	}

	if (!$_POST['comments']) {
		$errormsg_html[] = "'Comments' is required";
	}

}


if ( (isset($_POST['formposted'])) && (!count($errormsg_html)) ) {

	$message = <<<EOMAIL
Name: {$_POST['name']}
Email: {$_POST['email']}
Phone: {$_POST['phone']}

{$_POST['comments']}

------------------------------
Sender IP: {$_SERVER['REMOTE_ADDR']}
<http://www.ip-adress.com/whois/{$_SERVER['REMOTE_ADDR']}>

EOMAIL;

	$mail = new phpmailer();
	$mail->Mailer = $cfg['email_method'];
	$mail->From = $cfg['email_system_from'];
	$mail->Sender = $cfg['email_system_from'];
	$mail->FromName = $cfg['email_system_from'];
	$mail->Subject = $cfg['site_name'] . ' Comment';
	$mail->IsHTML(false);
	$mail->Body = $message;

	$email = $_POST['email'];
	if (isset($_GET['testform'])) {
		$email = $cfg['email_test'];
	}

	if ($_POST['email']) {
		$name = preg_replace("%[^a-zA-Z0-9\ ]%", '', $_POST['name']);
		$mail->AddReplyTo($_POST['email'], $name);
	}

	$mail->AddAddress($cfg['email_contact']);

	if (!$mail->Send()) {
		$errormsg_html[] = 'Internal website configuration error, please email manually';
		//throw new Exception("Unable to send email: {$mail->ErrorInfo}");
	}

}

if ( (isset($_POST['formposted'])) && (!count($errormsg_html)) ) {

	$form_html = <<<EOHTML
<p>Thank you, we will get back to you shortly.</p>
EOHTML;

} else {

	$errormsgall_html = appgeneral::errormsgs_html($errormsg_html);

	$formdata_h = lib::htmlentities_array($formdata);

	$link_self = navfr::self_h();

	$form_html = <<<EOHTML

<p>If you have a <strong>query regarding an order</strong> and <strong>have confirmation from us</strong> that it has been successful received by the restaurant, you should <strong>contact the restaurant directly</strong>.  The restaurants phone number will have been listed on the order confirmation email.</p>

<p>If you have a query regarding the online web ordering process or anything else, please email us on <a href="#" id="email_addr">[JavaScript Required]</a> or fill in your details below.</p>
	
{$errormsgall_html}

<form method="post" action="{$link_self}">
	<div><input type="hidden" name="formposted" value="1" /></div>

	<div class="standardform">

		<div class="group">
			<label for="name" class="fieldtitle">Name *</label>
			<input type="text" name="name" id="name" value="{$formdata_h['name']}" class="inputxt" />
		</div>

		<div class="group">
			<label for="email" class="fieldtitle">Email *</label>
			<input type="text" name="email" id="email" value="{$formdata_h['email']}" class="inputxt" />
		</div>

		<div class="group">
			<label for="phone" class="fieldtitle">Phone</label>
			<input type="text" name="phone" id="phone" value="{$formdata_h['phone']}" class="inputxt" />
		</div>

		<div class="group">
			<label for="comments" class="fieldtitle">Comments *</label>
			<textarea rows="10" name="comments" id="comments" cols="40" class="inputxt">{$formdata_h['comments']}</textarea>
		</div>

		<div><input type="image" src="{$link_base_path}resources/contact-us/send_details.gif" alt="Send Details" name="send" class="submit" /></div>

	</div>

</form>

<script type="text/javascript">
  <!--

	var partb = "{$email_parts[1]}";
	var parta = "{$email_parts[0]}";

	document.getElementById("email_addr").innerHTML = parta + "@" + partb;
	document.getElementById("email_addr").href = "mailto:" + parta + "@" + partb;

  //-->
</script>

EOHTML;

}


$body_html = <<<EOHTML

<div class="breadcrumbs"></div>

<div class="mainpanel">

	<h1>Contact Takeaways Direct</h1>

	{$form_html}

</div>

EOHTML;


$template = new template();
$template->settitle($page_title);
$template->setmetadesc($metadesc);
//$template->setheaderaddinhtml($headeraddin_html);
$template->setmainnavsection('contact-us');
$template->setbodyhtml($body_html);
$template->setshowsearch(true);
$template->setshowpopular(true);
$template->setshoworderprocess(true);
$template->display();

?>