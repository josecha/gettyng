
/*

			SELECT
				ta_postcode.id, ta_postcode.latlon, ta_postcode.postcode
			FROM
				ta_postcode
			WHERE
					REPLACE('SE9', ' ', '')
				LIKE
					CONCAT(ta_postcode.postcode, '%')
			ORDER BY
				LENGTH(postcode) DESC
			LIMIT
				0, 1

*/

SELECT * 
FROM ta_postcode 
WHERE MBRContains( ta_postcode.latlon, GeomFromText( 'point(51.357204011565 1.4790344238281)' ) ) 



SELECT
	ta_restaurant.id,
	ta_restaurant.name,
	ROUND(
		GLength(
			LineStringFromWKB(
				LineString(
					AsBinary(ta_restaurant.latlon),
					AsBinary(

						SELECT
							ta_postcode.latlon
						FROM
							ta_postcode
						WHERE
								REPLACE('SE9', ' ', '')
							LIKE
								CONCAT(ta_postcode.postcode, '%')
						ORDER BY
							LENGTH(postcode) DESC
						LIMIT
							0, 1

					)
				)
			)
		)
	) AS distance
FROM
	ta_restaurant






SELECT
		id,
			GLength(
				LineStringFromWKB(
					LineString(
						AsBinary(ta_restaurant.latlon),
						AsBinary(
							(
								SELECT
									ta_postcode.latlon
								FROM
									ta_postcode
								WHERE
										REPLACE('NW1', ' ', '')
									LIKE
										CONCAT(ta_postcode.postcode, '%')
								ORDER BY
									LENGTH(postcode) DESC
								LIMIT
									0, 1
							)
						)
					)
				)
			)
		AS
			distance
	FROM
		ta_restaurant








SELECT
		id,
			GLength(
				LineStringFromWKB(
					LineString(
						AsBinary(
							(
								SELECT
									ta_postcode.latlon
								FROM
									ta_postcode
								WHERE
										REPLACE('SE2', ' ', '')
									LIKE
										CONCAT(ta_postcode.postcode, '%')
								ORDER BY
									LENGTH(postcode) DESC
								LIMIT
									0, 1
							)
						
						),
						AsBinary(
							(
								SELECT
									ta_postcode.latlon
								FROM
									ta_postcode
								WHERE
										REPLACE('EC1', ' ', '')
									LIKE
										CONCAT(ta_postcode.postcode, '%')
								ORDER BY
									LENGTH(postcode) DESC
								LIMIT
									0, 1
							)
						)
					)
				)
			)
		AS
			distance
	FROM
		ta_restaurant


E2		0.041436698710201
E8		0.042485291572495
W2		0.076419892698171 
WC1		0.020099751242242
SW19	0.054280381927978
SE1		0.028178005607216
N1		0.014866068747315
SE2		0.22162355470482





SELECT @mylat:=ta_postcode.lat, @mylon:=ta_postcode.lon FROM ta_postcode WHERE REPLACE('DA15 8PR', ' ', '') LIKE CONCAT(ta_postcode.postcode, '%') ORDER BY LENGTH(postcode) DESC LIMIT 0, 1;



set @dist = 10;

set @lon1 = @mylon-@dist/abs(cos(radians(@mylat))*69);

set @lon2 = @mylon+@dist/abs(cos(radians(@mylat))*69);

set @lat1 = @mylat-(@dist/69);

set @lat2 = @mylat+(@dist/69);


SELECT *,

3956 * 2 * ASIN(SQRT( POWER(SIN((@mylat - ta_restaurant.lat) * pi()/180 / 2), 2) +

COS(@mylat * pi()/180) * COS(ta_restaurant.lat * pi()/180) * POWER(SIN((@mylon - ta_restaurant.lon) * pi()/180 / 2), 2) )) as

distance

FROM ta_restaurant

WHERE 

ta_restaurant.lon

between @lon1 and @lon2

and ta_restaurant.lat

between @lat1 and @lat2  

;





SET @dist = 10;

SET @lon1 = @orig_lon-@dist/abs(cos(radians(@orig_lat))*69);
SET @lon2 = @orig_lon+@dist/abs(cos(radians(@orig_lat))*69);
SET @lat1 = @orig_lat-(@dist/69);
SET @lat2 = @orig_lon+(@dist/69);




SELECT @orig_lat:=ta_postcode.lat, @orig_lon:=ta_postcode.lon FROM ta_postcode WHERE REPLACE('SE3', ' ', '') LIKE CONCAT(ta_postcode.postcode, '%') ORDER BY LENGTH(postcode) DESC LIMIT 0, 1;

SELECT
	id,
	3956 * 2 * ASIN(SQRT( POWER(SIN((@orig_lat - ta_restaurant.lat) * pi()/180 / 2), 2) + COS(@orig_lat * pi()/180) * COS(ta_restaurant.lat * pi()/180) * POWER(SIN((@orig_lon - ta_restaurant.lon) * pi()/180 / 2), 2) )) AS distance
FROM
	ta_restaurant
ORDER BY
	distance




//http://assets.en.oreilly.com/1/event/2/Geo%20Distance%20Search%20with%20MySQL%20Presentation.ppt

