
header('Content-type: text/plain');

$csvdata = file_get_contents('uk-postcodes.csv');
$csvdata_lines = csv::retrieve($csvdata);
unset($csvdata_lines[0]);

echo "Running " . date('r') . "\r\n";

foreach ($csvdata_lines as $line) {

	print_r($line);

	$record = array(
		'postcode'=> $line[0],
		'lat'=> $line[3],
		'lon'=> $line[4],
	);

	$aa_result = $db->table_query($db->tbl($tbl['postcode']), $db->col(array('id')), $db->cond(array("postcode='".$db->es($line[0])."'"), 'AND'));
	if (!($aa_record = $db->record_fetch($aa_result))) {

		$record = appgeneral::filternonascii_array($record);

		foreach ($record as $name => $value) {
			$record[$name] = ($value) ? $value : null;
		}
//lib::prh($record);
		$db->record_insert($tbl['postcode'], $db->rec($record));


	} else {
echo "{$aa_record['id']} not added\n";

	}

}

