<?php

require ( 'lib.php' );			


define ( "DBPREFIX", "" );


 define( "HOSTNAME",    getenv('OPENSHIFT_MYSQL_DB_HOST') );
 
 define( "DBUSER",      getenv('OPENSHIFT_MYSQL_DB_USERNAME') );
 
 define( "DBPASS",  getenv('OPENSHIFT_MYSQL_DB_PASSWORD') );
 
 define( "DATABASE",  getenv('OPENSHIFT_APP_NAME') );



$db = new db ( DBUSER, DBPASS, DATABASE, HOSTNAME );




 
	function checkLogin ( $levels )
	{
		session_start ();
		global $db;
		$kt = split ( ' ', $levels );
		
		if ( ! $_SESSION['logged_in'] ) {
		
			$access = FALSE;
			
			if ( isset ( $_COOKIE['cookie_id'] ) ) {//if we have a cookie
			
				$query =  'SELECT * FROM ' . DBPREFIX . 'usersa WHERE id = ' . $db->qstr ( $_COOKIE['cookie_id'] );

				if ( $db->RecordCount ( $query ) == 1 ) {//only one user can match that query
					$row = $db->getRow ( $query );
					
					//let's see if we pass the validation, no monkey business
					if ( $_COOKIE['authenticate'] ==  ( getIP () . $row->id. $_SERVER['USER_AGENT'] ) ) {
						//we set the sessions so we don't repeat this step over and over again
						$_SESSION['user_id'] = $row->id;				
						$_SESSION['logged_in'] = TRUE;
						
						//now we check the level access, we might not have the permission
						if ( in_array ( get_level_access ( $_SESSION['user_id'] ), $kt ) ) {
							//we do?! horray!
							$access = TRUE;
						}
					}
				}
			}
		}
		else {			
			$access = FALSE;
			
			if ( in_array ( get_level_access ( $_SESSION['user_id'] ), $kt ) ) {
				$access = TRUE;
			}
		}
		
		if ( $access == FALSE ) {
  header('Location: ../index.aspx');
		}		
	}





	/**
	 * set_login_sessions - sets the login sessions
	 *
	 * @access	public
	 * @param	string
	 * @return	none
	 */
	
	function set_login_sessions ( $user_id )
	{
		//start the session
		session_start();
		
		//set the sessions
		$_SESSION['user_id'] = $user_id;
		$_SESSION['logged_in'] = TRUE;
		
		//do we have "remember me"?
		if ( $remember ) {
			setcookie ( "cookie_id", $user_id, time() + KEEP_LOGGED_IN_FOR, COOKIE_PATH );
			setcookie ( "authenticate",  ( getIP () . $contrasena. $_SERVER['USER_AGENT'] ), time() + KEEP_LOGGED_IN_FOR, COOKIE_PATH );
		}
	}

	function logout ()
	{
		//session must be started before anything
		session_start ();
	
		//if we have a valid session
		if ( $_SESSION['logged_in'] == TRUE )
		{	
			//unset the sessions (all of them - array given)
			unset ( $_SESSION ); 
			//destroy what's left
			session_destroy (); 
		}
		
		//It is safest to set the cookies with a date that has already expired.
		if ( isset ( $_COOKIE['cookie_id'] ) && isset ( $_COOKIE['authenticate'] ) ) {
			/**
			 * uncomment the following line if you wish to remove all cookies 
			 * (don't forget to comment ore delete the following 2 lines if you decide to use clear_cookies)
			 */
			clear_cookies ();
			setcookie ( "cookie_id", '', time() - KEEP_LOGGED_IN_FOR, COOKIE_PATH );
			setcookie ( "authenticate", '', time() - KEEP_LOGGED_IN_FOR, COOKIE_PATH );
		}
		
		//redirect the user to the default "logout" page
  header('Location: ../index.aspx');
	}






	function get_level_access ( $user_id )
	{
		global $db;
		$row = $db->getRow ( 'SELECT Level_access FROM ' . DBPREFIX . 'usersa  WHERE id = ' . $db->qstr ( $user_id ) );
		return $row->Level_access;
	}

function get_activado ( $user_id )
	{
		global $db;
		$row = $db->getRow ( 'SELECT Active FROM ' . DBPREFIX . 'usersa  WHERE id = ' . $db->qstr ( $user_id ) );
		return $row->Active;
	}


	function isadmin ( $id )
	{
		global $db;
		
		$query = "SELECT `Level_access` FROM `" . DBPREFIX . "usersa` WHERE `id` = " . $db->qstr ( $id );
		
		if ( $db->RecordCount ( $query ) == 1 )
		{
			$row = $db->getRow ( $query );
			
			if ( $row->Level_access == 1 )
			{
				return TRUE;
			}
			else {
				return FALSE;
			}
		}
		else {
			return FALSE;
		}
	}







//-------------------------




function checkEmail($str)
{
	return preg_match("/^[\.A-z0-9_\-\+]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z]{1,4}$/", $str);
}


function send_mail($from,$to,$subject,$body)
{
	$headers = '';
	$headers .= "From: $from\n";
	$headers .= "Reply-to: $from\n";
	$headers .= "Return-Path: $from\n";
	$headers .= "Message-ID: <" . md5(uniqid(time())) . "@" . $_SERVER['SERVER_NAME'] . ">\n";
	$headers .= "MIME-Version: 1.0\n";
	$headers .= "Date: " . date('r', time()) . "\n";

	mail($to,$subject,$body,$headers);
}
function valid_cel($str){
		return ( ! ereg ( '^(312|313|314)+[0-9]{7}', $str ) ) ? FALSE : TRUE;


}
	


	

//checar unico en registro 
function checkUnique ( $field, $compared )
	{
		global $db;

		$query = $db->getRow ( "SELECT COUNT(*) as total FROM `" . DBPREFIX . "usersa` WHERE " . $field . " = " . $db->qstr ( $compared ) );

		if ( $query->total == 0 ) {
			return TRUE;
		}
		else {
			return FALSE;
		}
	}

//checar unico en profeco 

function checkUniqueprofeco ( $field, $compared )
	{
		global $db;

		$query = $db->getRow ( "SELECT COUNT(*) as total FROM `" . DBPREFIX . "profeco` WHERE " . $field . " = " . $db->qstr ( $compared ) );

		if ( $query->total == 0 ) {
			return TRUE;
		}
		else {
			return FALSE;
		}
	}

//checar unicos envios
function checkUniqueip ( $field, $compared )
	{
		global $db;

		$query = $db->getRow ( "SELECT COUNT(*) as total FROM `" . DBPREFIX . "deprueba` WHERE " . $field . " = " . $db->qstr ( $compared ) );

		if ( $query->total <= 1 ) {
			return TRUE;
		}
		else {
			return FALSE;
		}
	}





	function get_username ( $id )
	{
		global $db;
		
		$query = "SELECT `username` FROM `" . DBPREFIX . "usersa` WHERE `id` = " . $db->qstr ( $id );
		
		if ( $db->RecordCount ( $query ) == 1 )
		{
			$row = $db->getRow ( $query );
			
			return $row->usr;
		}
		else {
			return FALSE;
		}
	}

function get_nick ( $id )
	{
		global $db;
		
		$query = "SELECT `nick` FROM `" . DBPREFIX . "usersa` WHERE `id` = " . $db->qstr ( $id );
		
		if ( $db->RecordCount ( $query ) == 1 )
		{

			$row = $db->getRow ( $query );
			
			return $row->nick;
		}
		else {
			return FALSE;
		}
	}


	function get_celular ( $id )
	{
		global $db;
		
		$query = "SELECT `celular` FROM `" . DBPREFIX . "usersa` WHERE `id` = " . $db->qstr ( $id );
		
		if ( $db->RecordCount ( $query ) == 1 )
		{

			$row = $db->getRow ( $query );
			
			return $row->celular;
		}
		else {
			return FALSE;
		}
	}



	function get_puntos ( $id )
	{
		global $db;
		
		$query = "SELECT `Puntos` FROM `" . DBPREFIX . "usersa` WHERE `id` = " . $db->qstr ( $id );
		
		if ( $db->RecordCount ( $query ) == 1 )
		{
			$row = $db->getRow ( $query );
			
			return $row->Puntos;
		}
		else {
			return FALSE;
		}
	}
	





	
	function get_header ()
	{
		global $db;
		
		$query = "SELECT `header` FROM `" . DBPREFIX . "clientes` WHERE `cantidad` >= 1 ORDER BY  cantidad DESC Limit 1";
		//$query = "SELECT `header` FROM `" . DBPREFIX . "clientes` WHERE `salida_diaria` >= 1  and  `cantidad` >= 1 ORDER BY  salida_diaria DESC, cantidad DESC Limit 1";			

		if ( $db->RecordCount ( $query ) == 1 )
		{
			$row = $db->getRow ( $query );
			
			return $row->header;
		}
		else {
			return FALSE;
		}
	}
	
	function get_imagen ()
	{
		global $db;
		
		$query = "SELECT `foto` FROM `" . DBPREFIX . "clientes` WHERE `cantidad` >= 1 ORDER BY  cantidad DESC Limit 1";
		//$query = "SELECT `header` FROM `" . DBPREFIX . "clientes` WHERE `salida_diaria` >= 1  and  `cantidad` >= 1 ORDER BY  salida_diaria DESC, cantidad DESC Limit 1";			

		if ( $db->RecordCount ( $query ) == 1 )
		{
			$row = $db->getRow ( $query );
			
			return $row->foto;
		}
		else {
			return FALSE;
		}
	}

	
	
	
	
	
	
	
	
	

function get_cliente ()
	{
		global $db;
		
		$query = "SELECT `id` FROM `" . DBPREFIX . "clientes` WHERE `cantidad` >= 1 ORDER BY  cantidad DESC Limit 1";
		//$query = "SELECT `header` FROM `" . DBPREFIX . "clientes` WHERE `salida_diaria` >= 1  and  `cantidad` >= 1 ORDER BY  salida_diaria DESC, cantidad DESC Limit 1";			

		if ( $db->RecordCount ( $query ) == 1 )
		{
			$row = $db->getRow ( $query );
			
			return $row->id;
		}
		else {
			return FALSE;
		}
	}
	
function get_calle ( $id )
	{
		global $db;
		
		$query = "SELECT `calle` FROM `" . DBPREFIX . "direccion` WHERE `id_usuario` = " . $db->qstr ( $id );
		
		if ( $db->RecordCount ( $query ) == 1 )
		{
			$row = $db->getRow ( $query );
			
			return $row->calle;
		}
		else {
			return FALSE;
		}
	}	
	
function random_string ( $type = 'alnum', $len = 8 )
	{					
		switch ( $type )
		{
			case 'alnum'	:
			case 'numeric'	:
			case 'nozero'	:
			
					switch ($type)
					{
						case 'alnum'	:	$pool = '0123456789abcdefghijklmnopqrstuvwxyz';
							break;
						case 'numeric'	:	$pool = '0123456789';
							break;
						case 'nozero'	:	$pool = '123456789';
							break;
					}
	
					$str = '';
					for ( $i=0; $i < $len; $i++ )
					{
						$str .= substr ( $pool, mt_rand ( 0, strlen ( $pool ) -1 ), 1 );
					}
					return $str;
			break;
			case 'unique' : return md5 ( uniqid ( mt_rand () ) );
			break;
		}
	}
	
	
	


?>