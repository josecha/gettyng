<?php

require 'facebook/facebook.php';
require 'config/fbconfig.php';
require 'config/functions.php';
require 'validaciones/functions.php';
require 'validaciones/connect.php';

$facebook = new Facebook(array(
            'appId' => APP_ID,
            'secret' => APP_SECRET,
            'cookie' => true
        ));

$session = $facebook->getSession();

if (!empty($session)) {
    # Active session, let's try getting the user id (getUser()) and user info (api->('/me'))
    try {
        $uid = $facebook->getUser();
        $user = $facebook->api('/me');
    } catch (Exception $e) {
    error_log($e);
   $facebook->setSession(null);
    }

    if (!empty($user)) {
        # User info ok? Let's print it (Here we will be adding the login and registering routines)
     //  echo '<pre>';
      //print_r($user);
        //echo '</pre><br/>';
        $username = $user['name'];
        $email= $user['email'];
        $gender= $user['gender'];
       $birthday= $user['birthday'];


        $user = new User();
        $userdata = $user->checkUser($uid, 'facebook', $username,$email,$gender,$birthday);
        if(!empty($userdata)){
            session_start();
            $_SESSION['id'] = $userdata['id'];
           $_SESSION['oauth_id'] = $uid;
           $_SESSION['username'] = $userdata['username'];
            $_SESSION['oauth_provider'] = $userdata['oauth_provider'];
           
           
           $query = 'SELECT id  FROM usersa WHERE id= ' . $userdata['id'];



if ( $db->RecordCount ( $query ) == 1 )
			{
				$row = $db->getRow ( $query );
set_login_sessions ( $row->id);
          
          
          
            }

            header("Location: home?default.aspxwa=wsignin1.0");
        }
    } else {
        # For testing purposes, if there was an error, let's kill the script
        die("Borra las cookies.");
    }
} else {
    # There's no active session, let's generate one
    //$login_url = $facebook->getLoginUrl();
       // $login_url = $facebook->getLoginUrl(array('req_perms' => 'email,read_stream,user_photos,user_videos,user_birthday,offline_access,publish_stream,status_update'));
    
            $login_url = $facebook->getLoginUrl(array('req_perms' => 'email,user_birthday,status_update'));

    header("Location: " . $login_url);
}
?>