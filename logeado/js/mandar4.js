	  
	  /*
	  Esta funcion lo que hace es enviar mediante Ajax, por el metodo post, los datos (user y pass) a un archivo "checar.php" para ser comparados en la base de datos. De ser correctos se dar? mensaje de bienvenida, sino, entonces marcara error de usuario o password incorrectos
	  */
	  function POST_AJAX(url, variables) {
        objeto = false;
		//creamos el onjeto XMLHttpRequest para poder enviar datos mediante ajax
        if (window.XMLHttpRequest) { // Mozilla, Safari,...
           objeto = new XMLHttpRequest();
           if (objeto.overrideMimeType) {
           	objeto.overrideMimeType('text/xml');
           }
        } else if (window.ActiveXObject) { // IE
           try {
              objeto = new ActiveXObject("Msxml2.XMLHTTP");
           } catch (e) {
              try {
                 objeto = new ActiveXObject("Microsoft.XMLHTTP");
              } catch (e) {}
           }
        }
        if (!objeto) {
           alert("No se puede crear la instancia XMLHTTP");
           return false;
        } 
		     
        objeto.onreadystatechange = avisos;    /*Cuando el archivo que se mando llamar mediante ajax (checar.php) regrese un resultado, entonces lo primero que se hace es mandar llamar la funcion avios(), que es donde se imprimir? mensaje de bienvenida*/
        objeto.open("POST", url, true);  /* enviaremos los datos por el metodo POST hacia checar.php */
        objeto.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); /*asignamos header. Esto no tiene relacion con el sistema de logeo. Solo es necesario para poder enviar los datos mediante ajax*/
        objeto.setRequestHeader("Content-length", variables.length);
        objeto.setRequestHeader("Connection", "close");
        objeto.send(variables); /* enviamos las variables con un formato como este: "user=minombre&pass=123456&n=0" */
	  }
	  
	  
	  /* Esta funcion lo unico que hace, es acomodar los datos introducidos en el formulario a una cadena "variables" con este formato: "user=minombre&pass=123456&n=0" o este "n=0" 
	  NOTA: 
	  n=1 significa que el formulario esta siendo enviado por el usuario (es decir que en este punto, el ususario previamente tuvo que haber introducido su user y pass y despues presionado el boton "enviar").
	  n=0 significa que el formulario se esta enviando automaticamente cuando se carga la pagina. Esto lo hace para verificar si ya se habia logeado un usuario, entonces en cuanto se cargue la pagina, ya no le pedira su user y pass... ahora solo imprimir? mensaje de bienvenida. Es decir que si se logea, cierra el navegador sin haber cerrado sesion y en 3 dias por ejemplo vuelve a entrar a la pagina, lo que se le imprimir? ser? un mensaje de bienvenida, ya que no cerro sesion la ultima vez que salio. Esto solo dura una semana, es decir que las COOKIES de user y pass expiraran una semana despues de haber sido creadas. 
	  */
	  function enviar(id_form,n) {  
	    if (n=='1') //si el formulario fue enviado al darle en el boton
		 {
		 recargar();
		  document.getElementById('inp_enviar').innerHTML = '<p><input id="send" type="submit" class="bt_register" name="enviar" value="Enviar" onclick="replaceContent()"/></p><img src="/images/loading.gif"/>';

	      if (vacio(document.getElementById(id_form).celular.value)==false || vacio(document.getElementById(id_form).mensaje.value)==false )/*si alguno de los campos de user y pass estan vacios, entonces se imprime mensaje de error. NOTA: vacio() es una funcion que verifica que alla algo diferente a "" o puros espacios en blanco. Esta funcion esta mas abajo*/
		   {
		    document.getElementById('r').innerHTML = '<label class="res">Llena los dos  campos</label>'; //"r" es un div que tenemos debajo del formulario para imprimir los mensajes de error.
			document.getElementById('inp_enviar').innerHTML = '<p><input id="send" type="submit" class="bt_register" name="enviar" value="Enviar" onclick="replaceContent()"/></p>';
		   }
	      else //s� SI habia llenado correctamente el user y pass, entonces se crear una cadena "variables" con los datos de user y pass con el siguiente formato: "user=minombre&pass=123456&n=0"
		   {
		    var Formulario = document.getElementById(id_form);
		    var longitudFormulario = Formulario.elements.length;
		    var variables = "";
		    var sepCampos = "";
		    for (var i=0; i<=Formulario.elements.length-1; i++)
		      {
		       variables += sepCampos+Formulario.elements[i].name + '=' + encodeURI(Formulario.elements[i].value);
               sepCampos="&";
              }
		    //indice para saber si envio formulario
		    variables += '&n=' + n; 
		    POST_AJAX('checar2.php', variables); //se envia el nombre del archivo donde se verificaran los datos en la BD y la cadena que se acaba de crear con los datos de user y pass, hacia la funcion POST_AJAX, que lo que hace es enviar los datos por medio de AJAX utilizando el metodo POST hacia "checar.php"
		   	           	mostrarPublicidad();

		   }
		 }
		else //si el formulario fue enviado en el body onload solo para procesos
		 {
		    variables = 'n=' + n; 
		    POST_AJAX('checar2.php', variables);
		 }
      }
    
      function avisos() {
	     if ((objeto.readyState==4) && (objeto.status==200))                     
	       {
		    document.getElementById('form').innerHTML = objeto.responseText;  //se inserta en el DIV "form" el mensaje de bienvenida que nos imprimio "checar.php"                    
	       setTimeout("terminarProcesoEnvioSMS(\"ppenvio\",'Sigue usando ppenvio para llegar a tus contactos')",SMS_DURACION_MOSTRAR_PUBLICIDAD);

	       }//end if 
	  }
	  
	 enviar('login','0'); //se envia a la funcion "enviar()" el id del formulario y el valor 0 (cero); Este valor (0) nos indicar? que el formulario esta siendo enviado cuando la pagina se esta cargando apenas. NO lo esta enviando el usuario
	 
	  /*Esta funcion recorre una cadena en busca de algo diferente de espacios en blanco. Si la cadena contenia puros espacios en blanco entonces regresara False*/
	  function vacio(q) {
         for ( i = 0; i < q.length; i++ ) {
           if ( q.charAt(i) != " " ) { return true }
         }
         return false
	  }

function textCounter(field, countfield, maxlimit) {
if (field.value.length > maxlimit) // 
field.value = field.value.substring(0, maxlimit);
// otherwise, update 'characters left' counter
else 
countfield.value = maxlimit - field.value.length;
}
function mostrarPublicidad(){
	deshabilitarPantalla("Enviando mensaje..", "");
	
	document.getElementById('publicidadB').style.visibility = "visible";
	document.getElementById('publicidadB').style.display = "block";
}

function deshabilitarPantalla(header, info){
	establecerEstilos();
	ingresarMensaje(header, info);
	mostrarBloqueador();
}

function habilitarPantalla(header, mensaje, duracion){
	establecerEstilos();
	if( mensaje){
		ingresarMensaje(header, mensaje);
		setTimeout("quitarBloqueador()",duracion);
	}else{
		setTimeout("quitarBloqueador()",duracion);
	}
}

function habilitarPantallaImagen(imagen, duracion){
	if( imagen ){
		limpiarEstilos();
		ingresarMensaje("", imagen);
		setTimeout("quitarBloqueador()",duracion);
	}else{
		setTimeout("quitarBloqueador()",duracion);
	}
}
	
function quitarBloqueador(){
	document.getElementById('messagesBlock').style.visibility = "hidden";
	document.getElementById('messagesBlock').style.display = "none";
	
	document.getElementById('messagesLoading').style.visibility = "hidden";
	document.getElementById('messagesLoading').style.display = "none";
	
	var iframe_oculto = document.getElementById('iframe_oculto')
	iframe_oculto.style.filter="alpha(opacity=0)";
	iframe_oculto.style.opacity="0.00";
	iframe_oculto.style.width= 0;
	iframe_oculto.style.height= 0;
	iframe_oculto.style.zIndex = 95;
	iframe_oculto.style.visibility = "hidden";
	iframe_oculto.style.display = "none";
	
	limpiarMensajes();
}

function mostrarBloqueador(){
	var messagesBlock = document.getElementById('messagesBlock');
	var messagesLoading = document.getElementById('messagesLoading');
	var iframe_oculto = document.getElementById('iframe_oculto');
	
	messagesBlock.style.visibility = "visible";
	messagesBlock.style.display = "block";
	
	messagesLoading.style.visibility = "visible";
	messagesLoading.style.display = "block";
	
	iframe_oculto.style.filter="alpha(opacity=0)";
	iframe_oculto.style.opacity="0.00";
	iframe_oculto.style.width= '100%';
	iframe_oculto.style.height= '100%';
	iframe_oculto.style.zIndex = 95;
	iframe_oculto.style.visibility = "visible";
	iframe_oculto.style.display = "block";
	
	//limpiarMensajes();
}

function limpiarMensajes(){
	document.getElementById('loader_header').innerHTML = '';
	document.getElementById('loader_info').innerHTML = '';
}

function ingresarMensaje(header, info){
	document.getElementById('loader_header').innerHTML = header;
	document.getElementById('loader_info').innerHTML = info;
}

function limpiarEstilos(){
	var messagesContainer 	= document.getElementById('messagesContainer');
	var loader_header 		= document.getElementById('loader_header');
	var loader_contenedor 	= document.getElementById('loader_contenedor');
	
	messagesContainer.className = '';
	loader_contenedor.className = '';
	loader_header.style.display = "none";
}

function establecerEstilos(){
	var messagesContainer 	= document.getElementById('messagesContainer');
	var loader_header 		= document.getElementById('loader_header');
	var loader_contenedor 	= document.getElementById('loader_contenedor');

	messagesContainer.className = 'formularioMessagesBlock';
	loader_contenedor.className = 'loader_contenedor';
	loader_header.style.display = "block";
}
function ocultarPublicidad(){
	document.getElementById('publicidadB').style.visibility = "hidden";
	document.getElementById('publicidadB').style.display = "none";
}

function terminarProcesoEnvioSMS(header, mensaje){
	ocultarPublicidad();
	habilitarPantalla(header, "<span class='txt'>" + mensaje + "</span>", SMS_DURACION_MOSTRAR_MENSAJES);
}
function terminarProcesoEnvioSMSImagen(imagen){
	ocultarPublicidad();
	habilitarPantallaImagen(imagen, SMS_DURACION_MOSTRAR_MENSAJES);
}


function recargar() {
       document.getElementById("imagen").src = "imagen.php?rnd="+Math.random();

}
function recargaImg() {
	}