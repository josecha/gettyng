		//jQuery.noConflict();
	
	jQuery(document).ready(function(){


	/*--- Cufon Config  ----*/
	/*--------Pedidos---------*/
	Cufon.replace('h1', { 
			hover: true
		});

		Cufon.replace('h2', { 
			hover: true
		});			

		Cufon.replace('h3', { 
			hover: true
		});	
	Cufon.replace('h4', { 
			hover: true
		});			
				
	Cufon.replace('.maincontent h1', { 
			hover: true
		});

		Cufon.replace('.page-takeaway_menu h1', { 
			hover: true
		});			

		Cufon.replace('.sidepanel .menu_basket h2', { 
			hover: true
		});	
	Cufon.replace('.page-home .about_us .info h2', { 
			hover: true
		});			
			
	Cufon.replace('.sidepanel .postcode_search h2', { 
			hover: true
		});

		Cufon.replace('page-takeaway_menu .menu h2', { 
			hover: true
		});			

		Cufon.replace('.sidepanel .order_process h2', { 
			hover: true
		});	
	Cufon.replace('.sidepanel .order_process h2 .larger', { 
			hover: true
		});	
		Cufon.replace('.page-takeaway_listing .restaurants_list .restaurant .info h2', { 
			hover: true
		});		
	Cufon.replace('.sidepanel .popular_takeaways .info h2', { 
			hover: true
		});	
	Cufon.replace('.sidepanel .restaurant_open_times h2', { 
			hover: true
		});	
	Cufon.replace('.page-takeaway_menu .menu h3 ', { 
			hover: true
		});	

	/*********** termina pedidos***************/	
		Cufon.replace('h4.title', { 
			hover: true
		});			

		Cufon.replace('h1.title', { 
			hover: true
		});

		Cufon.replace('h2.title', { 
			hover: true
		});			

		Cufon.replace('h3.title', { 
			hover: true
		});			
		
		Cufon.replace('h4.title', { 
			hover: true
		});			
		
		Cufon.replace('h5.title', { 
			hover: true
		});

		Cufon.replace('h6.title', { 
			hover: true
		});
		
		Cufon.replace('.cufonblue', { 
			hover: true
		});

		Cufon.replace('.cufow', { 
			hover: true
		});	
		Cufon.replace('.cufonw', { 
			hover: true
		});

		Cufon.replace('.cufon', { 
			hover: true
		});



			/*--- Toggle Component ---*/

		jQuery('.jtoggle-list li').click(function(e) {
		jQuery(".jtoggle-list li div").hide(); 
		
		if(jQuery(this).find('div').attr('class') ==  'section'){
			jQuery(".jtoggle-list li div").hide();
			jQuery(this).find('div').attr('class', '');
		}
		else{
			jQuery(".jtoggle-list li div").attr('class', '');
			jQuery(this).find('div').attr('class', 'section');
			jQuery(this).find('div').animate({
				opacity: 'toggle'
			});
		}
		
		});

		/*---Tab Component ---*/
		

		jQuery(".tab_content").hide(); 
		jQuery("ul.tabs li:first").addClass("active").show(); 
		jQuery(".tab_content:first").show(); 
		

		jQuery("ul.tabs li").click(function() {
			jQuery("ul.tabs li").removeClass("active");
			jQuery(this).addClass("active"); 
			jQuery(".tab_content").hide(); 
			var activeTab = jQuery(this).find("a").attr("href"); 
			jQuery(activeTab).fadeIn(); 
			return false;
		});


	

	}); 